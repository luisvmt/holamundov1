/*
*
* Ejemplo de creacino de un objeto tsdk_regilla_sql
*
*/

public function ajax_get_html_producto_x_paquete()
{
        //validando el permiso sobre el objeto
        $idview="paquete";
        $idpaquete="";

        //$permisomodificar = global_has_permiso($this->m_basedatos,$idview,'modificar');
        //$permisobrowse = global_has_permiso($this->m_basedatos,$idview,'browse');

        //hallando variables
        //$nropagina= $this->input->post('nropagina');
        //$filtro = $this->input->post('filtro');
        //$orderby = $this->input->post('orderby');
        //$sentido = $this->input->post('sentido');
        //$parametros = $this->input->post('parametros');

        $accion = $this->input->post("accion_ejecutar");
        $idpaquete = $this->input->post('idpaquete');

        //------------------------------------------------------------
        // creando regilla (los productos del paquete)				
        //------------------------------------------------------------
        $control = new tsdk_regilla_sql($this->m_basedatos);	
        $control->set_id_ventana_control($idview,"regilla_productos");
        $control->handler_eventos = $control->id."_events"; //manejador de eventos
        $control->config_as_tablasql();

        $control->add_columna_regilla("Producto","left");
        $control->add_columna_regilla("Fabricante","left");
        $control->add_columna_regilla("Cantidad","right");
        $control->permit_columna_order = false; //no permite hacer clic en la columna

        $control->consulta->select = array("b.descripcioncorta","c.nombrefabricante","a.nroproductos",
                                                                                "concat('idpaquete=',a.idpaquete,',idproducto=',a.idproducto) as clave_primaria");
        $control->consulta->from   = array("producto_x_paquete a","producto b","fabricante c");
        $control->consulta->where  = array("a.idproducto=b.idproducto","b.idfabricante=c.idfabricante",
                                                                                "a.eliminar=false",
                                                                                "a.idpaquete=".$idpaquete);
        //$control->consulta->campos_filtro = array(); //campos para filtro

        $control->numero_pagina=1;
        $control->tamano_pagina=0; //significa todos los registros
        $control->set_orden(0,"asc"); //ordenando por la primera columna
        //$control->consulta->filtro_a_buscar="";

        //devolviendo html con los datos
        echo "<!-- OK -->\n".$control->get_html();
}


