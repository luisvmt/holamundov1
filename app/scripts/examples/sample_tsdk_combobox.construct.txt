/*
*
* Ejemplo de como crear un combobox fijo
*
*/

//-----
$control = new tsdk_combobox();
$control->config_as_combofijo($tabindex++);
$control->add_item( new tsdk_lista_item("A","Alto") );
$control->add_item( new tsdk_lista_item("M","Medio") );
$control->add_item( new tsdk_lista_item("B","Bajo") );

$this->add_item("prioridad",$control); //añadiendo a una lista de controles


/*
*
* Ejemplo de como crear un combobox dinámico creado con SQL
*
*/


//----- la salida debe ser id,texto
$texto_sql  = "select iddepartamento as id, nombre as texto ";
$texto_sql .= "from departamento where departamento.eliminar=false ";

$control = new tsdk_combobox();
$control->config_as_combosql($tabindex++,$texto_sql);
$this->add_item("iddepartamento",$control);
