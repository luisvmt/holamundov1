
/* 
*
* Ejemplo de uso de obtener_tabla_from_sql. Este ejemplo es llamado desde una clase derivada de trs_resultset
*
*/

//creando la consulta a ejecutar
$consulta = "select coalesce(max(idoportunidad),0)+1 as idoportunidad ".
                        "from oportunidades ".
                        "where anio=#anio# ".
                        "group by anio; ";

$tabla = $this->obtener_tabla_from_sql($consulta);
if ($tabla->filas->count() == 0)
{
        //es la pimera oportunidad del año
        $this->campos->add("idoportunidad")->value = "1";
}else{
        $this->campos->add("idoportunidad")->value = $tabla->get_valor_campo(0,"idoportunidad"); //obteniendo el primer registro de la consulta
}


