<?php

//----------------------------------------------------------
//Esta libreria se encarga de validar si se realizo login
//----------------------------------------------------------
class tsdk_login_library
{
    //protected   $m_base_url="";
    protected   $m_controlador_raiz="";
    protected   $m_mensaje="";

    // Constructor
    public function __construct()
    {
        if (!isset($this->CI))
        {
            $this->CI =& get_instance();
        }
    }
        
    public function check_is_logged()
    {
        //if ($this->CI->session->userdata('tsdk_is_logged') != TRUE)
        if ( !isset($_SESSION['tsdk_is_logged']) )
        {
            redirect(base_url().$this->m_controlador_raiz, 'refresh'); //direccionando al controlador inicial
            exit();
        }

        if ($_SESSION['tsdk_is_logged'] != TRUE)
        {
            redirect(base_url().$this->m_controlador_raiz, 'refresh'); //direccionando al controlador inicial
            exit();
        }
    }
}


//---------------------------------
//OJO: No se cierra el texto php
//---------------------------------
