<?php
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu_item.php");

	include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");

	//definiendo constantes
	include_once("../objetos/configuracion_app/constantes.php");

	//-------------------------------------------------------------------------------------------------------
    //definiendo variables
    define("VISTA_TITULO","HOLA MUNDO V1"); 	//TITULO DE LA VISTA
    define("VISTA_TITULO_TAB","Login");  					//TITULO DEL TAB DEL NAVEGADOR
    define("VISTA_IDVENTANA","login");  					//ID DE VENTANA
	//-------------------------------------------------------------------------------------------------------

	//definiendo controles
	$tabindex=0;
	$txtusuario = new tsdk_textbox();
	$txtusuario->config_as_text("",++$tabindex);
	//$txtusuario->propiedades->add("autocomplete",new tsdk_coleccion_item("off"));
	$txtusuario->propiedades->add("autocomplete")->value = "off";
	$txtusuario->set_id_ventana_control(VISTA_IDVENTANA,"txtusuario");
	$txtusuario->eventos->add_evento_standard("onkeypress","event,this");

	$txtpassword = new tsdk_textbox();
	$txtpassword->config_as_password("",++$tabindex);
	$txtusuario->propiedades->add("autocomplete")->value = "off";
	$txtpassword->set_id_ventana_control(VISTA_IDVENTANA,"txtpassword");
	$txtpassword->eventos->add_evento_standard("onkeypress","event,this");

	$cmdaceptar = new tsdk_button();
	$cmdaceptar->config_as_button_standard("Aceptar",++$tabindex);
	$cmdaceptar->estilos->add("width",new tsdk_coleccion_item("180px"));
	$cmdaceptar->set_id_ventana_control(VISTA_IDVENTANA,"cmdaceptar");
	$cmdaceptar->eventos->add_evento_standard("onclick");

	$cmdnewpassword = new tsdk_button();
	$cmdnewpassword->config_as_button_standard("Recuperar Contraseña",++$tabindex);
	$cmdnewpassword->estilos->add("width",new tsdk_coleccion_item("180px"));
	$cmdnewpassword->set_id_ventana_control(VISTA_IDVENTANA,"cmdnewpassword");
	$cmdnewpassword->eventos->add_evento_standard("onclick");

	$mensaje = new tsdk_span();
	$mensaje->config_as_mensaje();
	$mensaje->set_id_ventana_control(VISTA_IDVENTANA,"mensaje");

	$tabla = new tsdk_tabla(VISTA_IDVENTANA,"tabla",
							4,3,					//filas,columnas
							array("width"=>"430",
									"border"=>"0")
							);

	//definiendo variables de la vista
	//if (isset($param_mensaje))
	//	$mensaje->texto = $param_mensaje;

	//recuperando variables pasados a la vista
	$mensaje->texto = $parametros_vista->mensaje;

?>



<!-- ========================================== -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />

<title><?=VISTA_TITULO_TAB;?></title>


<!-- ========================================== -->
<?php

	//definiendo menu principal
	include_once('box_menuprincipal.php');

	//incluyendo los estilos en comun utilizados en el sistema
	include_once('box_estilos_comunes.php');

	//incluyendo los java.script comun para todas la vistas
	include_once('box_javascript_comun.php');

	//incluyendo los scripts personalizados
?>

<script language="javascript" src="javascript/vlogin.js" type="text/javascript"></script>

</head>

<body onLoad="evento_login_body_onload();">

	<center>
		<p>&nbsp;</p>
		<p><strong><?=VISTA_TITULO;?></strong></p>
		<p>&nbsp;</p>

        <form id="frmlogin" name="frmlogin" autocomplete="off" method="post" action="/clogin/process_login">
			<?php
			//imprimendo la tabla
			$tabla->set_valores_fila(0,array("Usuario:",$txtusuario->get_html(),$cmdaceptar->get_html()));

			$tabla->set_valores_fila(1,array(tsdk_reemplazar_html("Contraseña:"),
											 $txtpassword->get_html(),
											 $cmdnewpassword->get_html()
											 ));

			$tabla->set_valores_fila(3,array($mensaje->get_html()));

			$tabla->set_propiedades_celda(3,0,array("colspan"=>"3"));

			echo $tabla->get_html();

			?>

		</form>
	</center>

</body>

</html>
