<?php
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu_item.php");

	//definiendo constantes
	include_once("../objetos/configuracion_app/constantes.php");

	//-------------------------------------------------------------------------------------------------------
    //definiendo variables
    define("VISTA_TITULO","RECUPERAR NUEVA CONTRASEÑA"); 		//TITULO DE LA VISTA
    define("VISTA_TITULO_TAB","Cambio contraseña");  			//TITULO DEL TAB DEL NAVEGADOR
    define("VISTA_IDVENTANA","cambio");  						//ID DE VENTANA
	//-------------------------------------------------------------------------------------------------------

	//definiendo controles
	
	$tabindex=0;
	$txtcorreo = new tsdk_textbox();
	$txtcorreo->config_as_text("",++$tabindex);
	$txtcorreo->propiedades->add("autocomplete")->value="off";
	$txtcorreo->set_id_ventana_control(VISTA_IDVENTANA,"txtcorreo");
	$txtcorreo->eventos->add_evento_standard("onkeypress","event,this");
	
	$cmdaceptar = new tsdk_button();
	$cmdaceptar->config_as_button_standard("Aceptar",++$tabindex);
	$cmdaceptar->estilos->add("width",new tsdk_coleccion_item("150px"));

	$cmdaceptar->set_id_ventana_control(VISTA_IDVENTANA,"cmdaceptar");
	$cmdaceptar->eventos->add_evento_standard("onclick");

	$mensaje = new tsdk_span();
	$mensaje->config_as_mensaje();
	$mensaje->set_id_ventana_control(VISTA_IDVENTANA,"mensaje");
	
	//tsdk_error_log_trace("parametros de vista", $parametros_vista);
	//error_log("valor parametros de vista:".$parametros_vista->mensaje);
	
	//$mensaje->texto = global_get_session_post("mensaje");
	$mensaje->texto = $parametros_vista->mensaje;

?>

<!-- ========================================== -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />

<title><?=VISTA_TITULO_TAB;?></title>


<!-- ========================================== -->
<?php

	//definiendo menu principal
	include_once('box_menuprincipal.php');

	//incluyendo los estilos en comun utilizados en el sistema
	include_once('box_estilos_comunes.php');

	//incluyendo los java.script comun para todas la vistas
	include_once('box_javascript_comun.php');

	//incluyendo los scripts personalizados
?>

<script language="javascript" src="javascript/vnewpassword.js" type="text/javascript"></script>

</head>

<body onLoad="evento_newpassword_body_onload();">

	<center>
		<p>&nbsp;</p>
		<b><p><?=VISTA_TITULO;?></p></b>
		<p>&nbsp;</p>

        <form id="frmnewpassword" name="frmnewpassword" autocomplete="off" method="post" action="/Cnewpassword/process_newpassword">
			<?php
			
			//imprimendo la tabla
			
			$tabla = new tsdk_tabla(VISTA_IDVENTANA,"tabla",
									2,3,
									array("width"=>"600",
											"border"=>"0")
									); 

			$tabla->set_valores_fila(0,array("Ingrese su correo:",$txtcorreo->get_html(),
											$cmdaceptar->get_html()
											 ));
			
			$tabla->set_valores_fila(1,array("",$mensaje->get_html()));
			
			echo $tabla->get_html();
			
			?>

		</form>
	</center>

</body>

</html>
