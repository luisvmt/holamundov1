<?php

	//menu vertical
	/*$menuprincipal = new tsdk_menu("menuprincipal","687EAB", //nombremenu,color-fondo-menu
												"CED8F6", //color-fondo-submenu
												"F7F2E0", //color-texto-menu
												"6A0888"); //color-texto-submenu */

	//menu horizontal
	$menuprincipal = new tsdk_menu("menuprincipal","E6E6E6", //nombremenu, color-fondo-menu
												"CED8F6", //color-fondo-submenu-al pasar encima
												"084B8A", //color-texto-menu
												"B45F04", //color-texto-submenu-al pasar encima
												"0079b2", //color de borde de menu
												"FAFAFA"); //color de fondo de menu desplegable (submenu)

	//verificando permisos para la creacion de menus

	//---
	if ( global_habilitar_menu("micuenta") )
	{
		$item = new tsdk_menu_item("micuenta","Mi cuenta");

		if ( global_habilitar_menu("cambiarpassword") )
		{
			$subitem = new tsdk_menu_item("cambiarpassword","Cambiar contraseña",true);
			$item->add_subitem($subitem);

			/*$subitem = new tsdk_menu_item("admservicios","Servicios",true);
			$item->add_subitem($subitem);*/
		}

		$menuprincipal->add_item($item);
	}

	//---
	if ( global_habilitar_menu("administracion") )
	{
		$item = new tsdk_menu_item("administracion","Administración");

		if ( global_habilitar_menu("mnuactivos") )
		{
			$subitem = new tsdk_menu_item("mnuactivos","Activos",true);
			$item->add_subitem($subitem);
		}

		$menuprincipal->add_item($item);
	}

	//---
	if ( global_habilitar_menu("mantenimiento_tablas") )
	{
		$item = new tsdk_menu_item("mantenimiento_tablas","Mantenimiento de tablas");

		if ( global_habilitar_menu("mnusuarios") )
		{
			$subitem = new tsdk_menu_item("mnuusuarios","Usuarios",true);
			$item->add_subitem($subitem);
		}

		if ( global_habilitar_menu("mnudepartamentos") )
		{
			$subitem = new tsdk_menu_item("mnudepartamentos","Departamentos",true);
			$item->add_subitem($subitem);
		}

		$menuprincipal->add_item($item);
	}

	//---
	if ( global_habilitar_menu("salir") )
	{
		$item = new tsdk_menu_item("salir","Salir",true);
		$menuprincipal->add_item($item);
	}

?>

<!-- ========================================== -->
<script  type="text/javascript">

	//----------------------------------------------------
	//crea un formulario usando codigo y lo envia
	function submit_formulario(idmenu,iditem_parent,iditem,
							   parametros_form,url,mensaje)
	{
		var body = document.body;

		parametros_form['idmenu']=idmenu;
		parametros_form['iditem_parent']=iditem_parent;
		parametros_form['iditem']=iditem;
		parametros_form['mensaje']=mensaje;

		form=document.createElement('form');
		form.method = 'POST';
		form.action = url;
		form.name = 'jsform';
		for (index in parametros_form) {
			var input = document.createElement('input');
			input.type='hidden';
			input.name=index;
			input.id=index;
			input.value=parametros_form[index];
			form.appendChild(input);
		}
		body.appendChild(form);
		form.submit();
	}

	//------------------------------------------------------------------
	//creando los eventos de los menus
	//------------------------------------------------------------------
	//esta funcion busca el menu y lo despliega
	//llamando a la administración de usuarios

	function menuprincipal_cambiarpassword_onclick(idmenu,iditem_parent,iditem)
	{
		//llamando al dialogo para el cambio de contraseña
		(new tjs_dialog("cambiopassword_dialog")).show("Cambio de contraseña", //titulo
										"", //contenido
										"/cusuarios/ajax_get_html_cambiar_password", //URL con el contenido
										"", //parametros de la url
										"Aceptar", //nombre primer boton
										"Cancelar", //nombre segundo boton
										"", //nombre tercer boton
										function (){
											cambiarpassword_aceptar(); //funcion post primer boton

										},
										function (){
											cambiarpassword_cancelar(); //funcion post segundo boton
										},
										"", //funcion post tercer boton
										500, //width
										true //cierra con la tecla escape
										);

		//alert("mostrando ventana");
		//global_show_cambiarpassword(); //llamando a la funcion para cambio de contraseña
	}

	function menuprincipal_mnuactivos_onclick(idmenu,iditem_parent,iditem)
	{
		var url = "cactivos";
		var parametros_form = {  'mensaje':''};
		var mensaje="";

		submit_formulario(idmenu,iditem_parent,iditem,
							   parametros_form,url,mensaje);
	}

	function menuprincipal_mnuusuarios_onclick(idmenu,iditem_parent,iditem)
	{
		var url = "cusuarios";
		var parametros_form = {  'mensaje':''};
		var mensaje="";

		submit_formulario(idmenu,iditem_parent,iditem,
							   parametros_form,url,mensaje);
	}

	function menuprincipal_mnudepartamentos_onclick(idmenu,iditem_parent,iditem)
	{
		var url = "cdepartamentos";
		var parametros_form = {  'mensaje':''};
		var mensaje="";

		submit_formulario(idmenu,iditem_parent,iditem,
							   parametros_form,url,mensaje);
	}

	/*function prueba_validacion()
	{
		return true;
	}*/

	function menuprincipal_salir_onclick (idmenu,iditem_parent,iditem)
	{
		//cerrando session y dirigir a login
		(new tjs_messagebox("")).show("Confirmar",
										"¿Esta seguro que desea salir de la aplicación?",
										function(){   //funcion primer_boton
											window.location.replace('cfuncion_desconectar');
										},
										"", //funcion segundo_boton
										"", //funcion tercer_boton
										false, //boton_aceptar
										false, //boton_aceptar_cancelar
										true //botones SI_NO
										);

	}

	//---------------------------------------------------------------------
	//Funciones adicionales
	//---------------------------------------------------------------------

	//---------------------------------------------------------------------
	//Esta funcion se ejecuta cuando se da clic en "Aceptar" de la ventana
	//cambio de password
	function cambiarpassword_aceptar()
	{
		//validando que se haya ingresado dos contraseñas iguales
		var idventana = "cambiopassword_dialog";
		var respuesta = "";

		//validando que se haya ingresado el nuevo password
		password_nuevo = new tjs_textbox(idventana+"_contrasena_nueva");
		password_nuevo_confirmacion = new tjs_textbox(idventana+"_contrasena_nueva_confirmacion");
		control_mensaje = new tjs_span(idventana+"_mensaje");

		if (password_nuevo.get_text() == "")
		{
			control_mensaje.set_text("Debe ingresar una contraseña valida");
			password_nuevo.set_focus();
			return;
		}

		if (password_nuevo.get_text() != password_nuevo_confirmacion.get_text())
		{
			control_mensaje.set_text("Las contraseñas no coinciden ... volver a ingresar los datos");
			password_nuevo.set_focus();
			return;
		}

		//ejecutando por ajax el cambio de contraseña
		var salida = "";
		var parametros = { "password_nuevo" : password_nuevo.get_text() };

		salida = tjs_ajax_exec_sincrono("/cusuarios/ajax_procesar_cambiarpassword",parametros);

		//recuperando la respuesta (json)
		try {
			datos = jQuery.parseJSON(salida.trim());

			if (datos.response != "0")
			{
				control_mensaje.set_text(datos.mensaje);
				password_nuevo.set_focus();
				return;
			}

		} catch (e) {
				alert(salida);
				alert(e);
				return;
		}

		//cerrando el dialogo
		$("#"+idventana).dialog("close");
	}

	//---------------------------------------------------------------------
	//Esta funcion se ejecuta cuando se da clic en "Cancelar" de la ventana
	//cambio de password
	function cambiarpassword_cancelar()
	{
		//validando que se haya ingresado dos contraseñas iguales
		var idventana = "cambiopassword_dialog";

		//validando que se haya ingresado el nuevo password
		password_nuevo = new tjs_textbox(idventana+"_contrasena_nueva");
		password_nuevo_confirmacion = new tjs_textbox(idventana+"_contrasena_nueva_confirmacion");
		control_mensaje = new tjs_span(idventana+"_mensaje");

		password_nuevo.set_text("");
		password_nuevo_confirmacion.set_text("");
		control_mensaje.set_text("");

		//cerrando el dialogo
		$("#"+idventana).dialog("close");
	}

</script>
<!-- =============================================== -->
