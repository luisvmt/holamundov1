<?php
	include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_dialog.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu.php");
	include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_menu_item.php");

	//-------------------------------------------------------------------------------------------------------
    //definiendo variables
    define("VISTA_TITULO","ADMINISTRACION DE USUARIOS"); 	//TITULO DE LA VISTA
    define("VISTA_TITULO_TAB","Usuarios");  					//TITULO DEL TAB DEL NAVEGADOR
    define("VISTA_IDVENTANA","usuario");  					//ID DE VENTANA
	//-------------------------------------------------------------------------------------------------------

	//definiendo controles
	/*$tabindex=0;
	$txtusuario = new tsdk_textbox();
	$txtusuario->config_as_text("",++$tabindex);
	$txtusuario->add_propiedades(array("autocomplete"=>"off"));
	$txtusuario->set_id_ventana_control(VISTA_IDVENTANA,"txtusuario");
	$txtusuario->eventos->add_evento_standard("onkeypress","event,this");

	$txtpassword = new tsdk_textbox();
	$txtpassword->config_as_password("",++$tabindex);
	$txtpassword->add_propiedades(array("autocomplete"=>"off"));
	$txtpassword->set_id_ventana_control(VISTA_IDVENTANA,"txtpassword");
	$txtpassword->eventos->add_evento_standard("onkeypress","event,this");

	$cmdaceptar = new tsdk_button();
	$cmdaceptar->config_as_button_standard("Aceptar",++$tabindex);
	$cmdaceptar->set_id_ventana_control(VISTA_IDVENTANA,"cmdaceptar");
	$cmdaceptar->eventos->add_evento_standard("onclick");

	$mensaje = new tsdk_span();
	$mensaje->config_as_mensaje();
	$mensaje->set_id_ventana_control(VISTA_IDVENTANA,"mensaje");

	$tabla = new tsdk_tabla(idventana,idcontrol,
							4,3, 					//filas,columnas
						    array("width"=>"430",
								"border"=>"0")); 

	//definiendo variables de la vista
	if (isset($param_mensaje))
		$mensaje->texto = $param_mensaje;*/
?>

<!-- ========================================== -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />

<title><?=VISTA_TITULO_TAB;?></title>
 

<!-- ========================================== -->
<?php

	//definiendo menu principal
	include_once('box_menuprincipal.php');

	//incluyendo los estilos en comun utilizados en el sistema
	include_once('box_estilos_comunes.php');

	//incluyendo los java.script comun para todas la vistas
	include_once('box_javascript_comun.php');

	//incluyendo los scripts personalizados
	//include_once("../www/javascript/vusuarios_browse.js.php");
	//include_once("../www/javascript/vusuarios_edicion.js.php");
	//include_once("../www/javascript/vusuarios_importar_exportar.js.php");

	//Imprimiendo los estilos y script del menu
	echo $menuprincipal->get_estilo_horizontal();
	echo $menuprincipal->get_javascript();
?>

<script language="javascript" src="javascript/vusuarios_browse.js" type="text/javascript"></script>
<script language="javascript" src="javascript/vusuarios_edicion.js" type="text/javascript"></script>
<script language="javascript" src="javascript/vusuarios_importar_exportar.js" type="text/javascript"></script>

</head>

<!-- ========================================== -->
<body onLoad="evento_usuario_body_onload();">

	<div id="datos_menu">
		<?php
			$idventana = "usuario";

			$control = new tsdk_textbox($idventana,"idmenu");
			$control->config_as_text_hidden($parametros_vista->param_idmenu);
			$control->print_html();

			$control = new tsdk_textbox($idventana,"iditem_parent");
			$control->config_as_text_hidden($parametros_vista->param_iditem_parent);
			$control->print_html();

			$control = new tsdk_textbox($idventana,"iditem");
			$control->config_as_text_hidden($parametros_vista->param_iditem);
			$control->print_html();
		?>
	</div>

	<div id="diseno_contenedor">
		<div id="diseno_cabecera">
			<img src="images/bg-header.gif"  style="width: 100%; height: 40px">
		</div>

		<!-- diseño de menu, top o left -->
	    <div id="diseno_contenido_top" style="float:left; padding-top:5px; width: 100% ">

			<?php
				echo $menuprincipal->get_html();
			?>

		</div>

		<div id="diseno_contenido_left" style="clear:left; float:left; ">

			<?php
				//echo $menuprincipal->get_html();
			?>
		</div>

		<!-- Definiendo el titulo de la ventana -->
		<div id="diseno_titulo_contenido" class="diseno_titulo_contenido" style="clear: both;  ">
			<CENTER>
			<p style="padding-top: 10px;"><strong><?=VISTA_TITULO;?></strong></p>
			</CENTER>
		</div>

		<div id="diseno_contenido_right" class="diseno_contenido_right" style="clear: both; width: 96%; ">

			<div id="<?php echo VISTA_IDVENTANA.'_'; ?>contenido_browse" style="display: block; width: 100%;">
				<?php
					$imagen = new tsdk_image("","",array("src"=>"images/loading3.gif","border"=>"0"));
					echo $imagen->get_html();
				?>
			</div>

			<div id="<?php echo VISTA_IDVENTANA.'_'; ?>contenido_importar_exportar" style="display: none; float:left; width: 100%;">
				<?php
					$imagen = new tsdk_image("","",array("src"=>"images/loading3.gif","border"=>"0"));
					echo $imagen->get_html();
				?>
			</div>

			<div id="<?php echo VISTA_IDVENTANA.'_'; ?>contenido_modificar" style="display: none; float:left; width: 100%;">
				<?php
					$imagen = new tsdk_image("","",array("src"=>"images/loading3.gif","border"=>"0"));
					echo $imagen->get_html();
				?>
			</div>

			<div id="<?php echo VISTA_IDVENTANA.'_'; ?>contenido_waiting" style="display: none; float:left; ">
				<?php
					$imagen = new tsdk_image("","",array("src"=>"images/loading3.gif","border"=>"0"));
					echo $imagen->get_html();

				?>
			</div>

		</div>

		<div id="diseno_pie">
			<br/> 
			<?php
				include 'box_pie.php';
			?>
		</div>
		
		<div style="visibility: hidden;">
			
		<?php
			//------- creando una ventana de dialogo oculta, se utilizara como dialogo, messagebox, etc
			/*$control = new tsdk_dialog("Cambio de contraseña");
			$control->content = global_setcontent_cambiarpassword("global");
			$control->set_id_ventana_control("global","cambiarpassword");
			echo $control->get_html();
			*/
		?>

		</div>
		
	</div>

</body>

</html>
