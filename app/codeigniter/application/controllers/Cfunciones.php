<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed'); 

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
//include_once(APPPATH."../../objetos/app/tface_usuarios.php"); 					//definiendo la interfaz

class cfunciones extends tsdk_controlador_common
{
	function __construct()
	{ 
		parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD);

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

		//iniciando variables
		$this->m_nombrecontrolador="cfunciones";
		
		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}

    //-----------------------------------------------------------
    public function ajax_delay()
    {
        //hallando variables
        $tiempo_segundos= $this->input->post('tiempo_segundos');
        $mensaje = "OK";

        //durmiendo la ejecucion
        sleep($tiempo_segundos);
        
        //finalizando
        $trama_respuesta = array("response"=>"0",
                                    "mensaje"=>$mensaje);
        echo json_encode($trama_respuesta);
        return;
    }
}

