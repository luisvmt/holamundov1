<?php

//definiendo constantes
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");
include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_rest.php");
include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_response_rest.php");
include_once(APPPATH."../../objetos/app/trs_item.php");

class citems_rest extends tsdk_controlador_rest 
{
	// ******************************
    // variables miembro
    // ******************************
	// 
	// 
    
	//-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	//public	$encabezados=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	//public	$datos=null;

	
    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************
    public function __construct() {
        parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD ); 

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
        //iniciando variables
        //$this->m_nombre_controlador="clogin_rest";
				
    }
	
	//-------------------------------------------
	//funcion publica.
	//Metodo GET : browse
	//-------------------------------------------
	public function browse_get() 
	{
		$respuesta_rest = new tsdk_response_rest();

		//------------------------------
		//recuperando variables
		//------------------------------
		$token = $this->get("token");
		
		//validando 
		if ( $token == NULL )
		{
			$mensaje_error = "No ha ingresado el token.";
			
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x ".$mensaje_error.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$mensaje_error;
			$this->response($respuesta_rest->get_array());
            return;
		}
			
		//----------------------------------
		//validando token-ok
		//----------------------------------
		$objeto_token = $this->validar_token($token,CLAVETOKEN);
		if (empty($objeto_token))
		{
			$mensaje_error = "Token invalido.";
			
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x ".$mensaje_error.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$mensaje_error;
			$this->response($respuesta_rest->get_array());
            return;
		}  
		
		//-------------------------------------------------
		//leyendo datos de la base de datos
		$vitem = new trs_item($this->m_basedatos);
		
		//validando el item y password
		$consulta = "select id_item, cod_item, nom_item from item order by cod_item";		
		$tabladatos = $vitem->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql

		//generando el encabezado
		$respuesta_rest->encabezados->add("codrespuesta")->value="0";
		$respuesta_rest->encabezados->add("mensaje")->value="Consulta OK";
		
		//generando los datos
		$i=0;
		foreach ($tabladatos->filas as $clave => $fila)
		{
			$respuesta_rest->datos->set_valor($i,"id_item",$fila->item("id_item")->value);
			$respuesta_rest->datos->set_valor($i,"cod_item",$fila->item("cod_item")->value);
			$respuesta_rest->datos->set_valor($i,"nom_item",$fila->item("nom_item")->value);

			$i++;
		}

		//devolviendo el json
		$this->response($respuesta_rest->get_array());
    }
	
}