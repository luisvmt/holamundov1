<?php

//definiendo constantes
include_once("../objetos/configuracion_app/constantes.php");

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_rest.php");

class ejemplorest extends tsdk_controlador_rest {

    public function __construct() {
        parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD ); 

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;
        //tsdk_propiedades::$FILE_ERROR = FILE_ERROR;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
        //iniciando variables
        $this->m_nombre_controlador="ejemplorest";
    }
	
	public function users_get() {
        //$this->load->model('Users');
        //$this->response($this->Users>findAll());
		$salida["codigo"]="ok";
		$salida["mensaje"]="mensaje de respuesta";
		$salida["apppath"]=APPPATH;
		
		$this->response($salida);
    }
	
	public function pruebatoken_get()
	{
		
		//$datos_session["id"]=34;
		//$datos_session["nivel_usuario"]="U";
		$datos_session["aud"]=$this->get_aud();
		$token = $this->generar_token( $datos_session, CLAVETOKEN );
		
		echo "token-generado:".$token;
		echo "<br><br>";
		
		//validando token-ok
		echo "<pre>";
		$respuesta = $this->validar_token($token,CLAVETOKEN);
		if (empty($respuesta))
		{
			echo "Error en token";
		}else{
			print_r($respuesta);
		}
		echo "</pre>";
		
		//validando token-clave-incorrecta
		echo "<pre>";
		$respuesta = $this->validar_token($token,"claveinvalida");
		if (empty($respuesta))
		{
			echo "Error en token";
		}else{
			print_r($respuesta);
		}
		echo "</pre>";
		
		//validando token-incorrecto
		echo "<pre>";
		$respuesta = $this->validar_token($token."texto",CLAVETOKEN);
		if (empty($respuesta))
		{
			echo "Error en token";
		}else{
			print_r($respuesta);
		}
		echo "</pre>";
		
		
		//validando la excepcion
		echo "<pre>";
		$respuesta = $this->validar_token("prueba",CLAVETOKEN);
		if (empty($respuesta))
		{
			echo "Error en token";
		}else{
			print_r($respuesta);
		}
		echo "</pre>";
	}
}