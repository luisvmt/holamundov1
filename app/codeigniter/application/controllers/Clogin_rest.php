<?php

//definiendo constantes
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");
include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_rest.php");
include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_response_rest.php");
include_once(APPPATH."../../objetos/app/trs_usuario.php");

class clogin_rest extends tsdk_controlador_rest 
{
	// ******************************
    // variables miembro
    // ******************************
	// 
	// 
    
	//-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	//public	$encabezados=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	//public	$datos=null;

	
    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************
    public function __construct() {
        parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD ); 

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
        //iniciando variables
        //$this->m_nombre_controlador="clogin_rest";
				
    }
	
	//validando usuario y contraseña
	/*public function validar_get() 
	{
		tsdk_error_log_trace("GET", $_GET);
		
		$param_usuario = $this->get("usuario");
		$param_password = $this->get("password");
		
		//validando 
		if ( $param_usuario == NULL )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x No ha ingresado la variable usuario".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return;
		}
		
		if ( $param_password == NULL)
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x No ha ingresado la variable password".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return;
		}
		
		$vusuario = new trs_usuario($this->m_basedatos);
		$isOK = false;
		$respuesta_rest = new tsdk_response_rest();
		
		//validando el usuario y password
		$isOK = $vusuario->validarusuario($param_usuario, $param_password);
		
		if ($isOK == false)
		{
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$vusuario->mensaje;
			$this->response($respuesta_rest->get_array());
			return ;
		}
		
		//generando un texto aleatorio (se usara como clave de cifrado)
		$respuesta_rest->encabezados->add("codrespuesta")->value="0";
		$respuesta_rest->encabezados->add("mensaje")->value="Validacion OK";
		//$respuesta_rest->encabezados->add("clavecifrado")->value=tsdk_rand_string(10);
		
		$datos_session["id"]=34;
		$datos_session["nivel_usuario"]="U";
		$respuesta_rest->encabezados->add("token")->value = $this->generar_token( $datos_session, CLAVETOKEN );

	 sleep(1);
		$this->response($respuesta_rest->get_array());
    }*/
	
	
	//-------------------------------------------
	//funcion publica.
	//Metodo POST : validar (usuario,password)
	//-------------------------------------------
	public function validar_post() 
	{
		$respuesta_rest = new tsdk_response_rest();

		//------------------------------
		//recuperando variables
		//------------------------------
		$param_usuario = $this->post("usuario");
		$param_password = $this->post("password");

		//validando 
		if ( $param_usuario == NULL )
		{
			$mensaje_error = "No ha ingresado la variable usuario";
			
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x ".$mensaje_error.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$mensaje_error;
			$this->response($respuesta_rest->get_array());
            return;
		}
		
		if ( $param_password == NULL )
		{
			$mensaje_error = "No ha ingresado la variable password";
			
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
	             "#x ".$mensaje_error.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$mensaje_error;
			$this->response($respuesta_rest->get_array());
            return;
		}

		//------------------------------
		//validando usuario/password
		//------------------------------
		$vusuario = new trs_usuario($this->m_basedatos);
		$isOK = false;
		
		//validando el usuario y password
		$isOK = $vusuario->validarusuario($param_usuario, $param_password);
		
		if ($isOK == false)
		{
			//devolviendo el error
			$respuesta_rest->encabezados->add("codrespuesta")->value="-1";
			$respuesta_rest->encabezados->add("mensaje")->value=$vusuario->mensaje;
			$this->response($respuesta_rest->get_array());
			return ;
		}
		
		//generando un texto aleatorio (se usara como clave de cifrado)
		$respuesta_rest->encabezados->add("codrespuesta")->value="0";
		$respuesta_rest->encabezados->add("mensaje")->value="Validacion OK";
		//$respuesta_rest->encabezados->add("clavecifrado")->value=tsdk_rand_string(10);
		
		//$datos_session["id"]=34;
		//$datos_session["nivel_usuario"]="U";
		$datos_session["aud"]=$this->get_aud();
		$respuesta_rest->encabezados->add("token")->value = $this->generar_token( $datos_session, CLAVETOKEN );

		$this->response($respuesta_rest->get_array());
    }
}