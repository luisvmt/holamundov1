<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/tsdk_tipodato.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
include_once(APPPATH."../../objetos/app/tface_login.php"); 					//definiendo la interfaz

//----------------------------------------------------------------------------
//Este es el controlador que carga la ventana de login
//----------------------------------------------------------------------------
class clogin extends tsdk_controlador_common 
{
    // **********************************
	// variables miembro
    // **********************************

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

    public function __construct()
    {
        parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD,ESQUEMABD,MANEJADORBD,
        					"",false); //no validara que se halla hecho login

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
        //iniciando variables
        $this->m_nombre_controlador="clogin";
    }

    //-----------------------------------------------------------
	/**
	* Procesa llamada por defecto
	*
	* @example "sample_xxx.php"
	*/

	public function index()
    {
        //definiendo variables
        $mensaje = "";

        //llamando al padre
        parent::index();

        //iniciando variables. solo se llama en esta oportunidad
        $this->inicializar_variables();

        //recuperando parametros de session
        $this->load_parametros_controlador();

        //recuperando variables
        $mensaje = $this->m_parametros_controlador->mensaje;

        //creando variables para vista
        $parametros_vista = new tsdk_parametros_vista();
        $parametros_vista->mensaje = $mensaje;

		//echo "LOGIN:".APPPATH."<br>";

        //mostrando la vista
        $data['parametros_vista']=$parametros_vista;
        $this->load->view('vlogin',$data);
    }

	
	//-----------------------------------------------------------
	/**
	* Procesa llamada desde el formulario
	*
	* @example "sample_xxx.php"
	*/
	public function process_login()
    {
        //llamando a interfaz
		$interfaz = new tface_login($this,$this->m_basedatos);
		$respuesta = $interfaz->process_login();
	}


}

