<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed'); 

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
//include_once(APPPATH."../../objetos/app/tface_usuarios.php"); 					//definiendo la interfaz
include_once(APPPATH."../../objetos/app/trs_usuario.php");

//definiendo ventanas
include_once(APPPATH."../../objetos/app/twindow_usuarios_browse.php");
include_once(APPPATH."../../objetos/app/twindow_usuarios_edicion_controles.php");
include_once(APPPATH."../../objetos/app/twindow_usuarios_edicion.php");
include_once(APPPATH."../../objetos/app/twindow_usuarios_importar_exportar.php");

//----------------------------------------------------------------------------
//Este es el controlador que carga la ventana de login
//----------------------------------------------------------------------------
class Cnewpassword extends tsdk_controlador_common
{
    // ******************************
    // variables miembro
    // ******************************

    // ******************************
    // metodos
    // ******************************

    function __construct()
    {
        parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD,
        					"",false); //no validara que se halla hecho login

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;
        
        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
        //iniciando variables
        $this->m_nombre_controlador="cnewpassword";
    }

    function index()
    {
        //definiendo variables
        $mensaje = "";

        //llamando al padre
        parent::index();

        //iniciando variables. solo se llama en esta oportunidad
        $this->inicializar_variables();

        //recuperando parametros de session
        $this->load_parametros_controlador();

        //recuperando variables
        $mensaje = $this->m_parametros_controlador->mensaje.""; //se concatena '' para que se defina la variable

        //creando variables para vista
        $parametros_vista = new tsdk_parametros_vista();
        $parametros_vista->mensaje = $mensaje;

        //mostrando la vista
        $data['parametros_vista']=$parametros_vista;
        $this->load->view('vnewpassword',$data);

    }

    // **************************************************
    // procesando llamada desde el formulario
    // **************************************************

    function process_newpassword()
	{	
		//validando la existencia de variables
		$mensaje="";
		$control="";
        $idventana="cambio_";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array($idventana."txtcorreo")) == false)
		{
			error_log(basename(__FILE__).",".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$datos["mensaje"]=$this->m_mensaje;
			//$this->redirect(base_url()."c",$datos);
            $this->redirect("/cnewpassword",$datos);
			return;
		}

		//validando_controles_obligatorios  
		if ($this->validar_controles_obligatorios(
							array(
								  array($idventana."txtcorreo","Correo electronico",tsdk_tipodato::tstring_no_vacio,"")
								  ),
							$control //nombre del control donde mostrar el error
							) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$datos["mensaje"]=$this->m_mensaje;
			//$this->redirect(base_url()."cnewpassword",$datos);
            $this->redirect("/cnewpassword",$datos);
			return;
		}

		//recuperando variables
		$correo = trim($this->input->post($idventana."txtcorreo"));

		//validando el valor de las variables
		$vusuario = new trs_usuario( $this->m_basedatos );
		
		//obteniendo datos del usuario
		$vusuario->campos->add("email")->value = $correo;
		if ($vusuario->obtener_from_correo()<0)
		{
			$parametros = new tsdk_parametros_controlador();
			$parametros->mensaje = "No existe correo";
            $this->redirect("/cnewpassword",$parametros);
			return;
		}

		//modificando la contraseña
		$password = $vusuario->asignar_password_temporal();
		$vusuario->cambiar_password($vusuario->campos->item("idusuario")->value,$password);

		//llamando a la funcion tsdk_enviar_correo
		$body = "A continuacion se envia su nueva ".tsdk_reemplazar_html("contraseña")." para ingresar a casosnegocio.</br>";
		$body .= "Usuario:<b>".$vusuario->campos->item('usuario')->value."</b></br>";
		$body .= tsdk_reemplazar_html("Contraseña:")."<b>".$password."</b></br>";
		$respuesta=tsdk_enviar_correo($correo,"cambio de contrasena",$body);
		if($respuesta != 0)
		{
			$parametros = new tsdk_parametros_controlador();
			$parametros->mensaje = "error al enviar correo".$respuesta;
			$this->redirect("/newpassword",$respuesta);
			return;
		}

		//derivando a clogin
		$parametros = new tsdk_parametros_controlador();
		$parametros->mensaje = "Ingrese con usuario y contraseña enviada a su correo<br>[ ".$correo." ]";
        $this->redirect("/clogin",$parametros);
		return;

	}

}

?>
