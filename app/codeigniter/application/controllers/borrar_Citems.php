<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed'); 

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
include_once(APPPATH."../../objetos/app/tface_items.php"); 					//definiendo la interfaz

//definiendo ventanas
include_once(APPPATH."../../objetos/app/twindow_items_browse.php");
include_once(APPPATH."../../objetos/app/twindow_items_edicion_controles.php");
include_once(APPPATH."../../objetos/app/twindow_items_edicion.php");
include_once(APPPATH."../../objetos/app/twindow_items_importar_exportar.php");

class citems extends tsdk_controlador_common
{
	
    // **********************************
	// variables miembro
    // **********************************

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//Se recomienda que no tenga datos privados o protegidos
	//Solo métodos publicos, que sean los minimos que se publican en la web

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct()
	{ 

		parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD);

        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

		//iniciando variables
		$this->m_nombrecontrolador="citems";
		
		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}

	//-----------------------------------------------------------
	public function ajax_get_html_browse()
	{
		//sleep(5);
		$idview="item";

		//validando el permiso sobre el objeto
		$permisomodificar = global_has_permiso($this->m_basedatos,$idview,'mantenimiento');
		$permisobrowse = global_has_permiso($this->m_basedatos,$idview,'browse');

		//hallando variables
		$nropagina= $this->input->post('nropagina');
		$filtro = $this->input->post('filtro');
		$orderby = $this->input->post('orderby');
		$sentido = $this->input->post('sentido');
		$parametros = $this->input->post('parametros');

		//devolviendo tabla browse
		$objetobrowse = new twindow_items_browse($this->m_basedatos);

		if ($permisomodificar==true)
		{
			$objetobrowse->show_insertar=true;
			$objetobrowse->show_eliminar=true;
		}else{
			$objetobrowse->show_insertar=false;
			$objetobrowse->show_eliminar=false;
		}

		//html
		if ($permisobrowse==true)
		{
			echo "<!-- OK -->\n".$objetobrowse->get_html_browse($nropagina,$filtro,$orderby,$sentido,$parametros);
		}else{
			echo "<!-- OK -->\nNO TIENE PERMISO PARA VISUALIZAR ITEMS";
		}

		//javascript ... debe ser llamado luego de print_html
		//$browseitems->print_javascript($nropagina,$filtro,$orderby,$sentido);

	}

	//-----------------------------------------------------------
	public function ajax_get_reporte_excel()
	{
		//hallando variables
		$nropagina= $this->input->post('nropagina');
		$filtro = $this->input->post('filtro');
		$orderby = $this->input->post('orderby');
		$sentido = $this->input->post('sentido');
		$parametros = $this->input->post('parametros');

		//devolviendo tabla browse
		$objetobrowse = new twindow_items_browse($this->m_basedatos);

		//html
		$objetobrowse->print_reporte_excel($nropagina,$filtro,$orderby,$sentido,$parametros);
	}

	//-----------------------------------------------------------
	public function ajax_get_html_edicion()
	{
		$idview="item";

		//creando tabla de edicion
		$objeto_edicion = new twindow_items_edicion($this->m_basedatos);

		//validando el permiso sobre el objeto
		$objeto_edicion->permiso_modificar =  global_has_permiso($this->m_basedatos,$idview,'mantenimiento');

		$objeto_edicion->variables_post = $this->input->post();

		//html
		echo "<!-- OK -->\n".$objeto_edicion->get_html_edicion();

	}

	//-----------------------------------------------------------
	public function ajax_get_html_importar_exportar()
	{
		$idview="item";

		//creando tabla de edicion
		$objeto_ie = new twindow_items_importar_exportar($idview);

		//validando permiso
		//$objeto_edicion->permiso_importarexportar =  global_has_permiso($this->m_basedatos,$idview,'importar_exportar');
		$objeto_ie->permiso_importarexportar = false;

		//$objeto_edicion->variables_post = $this->input->post();

		//html
		echo "<!-- OK -->\n".$objeto_ie->get_html_ie();
	}

	//------------------------------------------------------------------
	//insertando el registro
	//es llamado desde ajax
	public function ajax_procesar_insertar_desde_form()
	{
		$control="";
		$recordset = null;

		//obteniendo objeto_vista
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			return;
		}

		//insertando
		if ( $recordset->insertar() < 0 )
		{
			$mensaje = $recordset->mensaje;

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//finalizando
		//$mensaje="Se ingresó correctamente";
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('id_item')->value );
		echo json_encode($trama_respuesta);

		return;

	}

	//------------------------------------------------------------------
	//modificar el registro (cuando se da clic en grabar)
	//es llamado desde ajax
	public function ajax_procesar_modificar_desde_form()
	{
		$control="";

		//obteniendo objeto_vista
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			return;
		}

		//modificando
		if ( $recordset->modificar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//finalizando
		//$mensaje="Se modificó correctamente";
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('id_item')->value );
		echo json_encode($trama_respuesta);

		return;
	}

	//------------------------------------------------------------------
	//eliminando el registro
	//es llamado desde ajax
	public function ajax_procesar_eliminar_registro()
	{
		$mensaje="";
		$control="";
		$idview  = "item";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para modificar.";
			
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "id_item"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("id_item") ) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//estableciendo campos de base de datos
		$recordset = new trs_item( $this->m_basedatos );
		//$recordset->set_item_valor('id_item',$this->input->post("id_item"));
		$recordset->campos->add("id_item")->value = $this->input->post("id_item");

		//eliminando
		if ( $recordset->eliminar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//finalizando
		$mensaje="Se eliminó el registro correctamente";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje);
		echo json_encode($trama_respuesta);

		return;

	}

	//------------------------------------------------------------------
	//clonando el registro
	//es llamado desde ajax
	public function ajax_procesar_clonar_registro()
	{
		$mensaje="";
		$control="";
		$idview  = "item";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para modificar.";
			
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "id_item"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("id")) == false) //se coloca la variable id desde javascript
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//decodificando la variable id
		$lista_id = json_decode($this->input->post("id"),true); //id es un texto json enviado desde el cliente
		if (is_null($lista_id))
		{
			error_log("error en json_decode");
			return;
		}
		
		//estableciendo campos de base de datos
		$recordset = new trs_item( $this->m_basedatos );
		$recordset->campos->add('id_item')->value = $lista_id["id_item"];

		//clonando
		if ( $recordset->clonar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//generando el json de respuesta
		$datos = "{".
					"\"id_item\":\"".strval($recordset->campos->item("id_item")->value)."\"".
					"}";
		
		//finalizando
		$mensaje="Se clonó el registro correctamente. Debe grabar si acepta el nuevo registro.";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$datos );
		echo json_encode($trama_respuesta);


		return;

	}

	//------------------------------------------------------------------
	//importando desde un archivo excel
	//es llamado desde ajax
	public function ajax_procesar_importar_desde_form()
	{
		$control="";
		$idview  = "item";

		//obteniendo datos
		$nombre_archivo = $_FILES[$idview.'_cmdimportfile']['name'];
		$archivador = DIRECTORIO_TEMPORAL."/".$nombre_archivo;
		$tipo_archivo = $_FILES[$idview.'_cmdimportfile']['type'];
		$tamano_archivo = $_FILES[$idview.'_cmdimportfile']['size'];
		$tmp_archivo = $_FILES[$idview.'_cmdimportfile']['tmp_name'];
		$error_archivo = $_FILES[$idview.'_cmdimportfile']['error'];

		if ($error_archivo != 0)
		{
			$this->m_mensaje = "Error al recibir archivo. Codigo: ".$error_archivo;
			if ($error_archivo==2)
			{
				$this->m_mensaje .= " - Archivo excede tamaño máximo.";
			}

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		if ($nombre_archivo == "")
		{
			$this->m_mensaje = "No ha seleccionado un archivo.";

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//grabando el archivo temporal
		if (!move_uploaded_file($tmp_archivo, $archivador))
		{
			$this->m_mensaje = "Error al guardar archivo. Archivo origen:".$tmp_archivo.", archivo destino:".$archivador;

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//========================================
		//Importando el archivo de datos
		//----------------------------------------

		//La primera fila del archivo excel debe ser el encabezado
		//aquellos que no tienen clave primaria son registro nuevos
		//aquellos que si tienen clave primaria son modificaciones
		//aquellos que si tienen clave primaria y no tienen otro campo son eliminaciones
		//finaliza cuando hay una fila en blanco

		$tabla_datos = obtener_datos_excel($archivador);

		foreach ($tabla_datos as $indice=>$fila)
		{
			//creando objeto de base de datos
			$objeto = new trs_item( $this->m_basedatos );

			switch ($fila['accion'])
			{
				case 'insertar':
					$respuesta = $this->insertar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				case 'modificar':
					$respuesta = $this->modificar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					if ($respuesta == 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. No se ha encontrado registro para modificar (clave primaria no existe).";
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				case 'eliminar':
					$respuesta = $this->eliminar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					if ($respuesta == 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. No se ha encontrado registro para eliminar.";
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				default:
					{
						$this->m_mensaje = "No ingreso una accion reconocida. Fila:".($indice+2).". Debe ser insertar, modificar o eliminar";

						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;
			}
		}
		//write_backtrace("xxx",$tabla_datos,array());


		//finalizando
		$this->m_mensaje="Se importo correctamente";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$this->m_mensaje,
								 "id_item"=>"0");
		echo json_encode($trama_respuesta);

		return;

	}

	//-------------------------------------------------------------------
	//exportando la tabla de base de datos hacia un archivo excel
	public function ajax_procesar_exportar_desde_form()
	{
		//hallando variables
		$mensaje= $this->input->post('mensaje');
		/*$filtro = $this->input->post('filtro');
		$orderby = $this->input->post('orderby');
		$sentido = $this->input->post('sentido');
		$parametros = $this->input->post('parametros');*/

		//devolviendo tabla browse
		$objetobrowse = new twindow_items_browse($this->m_basedatos);

		//html
		$objetobrowse->print_exportar_excel(); //$nropagina,$filtro,$orderby,$sentido,$parametros);
	}

	//-----------------------------------------------------------
	public function index()
	{
		//llamando al padre
		parent::index();

        //recuperando parametros de session
        $this->load_parametros_controlador();
        
		//leyendo parametros de post
		$idmenu = $this->m_parametros_controlador->idmenu;
		$iditem_parent = $this->m_parametros_controlador->iditem_parent;
		$iditem = $this->m_parametros_controlador->iditem;

		//leyendo parametros
        $mensaje = $this->m_parametros_controlador->mensaje;

        //creando variables para vista
        $parametros_vista = new tsdk_parametros_vista();
        $parametros_vista->param_idmenu = $idmenu;
        $parametros_vista->param_iditem_parent = $iditem_parent;
        $parametros_vista->param_iditem = $iditem;
        $parametros_vista->param_mensaje = $mensaje;

        //mostrando la vista
        $data['parametros_vista']=$parametros_vista;
        $this->load->view('vitems',$data);

	}
}

?>
