<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
include_once(APPPATH."../../objetos/app/tface_activos.php"); 				//definiendo la interfaz

//definiendo ventanas
include_once(APPPATH."../../objetos/app/twindow_activos_browse.php");
include_once(APPPATH."../../objetos/app/twindow_activos_edicion_controles.php");
include_once(APPPATH."../../objetos/app/twindow_activos_edicion.php");
include_once(APPPATH."../../objetos/app/twindow_activos_importar_exportar.php");

class cactivos extends tsdk_controlador_common
{

    // **********************************
	// variables miembro
    // **********************************

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//Se recomienda que no tenga datos privados o protegidos
	//Solo métodos publicos, que sean los minimos que se publican en la web

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/


	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct()
	{ 
		parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD,ESQUEMABD,MANEJADORBD);
		
        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

		//iniciando variables
		$this->m_nombrecontrolador="cactivos";
		
		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}

    //-----------------------------------------------------------
	/**
	* Procesa llamada por defecto
	*
	* @example "sample_xxx.php"
	*/

	public function index()
	{
		//llamando al padre
		parent::index();

        //recuperando parametros de session
        $this->load_parametros_controlador();
        
		//leyendo parametros de post
		$idmenu = $this->m_parametros_controlador->idmenu;
		$iditem_parent = $this->m_parametros_controlador->iditem_parent;
		$iditem = $this->m_parametros_controlador->iditem;

		//leyendo parametros
        $mensaje = $this->m_parametros_controlador->mensaje;

        //creando variables para vista
        $parametros_vista = new tsdk_parametros_vista();
        $parametros_vista->param_idmenu = $idmenu;
        $parametros_vista->param_iditem_parent = $iditem_parent;
        $parametros_vista->param_iditem = $iditem;
        $parametros_vista->param_mensaje = $mensaje;

        //mostrando la vista
        $data['parametros_vista']=$parametros_vista;
        $this->load->view('vactivos',$data);

	}

    //-----------------------------------------------------------
	/**
	* Devuelve el HTML de la ventana de modificacion (modificacion, insercion)
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/

	public function ajax_get_html_browse()
	{
		//llamando a interfaz
		$interfaz = new tface_activos($this,$this->m_basedatos);
		$respuesta = $interfaz->get_html_browse();
		echo $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* inserta un registro
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/

	public function ajax_procesar_insertar_desde_form()
	{
		$control="";
		$idview="activo";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para insertar.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "idactivo"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//llamando a interfaz
		$interfaz = new tface_activos($this,$this->m_basedatos);
		$respuesta = $interfaz->procesar_insertar_desde_form();
		echo $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* modificar el registro (cuando se da clic en grabar)
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
	
	public function ajax_procesar_modificar_desde_form()
	{
		$control="";
		$idview="activo";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para modificar.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "idactivo"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//llamando a interfaz
		$interfaz = new tface_activos($this,$this->m_basedatos);
		$respuesta = $interfaz->procesar_modificar_desde_form();
		echo $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* eliminando el registro
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/

	public function ajax_procesar_eliminar_registro()
	{
		$control="";
		$idview  = "activo";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para eliminar.";

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "idactivo"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//llamando a interfaz
		$interfaz = new tface_activos($this,$this->m_basedatos);
		$respuesta = $interfaz->procesar_eliminar_registro();
		echo $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* clonando el registro
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/

	public function ajax_procesar_clonar_registro()
	{
		$control="";
		$idview  = "activo";

		//validando los permisos sobre el objeto
		if (global_has_permiso($this->m_basedatos, //conexion a base de datos
							$idview,	//objeto
							"mantenimiento")==false) //accion_ejecutar
		{
			$mensaje="[ ".$idview." ] No tiene permiso para clonar.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje,
									 "idactivo"=>"0");
			echo json_encode($trama_respuesta);
			return;
		}

		//llamando a interfaz
		$interfaz = new tface_activos($this,$this->m_basedatos);
		$respuesta = $interfaz->procesar_clonar_registro();
		echo $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* importando desde un archivo excel
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
/*
	public function ajax_procesar_importar_desde_form()
	{
		$control="";
		$idview  = "activo";

		//obteniendo datos
		$nombre_archivo = $_FILES[$idview.'_cmdimportfile']['name'];
		$archivador = DIRECTORIO_TEMPORAL."/".$nombre_archivo;
		$tipo_archivo = $_FILES[$idview.'_cmdimportfile']['type'];
		$tamano_archivo = $_FILES[$idview.'_cmdimportfile']['size'];
		$tmp_archivo = $_FILES[$idview.'_cmdimportfile']['tmp_name'];
		$error_archivo = $_FILES[$idview.'_cmdimportfile']['error'];

		if ($error_archivo != 0)
		{
			$this->m_mensaje = "Error al recibir archivo. Codigo: ".$error_archivo;
			if ($error_archivo==2)
			{
				$this->m_mensaje .= " - Archivo excede tamaño máximo.";
			}

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		if ($nombre_archivo == "")
		{
			$this->m_mensaje = "No ha seleccionado un archivo.";

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//grabando el archivo temporal
		if (!move_uploaded_file($tmp_archivo, $archivador))
		{
			$this->m_mensaje = "Error al guardar archivo. Archivo origen:".$tmp_archivo.", archivo destino:".$archivador;

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			echo json_encode($trama_respuesta);
			return;
		}

		//========================================
		//Importando el archivo de datos
		//----------------------------------------

		//La primera fila del archivo excel debe ser el encabezado
		//aquellos que no tienen clave primaria son registro nuevos
		//aquellos que si tienen clave primaria son modificaciones
		//aquellos que si tienen clave primaria y no tienen otro campo son eliminaciones
		//finaliza cuando hay una fila en blanco

		$tabla_datos = obtener_datos_excel($archivador);

		foreach ($tabla_datos as $indice=>$fila)
		{
			//creando objeto de base de datos
			$objeto = new trs_activo( $this->m_basedatos );

			switch ($fila['accion'])
			{
				case 'insertar':
					$respuesta = $this->insertar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				case 'modificar':
					$respuesta = $this->modificar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					if ($respuesta == 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. No se ha encontrado registro para modificar (clave primaria no existe).";
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				case 'eliminar':
					$respuesta = $this->eliminar_from_array($objeto,$fila);
					if ($respuesta < 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. ".$this->m_mensaje;
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					if ($respuesta == 0)
					{
						$this->m_mensaje = "[ Fila:".($indice+2)." ]. No se ha encontrado registro para eliminar.";
						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;

				default:
					{
						$this->m_mensaje = "No ingreso una accion reconocida. Fila:".($indice+2).". Debe ser insertar, modificar o eliminar";

						error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
						$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
						echo json_encode($trama_respuesta);
						return;
					}
					break;
			}
		}
		//write_backtrace("xxx",$tabla_datos,array());


		//finalizando
		$this->m_mensaje="Se importo correctamente";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$this->m_mensaje,
								 "idactivo"=>"0");
		echo json_encode($trama_respuesta);

		return;

	}
*/
    //-----------------------------------------------------------
	/**
	* exportando hacia un archivo excel
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
/*
	public function ajax_procesar_exportar_desde_form()
	{
		//hallando variables
		$mensaje= $this->input->post('mensaje');
		
		//devolviendo tabla browse
		$objetobrowse = new twindow_activos_browse($this->m_basedatos);

		//html
		$objetobrowse->print_exportar_excel(); //$nropagina,$filtro,$orderby,$sentido,$parametros);
	}
	*/
	
    //-----------------------------------------------------------
	/**
	* obtiene reporte en excel
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
	
	public function ajax_get_reporte_excel()
	{
		//hallando variables
		$nropagina= $this->input->post('nropagina');
		$filtro = $this->input->post('filtro');
		$orderby = $this->input->post('orderby');
		$sentido = $this->input->post('sentido');
		$parametros = $this->input->post('parametros');

		//devolviendo tabla browse
		$objetobrowse = new twindow_activos_browse($this->m_basedatos);

		//html
		$objetobrowse->print_reporte_excel($nropagina,$filtro,$orderby,$sentido,$parametros);

	}
	
    //-----------------------------------------------------------
	/**
	* obtiene la ventana de edicion
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
	
	public function ajax_get_html_edicion()
	{
		$idview="activo";

		//creando tabla de edicion
		$objeto_edicion = new twindow_activos_edicion($this->m_basedatos);

		//validando el permiso sobre el objeto
		$objeto_edicion->permiso_modificar =  global_has_permiso($this->m_basedatos,$idview,'mantenimiento');

		$objeto_edicion->variables_post = $this->input->post();

		//html
		echo "<!-- OK -->\n".$objeto_edicion->get_html_edicion();

	}
	
    //-----------------------------------------------------------
	/**
	* obtiene la ventana de importar y exportar
	* Es llamado desde AJAX
	*
	* @example "sample_xxx.php"
	*/
	/*
	public function ajax_get_html_importar_exportar()
	{
		$idview="activo";

		//creando tabla de edicion
		$objeto_ie = new twindow_activos_importar_exportar($idview);

		//validando permiso
		//$objeto_edicion->permiso_importarexportar =  global_has_permiso($this->m_basedatos,$idview,'importar_exportar');
		$objeto_ie->permiso_importarexportar = false;

		//$objeto_edicion->variables_post = $this->input->post();

		//html
		echo "<!-- OK -->\n".$objeto_ie->get_html_ie();
	}
	*/
}

