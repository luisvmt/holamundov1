<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed'); 

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes
//include_once(APPPATH."../../objetos/app/tface_usuarios.php"); 					//definiendo la interfaz

class cfuncion_desconectar extends tsdk_controlador_common
{
	function __construct()
	{
		parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD,ESQUEMABD,MANEJADORBD);
		
        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;
        
		//iniciando variables
		$this->m_nombrecontrolador="cfunction_desconectar";
		
		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}

	function index()
	{		
		//llamando al padre
		parent::index();

        //recuperando parametros de session
        $this->load_parametros_controlador();
        
		//guardando variable login en session
		//$this->session->set_userdata('tsdk_is_logged', false);
		$_SESSION['tsdk_is_logged']=false;
		session_destroy(); //destruyendo la session
		//session_regenerate_id();

		//estableciendo parametros para controlador
		$parametros = new tsdk_parametros_controlador();
		$parametros->mensaje = "";

        $this->redirect("/clogin",$parametros);

	}
}

?>
