<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_codeigniter/tsdk_controlador_common.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_vista.php");
			
include_once(APPPATH."../../objetos/app/trs_usuario.php");

include_once(APPPATH."../../objetos/varios/funciones.php");					//definiendo funciones globales 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");		//definiendo constantes

//include_once(APPPATH."../../sdk_terceros/excel/PHPExcel.php"); //excel
//include_once(APPPATH."../../sdk_terceros/excel/PHPExcel/Writer/Excel2007.php"); // PHPExcel_Writer_Excel2007 

include_once(APPPATH."../../sdk_terceros/phpoffice_phpspreadsheet_1.10.1.0_require/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php");

//use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


//----------------------------------------------------------------------------
//Este es el controlador que carga la ventana de login
//----------------------------------------------------------------------------
class cprueba extends tsdk_controlador_common
{
    // ******************************
    // variables miembro
    // ******************************

    // ******************************
    // metodos
    // ******************************

    function __construct()
    { 
		parent::__construct(USUARIOBD,CONTRASENABD,SERVIDORBD,BASEDATOS,PUERTOBD,ESQUEMABD,MANEJADORBD);
		
        //definiendo variables del TSDK
        tsdk_propiedades::$TRACELOG = TRACELOG;

        //definiendo variables para envio de correo
        tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
        tsdk_propiedades::$SMTP_USER = SMTP_USER;
        tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
        tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
        tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
        tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
        tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

		//iniciando variables
		$this->m_nombrecontrolador="cprueba";
		
		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}


    function index()
    {
        //prueba de SQL
		$usuario = new trs_usuario($this->m_basedatos);
		
		//insertando
		/*$usuario->campos->add("nom_usuario")->value="luis abcde";
		$usuario->campos->add("hab_usuario")->value=true;
		$usuario->campos->add("correo_usuario")->value="luisvmt@hotmail.comxxx";
		$usuario->campos->add("pass_usuario")->value="nuevo";
		$usuario->campos->add("oficina_id_oficina")->value=1;
		$usuario->campos->add("usuariologin")->value="luisvmtxxx11";
		$usuario->campos->add("tipo_usuario")->value="u";
		if ($usuario->insertar()<0)
		{
			echo "Error en insertar: ".$usuario->codigo_error." - ".$usuario->mensaje;
		}
		echo "Se insertó el registro: ".$usuario->campos->item("id_usuario")->value;
		*/
		
		/*
		//modificando
		$usuario->campos->delete_all();
		$usuario->campos->add("id_usuario")->value=32;
		$usuario->campos->add("nom_usuario")->value="luis modificadoooo";
		$respuesta = $usuario->modificar();
		if ($respuesta<0)
		{
			echo "Error en modificar: ".$usuario->codigo_error." - ".$usuario->mensaje;
		}
		echo "Numero de usuarios modificados: ".$respuesta."<br>";
		*/
	
		/*
		//borrando
		$usuario->campos->delete_all();
		$usuario->campos->add("id_usuario")->value=35;
		$respuesta = $usuario->eliminar();
		if ($respuesta<0)
		{
			echo "Error en eliminar: ".$usuario->codigo_error." - ".$usuario->mensaje;
		}
		echo "Numero de usuarios eliminados: ".$respuesta."<br>";
		*/
			
		/*
		//obteniendo un registro
		$usuario->campos->delete_all();
		$usuario->campos->add("id_usuario")->value=33;
		if ($usuario->obtener()<0)
		{
			echo "Error en obtener: ".$usuario->cod_error." - ".$usuario->mensaje;
		}
		echo "Nombre recuperado: ".$usuario->campos->item("nom_usuario")->value;
		echo "<br><br>";*/
		/*
		//obteniendo una consulta1
		$usuario->consulta_sql = "select * from usuario";// where nom_usuario like #parametro1#";
		//$usuario->parametros->add("parametro1")->value="prue";
		$tabla_sql = $usuario->obtener_tabla_from_sql(); //retorna un objeto tsdk_datos_sql
		if ( is_null($tabla_sql) )
		{
			echo "Error en obtener_tabla: ".$usuario->cod_error." - ".$usuario->mensaje;
		}
		
		if ($tabla_sql->filas->count()==0) //no existe datos
		{
			$this->mensaje="No existe datos consultados";
			return -1;
		}
		
		
		echo "<pre>";
		foreach ($tabla_sql->filas as $clave => $registro)
		{
			echo "Fila - ".$clave." : ";
			
			foreach ($registro as $nombrex=>$valorx)
			{
				echo "campo:".$nombrex." - valor:".$valorx." * ";
			}
			echo "\n";
			
		}
		echo "</pre>";
		*/
		
		/*
		//obteniendo una consulta2 (pasando un parametro)
		$usuario->consulta_sql = "select * from usuario";
		$tabla_sql = $usuario->obtener_tabla();
		if ($tabla_sql == -1)
		{
			echo "Error en obtener_tabla: ".$usuario->cod_error." - ".$usuario->mensaje;
		}
		
		//obteniendo una consulta3 (pasando un campo como dato)
		$usuario->consulta_sql = "select * from usuario where idusuario=#idusuario#";
		$usuario->campos->add("idusuario")->value=23;
		$tabla_sql = $usuario->obtener_tabla();
		if ($tabla_sql == -1)
		{
			echo "Error en obtener_tabla: ".$usuario->cod_error." - ".$usuario->mensaje;
		}

		//obteniendo una consulta3 (pasando un campo como dato)
		$tabla_sql = $usuario->obtener_tabla_from_consulta("select * from usuario");
		if ($tabla_sql == -1)
		{
			echo "Error en obtener_tabla: ".$usuario->cod_error." - ".$usuario->mensaje;
		}
		
		//ejecutando una sentencia sql
		$usuario->consulta_sql = "create tabla xxx(aaa)";
		$usuario->parametros->add("parametro1")->value="as";
		$nro_registros = $usuario->execute_sql();
		if ($tabla_sql == -1)
		{
			echo "Error en ejecutar sentencia_sql: ".$usuario->cod_error." - ".$usuario->mensaje;
		}
		
		//ejecutando una sentencia sql
		$nro_registros = $usuario->execute_sql("create tabla xxx");
		if ($tabla_sql == -1)
		{
			echo "Error en ejecutar sentencia_sql: ".$usuario->cod_error." - ".$usuario->mensaje;
		}*/
		
		
		//llamando a url por POST
		
		// abrimos la sesión cURL
		$ch = curl_init();

		// definimos la URL a la que hacemos la petición
		curl_setopt($ch, CURLOPT_URL,"http://lxccancerapps.casosnegocio.surco.xyz/clogin_rest/validar/");
		// indicamos el tipo de petición: POST
		curl_setopt($ch, CURLOPT_POST, TRUE);
		// definimos cada uno de los parámetros
		curl_setopt($ch, CURLOPT_POSTFIELDS, "param1=value1&param2=value2&param3=value3");

		// recibimos la respuesta y la guardamos en una variable
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$remote_server_output = curl_exec ($ch);

		// cerramos la sesión cURL
		curl_close ($ch);

		// hacemos lo que queramos con los datos recibidos
		// por ejemplo, los mostramos
		print_r($remote_server_output);

	}
	
	public function generar_excel()
	{
				/*
		$helper = new Sample();
		if ($helper->isCli()) {
			$helper->log('This example should only be run from a Web Browser' . PHP_EOL);

			return;
		}
*/
		// Create new Spreadsheet object
		$spreadsheet = new Spreadsheet();

		// Set document properties
		$spreadsheet->getProperties()->setCreator('Maarten Balliauw')
			->setLastModifiedBy('Maarten Balliauw')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
			->setKeywords('office 2007 openxml php')
			->setCategory('Test result file');

		// Add some data
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Hello')
			->setCellValue('B2', 'world!')
			->setCellValue('C1', 'Hello')
			->setCellValue('D2', 'world!');

		// Miscellaneous glyphs, UTF-8
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A4', 'Miscellaneous glyphs')
			->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Simple');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Xls)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="01simple.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($spreadsheet, 'Xls');
		$writer->save('php://output');
		exit;


	}

	
}


