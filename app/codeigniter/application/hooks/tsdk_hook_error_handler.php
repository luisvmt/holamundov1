<?php

//definiendo constantes
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");

//----------------------------------------------------------------------
//Esta clase es llamada antes de iniciar un controlador.
//Se utiliza principalmente para establecer el manejador de errores
//----------------------------------------------------------------------

class tsdk_hook_error_handler {

    
    /**
     * This function used to block the every request except allowed ip address
     */
    function establecer_error_handler(){
        
        //if($_SERVER["REMOTE_ADDR"] != "49.248.51.230"){
        //    echo "not allowed";
        //    die;
        //}


        //estableciendo gestor de errores
        if ( strcmp(APP_DEBUG_CONTROLADOR,"web")!=0 )
        {
            $gestor_errores_antiguo = set_error_handler(APP_DEBUG_CONTROLADOR);
        }
    }
}
?>