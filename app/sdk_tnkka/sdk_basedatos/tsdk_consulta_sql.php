<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package base_datos
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/tsdk_resultset.php");

//-----------------------------------------------------------
/**
* Este objeto permite crear una consulta SQL
*
* Implementa la clase que representa una consulta SQL
*
*/

class tsdk_consulta_sql extends tsdk_resultset
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa la consulta sql
    * @var string
    */
    protected   $m_consulta_sql="";

    /**
    * Es el arreglo que componen el componente SELECT
    * @var array
    */
    protected   $m_select=array();
 
    /**
    * Es el arreglo que componen el componente FROM
    * @var array
    */
    protected   $m_from = array();

    /**
    * Es el arreglo que componen el componente WHERE
    * @var array
    */
    protected   $m_where = array();

    /**
    * Es el texto que representa el componente WHERE
    * @var string
    */
    protected   $m_where_text = "";

    /**
    * Es el arreglo que componen el componente ORDER BY
    * @var array
    */
    protected   $m_orderby = array();

    /**
    * Es el arreglo que componen el filtro a aplicar a la consulta
    * @var array
    */
    protected   $m_campos_filtro = array();

    /**
    * Es el texto que busca el filtro
    * @var string
    */
    protected   $m_filtro_a_buscar = "";

    /**
    * Es el numero de pagina a ubicar
    * @var integer
    */
    protected   $m_numero_pagina = 0;

    /**
    * Es el tamaño de pagina
    * @var integer
    */
    protected   $m_tamano_pagina = 0;
    
    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $basedatos          Se refiere al objeto base de datos
    * @example "sample_consulta_sql.construct.txt"
    */
    function __construct($basedatos,$manejadorbd)
    {
        //iniciando variables
        parent::__construct($basedatos,$manejadorbd);

        $this->m_where_text = "";
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>select</b>       :   Devuelve el arreglo de campos de SELECT</br>
    *    <b>from</b>        :   Devuelve el arreglo de campos de FROM</br>
    *    <b>where</b>          :   Devuelve el arreglo de campos de WHERE</br>
    *    <b>where_text</b>       :   Devuelve el texto de WHERE</br>
    *    <b>order_by</b>             :   Devuelve el arreglo de campos de ORDER BY</br>
    *    <b>campos_filtro</b>           :   Devuelve el arreglo de los campos que componen el filtro</br>
    *    <b>filtro_a_buscar</b>      :   Devuelve texto que se busca en el filtro</br>
    *    <b>numero_pagina</b>      :   Devuelve el numero de página de la consulta</br>
    *    <b>tamano_pagina</b>      :   Devuelve la tamaño de la pagina</br>
    * @example "sample_tsdk_consulta_sql.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "select":          
            return $this->m_select;
            break;
        case "from":
            return $this->m_from;
            break;
        case "where": 
            return $this->m_where;
            break;
        case "where_text":
            return $this->m_where_text;
            break;
        case "order_by":
            return $this->m_orderby;
            break;
        case "campos_filtro":
            return $this->m_campos_filtro;
            break;
        case "filtro_a_buscar":
            return $this->m_filtro_a_buscar;
            break;
        case "numero_pagina":
            return $this->m_numero_pagina;
            break;
        case "tamano_pagina":
            return $this->m_tamano_pagina;
            break;
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>select</b>       :   Establece el arreglo de campos de SELECT</br>
    *    <b>from</b>        :   Establece el arreglo de campos de FROM</br>
    *    <b>where</b>          :   Establece el arreglo de campos de WHERE</br>
    *    <b>where_text</b>       :   Establece el texto de WHERE</br>
    *    <b>order_by</b>             :   Establece el arreglo de campos de ORDER BY</br>
    *    <b>campos_filtro</b>           :   Establece el arreglo de los campos que componen el filtro</br>
    *    <b>filtro_a_buscar</b>      :   Establece texto que se busca en el filtro</br>
    *    <b>numero_pagina</b>      :   Establece el numero de página de la consulta</br>
    *    <b>tamano_pagina</b>      :   Establece la tamaño de la pagina</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_consulta_sql.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "numero_pagina":
            $this->m_numero_pagina = $value;
            break;
        case "tamano_pagina":
            $this->m_tamano_pagina = $value;
            break;
        case "select":
            if (!is_array($value))
            {
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x Error en set. Propiedad:".$propiedad.
                    ", Value: ".$value.
                    ", La propiedad no es un arreglo.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                    );
                return;
            }

            $this->m_select = $value;
            break;

        case "from":
            if (!is_array($value))
            {
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x Error en set. Propiedad:".$propiedad.
                    ", Value: ".$value.
                    ", La propiedad no es un arreglo.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                    );

                return;
            }

            $this->m_from = $value;
            break;

        case "where":
            if (!is_array($value))
            {
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x Error en set. Propiedad:".$propiedad.
                    ", Value: ".$value.
                    ", La propiedad no es un arreglo.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                    );
                return;
            }

            $this->m_where = $value;
            $this->m_where_text = "";
            break;

        case "where_text":
            $this->where = array();
            $this->m_where_text = $value;
            break;

        case "order_by":
            if (!is_array($value))
            {
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x Error en set. Propiedad:".$propiedad.
                    ", Value: ".$value.
                    ", La propiedad no es un arreglo.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                    );
                return;
            }

            $this->m_orderby = $value;
            break;

        case "campos_filtro":
            if (!is_array($value))
            {
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x Error en set. Propiedad:".$propiedad.
                    ", Value: ".$value.
                    ", La propiedad no es un arreglo.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                    );
                return;
            }

            $this->m_campos_filtro = $value;
            break;

        case "filtro_a_buscar":
            $this->m_filtro_a_buscar = $value;
            break;

        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Devuelve la consulta SQL paginada
	* @access public
    * @return       string      Consulta SQL (paginada)
    * @example "sample_tsdk_consulta_sql.php"
    */
    public function get_sentencia_sql_paginada()
    {
        //validando campos de select
        if (!is_array($this->m_select))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x Error en propiedad select".
                ", La propiedad no es un arreglo.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
                
            die();
            return "";
        }

        //validando campos de select
        if (count($this->m_select) == 0)
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x No ha definido los campos de SELECT.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
                
            die();
            return "";
        }

        //validando campos de from
        if (!is_array($this->m_from))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x Error en propiedad from".
                ", La propiedad no es un arreglo.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
            die();
            return "";
        }

        //validando campos de from
        if (count($this->m_from) == 0)
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x No ha definido los campos de FROM.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
            die();
            return "";
        }

        //creando el filtro
        $filtro = $this->get_sql_filtro($this->m_manejadorbd,"", $this->m_campos_filtro, $this->m_filtro_a_buscar);

        //construyendo la consulta (select)
        $consulta = "select ";

        foreach ($this->m_select as $clave=>$valor)
        {
            $consulta .= $valor;
            $consulta .= ", ";
        }
        $consulta = substr($consulta,0,strlen($consulta)-2); //quitando la ultima coma
    
        //construyendo la consulta (from)
        $consulta .=" from ";

        foreach ($this->m_from as $clave=>$valor)
        {
            $consulta .= $valor;
            $consulta .= ", ";
        }
        $consulta = substr($consulta,0,strlen($consulta)-2); //quitando la ultima coma

        //construyendo la consulta (where)
        if ($this->m_where_text != "" || count($this->m_where)>0 )
        {
            //construyendo la consulta (where) 
            $consulta .=" where ";

            if ($this->m_where_text == "")
            {
                foreach ($this->m_where as $clave=>$valor)
                {
                    $consulta .= $valor;
                    $consulta .= " and ";
                }

                if ( count($this->m_where)>0 )
                {
                    $consulta = substr($consulta,0,strlen($consulta)-5); //quitando el ultimo and
                    $consulta .= ($filtro=="")?"":" and ".$filtro; //añadiendo el filtro

                }else{
                    $consulta .= ($filtro=="")?"":$filtro;
                }           
            }else{
                $consulta .= $this->m_where_text;
                $consulta .= ($filtro=="")?"":" and ".$filtro;
            }
        }

        //verificando si where tiene campos
        //if ( substr($consulta, strlen($consulta)-8) == " where ")
        //{
        //    $consulta = $substr($consulta,0,strlen($consulta)-7);
        //}

        //construyendo la consulta (order by)
        if ( count($this->m_orderby)>0 )
        {
            $consulta .=" order by ";
        
            foreach ($this->m_orderby as $valor)
            {
                //obteniendo el numero de columna
                /*list($nrocolumna,$orden) = explode(" ",$valor);

                $consulta .= ($nrocolumna+1);  //el indice empieza en 1
                $consulta .= " ".$orden;
                $consulta .= ", ";*/

                $consulta .= $valor;
                $consulta .= ", ";
            }
            $consulta = substr($consulta,0,strlen($consulta)-2); //quitando la ultima coma
        }
        
        //sentencia para paginacion
        if ($this->m_tamano_pagina>0)
        {
            $offset = $this->m_tamano_pagina * ($this->m_numero_pagina - 1);
//error_log("xxx tamanopagina:".$this->m_tamano_pagina." numeropagina:".$this->m_numero_pagina);

            $consulta .= " LIMIT ".$this->m_tamano_pagina;
            $consulta .= " OFFSET ".$offset;
        }
        
        //devolviendo la consulta
        return $consulta;
    }


    //-----------------------------------------------------------
    /**
    * Devuelve la consulta SQL para contar las filas
	* @access public
    * @return       string      Consulta SQL para contar filas
    * @example "sample_tsdk_consulta_sql.php"
    */
    public function get_sentencia_sql_contador()
    {
        //validando campos de select
        if (!is_array($this->m_select))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x Error en propiedad select".
                ", La propiedad no es un arreglo.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
                
            die();
            return "";
        }

        //validando campos de select
        if (count($this->m_select) == 0)
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x No ha definido los campos de SELECT.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
                
            die();
            return "";
        }

        //validando campos de from
        if (!is_array($this->m_from))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x Error en propiedad from".
                ", La propiedad no es un arreglo.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
            die();
            return "";
        }

        //validando campos de from
        if (count($this->m_from) == 0)
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                "#x No ha definido los campos de FROM.".
                ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                "\n"
                );
            die();
            return "";
        }

        //creando filtro
        $filtro = $this->get_sql_filtro($this->m_manejadorbd,"", $this->m_campos_filtro, $this->m_filtro_a_buscar);

        //construyendo la consulta (select)
        $consulta = "select count(*) as contador";
        
        //construyendo la consulta (from)
        $consulta .=" from ";

        foreach ($this->m_from as $clave=>$valor)
        {
            $consulta .= $valor;
            $consulta .= ", ";
        }
        $consulta = substr($consulta,0,strlen($consulta)-2); //quitando la ultima coma

        //construyendo la consulta (where)
        if ($this->m_where_text != "" || count($this->m_where)>0 )
        {
            $consulta .=" where ";

            if ($this->m_where_text == "")
            {
                foreach ($this->m_where as $clave=>$valor)
                {
                    $consulta .= $valor;
                    $consulta .= " and ";
                }
    
                if ( count($this->m_where)>0 )
                {
                    $consulta = substr($consulta,0,strlen($consulta)-5); //quitando el ultimo and
                    $consulta .= ($filtro=="")?"":" and ".$filtro; //añadiendo el filtro
    
                }else{
                    $consulta .= ($filtro=="")?"":$filtro;
                }           
            }else{
                $consulta .= $this->m_where_text;
                $consulta .= ($filtro=="")?"":" and ".$filtro;
            }        
        }
        

        //verificando si where tiene campos
        //if ( substr($consulta, strlen($consulta)-8) == " where ")
        //{
        //    $consulta = $substr($consulta,0,strlen($consulta)-7);
        //}
        
        //devolviendo la consulta
        return $consulta;
    }
}

?>
