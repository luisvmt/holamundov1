<?php 
/**
 * Este archivo implementa la clase de un objeto Recordset
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package base_datos
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//-----------------------------------------------------------
/**
* Objecto tipodato 
*
* Implementa una clase que permite determinar el tipo de dato de una variable
*/

class tsdk_tipodato
{

    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Representa tipo de dato: string
    * @var integer
    */
    const	tstring=0;
    

    /**
    * Representa tipo de dato: string no vacio (al menos un caracter)
    * @var integer
    */
	const	tstring_no_vacio=2;

    /**
    * Representa tipo de dato: integer
    * @var integer
    */
	const	tinteger=3;

    /**
    * Representa tipo de dato: integer
    * @var integer
    */
	const	tinteger_mayor_cero=4;

    /**
    * Representa tipo de dato: integer
    * @var integer
    */
	const	tinteger_mayor_igual_cero=5;

    /**
    * Representa tipo de dato: double
    * @var integer
    */
	const	tdouble=6;

    /**
    * Representa tipo de dato: double
    * @var integer
    */
	const	tdouble_mayor_cero=7;

    /**
    * Representa tipo de dato: double
    * @var integer
    */
	const	tdouble_mayor_igual_cero=8;

    /**
    * Representa tipo de dato: string (correo electronico)
    * @var integer
    */
	const	temail=9;

    /**
    * Representa tipo de dato: string (correo electronico)
    * @var integer
    */
	const	temail_o_vacio=10;

    /**
    * Representa tipo de dato: boolean
    * @var integer
    */
	const	tboolean=11;

    /**#@-*/
	
		
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    */
	function __construct()
	{

	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
	function __destruct()
	{

	}

}


