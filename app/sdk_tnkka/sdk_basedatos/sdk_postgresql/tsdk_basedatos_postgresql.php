<?php   
/** 
 * Este archivo implementa la clase tsdk_basedatos_postgresql
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package base_datos
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

//-----------------------------------------------------------
/**
* Base de datos POSTGRESQL
*
* Administra la conexion a una base de datos POSTGRESQL.
*
* Esta clase implementa la conexion a una base de datos,
* para la ejecucion de sentencias se apoya en la clase tsdk_resultset.
*/
 
class tsdk_basedatos_postgresql
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access private
    */

    /**
    * usuario de conexion a base de datos
    * @var string
    */
    private     $m_usuario;

    /**
    * contraseña para conexion a base de datos
    * @var string
    */
    private     $m_contrasena;

    /**
    * servidor donde se encuentra alojado la base de datos
    * @var string
    */
    private     $m_servidor;

    /**
    * nombre de la base de datos
    * @var string
    */
    private     $m_basedatos;

    /**
    * puerto TCP donde escucha la base de datos
    * @var string
    */    
    private     $m_puerto;

    /**
    * nombre del esquema que se utiliza en la base de datos
    * @var string
    */    
    private     $m_esquema;

    /**#@-*/

    //-----------------------------------------------------------

    /**#@+
    * @access protected
    */

    /**
    * mensaje de error o exito de la clase
    * @var string
    */
    protected   $m_mensaje;

    /**
    * conexion a una base de datos
    * @var string
    */
    protected   $m_conexion;

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /** 
    * Implementa el constructor de la clase
    *
    */
    function __construct()
    {
        $this->m_conexion = null;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    	//error_log("destruyendo la bd");
        $this->m_conexion = null;
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    mensaje    :   Mensaje de la clase </br>
    *    conexion   :   Conexion de base de datos
    *
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "mensaje":
            return $this->m_mensaje;
            break;
        case "conexion":
            return $this->m_conexion;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en get. ".
                 "Propiedad no encontrada: ".$propiedad.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificación de las propiedades de la clase
    *
    * @param string $propiedad      La propiedad a establecer. 
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    mensaje    :   Mensaje de la clase </br>
    *    conexion   :   Conexion de base de datos
    *
    * @param string $value          El valor a establecer
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "mensaje":
            $this->m_mensaje = $value;
            break;
        case "conexion":
            $this->m_conexion = $value;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en set. ".
                 "Propiedad no encontrada: ".$propiedad.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Implementa la conexion a la base de datos
    *
    * @param  string $usuariobd     usuario de conexion a la base de datos
    * @param  string $contrasena    contraseña de conexion
    * @param  string $servidor      IP o nombre del servidor de base de datos
    * @param  string $basedatos     Nombre de la base de datos
    * @param  int    $puerto        Puerto TCP donde escucha la base de datos
    * @param  boolean    $is_persistent        Indica si se abre una conexion persistente
    * @return bool                  Devuelve true si la conexion es OK. False si ocurrio un error
    * @example "sample_tsdk_basedatos_mysql.conectar.txt"
    */
    public function conectar($usuariobd,$contrasena,$servidor,$basedatos,$puerto,$esquema,$is_persistent)
    {
        $respuesta = true;

        $this->m_usuario=$usuariobd;
        $this->m_contrasena=$contrasena;
        $this->m_servidor=$servidor;
        $this->m_basedatos=$basedatos;
        $this->m_puerto=$puerto;
        $this->m_esquema=$esquema;
        $this->m_mensaje=""; 

        //abriendo la base de datos
        try
        {
            //conexion a postgresql
            if ($is_persistent)
            {
	            $this->m_conexion = new PDO("pgsql:dbname=".$this->m_basedatos.";host=".$this->m_servidor.";port=".$this->m_puerto.";options='-c client_encoding=utf8'",
					    					$this->m_usuario,$this->m_contrasena, array(PDO::ATTR_PERSISTENT => TRUE)
                                            );
	        }else{
				$this->m_conexion = new PDO("pgsql:dbname=".$this->m_basedatos.";host=".$this->m_servidor.";port=".$this->m_puerto.";options='-c client_encoding=utf8'",
                                            $this->m_usuario,$this->m_contrasena
                                            );
	        }

            //estableciendo modo de error (mysql y postgresql)
            $this->m_conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //estableciendo esquema por defecto (postgresql)
            $consulta = "SET search_path TO ".$this->m_esquema;
            $tabla=$this->m_conexion->prepare($consulta);
    		$tabla->execute();
   		
            //estableciendo el encoding del cliente (mysql - opcional)
            //$consulta = "SET NAMES 'utf8'";
            //$tabla=$this->m_conexion->prepare($consulta);
            //$tabla->execute();


        }catch(PDOException $e)
        {
            $this->m_conexion=null;

            $this->m_mensaje = $e->getMessage();
            $lineaerror = $e->getMessage()."\n".
                          "      Codigo  : ".$e->getCode()."\n".
                          "      Archivo : ".$e->getFile()."\n".
                          "      Linea   : ".$e->getLine()."\n";
            error_log($lineaerror);
            $respuesta = false;
        }

        return $respuesta;
    }

}

?>
