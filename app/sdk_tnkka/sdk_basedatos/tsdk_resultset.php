<?php 
/**
 * Este archivo implementa la clase de un objeto Resultset
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package base_datos
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");
include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/tsdk_datos_sql.php");

//-----------------------------------------------------------
/**
* Objecto resultset 
*
* Implementa una clase que permite interactuar con la base de datos
*
* @author luisvmt@hotmail.com
* @package base_datos
*/

class tsdk_resultset
{
    
    // **********************************
	// variables miembro
    // **********************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa la base de datos
    * @var object
    */
 	protected $m_basedatos=null;        //indica la base de datos
    
    /**
    * Representa el manejador de base de datos (mysql o postgresql)
    * @var object
    */
 	protected $m_manejadorbd=null;        //indica el manejador de base de datos
    
    /**
    * Representa el mensaje a interactuar
    * @var string
    */
    protected $m_mensaje="";
    
    /**
    * Guarda el valor de SQLSTATE
    * @var integer
    */
    //protected $m_sqlstate;   //guarda el SQLSTATE de la consulta
    
    /**
    * Guarda el codigo de error de la consulta
    * @var integer
    */
    protected $m_codigoerror; //guarda el codigo de error de la ejecucion de la consulta

    /**
    * Guarda la lista de items que componen el recordset
    * @var array
    */
    //protected $m_listaitems;    //contiene la lista de items de la tabla (registros)
    
    /**
    * Guarda la consulta sql
    * @var string
    */
    protected $m_consulta_sql="";

    /**
    * Guarda el nombre de la tabla donde se insertara, modificara, eliminara
    * @var string
    */
    protected $m_nombretabla="";
	
    /**
    * Guarda el nombre de la vista donde se consultara los datos existentes
    * @var string
    */
    protected $m_nombrevista="";

    /**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de estilos
    * @var object tsdk_coleccion
    */
	public	$campos=null;

    /**
    * Representa la coleccion de estilos
    * @var object tsdk_coleccion
    */ 
	public	$parametros=null;
	
    /**#@-*/
    
    // **********************************
    // metodos - privados y protegidos
    // **********************************

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta INSERTAR clasica (solo para postgresql).
    * Utiliza los campos para su contruccion. 
    * 
    * Los parametros de la funcion son:
    * 
    * $campoautoincrement = "" 
    * : Texto que indica el campo autoincrement, este no se incluye en la consulta.
    * 
    * $campos_as_basedatos = array()
    * : Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones.
    * 
	* @access public
    * @return   integer   Devuelve el numero de filas afectadas. Devuelve -1 en caso de error.
    * @example "sample_tsdk_resultset.php"
    */
    protected function insertar_postgresql()
    {
        //obteniendo los parametros
        $parametros = func_get_args();
        $campoautoincrement = isset($parametros['0'])?$parametros['0']:"";
        $campos_as_basedatos = isset($parametros['1'])?$parametros['1']:array();

        //Inicio de funcion
        /*$arreglo=array();

        //creando arreglo y validando los campos
        foreach ($this->m_listaitems as $key => $objeto)
        {
            if ( $objeto->iscolumn && $key != $campoautoincrement && !in_array($key,$campos_no_writables,true)
                 && !in_array($key,$campos_as_basedatos,true) )
            {
                $arreglo[]=$key;
            }
        }*/

        //$respuesta=$this->validar_variables($arreglo);
        //if ($respuesta<0)
        //{
        //    return $respuesta;
        //}

        $respuesta = 0; //indica un error, o el numero de filas afectadas

        //llamando a SQL
        $consulta = $this->get_sql_insert($campoautoincrement, $campos_as_basedatos);
        
        if ($campoautoincrement == "") {
            //ejecutando la sentencia
            $respuesta=$this->execute_sql($consulta); //--> esta funcion no actualiza ningun campo, tampoco devuelve ningun autoincrement, solo devuelve el  numero de filas afectadas
            if ($respuesta<0)
            {
                return $respuesta;
            }
            return $respuesta;

        }else{ 
            
            //obteniendo el valor de autoincrement
            //La siguiente sentencia solo funciona para mysql
            //$this->campos->add($campoautoincrement)->value = $this->m_basedatos->conexion->lastInsertId();
            
            //obteniendo la respuesta, para postgresql
            $tabladatos = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
            if ( is_null($tabladatos) )
            {
                return -1;
            }

            $respuesta = $tabladatos->filas->count();
            if ($respuesta == 0) //no existe datos devueltos (no devolvio el campo autoincrement)
            {
                //----------------------------------------------
                //Imprimiendo el error
                //----------------------------------------------
                $this->m_mensaje = $e->getMessage()." - No devolvio el campo autoincrement";
                $this->m_codigoerror = $e->getCode();
                $lineaerror = $e->getMessage()."\n".
                            "      Codigo  : ".$e->getCode()."\n".
                            "      Archivo : ".$e->getFile()."\n".
                            "      Linea   : ".$e->getLine()."\n".
                            "      SQL     : ".$consulta."\n";

                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x ".$lineaerror.
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );

                return 0;
            }
                
            //recuperando los campos
            foreach ($tabladatos->filas as $clave => $fila)
            {
                $this->campos->add($campoautoincrement)->value = $fila->item($campoautoincrement)->value;
                //$i++; //si hubiera mas filas, deberia existir solo una fila
            }

            //devolviendo el json
            //$this->response($respuesta_rest->get_array());
            return $respuesta;
        }
        
        return $respuesta;
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta INSERTAR clasica (solo para mysql).
    * Utiliza los campos para su contruccion. 
    * 
    * Los parametros de la funcion son:
    * 
    * $campoautoincrement = "" 
    * : Texto que indica el campo autoincrement, este no se incluye en la consulta.
    * 
    * $campos_as_basedatos = array()
    * : Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones.
    * 
	* @access public
    * @return   integer   Devuelve el numero de filas afectadas. Devuelve -1 en caso de error.
    * @example "sample_tsdk_resultset.php"
    */
    protected function insertar_mysql()
    {
        //obteniendo los parametros
        $parametros = func_get_args();
        $campoautoincrement = isset($parametros['0'])?$parametros['0']:"";
        $campos_as_basedatos = isset($parametros['1'])?$parametros['1']:array();

        //Inicio de funcion
        /*$arreglo=array();

        //creando arreglo y validando los campos
        foreach ($this->m_listaitems as $key => $objeto)
        {
            if ( $objeto->iscolumn && $key != $campoautoincrement && !in_array($key,$campos_no_writables,true)
                 && !in_array($key,$campos_as_basedatos,true) )
            {
                $arreglo[]=$key;
            }
        }*/

        //$respuesta=$this->validar_variables($arreglo);
        //if ($respuesta<0)
        //{
        //    return $respuesta;
        //}

        //llamando a SQL
        $consulta = $this->get_sql_insert($campoautoincrement, $campos_as_basedatos);
		
        //solo para mysql
        $respuesta=$this->execute_sql($consulta); //--> esta funcion no actualiza ningun campo
        if ($respuesta<0)
        {
            return $respuesta;
        }

        //obteniendo el autoincrement
        if ($campoautoincrement!="")
        {
			$this->campos->add($campoautoincrement)->value = $this->m_basedatos->conexion->lastInsertId();
        }

        return $respuesta;
    }
    
	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $basedatos          Se refiere al objeto base de datos
    *
    */
    public function __construct($basedatos,$manejadorbd)
    {
        $this->m_basedatos = $basedatos;
        $this->m_manejadorbd = $manejadorbd;
        
		//iniciando la lista de campos
		$this->campos = new tsdk_coleccion();
		
		//iniciando la lista de parametros
		$this->parametros = new tsdk_coleccion();
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    public function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>basedatos</b>       :   Devuelve la base de datos</br>
    *    <b>conexion</b>        :   Devuelve la conexion a la base de datos</br>
    *    <b>sqlstate</b>        :   Devuelve el valor de SQLSTATE</br>
    *    <b>codigoerror</b>     :   Devuelve el codigo de error de la consulta</br>
    *    <b>nombretabla</b>     :   Devuelve el nombre de la tabla</br>
    *    <b>mensaje</b>         :   Devuelve el mensaje de texto respectivo</br>
    * 
    * @example "sample_tsdk_resultset.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "basedatos":
            return $this->m_basedatos;
            break;
        case "mensaje":
            return $this->m_mensaje;
            break;
        case "consulta_sql":
            return $this->m_consulta_sql;
            break;
		case "codigo_error":
            return $this->m_codigoerror;
            break;
		case "nombre_tabla":
            return $this->m_nombretabla;
            break;
        case "nombre_vista":
                return $this->m_nombrevista;
                break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get. Propiedad no encontrada: ".$propiedad.
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nombretabla</b>     :   Devuelve el nombre de la tabla</br>
    *    <b>mensaje</b>         :   Devuelve el mensaje de texto respectivo</br>
    * 
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_resultset.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "consulta_sql":
            $this->m_consulta_sql=$value;
			break;
        case "mensaje":
            $this->m_mensaje=$value;
            break;
        case "nombre_tabla":
            $this->m_nombretabla=$value;
            break;
        case "nombre_vista":
            $this->m_nombrevista=$value;
            break;
        default:
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en set. Propiedad no encontrada: ".$propiedad.
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion devuelve la porcion de la consulta utilizada para el filtro
	* @access public 
	* @param    string      $prefijo 					Indica el prefijo de los campos
    * @param   	array 		$campos_filtro 				Arreglo con la lista de campos que intervienen en el filtro
    * @param   	array 		$filtro 			 		Texto a buscar en el filtro.
    * @return   string 		 							Retorna la consulta solicitada
    * @example "sample_tsdk_resultset.php"
    */
    public static function get_sql_filtro($manejadorbd,$prefijo="",$campos_filtro, $filtro)
    {
        $comando ="";
        $texto_campos="";

        //obteniendo la lista de los campos a concatenar
        if ( is_array($campos_filtro) )
        {
            $texto_campos="concat(";  //--> utilizado para mysql
            foreach ($campos_filtro as $nombre_campo)
            {
                if ($prefijo != "")
                {
                    $texto_campos .= $prefijo.".";
                }
                $texto_campos .= trim($nombre_campo).",' ',";  //--> utilizado para mysql
                //$texto_campos .= trim($nombre_campo)." || "; //--> utilizado para postgresql
            }

            //-- quitando el ultimo espacio en blanco y coma
            $texto_campos = substr($texto_campos,0,strlen($texto_campos)-5); //--> utilizado en mysql

            //-- quitando el ultimo ||
            //$texto_campos = substr($texto_campos,0,strlen($texto_campos)-4);  //--> utilizado en postgresql

            $texto_campos .= ")";  //--> utilizado en mysql

            //construyendo el filtro
            $arreglo_filtro = explode(" ",$filtro);

            //print_r($arreglo_filtro);

            if ( count($arreglo_filtro)>0 )
            {
                foreach ($arreglo_filtro as $xfiltro)
                {
                    $xfiltro = trim($xfiltro);
                    //replace('abcdefabcdef', 'cd', 'XYZ')

                    if ($xfiltro != "")
                    {
                        switch ( $manejadorbd ){
                            case "mysql":
                                //utilizando por mysql, utilizando charset=utf8 y collation=utf8_general_ci
                                $comando .= $texto_campos." like '%".$xfiltro."%' and ";
                                break;
                            case "postgresql":
                                //utilizando por postgresql
                                $comando .= "translate(lower(".$texto_campos."),'ñáéíóú','naeiou') like translate(lower('%".$xfiltro."%'),'ñáéíóú','naeiou') and ";
                                break;
                            default:
                                $lineaerror = "Manejador de base de datos [ ".$manejadorbd." ] no reconocido.\n";
                                //error_log($lineaerror);

                                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                                    "#x ".$lineaerror.
                                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                                        "\n"
                                    );

                                return "";                                
                                break;
                        }
                        
                    }
                }

                //quitando el ultimo and
                $comando = substr($comando,0,strlen($comando)-5);
            }
        }
        //echo "comando:".$comando;
        return $comando;
    }

    //-----------------------------------------------------------
    /**
    * Reemplaza campos en la consulta 
	* @access private
    * @param   	string 		$consulta 		Consulta SQL donde buscar
    * @return   string 						Consulta modificada
    * @example "sample_tsdk_resultset.php"
    */
    private function reemplazar_campos($consulta)
    {
        $salida=$consulta;

        //reemplazando los valores
		foreach ($this->campos as $key => $objeto)
        {
            $textobuscado="#".$key."#";
            $salida = str_ireplace($textobuscado, $objeto->value, $salida);
        }

        //error_log("comando-sql:".$salida);
        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Reemplaza parametros en la consulta 
	* @access private
    * @param   	string 		$consulta 		Consulta SQL donde buscar
    * @return   string 						Consulta modificada
    * @example "sample_tsdk_resultset.php"
    */
    private function reemplazar_parametros($consulta)
    {
        $salida=$consulta;

        //reemplazando los valores
		foreach ($this->parametros as $key => $objeto)
        {
            $textobuscado="#".$key."#";

			$salida = str_ireplace($textobuscado, $objeto->value, $salida);
        }

        //error_log("comando-sql:".$salida);
        return $salida;
    }
	
    //-----------------------------------------------------------
    /**
    * Busca las variables a reemplazar
	* @access public
    * @param   	string 		$consulta 		Consulta SQL donde buscar las variables
    * @param   	object 		$tabla 			Objeto tabla
    * @return   integer 					Retorna -1 en caso de error, retorna 0 si es OK
    * @example "sample_tsdk_resultset.php"
    */
    private function bind_campos_parametros($consulta,$tabla)
    {
        //reemplazando los valores
        try
        {
            //buscando si la clave existe, eliminando subcadenas con la misma clave
            $consulta_buscar=$consulta." ";
//tsdk_error_log_trace("campos",$this->campos);

			foreach ($this->campos as $key => $objeto)
            {
                $texto = ":".$key;
                $texto1 = ":".$key.",";
                $texto2 = ":".$key.")";
                $texto3 = ":".$key." ";

                if ( strpos($consulta_buscar,$texto1)!=false || strpos($consulta_buscar,$texto2)!=false ||
                     strpos($consulta_buscar,$texto3)!=false )
                {

                    //error_log("texto:".$texto."\n");
                    //error_log("valor:".$objeto->value."\n\n");

                    //$tabla->bindParam($texto,$objeto->value);
                    $tabla->bindValue($texto,$objeto->value);

                }
			}
			
			foreach ($this->parametros as $key => $objeto)
            {
                $texto = ":".$key;
                $texto1 = ":".$key.",";
                $texto2 = ":".$key.")";
                $texto3 = ":".$key." ";

                if ( strpos($consulta_buscar,$texto1)!=false || strpos($consulta_buscar,$texto2)!=false ||
                     strpos($consulta_buscar,$texto3)!=false )
                {
                    //$tabla->bindParam($texto,$objeto->value);
                    $tabla->bindValue($texto,$objeto->value);

                    //error_log("texto:".$texto."\n");
                    //error_log("valor:".$objeto->value."\n\n");
                }
            }
			
        }catch(PDOException $e)
        {
            $this->mensaje = $e->getMessage();
            $this->m_codigoerror = $e->getCode();
            $lineaerror = $e->getMessage()."\n".
                          "      Codigo  : ".$e->getCode()."\n".
                          "      Archivo : ".$e->getFile()."\n".
                          "      Linea   : ".$e->getLine()."\n".
                          "      SQL     : ".$consulta."\n";
            //error_log($lineaerror);

            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                 "#x ".$lineaerror.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			
            return -1;
        }
        return 0;
    }
	
    //-----------------------------------------------------------
    /**
    * Ejecuta una consulta SQL, solo devuelve el numero de filas afectadas
	* @access public
    * @param   	string 		$consulta 		Consulta SQL a ejecutar
    * @return   integer 					Nro de filas afectadas
    * @example "sample_tsdk_resultset.php"
    */
    public function execute_sql($consulta)
    {
        if ($this->m_basedatos == null)
        {
            $this->m_mensaje="execute_sql: basedatos no establecida";

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x ".$this->mensaje.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );

            return -1;
        }

        //reemplazando los valores de los campos
        $consulta=$this->reemplazar_campos($consulta);
        $consulta=$this->reemplazar_parametros($consulta);

        $tabla=$this->m_basedatos->conexion->prepare($consulta);

		if ($this->bind_campos_parametros($consulta,$tabla)<0)
        {
            $this->m_mensaje="execute_sql:".$this->m_mensaje;

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x ".$this->m_mensaje.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			
            return -1;
        }

        try
        {
            if ($tabla->execute()==false)
            {
                $error=$tabla->errorInfo();
                $this->m_mensaje="SQLSTATE: ".$error[0];
                $this->m_mensaje.=", CODE: ".$error[1];
                $this->m_mensaje.=", MESSAGE: ".$error[2];

                $this->m_sqlstate=$error[0];

				tsdk_error_log(tsdk_propiedades::$TRACELOG,
	            	 "#x ".$this->m_mensaje.
	                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
	                    "\n"
	                );
				
                unset($tabla);
                return -1;
            }

            $nrofilas = $tabla->rowCount();
        }catch(PDOException $e)
        {
            $this->m_mensaje = $e->getMessage();
            $this->m_codigoerror = $e->getCode();
            $lineaerror = $e->getMessage()."\n".
                          "      Codigo  : ".$e->getCode()."\n".
                          "      Archivo : ".$e->getFile()."\n".
                          "      Linea   : ".$e->getLine()."\n".
                          "      SQL     : ".$consulta."\n";

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x ".$lineaerror.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			
            return -1;
        }
        unset($tabla);

        return $nrofilas;
    }

    //-----------------------------------------------------------
    /**
    * Recupera los valores de una consulta y devuelve una tabla con los valores solicitados
	* @access public
    * @param   	string 		$consulta 		Consulta SQL a ejecutar. En caso no pasar este parametro, se utiliza la variable m_consulta_sql de la clase.
    * @return   tsdk_datos_sql		        Retorna un objeto(tsdk_datos_sql) con los datos solicitados, retorna null en caso de error.
    * @example "sample_tsdk_resultset_obtener_tabla_from_sql.txt"
    */
    public function obtener_tabla_from_sql($consulta="")
    {
        //$arreglo=array();  //tiene la lista de filas
        //$fila=array();  //tiene los datos de una fila
		
		if ($consulta=="") //si no se pasa por parametro, se utiliza la variable consulta_sql
		{
			$consulta = $this->m_consulta_sql;
		}
		
		$datos = new tsdk_datos_sql();

        //validando la conexion
        if ($this->m_basedatos == null)
        {
            $this->m_mensaje="obtener_tabla_from_sql: basedatos no establecida";

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x ".$this->m_mensaje.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			
            return null;
        }

		//reemplazando los valores
        $consulta=$this->reemplazar_campos($consulta);
        $consulta=$this->reemplazar_parametros($consulta);

        try
        {

            //preparando la consulta
            $tabla= $this->m_basedatos->conexion->prepare($consulta);
            if ($this->bind_campos_parametros($consulta,$tabla)<0) //se reemplaza las variables :variable
            {
                $this->m_mensaje="obtener_tabla_from_sql:".$this->m_mensaje;
                //error_log($this->m_mensaje);

				tsdk_error_log(tsdk_propiedades::$TRACELOG,
	            	 "#x ".$this->m_mensaje.
	                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
	                    "\n"
	                );
				
                return null;
            }

            if ($tabla->execute()==false)
            {
                $error=$tabla->errorInfo();
                $this->m_mensaje="SQLSTATE: ".$error[0];
                $this->m_mensaje.=", CODE: ".$error[1];
                $this->m_mensaje.=", MESSAGE: ".$error[2];

                $this->m_sqlstate=$error[0];

				tsdk_error_log(tsdk_propiedades::$TRACELOG,
	            	 "#x ".$this->m_mensaje.
	                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
	                    "\n"
	                );

                unset($tabla);
                return null;
            }

            //recorriendo el resultado
            while($row = $tabla->fetch(PDO::FETCH_ASSOC))
            {
                
				//creando la fila
				$fila = new tsdk_coleccion();
				
				foreach ($row as $key => $valor)
				{
					$fila->add($key)->value = $valor;
				}
					
				//añadiendo la fila
				$datos->filas->add(null,$fila); //se añade al final con clave numerica
            }
            //$nrofilas = $tabla->rowCount();
            unset($tabla);
			
        }catch(PDOException $e)
        {
            //----------------------------------------------
            //Imprimiendo el error
            //----------------------------------------------
            $this->m_mensaje = $e->getMessage();
            $this->m_codigoerror = $e->getCode();
            $lineaerror = $e->getMessage()."\n".
                          "      Codigo  : ".$e->getCode()."\n".
                          "      Archivo : ".$e->getFile()."\n".
                          "      Linea   : ".$e->getLine()."\n".
                          "      SQL     : ".$consulta."\n";


			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x ".$lineaerror.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );

            //----------------------------------------------
            //Imprimiendo los valores del objeto actual
            //----------------------------------------------
            tsdk_error_log_trace("------- campos de resulset -------",$this);

            return null;
        }

        return $datos;
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion devuelve una consulta INSERT clasica, utiliza los campos para su contruccion
	* @access public
    * @param   	string 		$campoautoincrement 		Indica el campo que automaticamente incrementará su valor
    * @param   	array 		$campos_as_basedatos 		Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones
    * @return   string 		 							Retorna la consulta solicitada, retorna -1 en caso de error
    * @example "sample_tsdk_resultset.php"
    */
    public function get_sql_insert( $campoautoincrement = "", $campos_as_basedatos=array() )
    {
        $comando="insert into ";
        $comando=$comando.$this->m_nombretabla."(";

		foreach ($this->campos as $key => $objeto)
        {
            if ( $key != $campoautoincrement )
            {
                $comando .= $key.",";
            }
        }
        //quitando la ultima coma
        if ( substr($comando,strlen($comando)-1,1) == "," )
        {
            $comando=substr($comando,0,strlen($comando)-1);
        }

        $comando .= ") values(";
		foreach ($this->campos as $key => $objeto)
        {
            if ( $key != $campoautoincrement )
            {
                if (in_array($key,$campos_as_basedatos,true))
                {
                    $comando=$comando.$objeto->value.",";
                }else{
                    $comando=$comando.":".$key.","; //para usar bind posteriormente
                }
            }
        }

        //quitando la ultima coma
        if ( substr($comando,strlen($comando)-1,1) == "," )
            $comando = substr($comando,0,strlen($comando)-1);

        $comando .= ")";

        if ($this->m_manejadorbd == "postgresql"){
            //aumentando para retornar el campo autoincrement (solo para postgresql)
            if ($campoautoincrement != "")
            {
                $comando .= " RETURNING ".$campoautoincrement;
            }
        }
        
        return $comando;
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion devuelve una consulta UPDATE clasica, utiliza los campos para su contruccion
	* @access public
    * @param   	array 		$campos_claveprimaria 		Arreglo con la lista de campos que conforman la clave primaria
	* @param   	array 		$campos_as_basedatos 		Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones
    * @return   string 		 							Retorna la consulta solicitada, retorna -1 en caso de error
    * @example "sample_tsdk_resultset.php"
    */
    public function get_sql_update( $campos_claveprimaria=array(), $campos_as_basedatos=array() )
    {
        $comando="update ";
        $comando=$comando.$this->m_nombretabla." set ";

        /*foreach ($this->m_listaitems as $key => $objeto)
        {
            if ($objeto->iscolumn == true && $objeto->isclaveprimaria==false && !in_array($key,$campos_no_writables,true) )
            {
                if (in_array($key,$campos_as_basedatos,true))
                {
                    $comando=$comando.$key."=".$objeto->value.",";
                }else{
                    $comando=$comando.$key."=:".$key.",";
                }
            }
        }*/

		foreach ($this->campos as $key => $objeto)
        {
			if ( !in_array($key,$campos_claveprimaria,true))
            {
                if (in_array($key,$campos_as_basedatos,true))
				{
                    $comando=$comando.$key."=".$objeto->value.",";
                }else{
                    $comando=$comando.$key."=:".$key.",";
                }
            }
        }

        //quitando la ultima coma
        $comando=substr($comando,0,strlen($comando)-1);
        $comando=$comando." where ";

        /*foreach ($this->m_listaitems as $key => $objeto)
        {
            if ($objeto->iscolumn == true && $objeto->isclaveprimaria==true)
            {
                if (in_array($key,$campos_as_basedatos,true))
                {
                    $comando=$comando.$key."=".$objeto->value." and ";
                }else{
                    $comando=$comando.$key."=:".$key." and ";
                }
            }
        }*/

		foreach ($this->campos as $key => $objeto)
        {
			if ( in_array($key,$campos_claveprimaria,true))
            {
                if (in_array($key,$campos_as_basedatos,true))
				{
                    $comando=$comando.$key."=".$objeto->value." and ";
                }else{
                    $comando=$comando.$key."=:".$key." and ";
                }
            }
        }

		//quitando el ultimo and
        $comando=substr($comando,0,strlen($comando)-5);

        return $comando;
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion devuelve la consulta SQL para borrar los items
	* @access public 
    * @return   string 		 							Retorna la consulta solicitada
    * @example "sample_tsdk_resultset.php"
    */
    public function get_sql_delete()
    {
        $comando="delete from ";
        $comando=$comando.$this->m_nombretabla." where ";

		foreach ($this->campos as $key => $objeto)
        {
            $comando=$comando.$key."=:".$key." and ";
        }

		//quitando el ultimo and
        $comando=substr($comando,0,strlen($comando)-5);
		
        return $comando;
    }
	
    //-----------------------------------------------------------
    /**
    * Esta funcion devuelve una consulta SELECT clasica para un registro
	* @access public
    * @return   string 		 							Retorna la consulta solicitada
    * @example "sample_tsdk_resultset.php"
    */
    public function get_sql_select()
    {
        $comando="select * ";

		//creando la parte from
        $comando=$comando." from ".$this->m_nombretabla." where ";

		foreach ($this->campos as $key => $objeto)
        {
            $comando=$comando.$key."=:".$key." and ";
        }

        //quitando el ultimo and
        $comando=substr($comando,0,strlen($comando)-5);

        return $comando;
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta INSERTAR clasica 
    * Utiliza los campos para su contruccion. 
    * 
    * Los parametros de la funcion son:
    * 
    * $campoautoincrement = "" 
    * : Texto que indica el campo autoincrement, este no se incluye en la consulta.
    * 
    * $campos_as_basedatos = array()
    * : Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones.
    * 
	* @access public
    * @return   integer   Devuelve el numero de filas afectadas. Devuelve -1 en caso de error.
    * @example "sample_tsdk_resultset.php"
    */
    protected function insertar()
    {
        //obteniendo los parametros
        $parametros = func_get_args();
        $campo_autoincrement = isset($parametros['0'])?$parametros['0']:"";
        $campos_as_basedatos = isset($parametros['1'])?$parametros['1']:array();
        
        //evaluando el tipo de manejador de base de datos
        switch ($this->m_manejadorbd){
            case "mysql":
                return $this->insertar_mysql($campo_autoincrement,$campos_as_basedatos);
                break;
            case "postgresql":
                return $this->insertar_postgresql($campo_autoincrement,$campos_as_basedatos);
                break;
            default:
                $lineaerror = "Manejador de base de datos [ ".$manejadorbd." ] no reconocido.\n";
                //error_log($lineaerror);

                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x ".$lineaerror.
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );
                    
                return -1;                                
                break;
        }
        
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta MODIFICAR clasica.
    * Utiliza los campos para su contruccion. 
    * 
    * Los parametros de la funcion son:
    * 
    * $campos_claveprimaria = array()
    * : Arreglo con la lista de campos que conforman la clave primaria
    * 
    * $campos_as_basedatos = array()
    * : Arreglo con la lista de campos que se deben colocar en la consulta tal cual porque son funciones.
    * 
	* @access public
    * @return   integer   Devuelve el numero de filas afectadas. Devuelve -1 en caso de error.
    * @example "sample_tsdk_resultset.php"
    */
    public function modificar()
    {
        //obteniendo los parametros
        $parametros = func_get_args();
        $campos_claveprimaria = isset($parametros['0'])?$parametros['0']:array();
        $campos_as_basedatos = isset($parametros['1'])?$parametros['1']:array();

        //Inicio de funcion
        /*$arreglo=array();

        //creando arreglo y validando los campos
        foreach ($this->m_listaitems as $key => $objeto )
        {
            if ( $objeto->iscolumn && !in_array($key,$campos_no_writables,true)
                 && !in_array($key,$campos_as_basedatos,true) )
            {
                $arreglo[]=$key;
            }
        }

        $respuesta=$this->validar_variables($arreglo);
        if ($respuesta<0)
        {
            return $respuesta;
        }*/

        //llamando a SQL
        $consulta = $this->get_sql_update($campos_claveprimaria, $campos_as_basedatos);

		//error_log($consulta);
        return $this->execute_sql($consulta);
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta ELIMINAR clasica.
    * Utiliza los campos para su contruccion. 
    * 
	* @access public
    * @return   integer   Devuelve el numero de filas afectadas. Devuelve -1 en caso de error.
    * @example "sample_tsdk_resultset.php"
    */
    public function eliminar()
    {
        /*$arreglo=array();

        //creando arreglo y validando los campos
        foreach ($this->m_listaitems as $key => $objeto)
        {
            if ($objeto->isclaveprimaria)
            {
                $arreglo[]=$key;
            }
        }

        $respuesta=$this->validar_variables($arreglo);
        if ($respuesta<0)
        {
            return $respuesta;
        }*/

        //llamando a SQL
        $consulta = $this->get_sql_delete();

        //error_log("eliminar:".$consulta);
        return $this->execute_sql($consulta);
    }

    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta SELECT clasica para un registro
	* @access public 
    * @return   string 		 							Retorna la consulta solicitada
    * @example "sample_tsdk_resultset.php"
    */
    public function obtener()
    {
        /*$arreglo=array();

        //creando arreglo y validando los campos
        foreach ($this->m_listaitems as $key => $objeto)
        {
            if ($objeto->isclaveprimaria)
            {
                $arreglo[]=$key;
            }
        }

        $respuesta=$this->validar_variables($arreglo);
        if ($respuesta<0)
        {
            return $respuesta;
        }*/

        //llamando a SQL
        $consulta = $this->get_sql_select();
			
		return $this->obtener_campos_from_sql($consulta);
        //return $this->obtener_campos_from_sql($consulta);
    }

	
    //-----------------------------------------------------------
    /**
    * Esta funcion ejecuta una consulta SELECT clasica para un registro y obtienen los datos
	* @access  public 
	* @param   string		$consulta	Se refiere a la consulta a ejecutar
    * @return  integer					Retorna -1 en caso de error, 0 si no existe ningun registro, mayor a 0 el numero de registros encontrados
    * @example "sample_tsdk_resultset.php"
    */
    public function obtener_campos_from_sql($consulta)
    {
        $tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
		if ( is_null($tabla) )
		{
			return -1;
		}

		if ($tabla->filas->count()==0) //no existe datos
		{
			$this->mensaje="No existe datos";
			return 0;
		}
			
		//recuperando los campos
		foreach ($tabla->filas as $key=>$campo) //key es el nro. de fila, $campo es el campo de la base de datos
		{
			foreach ($campo as $nombrex=>$valorx) //$nombrex es el nombre del campo, $valorx es un item_collection
			{
				$this->campos->add($nombrex)->value = $valorx->value;
			}
		}
		
		return $tabla->filas->count();
    }

}