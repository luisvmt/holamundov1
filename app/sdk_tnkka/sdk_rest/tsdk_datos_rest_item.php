<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_parametros_rest.php");

//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_datos_rest_item
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Representa los eventos de la fila
    * @var coleccion
    */
	public	$parametros=null;
	
    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    */
	function __construct()
	{
		//iniciando las celdas
		$this->parametros = new tsdk_parametros_rest(); 
		
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }
	
}


