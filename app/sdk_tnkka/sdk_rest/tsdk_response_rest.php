<?php   
/**
 * Este archivo implementa la clase tsdk_response_rest
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controladores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

//Incluyendo archivos
include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_encabezados_rest.php");
include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_datos_rest.php");

//-----------------------------------------------------------
/**
* Clase que implementa la respuesta de una solicitud REST
*
* @author luisvmt@hotmail.com
* @package controladores
*/
class tsdk_response_rest 
{
	// ******************************
	// variables miembro
	// ******************************


	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* nombre del controlador
	* @var string
	*/
	//protected  $m_nombre_controlador="";
	
	/**#@-*/

	
	//-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$encabezados=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$datos=null;
	
    /**#@-*/
	
	// ******************************
	// metodos
	// ******************************
 
 
	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	*/
	public function __construct() 
	{
		//parent::__construct();

		//obteniendo los parametros
			//$parametros = func_get_args();
			//$validar_login = isset($parametros['0'])?$parametros['0']:true;

		//inicializando sessiones
		//session_start();

		//inicializando variables
		//$this->load->helper('url');
		//$this->load->library('tsdk_login_library'); //libreria personal

		//obteniendo el id de session
		//$this->m_session_id = session_id();

		//validando si el usuario esta loggeado
		/*if ($validar_login==true)
		{
			//$this->tsdk_login_library->base_url = "http://adminfabystore_cancer01.tnkka.xyz/";
			$this->tsdk_login_library->controlador_raiz = $controlador_raiz;

			$this->tsdk_login_library->check_is_logged();

		}*/

		//inicializando los parametros del controlador
		//$this->m_parametros_controlador = new tsdk_parametros_controlador($this);

		//conectando a la base de datos
		//$this->m_basedatos = new tsdk_basedatos_mysql();
		//$this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$bd_is_persistent);
		
		
		//iniciando las colecciones
		$this->encabezados = new tsdk_encabezados_rest();
		
		//iniciando las colecciones
		$this->datos = new tsdk_datos_rest();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    	//error_log("Dentro de destructor del controlador tsdk");
        //$this->m_conexion = null;
    }
	
	
    //-----------------------------------------------------------
    /** 
    * Implementa la funcion que devuelve los datos
    *
    */
    function get_array()
    {
		$respuesta = array();
		
		//generando el encabezado
    	foreach ($this->encabezados as $clave => $item)
		{
			$respuesta[$clave]=$item->value;
		}
		
		//generando los datos
		foreach ($this->datos as $i => $dato_item)
		{
			foreach ($dato_item->parametros as $nombre => $nodo)
			{
				$respuesta["datos"][$i][$nombre]=$nodo->value;
			}
		}
		//
		//
		////error_log("Dentro de destructor del controlador tsdk");
        //$this->m_conexion = null;
		//$datos["codigo"]="ok";
		//$datos["nombre"]="saludos";
		
		//tsdk_error_log_trace("datos", $this);
		//tsdk_error_log_trace("respuesta", $respuesta);

		return $respuesta;
    }


} //fin de clase

