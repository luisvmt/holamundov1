<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de estilos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_parametros_rest extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
	
    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */
	
    /**
    * Representa la funcion que atiende un evento de varios controles
    * @var string
    */
	//protected		$m_handler_eventos=""; 

    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }


}

?>
