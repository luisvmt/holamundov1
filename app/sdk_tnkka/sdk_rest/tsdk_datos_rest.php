<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de estilos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_rest/tsdk_datos_rest_item.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_datos_rest extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }
	
    //-----------------------------------------------------------
    /**
    * Establece un dato rest
    * @access public
    * @param	integer		$indice			  Indica el numero de fila del dato
    * @param	string		$nombrevariable	  Indica el nombre de la variable a modificar
    * @param	object		$valor			  Indica el valor a establecer
	* @return   object					  	  Devuelve el objeto añadido
    */
    public function set_valor($indice,$nombrevariable,$valor)
    { 
		$item = new tsdk_datos_rest_item(); //se crea un item
		
		//$dato_item = new tsdk_datos_rest_item();	
		$dato_item = $this->add($indice,$item); //si no existe el indice, se crea con el parametro que se indica (segundo parametro)
												//si existe el indice, se devuelve el objeto de esa posicion
		$dato_item->parametros->add($nombrevariable)->value = $valor;
		
		return $this->add($indice,$dato_item);
    }
	
}
