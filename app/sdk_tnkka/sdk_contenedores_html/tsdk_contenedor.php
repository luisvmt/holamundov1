<?php 
/**
 * Este archivo implementa la clase base de todos los objetos html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package contenedores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - contenedor
*
* Implementa la clase que contiene a otros controles HTML
*
* Esta clase implementa la clase que contiene a otros controles HTML
*/ 
 
class tsdk_contenedor extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa el contenido del control
    * @var string
    */
    protected   $m_content="";

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    *
    */
    public function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
		parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);
		$this->tipocontrol = tsdk_tipocontrol::contenedor;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>content</b>       :   Devuelve el contenido del control</br>
    * @example "sample_tsdk_contenedor.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "content":
            return $this->m_content;
            break;
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>content</b>       :   Establece el contenido del control. </br>
    *
    * @param    string  $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_contenedor.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "content":
            $this->m_content = $value;
            break;
        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }

    //----------------------------------------
    //--- definiendo funciones publicas
    //----------------------------------------
    /*public function add_content($contenido) //se puede simular control->content = control->content & texto;
    {
        $this->m_content .= $contenido;
    }

    public function set_content($contenido)
    {
        $this->m_content = $contenido;
    }*/

}

?>
