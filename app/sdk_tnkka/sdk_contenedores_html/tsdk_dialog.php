<?php 
/**
 * Este archivo implementa la clase DIV para crear un dialogo usando jquery
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package contenedores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_contenedor.php");
include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_div.php");
 
//-----------------------------------------------------------
/**
* Control JQUERY - Dialogo
*
* Implementa la clase que representa un dialogo/JQUERY
*
* @author luisvmt@hotmail.com
* @package contenedores
*/

class tsdk_dialog extends tsdk_div
{

    // ******************************
    // variables miembro
    // ******************************

    //no tiene


    // ******************************
    // metodos
    // ******************************
 
    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $titulo         Indica el titulo del dialogo a crear
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    *
    */
    function __construct($titulo="",$idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);
        
        $this->title = $titulo;
        $this->tipocontrol = tsdk_tipocontrol::dialog;
        
        //configurando el dialogo para que se vaya a la izquierda
		$this->estilos->add("float",new tsdk_coleccion_item("left"));
		$this->estilos->add("clear",new tsdk_coleccion_item("left"));

    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>       :   aun no implementado</br>
    * @example "sample_tsdk_div.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        /*case "title":
            if ( $this->propiedades->key_exists($propiedad)  )
            {
                return $this->propiedades($propiedad)->value;
            }else{
                return null;
            }
            break;*/
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>       :   aun no implementado</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_div.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        /*case "title":
            $this->propiedades($propiedad)->value = $value;
            break;*/
        default:
            parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Obtiene el texto HTML del control
	* @access public
    * @return   string      Texto HTML del control
    * @example "sample_tsdk_dialog.php"
    */
    public function get_html()
    {
        //iniciando el div
        $salida = "<div ";
        $salida .= $this->propiedades->get_propiedades_text();
		$salida .= " ".$this->estilos->get_estilos_text();
		$salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >";
        $salida .= $this->m_content;

        //imprimiendo los subcontroles
        $salida .= $this->get_html_subcontroles();

        //finalizando el div
        $salida .= "</div>";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en la salida estandar el texto HTML del control
	* @access public 
    * 
    * @return   none
    * @example "sample_tsdk_dialog.php"
    */
    public function print_html()
    {
        echo $this->get_html();
    }
}

?>
