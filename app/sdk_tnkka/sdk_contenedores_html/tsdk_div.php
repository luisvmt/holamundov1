<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package contenedores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_contenedor.php");

//-----------------------------------------------------------
/**
* Control HTML - DIV
*
* Implementa la clase que representa un accordion de html
*
* @author luisvmt@hotmail.com
* @package contenedores
*/

class tsdk_div extends tsdk_contenedor
{

    // ******************************
    // variables miembro
    // ******************************

    //no tiene


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    *
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);
        $this->tipocontrol = tsdk_tipocontrol::div;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>       :   aun no implementado</br>
    * @example "sample_tsdk_div.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        /*case "title":
            if ( $this->propiedades->key_exists($propiedad)  )
            {
                return $this->propiedades($propiedad)->value;
            }else{
                return null;
            }
            break;*/
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>       :   aun no implementado</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_div.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        /*case "title":
            $this->propiedades($propiedad)->value = $value;
            break;*/
        default:
            parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece el div para que se coloque a la izquierda
	* @access public
    * @param    boolean  $is_first       Si es TRUE, coloca el div al inicio de la fila. No hay otro div a su izquierda.
    * @return none
    * @example "sample_tsdk_div.php"
    */
    public function config_as_left($is_first=false)
    {
		$this->estilos->add("float",new tsdk_coleccion_item("left"));

        if ($is_first)
        {
			$this->estilos->add("clear",new tsdk_coleccion_item("left"));
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece el div para que se coloque a la derecha
	* @access public 
    * @param    boolean  $is_first       Si es TRUE, coloca el div a la derecha de la fila. No hay otro div a su derecha
    * @return none
    * @example "sample_tsdk_div.php"
    */
    public function config_as_right($is_first=false)
    {
		$this->estilos->add("clear",new tsdk_coleccion_item("right"));

        if ($is_first)
        {
			$this->estilos->add("clear",new tsdk_coleccion_item("right"));
        }
    }

    //-----------------------------------------------------------
    /**
    * Configura el DIV como un dialogo
	* @access public 
    * @return none
    * @example "sample_tsdk_div.php"
    */
    public function config_as_dialog()
    {
        //configurando para que se vaya a la izquierda
		$this->estilos->add("float",new tsdk_coleccion_item("left"));
		$this->estilos->add("clear",new tsdk_coleccion_item("left"));

    }

    //-----------------------------------------------------------
    /**
    * Obtiene el texto HTML del control
	* @access public
    * @return   string      Texto HTML del control
    * @example "sample_tsdk_div.php"
    */
    public function get_html()
    {
        //iniciando el div
        $salida = "<div ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >";
        $salida .= $this->m_content;

        //imprimiendo los subcontroles
        $salida .= $this->get_html_subcontroles();

        //finalizando el div
        $salida .= "</div>";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en la salida estandar el texto HTML del control
	* @access public 
    * 
    * @return   none
    * @example "sample_tsdk_div.php"
    */
    public function print_html()
    {
        echo $this->get_html();
    }
}

?>
