<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package contenedores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_contenedor.php");

//-----------------------------------------------------------
/**
* Control HTML - Formulario
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package contenedores
*/

class tsdk_form extends tsdk_contenedor
{
// ******************************
    // variables miembro
    // ******************************

    //No tiene


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_form.php"
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        $this->tipocontrol = tsdk_tipocontrol::form;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_form.php"
    */
    public function get_html()
    {
        //iniciando el form
        $salida = "<form ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >";
        $salida .= $this->m_content;
        
        //imprimiendo los subcontroles
        $salida .= $this->get_html_subcontroles();

        //finalizando el form
        $salida .= "</form>";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_form.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
