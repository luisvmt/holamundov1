<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_tabs_coleccion extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }

    //-----------------------------------------------------------
    /** 
     * Añade un elemento a la coleccion
     * @param  string    $clave     Es la clave del elemento a añadir
     * @param  object    $objeto        Es el objeto a añadir a la coleccion
     * 
     * @return object   Una referencia al objeto añadido
     * @example "sample_tsdk_tabs_coleccion.add.txt"
     */
    function & add($clave=null,$objeto=null) 
    {
        if (is_null($objeto))
        {
            $objeto = new tsdk_tab_item();
        }

        return parent::add($clave,$objeto); //llamando al padre
    }
    

}
