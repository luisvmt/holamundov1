<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");

//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_tab_item extends tsdk_control
{
    
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access private
	*/

	/**
	* Representa el titulo que tendra el tab
	* @var string
	*/
	private   $m_titulo=0;

	/**
	* Representa el contenido HTML que tendra el tab
	* @var string
	*/
	private   $m_contenido=0;

	/**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $titulo			Este el titulo del TAB
    * @param  string    $contenido      Es el codigo HTML que se colocara dentro del tas el ID de la ventana que contiene el control
    * @example "sample_tsdk_button.php"
    */
    function __construct($titulo="",$contenido="")
	{
		$idventana = "";
		$idcontrol = "";
		$propiedades = null;
		$estilos = null;
		$eventos = null;

		parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

		//iniciando variables
		$this->tipocontrol = tsdk_tipocontrol::tab_item;
		$this->m_titulo = $titulo;
		$this->m_contenido = $contenido;

	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __toString()
    {
		//return print_r($this->m_valor,true);
		return "";
    }
	
    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   Valor del item</br>
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "titulo":
			return $this->m_titulo;
			break;

		case "contenido":
			return $this->m_contenido;
			break;
		
		
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   valor del item</br>
    *
    * @param	object	 $value        El valor a establecer en la propiedad
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "titulo":
			$this->m_titulo = $value;
			break;
		case "contenido":
			$this->m_contenido = $value;
			break;
		
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

	
}


