<?php 
/**
 * Este archivo implementa las funciones comunes para una vista browse estandard
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package varios
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_link.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_group.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php"); //funciones

//funciones obsoletas
//include_once(APPPATH."../../sdk_terceros/excel/PHPExcel.php"); //excel
//include_once(APPPATH."../../sdk_terceros/excel/PHPExcel/Writer/Excel2007.php"); // PHPExcel_Writer_Excel2007 

//funciones nuevas para excel
include_once(APPPATH."../../sdk_terceros/phpoffice_phpspreadsheet_1.10.1.0_require/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php");
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


//-----------------------------------------------------------
/**
* Ventana estandar - Windows Browse
*
* Implementa una ventana estandar que implementa un browse
*
* @author luisvmt@hotmail.com
* @package varios
*/

class tsdk_window_browse
{
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa el objeto base de datos
	* @var object
	*/
	protected	$m_basedatos;
	
	/**
	* Representa el ID de la ventana a crear
	* @var string
	*/
	protected	$m_idventana;
	
	/**
	* Representa el titulo del reporte EXCEL
	* @var string
	*/
	protected   $m_tituloreporte_excel;
	
	/**
	* Representa el número de página que esta mostrando
	* @var integer
	*/
	protected	$m_nropagina;
	
	/**
	* Representa el total de páginas de la consulta
	* @var integer
	*/
	protected   $m_totalpaginas;

	/**
	* Determina el numero de registros que tiene la consulta
	* @var integer
	*/
	protected   $m_totalregistros; 
	
	/**
	* Representa el control tsdk_tabla que forma la ventana
	* @var object
	*/
	//protected	$m_tabla; //ya no se utiliza, se crea y pasa por parametro
	
	/**
	* Arreglo con los titulos de la regilla (browse)
	* @var array
	*/
	protected	$m_titulos_browse;
	
	/**
	* Arreglo con los titulos del reporte EXCEL
	* @var array
	*/
	protected	$m_titulos_reporte;
	
	/**
	* Arreglo con los campos que componen la regilla (browse)
	* @var array
	*/
	protected	$m_campos_browse;
	
	/**
	* Arreglo con los campos que componene el reporte EXCEL
	* @var array
	*/
	protected	$m_campos_reporte;
	
	/**
	* Arreglo con los campos que componen la clave primaria 
	* @var array
	*/
	protected 	$m_claveprimaria;

	/**
	* Representa el subtitulo de la ventana
	* @var string
	*/
	protected   $m_subtitulo;
	
	/**
	* Texto que contiene el filtro a buscar
	* @var string
	*/
	protected   $m_filtro;
	
	/**
	* Nombre del campo por el cual es ordenada la consulta
	* @var string
	*/
	protected	$m_orderby;
	
	/**
	* Texto que contiene el sentido del ordenamiento
	* @var string
	*/
	protected	$m_sentido;
	
	/**
	* Texto utilizado para el intercambio de datos con los controladores
	* @var string
	*/
	protected   $m_parametros;
	
	/**
	* Arreglo con el alineamiento de las columnas
	* @var array
	*/
	protected 	$m_alineamiento_columna;

	/**
	* Control HTML - filtro
	* @var object
	*/
	protected	$m_textbox_filtro;
	
	/**
	* Control HTML - nropagina
	* @var object
	*/
	protected	$m_textbox_nropagina;
	
	/**
	* Control HTML - orderby
	* @var object
	*/
	protected	$m_textbox_orderby;
	
	/**
	* Control HTML - textbox-sentido
	* @var object
	*/
	protected	$m_textbox_sentido;
	
	/**
	* Control HTML - textbox-parametros
	* @var object
	*/
	protected	$m_textbox_parametros;
	
	/**
	* Control HTML - link_reporte
	* @var object
	*/
	protected	$m_link_reporte;
	
	/**
	* Control HTML - link_importar_exportar
	* @var object
	*/
	protected	$m_link_importar_exportar;
	
	/**
	* Control HTML - link_insertar
	* @var object
	*/
	protected	$m_link_insertar;
	
	/**
	* Control HTML - link_actualizar
	* @var object
	*/
	protected 	$m_link_actualizar;

	/**
	* Arreglo con la lista de acciones
	* @var array
	*/
	protected	$m_lista_acciones;
	
	/**
	* Control HTML - Combobox con lista de acciones
	* @var object
	*/
	protected	$m_combo_acciones;
	
	/**
	* Control HTML - link ejecutar varios
	* @var object
	*/
	protected	$m_link_ejecutar_varios;

	/**
	* Control HTML - textbox id seleccionado
	* @var object
	*/
	protected   $m_textbox_idseleccionado;

	/**
	* Muestra la opcion insertar
	* @var boolean
	*/
	protected	$m_showinsertar;
	
	/**
	* Muestra la opcion eliminar
	* @var boolean
	*/
	protected	$m_showeliminar;
	
	/**
	* Muestra la opcion reporte
	* @var boolean
	*/
	protected	$m_showreporte;
	
	/**
	* Muestra la opcion importar/exportar
	* @var boolean
	*/
	protected	$m_show_importar_exportar;

	/**
	* Objeto EXCEL
	* @var object
	*/
	//protected	$m_objPHPExcel;
	protected	$m_spreadsheet;

	/**
	* Objeto WRITER
	* @var object
	*/
	//protected	$m_objWriter;

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  	object 		$basedatos 		Base de datos
	* @example "sample_tsdk_window_browse.php"
	*/
	function __construct($basedatos)
	{
		$this->m_basedatos = $basedatos;
		$this->m_alineamiento_columna=array();
		$this->m_filtro="";
		$this->m_textbox_idseleccionado = new tsdk_textbox();
		$this->m_textbox_filtro = new tsdk_textbox();
		$this->m_textbox_nropagina = new tsdk_textbox();
		$this->m_textbox_orderby = new tsdk_textbox();
		$this->m_textbox_sentido = new tsdk_textbox();
		$this->m_textbox_parametros = new tsdk_textbox();
		$this->m_link_reporte = new tsdk_link();
		$this->m_link_importar_exportar = new tsdk_link();
		$this->m_link_insertar = new tsdk_link();
		$this->m_link_actualizar = new tsdk_link();

		//grupo acciones multiples
		$this->m_lista_acciones=array();
		//$this->m_group_acciones = new tsdk_group(" Acción múltiple ");
		$this->m_combo_acciones = new tsdk_combobox();
		$this->m_link_ejecutar_varios = new tsdk_link();

		//opciones a mostrar
		$this->m_showinsertar=false;
		$this->m_showeliminar=false;
		$this->m_showreporte=false;
		$this->m_show_importar_exportar=false;
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>base_datos</b>      :   Objeto base de datos</br>
	*
	* @example "sample_tsdk_window_browse.php"
	*/
	public function __get($propiedad)
	{
		switch ($propiedad){
		
		case "base_datos":
			return $this->m_basedatos;
			break;

		default:
		
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get.".
				 "  Propiedad no encontrada: [".$propiedad."]".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			break;
		}

	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>nropagina</b>       :   número de pagina de la ventana. </br>
	*    <b>parametros</b>      :   Párametros</br>
	*    <b>show_insertar</b>   :   Muestra la opcion insertar</br>
	*    <b>show_eliminar</b>   :   Muestra la opción eliminar</br>
	*    <b>show_reporte</b>    :   Muestra la opción reporte</br>
	*    <b>show_importar_exportar</b>           :   Muestra la opción importar/exportar</br>
	*
	* @param string $value        El valor a establecer en la propiedad
	* @example "sample_tsdk_window_browse.php"
	*/
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "nropagina":
			$this->m_nropagina = $value;
			break;
		case "parametros":
			$this->m_parametros = $value;
			break;
		case "show_insertar":
            $this->m_showinsertar = $value;
            break;
		case "show_eliminar":
            $this->m_showeliminar = $value;
            break;
		case "show_reporte":
            $this->m_showreporte = $value;
            break;
		case "show_importar_exportar":
            $this->m_show_importar_exportar = $value;
            break;

		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
			     "#x Error en set.".
				 "  Propiedad no encontrada: [".$propiedad."]".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			break;
		}
	}
 
    //-----------------------------------------------------------
    /**
    * Esta funcion se encarga de crear los controles que se utilizaran para la creacion del browse
    * Los parametros son los parametros que se pasara a los eventos de los links
    *
	* @access protected
    * @param  string    $parametros          Parametros que se pasaran a los eventos de los links
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	protected function crear_controles($parametros="")
	{
		//--- creando el textbox filtro
		$this->m_textbox_filtro->set_id_ventana_control($this->m_idventana,"filtro");
		$this->m_textbox_filtro->propiedades->set_propiedades(array("type"=>"text",
																	"value"=>$this->m_filtro,
																	"size"=>"30",
																	"maxlength"=>"128"));
        
        $this->m_textbox_filtro->estilos->set_estilos(array("height"=>"80%"));
		//$this->m_textbox_filtro->set_estilos(array("width"=>"80%"));
		//$this->m_textbox_filtro->set_eventos(array("onkeypress"=>$this->m_idventana."_filtro_onkeypress(event)"));
		//$this->m_textbox_filtro->eventos->add("onkeypress")->value = $this->m_idventana."_filtro_onkeypress(event)";
		$this->m_textbox_filtro->eventos->add("onkeypress")->value = "onkeypress(event)";

		//--- creando el textbox (oculto) nropagina
		$this->m_textbox_nropagina->set_id_ventana_control($this->m_idventana,"nropagina");
		$this->m_textbox_nropagina->propiedades->set_propiedades(array ("type"=>"hidden",
																		"value"=>$this->m_nropagina));

		//--- creando el textbox (oculto) orderby
		$this->m_textbox_orderby->set_id_ventana_control($this->m_idventana,"orderby");
		$this->m_textbox_orderby->propiedades->set_propiedades(array("type"=>"hidden",
															"value"=>$this->m_orderby));

		//--- creando el textbox (oculto) sentido
		$this->m_textbox_sentido->set_id_ventana_control($this->m_idventana,"sentido");
		$this->m_textbox_sentido->propiedades->set_propiedades(array("type"=>"hidden",
																"value"=>$this->m_sentido));

		//--- creando el textbox (oculto) idseleccionado
		$this->m_textbox_idseleccionado->set_id_ventana_control($this->m_idventana,"idseleccionado");
		$this->m_textbox_idseleccionado->propiedades->set_propiedades(array("type"=>"hidden"));

		//--- creando el textbox (oculto) parametros
		$this->m_textbox_parametros->set_id_ventana_control($this->m_idventana,"parametros");
		$this->m_textbox_parametros->propiedades->set_propiedades(array("type"=>"hidden",
																"value"=>$this->m_parametros));

		//--- creando link reporte
		$this->m_link_reporte->texto = "Reporte";
		$this->m_link_reporte->set_id_ventana_control($this->m_idventana,"link_reporte");
		$this->m_link_reporte->propiedades->set_propiedades(array("href"=>"#"));
		//$this->m_link_reporte->set_eventos(array("onclick"=>$this->m_idventana."_reporte_onclick(".$parametros.")"));
		//$this->m_link_reporte->eventos->add("onclick")->value = $this->m_idventana."_reporte_onclick(".$parametros.")";
		$this->m_link_reporte->eventos->add("onclick")->value = "onclick(".$parametros.")";

		//--- creando link importar.exportar
		$this->m_link_importar_exportar->texto = "Importar / Exportar";
		$this->m_link_importar_exportar->set_id_ventana_control($this->m_idventana,"link_importar_exportar");
		$this->m_link_importar_exportar->propiedades->set_propiedades(array("href"=>"#"));
		//$this->m_link_importar_exportar->set_eventos(array("onclick"=>$this->m_idventana."_importar_exportar_onclick(".$parametros.")"));
		//$this->m_link_importar_exportar->eventos->add("onclick")->value = $this->m_idventana."_importar_exportar_onclick(".$parametros.")";
		$this->m_link_importar_exportar->eventos->add("onclick")->value = "onclick(".$parametros.")";

		//--- creando link insertar
		$this->m_link_insertar->texto = "Insertar";
		$this->m_link_insertar->set_id_ventana_control($this->m_idventana,"link_insertar");
		$this->m_link_insertar->propiedades->set_propiedades(array("href"=>"#"));
		//$this->m_link_insertar->set_eventos(array("onclick"=>$this->m_idventana."_insertar_onclick(".$parametros.")"));
		//$this->m_link_insertar->eventos->add("onclick")->value = $this->m_idventana."_insertar_onclick(".$parametros.")";
		$this->m_link_insertar->eventos->add("onclick")->value = "onclick(".$parametros.")";

		//--creando link actualizar
		$this->m_link_actualizar->texto = "Actualizar";
		$this->m_link_actualizar->set_id_ventana_control($this->m_idventana,"link_actualizar");
		$this->m_link_actualizar->propiedades->set_propiedades(array("href"=>"#"));
		//$this->m_link_actualizar->set_eventos(array("onclick"=>$this->m_idventana."_gopagina_onclick('".$this->m_nropagina."')"));
		//$this->m_link_actualizar->eventos->add("onclick")->value = $this->m_idventana."_gopagina_onclick('".$this->m_nropagina."')";
		$this->m_link_actualizar->eventos->add("onclick")->value = "onclick('".$this->m_nropagina."')";
	
		//creando opciones de grupo
		if ( count($this->m_lista_acciones)>0 )
		{
			//creando combo de grupo
			$this->m_combo_acciones->set_id_ventana_control($this->m_idventana,"combo_acciones");

			//agregando las acciones del grupo
			foreach ($this->m_lista_acciones as $clave=>$valor)
			{
				$this->m_combo_acciones->add_item( new tsdk_lista_item($clave,$valor) );
			}

			//creando link ejecutar (del grupo)
			$this->m_link_ejecutar_varios->texto = "Ejecutar";
			$this->m_link_ejecutar_varios->set_id_ventana_control($this->m_idventana,"link_ejecutar_varios");
			$this->m_link_ejecutar_varios->propiedades->set_propiedades(array("href"=>"#"));
			//$this->m_link_ejecutar_varios->set_eventos(array("onclick"=>$this->m_idventana."_link_ejecutar_varios_onclick()"));
			//$this->m_link_ejecutar_varios->eventos->add("onclick")->value = $this->m_idventana."_link_ejecutar_varios_onclick()";
			$this->m_link_ejecutar_varios->eventos->add("onclick")->value = "onclick()";

			//$contenidogrupo  = "Para los items seleccionados ejecutar: <br/>";
			//$contenidogrupo = "";
			//$contenidogrupo .= $this->m_combo_acciones->get_html();
			//$contenidogrupo .= "<br/>";
			//$contenidogrupo .= "&nbsp;&nbsp;";
			//$contenidogrupo .= $this->m_link_ejecutar_varios->get_html();
			//$contenidogrupo .= "<br/>";
			//$this->m_group_acciones->content = $contenidogrupo;
		}//else{
			//$this->m_group_acciones->content = "";
		//}

	}

    //-----------------------------------------------------------
    /**
    * Crea la fila de controles
    *
	* @access private
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	private function crear_fila_controles() //se refiere a la fila de controles del browse
	{
		$texto_fila="";

		//----------------------------------------
		//creando combobox nro de pagina
		$combo = new tsdk_combobox();
		$combo->set_id_ventana_control($this->m_idventana,"combo_nropagina");
		for ($i=1; $i<=$this->m_totalpaginas; $i++)
		{
			$subitem = new tsdk_lista_item($i,$i);
			if ($i==$this->m_nropagina)
			{
				$subitem->selected=true;
			}else{
				$subitem->selected=false;
			}
			$combo->add_item( $subitem );
		}
		//$combo->set_eventos(array("onchange"=>$this->m_idventana."_combo_nropagina_onchange()"));
		$combo->eventos->add("onchange")->value = "onchange()";

		//----------------------------------------
		//creando link anterior
		$imagen_anterior = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_prevpage.png","title"=>"Pagina Anterior","border"=>"0"));

		$linkanterior = new tsdk_link($this->m_idventana,"link_anterior");
		$linkanterior->texto = "Anterior";
		$linkanterior->propiedades->set_propiedades(array("href"=>"#"));

		if ($this->m_nropagina>1)
			$linkanterior->eventos->add("onclick")->value = "onclick(".($this->m_nropagina-1).")";
		else
			$linkanterior->eventos->add("onclick")->value = "onclick(1)";

		//----------------------------------------
		//creando link siguiente
		$linksiguiente = new tsdk_link($this->m_idventana,"link_siguiente");
		$linksiguiente->texto = "Siguiente";
		$linksiguiente->propiedades->set_propiedades(array("href"=>"#"));

		if ($this->m_nropagina < $this->m_totalpaginas)
			$linksiguiente->eventos->add("onclick")->value = "onclick(".($this->m_nropagina+1).")";
		else
			$linksiguiente->eventos->add("onclick")->value = "onclick(".$this->m_totalpaginas.")";

		$imagen_siguiente = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_nextpage.png","title"=>"Pagina Siguiente","border"=>"0"));

		//--------------------------------
		//creando el texto de la fila

		$texto_fila .= tsdk_reemplazar_html("Página ").$combo->get_html()." de ".$this->m_totalpaginas;
		$texto_fila .= " &nbsp;&nbsp;&nbsp;&nbsp;";

		$texto_fila .= $imagen_anterior->get_html();
		$texto_fila .= " ";
		$texto_fila .= $linkanterior->get_html();
		$texto_fila .= " &nbsp; ";

		$texto_fila .= $linksiguiente->get_html()." ";
		$texto_fila .= $imagen_siguiente->get_html();

		if ( count($this->m_lista_acciones)>0 )
		{
			$texto_fila .= "<div style='margin-left: 20px; float: right;' >";
			$texto_fila .= $this->m_combo_acciones->get_html();
			$texto_fila .= "&nbsp;&nbsp;";
			$texto_fila .= $this->m_link_ejecutar_varios->get_html();
			$texto_fila .= "</div>";
		}

		return $texto_fila;
	}

    //-----------------------------------------------------------
    /**
    * Crea la tabla 
    *
	* @access protected
    * @param  object    $tabladatos          Datos a mostrar en la tabla del tipo tsdk_datos_sql
	* @param  boolean   $habilitar_onclick_by_fila          Habilita el evento onclick por cada fila
	* @param  boolean   $habilitar_ondblclick_by_fila          Habilita el evento ondblclick por cada fila
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	protected function create_tabla($tabladatos, //tabla que contiene los datos a mostrar
										$habilitar_onclick_by_fila=false, //habilita el evento onclick
										$habilitar_ondblclick_by_fila=false)  //habilita el evento ondblclick
	{
		//creando la tabla
		$regilla = new tsdk_tabla($this->m_idventana,"tabla",
										$tabladatos->filas->count()+3, //nrofilas
										count($this->m_titulos_browse)+1,	//nrocolumnas
										array(	"class"=>"tvbrowse_tabla_class",
												"border"=>"0"
											)
									  );
		$linkordenar = new tsdk_link();

		//estableciendo la propiedad de encabezado y pie de tabla
		$regilla->nro_header=1;
		$regilla->nro_footer=2;

		//creando checkbox del encabezado
		$controlchk = new tsdk_textbox($this->m_idventana,"chktitulo");
		$controlchk->propiedades->set_propiedades(array("type"=>"checkbox",
														"class"=>"tvbrowse_chktitulo_class"
													));
		$controlchk->eventos->add("onclick")->value = "onclick()";

		//colocando el icono del orden en el titulo
		$titulos=$this->m_titulos_browse;

		foreach ($this->m_campos_browse as $clave=>$valor)
		{
			//creando link ordenar
			$linkordenar = new tsdk_link($this->m_idventana,"link_ordenar");

			$linkordenar->propiedades->set_propiedades(array("class"=>"tvbrowse_link_ordenar_class",
															"href"=>"#"));
			//$linkordenar->set_estilo("text-decoration","none");
			//$linkordenar->set_estilo("this:visited {color: #000000}",null);

			//agregando el icono
			if ($valor == $this->m_orderby)
			{
				if ( strtoupper($this->m_sentido)=="ASC" )
				{
					//estableciendo el evento
					$linkordenar->eventos->add("onclick")->value = "onclick('".$valor."','desc')";

					//modificando el titulo
					$titulos[$clave] .= " &#9660;"; //flecha abajo
				}else{
					//estableciendo el evento
					$linkordenar->eventos->add("onclick")->value = "onclick('".$valor."','asc')";

					//modificando el titulo
					$titulos[$clave] .= " &#9650;"; //flecha arriba
				}
			}else{
				$linkordenar->eventos->add("onclick")->value = "onclick('".$valor."','asc')";
			}

			//convirtiendo en link
			$linkordenar->texto = $titulos[$clave];
			$titulos[$clave] = $linkordenar->get_html();
		}

		//añadiendo el check del titulo
		if ( count($this->m_lista_acciones)>0 )
		{
			array_unshift( $titulos,$controlchk->get_html() ); //añadiendo el check del encabezado
		}else{
			array_unshift( $titulos, "" ); //añadiendo el check del encabezado
		}

		//colocando los titulos dentro de div
		//foreach($titulos as &$valor)
		//{
		//	$valor="<div>".$valor."</div>";
		//}

		//estableciendo valores al encabezado
		$regilla->set_valores_fila(0,$titulos);

		//validando que el numero de titulos_browse sea igual a la variable alineamiento
		if ( count($this->m_titulos_browse)-2 != count($this->m_alineamiento_columna) )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Los campos de alineamiento columna no es igual al numero de columnas de la tabla. Revisar la variable m_titulos_browse y m_alineamiento_columna".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			exit;
		}

		//estableciendo el alineamiento de los titulos
		for ($i=0; $i<count($this->m_titulos_browse)-2;$i++)
		{
			$regilla->set_estilos_celda(0,$i+1,
												array("text-align"=>$this->m_alineamiento_columna[$i]) 
												);
		}

		//fila divisora (mensaje)
		$regilla->set_propiedades_celda($tabladatos->filas->count()+1,0,array("colspan"=>$regilla->nro_columnas ));

		//fila de controles
		$regilla->set_propiedades_celda($tabladatos->filas->count()+2,0,array("colspan"=>$regilla->nro_columnas ));

		//rellenando los datos
		$nrofila = 1;
		foreach ($tabladatos->filas as $indice => $fila)
		{
			//creando los parametros de la clave primaria como json
			$listaclave=array();
			foreach ($this->m_claveprimaria as $valor)
			{
				//$listaclave[$valor] = $row[$valor];
				$listaclave[$valor] = $fila->item($valor)->value;
			}
			$parametros_clave=tsdk_reemplazar_html(json_encode($listaclave));

			//creando link modificar
			$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_modificar.png","title"=>"Modificar","border"=>"0"));

			$linkmodificar = new tsdk_link($this->m_idventana,"link_modificar");
			$linkmodificar->texto = $imagen->get_html();
			$linkmodificar->propiedades->set_propiedades(array(	"id"=>$this->m_idventana."_link_modificar",
													"name"=>$this->m_idventana."_link_modificar",
													"class"=>"tvbrowse_link_modificar_class",
													"href"=>"#"));
			$linkmodificar->eventos->add("onclick")->value = "onclick('".$parametros_clave."')";

			//creando link eliminar
			$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_eliminar.png","title"=>"Eliminar","border"=>"0"));

			$linkeliminar = new tsdk_link($this->m_idventana,"link_eliminar");
			$linkeliminar->texto = $imagen->get_html();
			$linkeliminar->propiedades->set_propiedades(array("class"=>"tvbrowse_link_eliminar_class",
																"href"=>"#"));
			$linkeliminar->eventos->add("onclick")->value = "onclick('".$parametros_clave."')";


			//creando checkbox de la fila
			$controlchk = new tsdk_textbox($this->m_idventana,"chkfila".$nrofila);
			$controlchk->propiedades->set_propiedades(array("type"=>"checkbox",
															"class"=>"tvbrowse_chkfila_class",
															"value"=>$parametros_clave));

			//estableciendo los datos de la fila
			if ( count($this->m_lista_acciones)>0 )
			{
				$valores_fila=array( $controlchk->get_html() ); //estableciendo primera columna
			}else{
				$valores_fila=array("");
			}

			//agregando valores de las columnas
			foreach ($this->m_campos_browse as $nrocolumna=>$valor)
			{
				//"<div style=\"width:200px; word-wrap: break-word;\" >".
				//."</div>"
				//$valores_fila[] = tsdk_reemplazar_html($row[$valor]);
				
				//verificando si el campo del browse esta presente en la tabla de datos
				if ($fila->key_exists($valor) == false)
				{
					$valores_fila[] = tsdk_reemplazar_html( "#ERROR-E00001# : VALOR NO ESTABLECIDO EN TABLA DE DATOS " );
				}else{
					$valores_fila[] = tsdk_reemplazar_html( $fila->item($valor)->value );
				}
				
				//estableciendo el alineamiento
				$regilla->set_estilos_celda($nrofila,$nrocolumna+1,
													array("text-align"=>$this->m_alineamiento_columna[$nrocolumna]) 
													);
			}

			//añadiendo link_modificar
			$valores_fila[] = $linkmodificar->get_html();
			$regilla->set_estilos_celda($nrofila,count($this->m_campos_browse)+1,
												array("text-align"=>"center") 
												);

			//añadiendo link_eliminar
			if ($this->m_showeliminar)
			{
				$valores_fila[] = $linkeliminar->get_html();
				$regilla->set_estilos_celda($nrofila,count($this->m_campos_browse)+2,
													array("text-align"=>"center") 
													);
			}else{
				$valores_fila[] = "";
			}

			//estableciendo el estilo que no selecciona la fila
			$regilla->set_propiedades_fila( $nrofila, array("class"=>"tvbrowse_tabla_noselected_class") );
			
			//estableciendo el numero de fila
			$regilla->set_propiedades_fila( $nrofila,array("data-nrofila"=>$nrofila));

			//añadiendo propiedad is_selected
			$regilla->set_propiedades_fila($nrofila,array("data-is_selected"=>"false"));

			//estableciendo la clave primaria
			$regilla->set_propiedades_fila($nrofila,array("data-clave"=>$parametros_clave));

			//estableciendo los eventos de la fila
			//$regilla->set_eventos_fila( $nrofila,
			//				array( "onclick"=>"fila_onclick(".$nrofila.",'".$parametros_clave."')",
			//					   "ondblclick"=>"fila_ondblclick('".$parametros_clave."')" 
			//					 )
			//				);

			//habilitando onclick 
			if ($habilitar_onclick_by_fila)
			{
				$regilla->filas->item($nrofila)->eventos->add("onclick")->value = "fila_onclick(".$nrofila.",'".$parametros_clave."')";
			}

			//habilitando ondblclick
			if ($habilitar_ondblclick_by_fila)
			{
				$regilla->filas->item($nrofila)->eventos->add("ondblclick")->value = "fila_ondblclick(".$nrofila.",'".$parametros_clave."')";
			}

			//estableciendo los valores de la fila
			$regilla->set_valores_fila($nrofila,$valores_fila);

			$nrofila++;
		}

		//-- creando la fila de mensajes
		$control_mensaje = new tsdk_span($this->m_idventana,"mensaje","&nbsp;");

		$control_mensaje->propiedades->set_propiedades(array(
															"class"=>"tvbrowse_mensaje_class"
															));


		if ($tabladatos->filas->count() == 0)
		{
			$control_mensaje->texto = "No existen items";
		}else{
			$control_mensaje->texto = "Nro de items: ".$tabladatos->filas->count()." / ".$this->m_totalregistros;
		}
		$regilla->set_valores_fila( $tabladatos->filas->count()+1, array( $control_mensaje->get_html()."</br>&nbsp;" ));


		//creando fila de controles
		$texto_fila = $this->crear_fila_controles();

		$regilla->set_valores_fila($regilla->nro_filas-1,
										 array($texto_fila));

		//devolviendo la regilla (tabla html con datos)
		return $regilla;
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion imprime o devuelve la ventana browse
    *
	* @access public
    * @return string 	Devuelve texto HTML
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	public function get_html_base($regilla) 
	{
		//-----------------------------------------------------------------
		//iniciando la impresion
		$salida = "";

		//-----------------------------------------------------------------
		//imprimiendo las opciones
		//$salida .="<br/>";
		$salida .= "<div style='height:30px;'>";

		$salida .= "<div style=\"margin-left: 20px; float: right;\" id='".$this->m_idventana."_divfiltro'>"."\n";
		$salida .= "Filtro: ".$this->m_textbox_filtro->get_html()."<br/>"."\n";
		$salida .= "</div>\n";

		if ($this->m_show_importar_exportar)
		{
			$salida .= "<div style=\"margin-left: 20px; float: right;\" id='".$this->m_idventana."_divimportar_exportar'>"."\n";
			$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_importar_exportar.png","title"=>"Importar/Exportar","border"=>"0"));
			$salida .= $imagen->get_html()."&nbsp;".$this->m_link_importar_exportar->get_html()."<br/>"."\n";
			$salida .= "</div>"."\n";
			//luis
		}

		if ($this->m_showreporte)
		{
			$salida .= "<div style=\"margin-left: 20px; float: right;\" id='".$this->m_idventana."_divreporte'>"."\n";
			$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_reporte.png","title"=>"Actualizar","border"=>"0"));
			$salida .= $imagen->get_html()."&nbsp;".$this->m_link_reporte->get_html()."<br/>"."\n";
			$salida .= "</div>"."\n";
		}

		$salida .= "<div style=\"margin-left: 20px; float: right;\" id='".$this->m_idventana."_divactualizar'>"."\n";
		$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_actualizar.png","title"=>"Actualizar","border"=>"0"));
		$salida .= $imagen->get_html()."&nbsp;".$this->m_link_actualizar->get_html()."<br/>"."\n";
		$salida .= "</div>"."\n";

		if ($this->m_showinsertar)
		{
			$salida .= "<div style=\"margin-left: 20px; float: right ;\" id='".$this->m_idventana."_divinsertar'>"."\n";
			$imagen = new tsdk_image("","",array("src"=>"images/tsdk/tsdk_insertar.png","title"=>"Insertar","border"=>"0"));
			$salida .= $imagen->get_html()."&nbsp;".$this->m_link_insertar->get_html()."<br/>"."\n";
			$salida .= "</div>"."\n";
		}

		$salida .= "<div style=\"font-weight: bold; float: left ;\" id='".$this->m_idventana."_divtitulo'>"."\n";
		$salida .= $this->m_subtitulo."\n";
		$salida .= "</div>"."\n";

		$salida .= "</div>";

		//-----------------------------------------------------------------
		//imprimiendo div tabla
		$salida .= "<div style=\"clear: left;\" id='".$this->m_idventana."_div1'>"."\n";
		$salida .= $regilla->get_html()."\n";
		$salida .= "</div>"."\n";

		//-----------------------------------------------------------------
		//imprimiendo controles ocultos
		$salida .= $this->m_textbox_nropagina->get_html(); //oculto
		$salida .= $this->m_textbox_orderby->get_html(); //oculto
		$salida .= $this->m_textbox_sentido->get_html(); //oculto
		$salida .= $this->m_textbox_idseleccionado->get_html(); //oculto
		$salida .= $this->m_textbox_parametros->get_html(); //oculto

		return $salida;

	}

    //-----------------------------------------------------------
    /**
	* Esta funcion crea el objeto EXCEL.
	* La siguiente funcion fue creada con PHPExcel, pero ya fue deprecada, se ha migrado a Phpspreadsheet. Por ello esta funcion se comenta
    *
	* @access public
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
	*/
	/*
	public function create_reporte_excel($tabladatos)
	{
		$this->m_objPHPExcel = new PHPExcel();

		//estilo negrita
		$estilo_negrita = array(
							'font' => array(
								'bold' => true
							)
						);
		$estilo_bordes = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN
								)
							)
						);


		// Set properties
		$this->m_objPHPExcel->getProperties()->setCreator("Test");
		$this->m_objPHPExcel->getProperties()->setLastModifiedBy("Test");
		$this->m_objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$this->m_objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$this->m_objPHPExcel->getProperties()->setDescription("Test document");

		// Activando la primera hoja
		$this->m_objPHPExcel->setActiveSheetIndex(0);

		// Rename sheet
		$this->m_objPHPExcel->getActiveSheet()->setTitle('Reporte');

		//colocando titulo de reporte
		$nrofila=1;
		$this->m_objPHPExcel->getActiveSheet()->SetCellValue('A'.$nrofila, $this->m_tituloreporte_excel);

		//combinando el titulo
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_objPHPExcel->getActiveSheet()->mergeCells($columnainicial.'1:'.$columnafinal.'1');

		//colocando negrita
		$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->applyFromArray($estilo_negrita);

		//centrando el titulo
		$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//colocando titulos de la tabla
		$nrofila=$nrofila+2;
		$columna='A';
		foreach ($this->m_titulos_reporte as $valor)
		{
			if ( strcasecmp($valor,"&nbsp;")!=0 )
				$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $columna.$nrofila, $valor);

			$columna++;
		}

		//colocando negrita
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_negrita);

		//colocando bordes
		$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);

		//rellenando los datos
		//$nrofila = 1;
		foreach ($tabladatos->filas as $indice => $fila)
		{
			$nrofila++;

			//agregando valores de las columnas
			$columna='A';
			foreach ($this->m_campos_reporte as $valor)
			{
				if ( strcasecmp($valor,"&nbsp;")!=0 )
				{
					//$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $columna.$fila, $row[$valor]);
					$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $columna.$nrofila, $fila->item($valor)->value );
				}
				
				$columna++;
			}

			//colocando bordes
			$columnainicial='A';
			$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
			$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);
		}

		//-- creando la fila de mensajes
		$nrofila=$nrofila+2;
		//if (count($tabladatos)==0)
		if ($tabladatos->filas->count()==0)
		{
			$this->m_objPHPExcel->getActiveSheet()->SetCellValue('A'.$nrofila,"No existen items");
		}else{
			$this->m_objPHPExcel->getActiveSheet()->SetCellValue('A'.$nrofila,"Nro de items: ".$tabladatos->filas->count() );
		}

		//combinando el mensaje
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_objPHPExcel->getActiveSheet()->mergeCells($columnainicial.$nrofila.':'.$columnafinal.$nrofila);

		//colocando autosize
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	} 
*/

    //-----------------------------------------------------------
    /**
	* Esta funcion crea el objeto EXCEL.
	* Nueva funcion creada en Phpspreadsheet
    *
	* @access public
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	public function create_reporte_excel($tabladatos)
	{		
		// creando la hoja excel
		$this->m_spreadsheet = new Spreadsheet();

		//estilo negrita
		$estilo_negrita = array(
							'font' => array(
								'bold' => true
							)
						);
		$estilo_bordes = array(
							'borders' => array(
								'allBorders' => array(
									'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
								)
							)
						);

		// Set document properties
		$this->m_spreadsheet->getProperties()->setCreator('Test')
			->setLastModifiedBy('Test')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
			->setKeywords('office 2007 openxml php')
			->setCategory('Test result file');

		// Activando la primera hoja
		$this->m_spreadsheet->setActiveSheetIndex(0);

		// Rename sheet
		$this->m_spreadsheet->getActiveSheet()->setTitle('Reporte');

		//colocando titulo de reporte
		$nrofila=1;
		$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila, $this->m_tituloreporte_excel);
		
		//combinando el titulo
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->mergeCells($columnainicial.'1:'.$columnafinal.'1');

		//colocando negrita
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->applyFromArray($estilo_negrita);

		//centrando el titulo
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//colocando titulos de la tabla
		$nrofila=$nrofila+2;
		$columna='A';
		foreach ($this->m_titulos_reporte as $valor)
		{
			if ( strcasecmp($valor,"&nbsp;")!=0 )
				$this->m_spreadsheet->getActiveSheet()->SetCellValue( $columna.$nrofila, $valor); //colocando el titulo

			//estableciendo autosize
			$this->m_spreadsheet->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

			//aumentando a la siguiente columna
			$columna++;
		}

		//colocando negrita
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_negrita);

		//colocando bordes
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);

		//rellenando los datos
		foreach ($tabladatos->filas as $indice => $fila)
		{
			$nrofila++;

			//agregando valores de las columnas
			$columna='A';
			foreach ($this->m_campos_reporte as $valor)
			{
				if ( strcasecmp($valor,"&nbsp;")!=0 )
				{
					$this->m_spreadsheet->getActiveSheet()->SetCellValue( $columna.$nrofila, $fila->item($valor)->value );
				}
				
				$columna++;
			}

			//colocando bordes
			$columnainicial='A';
			$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
			$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);

		}

		//-- creando la fila de mensajes
		$nrofila=$nrofila+2;
		if ($tabladatos->filas->count()==0)
		{
			$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila,"No existen items");
		}else{
			$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila,"Nro de items: ".$tabladatos->filas->count() );
		}

		//combinando el mensaje
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->mergeCells($columnainicial.$nrofila.':'.$columnafinal.$nrofila);

		//colocando autosize
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion crea el reporte (para importar/exportar datos)
    *
	* @access public
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
	*/
	
	public function create_reporte_exportar($tabladatos)
	{
		//FALTA ADECUAR PARA QUE FUNCIONE CON LA NUEVA VERSION DE SPREADSHEET (EXCEL)

		/*
		$this->m_objPHPExcel = new PHPExcel();

		//estilo negrita
		$estilo_negrita = array(
							'font' => array(
								'bold' => true
							)
						);
		$estilo_bordes = array(
							'borders' => array(
								'allborders' => array(
									'style' => PHPExcel_Style_Border::BORDER_THIN
								)
							)
						);


		// Set properties
		$this->m_objPHPExcel->getProperties()->setCreator("Test");
		$this->m_objPHPExcel->getProperties()->setLastModifiedBy("Test");
		$this->m_objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
		$this->m_objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
		$this->m_objPHPExcel->getProperties()->setDescription("Test document");

		// Activando la primera hoja
		$this->m_objPHPExcel->setActiveSheetIndex(0);

		// Rename sheet
		$this->m_objPHPExcel->getActiveSheet()->setTitle('Reporte');

		//iniciando la variable fila
		$nrofila=0;

		//colocando titulo de reporte
		//$fila=1;
		//$this->m_objPHPExcel->getActiveSheet()->SetCellValue('A'.$fila, $this->m_tituloreporte_excel);

		//combinando el titulo
		//$columnainicial='A';
		//$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		//$this->m_objPHPExcel->getActiveSheet()->mergeCells($columnainicial.'1:'.$columnafinal.'1');

		//colocando negrita
		//$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->applyFromArray($estilo_negrita);

		//centrando el titulo
		//$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//validando si existe datos
		if ($tabladatos->filas->count() == 0)
		{
			return;
		}

		//colocando titulos de la tabla
		$nrofila++;

		//$row = $tabladatos[0];
		$fila = $tabladatos->filas->item(0);
		$columna='A';
		foreach ($fila as $clave=>$campo)
		{
			$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $campo->value.$nrofila, $clave);
			$columna++;
		}

		//añadiendo columna final
		$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $columna.$nrofila, 'accion (insertar,modificar,eliminar)');

		//colocando negrita
		//$columnainicial='A';
		//$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		//$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$fila.':'.$columnafinal.$fila)->applyFromArray($estilo_negrita);

		//colocando bordes
		//$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$fila.':'.$columnafinal.$fila)->applyFromArray($estilo_bordes);


		//rellenando los datos
		//$nrofila = 1;
		foreach ($tabladatos as $fila)
		{
			$nrofila++;

			//agregando los valores de las columnas
			$columna='A';
			foreach ($fila as $clave=>$campo)
			{
				$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $campo->value.$nrofila, $valor);
				$columna++;
			}

			//añadiendo la accion
			$this->m_objPHPExcel->getActiveSheet()->SetCellValue( $columna.$nrofila, 'modificar');

		}


		//colocando autosize
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$this->m_objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		*/
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion imprime el archivo EXCEL 
    * La salida del reporte siempre es hacia la pantalla
    * 
	* @access public
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	function print_excel_base()
	{
		/*
		//LAS SIGUIENTES SENTENCIAS SE ADECUARON PARA QUE FUNCIONEN CON SPREADSHEET
		// Se manda el archivo al navegador web, con el nombre que se indica, en formato 2007
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte.xlsx"');
		header('Cache-Control: max-age=0');
		$this->m_objWriter = PHPExcel_IOFactory::createWriter($this->m_objPHPExcel, 'Excel2007');
		
		//ob_end_clean(); //comentado al usarse en OSX

		//$this->m_objWriter->save('php://output'); //reemplazado por la siguiente funcion, para evitar un error en php 7
		tsdk_SaveViaTempFile($this->m_objWriter);
		*/

		// Redirect output to a client’s web browser (Xls)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($this->m_spreadsheet, 'Xls');
		$writer->save('php://output');
		exit;

	}


}

?>
