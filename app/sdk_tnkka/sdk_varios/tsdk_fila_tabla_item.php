<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_estilos.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_eventos.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_celdas_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_celda_tabla_item.php");

//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_fila_tabla_item 
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$propiedades=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$estilos=null;
	
    /**
    * Representa los eventos de la fila
    * @var coleccion
    */
	public	$eventos=null;
	
    /**
    * Representa los eventos de la fila
    * @var coleccion
    */
	public	$celdas=null;
	
    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  integer    $nrocolumnas       Indica el numero de columnas que contiene la fila
    * @example "sample_fila_tabla_item.construct.txt"
    */
	function __construct($nrocolumnas=0)
	{
		//$this->m_valor = $valor;
		
		//iniciando las propiedades
		$this->propiedades = new tsdk_propiedades_html();
		
		//iniciando los estilos
		$this->estilos = new tsdk_estilos();
		
		//iniciando los estilos
		$this->eventos = new tsdk_eventos();
		
		//iniciando las celdas
		$this->celdas = new tsdk_celdas_tabla(); 
		
		//creando las celdas segun el numero de columnas
		while ($this->celdas->count() < $nrocolumnas) //faltan crear celdas
		{
			$this->celdas->add(null,new tsdk_celda_tabla_item());
		}
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __toString()
    {
		//return print_r($this->m_valor,true);
		return "";
    }
	
    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   Valor del item</br>
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		//case "value":
		//	return $this->m_valor;
		//	break;
		/*
		case "ancho":
		case "width":
			if (array_key_exists("width"))
				return $this->estilos->item("width")->value;
			else
				return null;
			break;
		
		case "alineacion-horizontal":
		case "text-align": //funciona para el div de la celda
			if (array_key_exists("text-align"))
				return $this->estilos->item("text-align")->value;
			else
				return null;
			break;
		*/
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   valor del item</br>
    *
    * @param	object	 $value        El valor a establecer en la propiedad
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		//case "value":
		//	$this->m_valor = $value;
		//	break;
		/*
		case "ancho":
		case "width":
			$this->estilos->add("width")->value = $value;
			break;
		
		case "alineacion-horizontal":
		case "text-align": //funciona para el div de la celda
			$this->estilos->add("text-align")->value = $value;
			break;
		*/
        case "idventana":
            $this->eventos->idventana = $value;
            break;
            
        case "idcontrol":
            $this->eventos->idcontrol = $value;
            break;

		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion permite acceder directamente a la coleccion.
    * Tiene el mismo nombre que la coleccion
    *
    * @access public
    * @param  string    $indice     Indice del item al cual acceder
    * @return item
    * 
    * @example "sample_tsdk_tabla.php"
    */
    public function &celdas($indice)
    {
        return $this->celdas->item($indice);
    }

    //-----------------------------------------------------------
    /**
    * crea las celdas, segun el numero de columna indicado
    *
    * @access public
    * @param  integer   $nrocolumnas      El numero de columnas que contiene la fila
    */
	public function create_celdas($nrocolumnas) //nro de columnas que deben tener las celdas
	{
		//creando las celdas segun el numero de columnas
		while ($this->celdas->count() < $nrocolumnas) //faltan crear celdas
		{
			$this->celdas->add(null,new tsdk_celda_tabla_item()); //añade celdas al final
		}
	}
	
}


