<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de estilos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

if (file_exists("../sdk_tnkka/sdk_varios/tsdk_coleccion.php"))
	include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_propiedades_html extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $texto          Se refiere al texto que aparece en el control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>disabled</b>       :   Devuelve si el control esta deshabilitado</br>
    *    <b>enabled</b>        :   Devuelve si el control esta habilitado</br>
    *    <b>title</b>          :   Devuelve el tooltip del control</br>
    *    <b>tabindex</b>       :   Devuelve el index del control</br>
    *    <b>id</b>             :   Devuelve el id del control (la union de idventana_idcontrol)</br>
    *    <b>name</b>           :   Devuelve el valor de name del control (la union de idventana_idcontrol)</br>
    *    <b>idcontrol</b>      :   Devuelve el id del control</br>
    *    <b>idventana</b>      :   Devuelve el id de la ventana que contiene el control</br>
    *    <b>handler_eventos</b>      :   Devuelve la funcion que atenderá de los eventos</br>
    *    <b>texto</b>                :   Devuelve el texto del control. Depende del control para determinar que representa el texto</br>
    *    <b>tipocontrol</b>          :   Devuelve el tipo de control</br>
    * @example "sample_tsdk_button.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "disabled":
            if ( $this->propiedades->key_exists($propiedad)  )
            {
                return true;
            }else{
                return false;
            }
            break;
        case "enabled":
            if ( $this->propiedades->key_exists('disabled')  )
            {
                return false;
            }else{
                return true;
            }
            break;
        case "title": //implementa el tooltip
        case "tabindex":
        case "id": //debe ser la union de idventana_idcontrol
        case "name": //debe ser la union de idventana_idcontrol
            if ( $this->propiedades->key_exists($propiedad)  )
            {
                return $this->propiedades($propiedad)->value;
            }else{
                return null;
            }
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>disabled</b>       :   Deshabilita el control. </br>
    *    <b>enabled</b>        :   Habilita el control</br>
    *    <b>title</b>          :   Establece el tooltip del control</br>
    *    <b>tabindex</b>       :   Establece el index del control</br>
    *    <b>id</b>             :   Establece el id del control (la union de idventana_idcontrol)</br>
    *    <b>name</b>           :   Establece el valor de name del control (la union de idventana_idcontrol)</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_button.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "disabled":
            if ($value==true){
                $this->m_propiedad[$propiedad] = null;
            }else{
                if ( $this->propiedades->key_exists($propiedad)) //ojo no usar isset
                {
                    unset($this->m_propiedad[$propiedad]);
                }
            }
            break;

        case "enabled": //propiedad creada por luis diaz
            if ($value==true){
                if ( $this->propiedades->key_exists('disabled')  )
                {
                    unset($this->m_propiedad['disabled']);
                }
            }else{
                $this->m_propiedad['disabled'] = null;
            }
            break;

        case "title": //implementa el tooltip
        case "tabindex":
			$this->propiedades->add($propiedad)->value = $value;
            break;
        case "name": //debe ser la union de idventana_idcontrol
        case "id":  //debe ser la union de idventana_idcontrol
            $lista = explode ("_",$value);

            $this->m_idventana = $lista[0];
            if (count($lista)>=2){
                $this->m_idcontrol = $lista[1];
                $this->m_idcontrol .= (isset($lista[2])?"_".$lista[2]:""); //agregando los demas textos
                $this->m_idcontrol .= (isset($lista[3])?"_".$lista[3]:""); //agregando los demas textos
                $this->m_idcontrol .= (isset($lista[4])?"_".$lista[4]:""); //agregando los demas textos
            }else
                $this->m_idcontrol="";

            $this->m_propiedad["id"] = $value;
            $this->m_propiedad["name"] = $value;
            break;
   
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece las propiedades del control
    * @access public
    * @param array $lista		Arreglo con la lista de propiedades a establecer
    * @return none
    * @example "sample_tsdk_control.php"
    */
    public function set_propiedades($lista)
    { 
		if (is_array($lista))
		{
			foreach ($lista as $clave=>$valor)
			{
				$this->add($clave)->value=$valor;
			}
		}
    }

    //-----------------------------------------------------------
    /**
    * Devuelve la lista (arreglo) de estilos
    * @access public
    * @return string         Texto que representa la lista de estilos en formato html
    * @example "sample_tsdk_control.php"
    */
    public function get_propiedades_text()
    { 
        $salida = "";

        if ( $this->count() > 0) //si el total de estilos es mayor a cero
        {
			
			//recorriendo cada elemento
			foreach ($this as $clave => $item) //cada item es del tipo tsdlk_item_coleccion
			{
                $salida = $salida.$clave;
                if (!is_null($item->value))
                {
                    $salida=$salida."='".$item->value."'";
                }
                $salida.=" ";
            }

        }

        return $salida;
    }

}

?>
