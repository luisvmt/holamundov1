<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de estilos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_eventos extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
	
    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa el id de la ventana de los controles que contiene el evento
    * @var string
    */
    protected		$m_idventana="";
	
    /**
    * Representa el id del control que contiene los eventos
    * @var string
    */
	protected		$m_idcontrol="";
	
    /**
    * Representa la funcion que atiende un evento de varios controles
    * @var string
    */
	protected		$m_handler_eventos=""; 

    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>idcontrol</b>      :   Devuelve el id del control</br>
    *    <b>idventana</b>      :   Devuelve el id de la ventana que contiene el control</br>
    * @example "sample_tsdk_button.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "idcontrol":
            return $this->m_idcontrol;
            break;
		
        case "idventana":
            return $this->m_idventana;
            break;
        
		case "handler_eventos": //establece el prefijo para los eventos
            return $this->m_handler_eventos;
            break;
		
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>idcontrol</b>      :   Establece el id del control</br>
    *    <b>idventana</b>      :   Establece el id de la ventana que contiene el control</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_button.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "idcontrol":
            $this->m_idcontrol = $value;
            break;
		
        case "idventana":
            $this->m_idventana = $value;
            break;
		
        case "handler_eventos":
            $this->m_handler_eventos = $value;
            break;
		
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }
 
    //-----------------------------------------------------------
    /**
    * Establece los estilos del control
    * @access public
    * @param array $lista		Arreglo con la lista de estilos a establecer
    * @return none
    * @example "sample_tsdk_control.php"
    */
    public function set_eventos($lista)
    { 
		if (is_array($lista))
		{
			foreach ($lista as $clave=>$valor)
			{
				$this->add($clave)->value=$valor;
			}
		}
    }
	
    //-----------------------------------------------------------
    /**
    * Devuelve la lista (arreglo) de eventos en formato texto
    * @access public
    * @return string         Texto que representa la lista de estilos en formato html
    * @example "sample_tsdk_control.php"
    */
    public function get_eventos_text()
    {
        $salida = "";

        if ( $this->count() > 0) //si el total de eventos es mayor a cero
        {
			//recorriendo cada elemento
			foreach ($this as $clave => $item) //cada item es del tipo tsdlk_item_coleccion
			{
                $salida = $salida.$clave;
                if (!is_null($item->value))
                {
                    $salida=$salida."=\"";

                    if ($this->m_handler_eventos == "") //si el handler es vacio, entonces se concatena el idventana y idcontrol
                    {
                        $salida .= "evento_"; //se añade la palabra evento al inicio de la definicion

                        if ($this->m_idventana != "")
                        {
                            $salida=$salida.$this->m_idventana."_";
                        }
                        if ($this->m_idcontrol != "")
                        {
                            $salida=$salida.$this->m_idcontrol."_";
                        }
                    }

                    $salida=$salida.$item->value;
                    $salida=$salida."\"";
                }
                $salida.=" ";
            }
        }

        return $salida;
    }
	
    //-----------------------------------------------------------
    /**
    * Añade un evento, el handler lo atendera. Antes de llamar a esta funcion se debe establecer el handler_eventos.
    * Cuando se usa esta funcion, ya no se utiliza idventana_idcontrol
    *
    * @access public
    * @param  string        $nombre_evento      Se refiere al evento que se añadira
    * @param  string        $parametros         Se refiere a los parametros que tendra la funciona
    * @return none
    * @example "sample_tsdk_control.php"
    */

	public function add_evento_by_handler($nombre_evento,$parametros="")
    {
        //$objeto = "document.getElementById('".$this->get_idventana()."_".$this->get_idcontrol()."')";

        if ($this->m_handler_eventos == "")
        {
            error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".
                    "No ha establecido el handler del control. ".
                    "Ventana:".$this->m_idventana.", ".
                    "Control:".$this->m_idcontrol );
            return;
        }

        //estableciendo el evento
		$this->add($nombre_evento)->value = $this->m_handler_eventos.
                                            "(this,'".$nombre_evento."','".$parametros."')";
    }
	
    //-----------------------------------------------------------
    /**
    * Añade un evento comun
    * @access public
    * @param  string        $nombre_evento      Se refiere al evento que se añadira
    * @param  string        $parametros         Se refiere a los parametros que tendra la funcion
    * @return none
    * @example "sample_tsdk_control.php"
    */
    public function add_evento_standard($nombre_evento,$parametros="")
    {
		//$this->add($nombre_evento)->value = $this->m_idventana."_".
        //                                    $this->m_idcontrol."_".
        //                                    $nombre_evento."(".
        //                                    $parametros.")";

        $this->add($nombre_evento)->value = $nombre_evento."(".$parametros.")";
    }

}

?>
