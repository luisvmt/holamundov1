<?php 
/**
 * Este archivo implementa las funciones comunes para una vista Importar-Exportar
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package varios
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_link.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_group.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_group.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_accordion.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php"); //funciones

include_once(APPPATH."../../sdk_terceros/excel/PHPExcel.php"); //excel
include_once(APPPATH."../../sdk_terceros/excel/PHPExcel/Writer/Excel2007.php"); // PHPExcel_Writer_Excel2007 

//-----------------------------------------------------------
/**
* Ventana estandar - Windows Importar Exportar
*
* Implementa una ventana estandar que implementa unav ventana importar-exportar
*
* @author luisvmt@hotmail.com
* @package varios
*/

class tsdk_window_importar_exportar
{
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa el ancho de la primera columna de la tabla que contiene la ventana
	* @var integer
	*/
	protected	$m_ancho_1racolumna;
	
	/**
	* Representa el ancho de la segunda columna de la tabla que contiene la ventana
	* @var integer
	*/
	protected	$m_ancho_2dacolumna;

	/**
	* Contiene el codigo javascript
	* @var string
	*/
	protected	$m_codigo_javascript;

	/**
	* Determina si se tiene permiso para importar/exportar
	* @var boolean
	*/
	protected	$m_permiso_importarexportar=false;
	
	/**
	* Establece el titulo de la ventana
	* @var string
	*/
	protected	$m_titulo="";
	
	/**
	* Determina el ID de la ventana
	* @var string
	*/
	protected	$m_idventana="";
	
	/**
	* Determina el objeto accordeon que compone la ventana
	* @var object
	*/
	protected	$m_accordeon;
	
	/**
	* Arreglo con la lista de campos
	* @var array
	*/
	protected	$m_lista_campos="";
	
	/**
	* Control HTML - span-mensaje
	* @var object
	*/
	protected	$m_mensaje; //tv_span
	
	/**
	* Control HTML - button cerrar
	* @var object
	*/
	protected	$m_cmdcerrar; //cmdcerrar
	
	/**
	* Control HTML - button seleccionar archivo
	* @var object
	*/
	protected   $m_cmdselect_importfile; //seleccionar el archivo a importar
	
	/**
	* Control HTML - button seleccionar archivo
	* @var object
	*/
	protected   $m_cmdimportfile; //importar archivo
	
	/**
	* Control HTML - button seleccionar archivo
	* @var object
	*/
	protected   $m_cmdexportfile; //exportar archivo
	
	/**
	* Control HTML - formulario que contiene las funciones para importar
	* @var object
	*/
	protected	$m_form_importar; //formulario para importar un archivo

	/**
	* Control HTML - Tabla que contiene cada accordeon
	* @var object
	*/
	protected	$m_tabla;  //contenido de cada acordeon

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  	string 		$id 		ID de la ventana
	* @example "sample_tsdk_window_importar_exportar.php"
	*/
	public function __construct($idventana)
	{
		$this->m_idventana=$idventana;

		//----- mensaje
		$this->m_mensaje = new tsdk_span();
		$this->m_mensaje->set_id_ventana_control($this->m_idventana,"mensaje");
		$this->m_mensaje->set_estilos(array("color"=>"#FF0000",
									   "font-size"=>"15px"));

		//------ controles
		$tabindex=0;

		/* //----- txtimportar
		$this->m_txtimportar = new tsdk_textbox();
		$this->m_txtimportar->config_as_text_disabled("",$tabindex++);
		$this->m_txtimportar->set_id_ventana_control($this->m_idventana,"txtimportar");
		*/

		//----- cmdselect_importfile
		$this->m_cmdselect_importfile = new tsdk_button();
		$this->m_cmdselect_importfile->set_id_ventana_control($this->m_idventana,"cmdimportfile");

		$this->m_cmdselect_importfile->propiedades->add("type")->value="file";
		$this->m_cmdselect_importfile->propiedades->add("tabindex")->value=$tabindex++;
		$this->m_cmdselect_importfile->propiedades->add("value")->value="Seleccionar Archivo";

		//----- cmdimportfile
		$this->m_cmdimportfile = new tsdk_button();
		$this->m_cmdimportfile->set_id_ventana_control($this->m_idventana,"cmdimportfile");

		$this->m_cmdimportfile->propiedades->add("type")->value="button";
		$this->m_cmdimportfile->propiedades->add("tabindex")->value=$tabindex++;
		$this->m_cmdimportfile->propiedades->add("value")->value="Importar Archivo";
		
		$this->m_cmdimportfile->eventos->add_evento_standard( "onclick" );

		//----- cmdexportfile
		$this->m_cmdexportfile = new tsdk_button();
		$this->m_cmdexportfile->set_id_ventana_control($this->m_idventana,"cmdexportfile");

		$this->m_cmdexportfile->propiedades->set_propiedades(array( "type"=>"button",
										 "tabindex"=>$tabindex++,
										 "value"=>"Exportar Archivo"));
		$this->m_cmdexportfile->eventos->add_evento_standard( "onclick" );

		//----- cmdcerrar
		$this->m_cmdcerrar = new tsdk_button();
		$this->m_cmdcerrar->set_id_ventana_control($this->m_idventana,"cmdcerrar_ie");

		$this->m_cmdcerrar->propiedades->set_propiedades(array( "type"=>"button",
										 "tabindex"=>$tabindex++,
										 "value"=>"Cerrar"));
		$this->m_cmdcerrar->estilos->add("width",new tsdk_coleccion_item("80px"));
		$this->m_cmdcerrar->eventos->add_evento_standard( "onclick" );

		//----- form-importar
		$this->m_form_importar = new tsdk_form("","",
												array("action"=>"xxx.php",
													"method"=>"post",
													"enctype"=>"multipart/form-data")
											);
		$this->m_form_importar->set_id_ventana_control($this->m_idventana,"form_importar");

		//lista de sub-tablas del acordeon
		$this->m_tabla=array();
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	public function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>permiso_importarexportar</b>      :   Establece si se tiene permiso para importar/exportar</br>
	*    <b>titulo</b>      		:   Establece el titulo de la ventana</br>
	*    <b>id</b>      			:   Establece el ID de la ventana</br>
	*    <b>lista_campos</b>     	:   Establece la lista de campos para importar/exportar</br>
	*
	* @example "sample_tsdk_window_importarexportar.php"
	*/
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "permiso_importarexportar":
			return $this->m_permiso_importarexportar;
			break;
		case "titulo":
			return $this->m_titulo;
			break;
		case "idventana":
			return $this->m_idventana;
			break;
		case "lista_campos":
			return $this->m_lista_campos;
			break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get. Propiedad no encontrada: ".
					$propiedad.". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			return null;
			break;
		}

	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>permiso_importarexportar</b>      :   Establece si se tiene permiso para importar/exportar</br>
	*    <b>titulo</b>      		:   Establece el titulo de la ventana</br>
	*    <b>id</b>      			:   Establece el ID de la ventana</br>
	*    <b>lista_campos</b>     	:   Establece la lista de campos para importar/exportar</br>
	*
	* @param string $value        El valor a establecer en la propiedad
	* @example "sample_tsdk_window_importarexportar.php"
	*/
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "permiso_importarexportar":
			$this->m_permiso_importarexportar = $value;
			break;
		case "titulo":
			$this->m_titulo = $value;
			break;
		case "idventana":
			$this->m_idventana = $value;
			break;
		case "lista_campos":
			$this->m_lista_campos=$value;
			break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en set. Propiedad no encontrada: ".
					$propiedad.". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			break;
		}
	}

	//-----------------------------------------------------------
	/**
	* Modifica el mensaje de la ventana
	*
	* @access public
	* @param  string    $mensaje          Mensaje de la ventana importar/exportar
	* @return none
	* 
	* @example "sample_tsdk_window_importar_exportar.php"
	*/
	public function set_mensaje($mensaje)
	{
		$this->m_mensaje->texto = $mensaje;
	}

	//-----------------------------------------------------------
	/**
	* Añade texto al mensaje de la ventana
	*
	* @access public
	* @param  string    $mensaje          Mensaje de la ventana importar/exportar
	* @return none
	* 
	* @example "sample_tsdk_window_importar_exportar.php"
	*/
	public function add_mensaje($mensaje)
	{
		$this->m_mensaje->texto = $this->m_mensaje->texto.$mensaje;
	}

	//-----------------------------------------------------------
	/**
	* Crea el control accordeon
	*
	* @access private
	* @return none
	*/
	private function crear_accordeon()
	{
		$this->m_accordeon = new tsdk_accordion($this->m_idventana,"accordeon_importar_exportar");
		$this->m_accordeon->set_clase("tv_accordion_class1");

		//----------------------------------------------------------------------------------------------

		//creando sub.tabla 1 (contenido de panel)
		$contadorfila=0;
		$nrocolumnas=2;
		$nropanel = 0;

		$this->m_accordeon->set_titulo_panel($nropanel,"<h3>IMPORTAR</h3>");

		$this->m_tabla[$nropanel] = new tsdk_tabla($this->m_idventana,"tabla-ie",
													50,2,
													array(	"border"=>"0",
															"class"=>"tvimportarexportar_class"
														));

		$this->m_tabla[$nropanel]->nro_header=0;
		$this->m_tabla[$nropanel]->nro_footer=0;

		$this->m_tabla[$nropanel]->set_propiedades_celda(0,0,array("style"=>"width: ".$this->m_ancho_1racolumna.";"));
		$this->m_tabla[$nropanel]->set_propiedades_celda(0,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));

		if ($this->m_permiso_importarexportar == true){
			$this->m_tabla[$nropanel]->set_valores_fila($contadorfila++,
												array("<input type='hidden' name='MAX_FILE_SIZE' value='".TAMANO_MAXIMO_FILE."'>".
														$this->m_cmdselect_importfile->get_html(),
														$this->m_cmdimportfile->get_html()
														));
			$this->m_tabla[$nropanel]->set_valores_fila($contadorfila++,
														array($this->m_lista_campos,""
														));
		}else{
			$this->m_tabla[$nropanel]->set_valores_fila($contadorfila++,
												array("",""));

		}

		$this->m_tabla[$nropanel]->set_dimensiones($contadorfila,$nrocolumnas,false);

		//colocando la tabla dentro del formulario
		$this->m_form_importar->content = $this->m_tabla[$nropanel]->get_html();

		//colocando el formulario dentro del accordeon
		$this->m_accordeon->set_content_panel($nropanel,$this->m_form_importar->get_html() );

		//----------------------------------------------------------------------------------------------

		//creando sub.tabla 2 (contenido de panel)
		$contadorfila=0;
		$nrocolumnas=2;
		$nropanel++;

		$this->m_accordeon->set_titulo_panel($nropanel,"<h3>EXPORTAR</h3>");

		$this->m_tabla[$nropanel] = new tsdk_tabla($this->m_idventana,"tabla-ie",
													50,2,
													array(	"border"=>"0",
															"class"=>"tvimportarexportar_class"
														));

		$this->m_tabla[$nropanel]->nro_header=0;
		$this->m_tabla[$nropanel]->nro_footer=0;

		$this->m_tabla[$nropanel]->set_propiedades_celda(0,0,array("style"=>"width: ".$this->m_ancho_1racolumna.";"));
		$this->m_tabla[$nropanel]->set_propiedades_celda(0,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));

		if ($this->m_permiso_importarexportar == true){
			$this->m_tabla[$nropanel]->set_valores_fila($contadorfila++,array("Para exportar dar click en:", $this->m_cmdexportfile->get_html() ));
		}else{
			$this->m_tabla[$nropanel]->set_valores_fila($contadorfila++,array("",""));

		}

		$this->m_tabla[$nropanel]->set_dimensiones($contadorfila,$nrocolumnas,false);

		$this->m_accordeon->set_content_panel($nropanel,$this->m_tabla[$nropanel]->get_html());

	}

	//-----------------------------------------------------------
	/**
	* Crea el control div-principal
	*
	* @access private
	* @return none
	*/
	private function get_divprincipal()
	{
		define('TABLA_FILA_TITULO',0);
		define('TABLA_FILA_CONTENIDO',1);
		define('TABLA_FILA_2',2);
		define('TABLA_FILA_MENSAJE',3);

		define('TABLA_NROFILAS',4);
		define('TABLA_NRO_COLUMNAS',2);

		//--- validando permiso
		if ($this->m_permiso_importarexportar == false)
		{
			$this->set_mensaje('No tiene permisos para importar registros');
		}else{
			$this->set_mensaje('');
		}

		//--- creando div principal
		$divprincipal = new tsdk_div($this->m_idventana,
									 "divprincipal-ie"   //array("id"=>"divprincipal_".$this->m_idventana));
									);
		$divprincipal->set_estilos(array("float"=>"left","margin-left"=>"20px"));

		//----------------------------------------------------------------------------------------------
		//-- definiendo la tabla-general
		//-- es una tabla compuesta de: 2 encabezados, 1 fila de contenido, 2 filas de pies = 5 filas
		$nrofilas=TABLA_NROFILAS;
		$nrocolumnas=TABLA_NRO_COLUMNAS;
		$tabla_general = new tsdk_tabla($this->m_idventana,"tabla-parent",
										$nrofilas,$nrocolumnas,
									  array(//"id"=>"tabla_".$this->m_idventana."_parent",
											"border"=>"0"
											));

		$tabla_general->nro_header=2;
		$tabla_general->nro_footer=2;

		$tabla_general->set_propiedades_celda(TABLA_FILA_TITULO,0,array("class"=>"tvimportarexportar_titulo_class",
														"colspan"=>"2",
														"align"=>"center"));  //titulo

		$tabla_general->set_propiedades_celda(TABLA_FILA_CONTENIDO,0,array("colspan"=>"2"));  //contenido

		$tabla_general->set_propiedades_celda(TABLA_FILA_MENSAJE,0,array("colspan"=>"2","style"=>"width: ".$this->m_ancho_1racolumna.";"));  //mensaje
		$tabla_general->set_propiedades_celda(TABLA_FILA_MENSAJE,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));  //mensaje

		//estableciendo titulo a tabla-general
		$tabla_general->set_valores_fila(0,array($this->m_titulo));

		//creando el accordeon
		$this->crear_accordeon();
		$tabla_general->add_valores_fila(TABLA_FILA_CONTENIDO, array($this->m_accordeon->get_html()) );

		//creando el mensaje
		$tabla_general->set_valores_fila(TABLA_FILA_MENSAJE,array($this->m_mensaje->get_html()));

		//---------------------------------------------

		//-- añadiendo al divrecordset
		$divprincipal->content = "<br/>".$tabla_general->get_html();

		return $divprincipal;
	}

	//-----------------------------------------------------------
	/**
	* Crea el control div-controles
	*
	* @access private
	* @return none
	*/
	private function get_divcontroles()
	{
		$divcontroles = new tsdk_div($this->m_idventana,
									"divcontroles-ie" //array("id"=>"divcontroles_".$this->m_idventana));
								);
							
		$divcontroles->set_estilos(array("margin-left"=>"20px","float"=>"left"));

		//-- añadiendo al divcontroles
		$divcontroles->content = $divcontroles->content . "</br></br></br></br></br></br></br>";

		$divcontroles->content = $divcontroles->content . $this->m_cmdcerrar->get_html();

		return $divcontroles;
	}

	//-----------------------------------------------------------
	/**
	* Devuelve el texto HTML que implementa la ventana
	* @access public
	* @return string                  Devuelve el texto HTML del control
	* @example "sample_tsdk_window_importar_exportar.php"
	*/
	public function get_html()
	{

		$divprincipal = $this->get_divprincipal();
		$divcontroles = $this->get_divcontroles();

		return $this->m_codigo_javascript.$divprincipal->get_html().$divcontroles->get_html();

	}

	//-----------------------------------------------------------
	/**
	* Imprime en salida estandar el texto HTML que implementa el control
	* @access public
	* @return none
	* @example "sample_tsdk_window_importar_exportar.php"
	*/
	public function print_html()
	{
		 echo $this->get_html();
	}

}

?>
