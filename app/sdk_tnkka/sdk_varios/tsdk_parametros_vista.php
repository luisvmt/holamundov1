<?php 
/**
 * Este archivo implementa las propiedades utilizadas en todas las clases de tsdk
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades.php");

//-----------------------------------------------------------
/**
* Propiedades
*
* Implementa las propiedades utilizadas por las funciones del TSDK
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_parametros_vista
{
	// ******************************
	// variables miembro
	// ******************************
	
    //-----------------------------------------------------------
    /**#@+
    * @access private
    */
    
    /**
    * Guarda una lista de los parametros que se pasa a la vista. Es un arreglo de datos
	* @var array
    */

	private 	$m_parametros=array();
	
    /**#@-*/


	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @example "sample_tsdk_parametros_vista.php"
	*/
	function __construct()
	{
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a devolver
	* @example "sample_tsdk_parametros_vista.php"
	*/
	public function __get($propiedad)
	{
		//verificando si existe la variable
		if (isset($this->m_parametros[$propiedad]))
			return $this->m_parametros[$propiedad];
		else{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get.".
				 "  Propiedad no encontrada: [".$propiedad."]".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
			return null;
		}
	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a establecer
	* @param string $value          El valor de la propiedad
	* @example "sample_tsdk_parametros_vista.php"
	*/
	public function __set($propiedad, $value)
	{
		$this->m_parametros[$propiedad] = $value;
	}


}

?>
