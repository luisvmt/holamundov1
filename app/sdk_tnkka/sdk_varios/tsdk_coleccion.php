<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de objetos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_coleccion implements Iterator 
{	
    // ******************************
    // variables miembro
    // ******************************
	
    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa la clave del item seleccionado
    * @var string
    */
    protected $m_clave_actual = null;
	
    /**
    * Representa la lista de items de la coleccion
    * @var array
    */
	protected $m_lista=array();

    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		$this->m_clave_actual = null;
		$this->m_lista = array();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa la funcion que inicia el puntero al inicio del arreglo
    *
    */
    public function rewind() 
	{
		if (count($this->m_lista)==0)
		{
			$this->m_clave_actual=null;
			return;
		}
		
		//reiniciando el contador
		reset($this->m_lista);
		
		//hallando la clave actual
		$this->m_clave_actual = $this->key();
    }

    //-----------------------------------------------------------
    /** 
    * Implementa la funcion que devuelve el elemento actual del arreglo
    *
    */
    function current() 
	{
		if ( is_null($this->m_clave_actual) )
		{
			return null;
		}
		
        return current($this->m_lista);
    }

    //-----------------------------------------------------------
    /** 
    * Implementa la funcion que devuelve la clave actual
    *
    */
    function key() 
	{
        return key($this->m_lista);
		//return $this->m_clave_actual;
    }

    //-----------------------------------------------------------
    /** 
    * Avanza a la siguiente posicion de la coleccion
    *
    */
    function next() 
	{
		//avanzando el puntero
        $respuesta = next($this->m_lista);
		
		//hallando la clave nueva
		if ($respuesta == false) //no es una posicion valida
		{
			$this->m_clave_actual = null;
		}
    }

    //-----------------------------------------------------------
    /** 
    * Valida si el puntero se encuentra en una posicion valida de la coleccion
    *
    */
    function valid() 
	{
		if ( is_null($this->m_clave_actual) )
		{
			return false;
		}else{
			return true;
		}
    }
	
    //-----------------------------------------------------------
    /** 
     * Añade un elemento a la coleccion
     * @param  string    $clave		Es la clave del elemento a añadir
     * @param  object    $objeto		Es el objeto a añadir a la coleccion
	 * 
	 * @return object	Una referencia al objeto añadido
     * @example "sample_tsdk_coleccion.add.txt"
     */
    function & add($clave=null,$objeto=null) 
	{
		if ( is_null($clave) ) //significa que el indice sera numerico
		{
			if ( is_null($objeto)) //si es nulo, crea un item del tipo tsdk_coleccion_item
			{
				$this->m_lista[] = new tsdk_coleccion_item($objeto);
			}else{
				$this->m_lista[] = $objeto;
			}

			end($this->m_lista);
			$clave = $this->key();
			
			//devolviendo una referencia
			return $this->m_lista[$clave];
		}
		
		//si la clave es texto
	
		if ( !array_key_exists($clave,$this->m_lista)) //creando el item, solo si el objeto es null
		{
			if ( is_null($objeto))
			{
				$this->m_lista[$clave] = new tsdk_coleccion_item($objeto); //se crea un elemento por defecto
				return $this->m_lista[$clave];
				
			}else{ //crea el elemento si la clave es texto y el objeto no es nulo
				$this->m_lista[$clave] = $objeto;
				
			}
		}
		
		//devolviendo una referencia
		return $this->m_lista[$clave]; 
    } 
	
    //-----------------------------------------------------------
    /** 
    * Elimina todos los elementos de la coleccion
    *
    * @example "sample_tsdk_coleccion.php"
    */
    function delete_all()
	{
		$this->m_lista=array();
		$this->m_clave_actual=null;
    }
	
    //-----------------------------------------------------------
    /** 
    * Elimina un elemento de la coleccion
    * @param  string    $clave		Es la clave del elemento a eliminar
    * @example "sample_tsdk_coleccion.delete.txt"
    */
    function delete($clave)
	{
		if ( array_key_exists($clave,$this->m_lista) )
			unset($this->m_lista[$clave]);
		
		if ($this->m_clave_actual == $clave)
		{
			$this->rewind();
		}
    }
	
    //-----------------------------------------------------------
    /** 
    * Elimina un elemento de la coleccion (alias de delete)
    * @param  string    $clave		Es la clave del elemento a eliminar
    * @example "sample_tsdk_coleccion.delete.txt"
    */
    function remove($clave)
	{
		return $this->delete($clave);
    }

    //-----------------------------------------------------------
    /** 
    * Devuelve el numero de elementos
    *
    * @example "sample_tsdk_coleccion.php"
    */
    function count()
	{
		return count($this->m_lista);
    }
	
    //-----------------------------------------------------------
    /** 
     * Devuelve una referencia a un elemento de la coleccion
     * @param  string    $clave		Es la clave del elemento
	 * 
	 * @return object	Una referencia al objeto añadido
     * @example "sample_tsdk_coleccion.item.txt"
     */
    function & item($clave)
	{
        $variable_nulo = null;

        //evaluando si el item existe
        if ( !array_key_exists($clave,$this->m_lista) ) //si no existe el item devuelve nulo
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                 "#x No existe el item [ ".$clave." ] en la coleccion. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return $variable_nulo;
        }
		return $this->m_lista[$clave];
    }
	
    //-----------------------------------------------------------
    /** 
     * Devuelve una referencia a un elemento de la coleccion
     * @param  string    $clave		Es la clave del elemento
	 * 
	 * @return object	Una referencia al objeto añadido
     */
    function & __invoke($clave)
	{
		//if (!isset($this->m_lista[$clave])) //creando el item
		if ( !array_key_exists($clave,$this->m_lista) ) //si no existe el item devuelve nulo
		{
			//$this->m_lista[$clave] = new tsdk_coleccion_item();
			return null;
		}
		
		//devolviendo una referencia
		return $this->m_lista[$clave]; 
    }
	
    //-----------------------------------------------------------
    /** 
     * Devuelve un valor boolean si el elemento existe
     * @param  string    $clave		Es la clave del elemento
	 * 
	 * @return boolean   Devuelve true si el elemento existe, false si el elemento no existe
     * @example "sample_tsdk_coleccion.key_exists.txt"
     */
    function key_exists($clave)
	{
		return array_key_exists($clave,$this->m_lista); //ojo no usar isset
    }
	
}

?>
