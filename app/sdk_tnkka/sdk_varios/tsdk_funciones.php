<?php
/**
 * Este archivo implementa funciones comunes a SDK
 *
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package funciones
 */ 

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

//include_once(APPPATH."../../objetos/configuracion_app/constantes.php"); //constantes
include_once(APPPATH."../../sdk_terceros/excel/PHPExcel.php");

include_once(APPPATH."../../sdk_terceros/excel/PHPExcel/Writer/Excel2007.php"); // PHPExcel_Writer_Excel2007
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades.php");

include_once(APPPATH."../../sdk_terceros/phpmailer_src/PHPMailer.php");
include_once(APPPATH."../../sdk_terceros/phpmailer_src/SMTP.php");
include_once(APPPATH."../../sdk_terceros/phpmailer_src/Exception.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//-----------------------------------------------------------
/**
* Funciones comunes
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package funciones
*/

//-----------------------------------------------------------
/**
* Obtiene datos desde un excel
* OJO: ex obtener_datos_excel
*
* @param  string     $archivo 			Nombre del archivo a leer
*/
function tsdk_obtener_datos_excel($archivo)
{
	//abriendo el archivo
	$num_campos_con_valor=0;
	$numfila=0;
	$numcolumna=0;
	$total_columnas=0;
	$tabla_datos = array();
	$lista_nombres=array();
	$fila=array();

	$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	$objReader->setReadDataOnly(true);

	$objPHPExcel = $objReader->load($archivo);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	foreach ($objWorksheet->getRowIterator() as $row)
	{
		if ($numfila==0)
		{
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,

			$nrocolumna=0;
			foreach ($cellIterator as $cell)
			{
				$valor=trim($cell->getValue());
				if ( $valor=="" )
				{
					break;
				}else{
					$fila[$valor]=null;
					$lista_nombres[$nrocolumna]=$valor;
				}
				$nrocolumna++;
			}
			$total_columnas=$nrocolumna;

		}else	{
			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false); // This loops all cells,

			$nrocolumna=0;
			$num_campos_con_valor=0;

			foreach ($cellIterator as $cell)
			{
				$valor=trim($cell->getValue());
				if ($valor!="")
				{
					$num_campos_con_valor++;
				}
				$fila[ $lista_nombres[$nrocolumna] ]=$valor;
				$nrocolumna++;

				if ($nrocolumna>=$total_columnas)
				{
					break;
				}
			}

			if ($num_campos_con_valor<=1)
			{
				break; //fin de carga de registros
			}else{
				$tabla_datos[] = $fila;
			}
		}

		$numfila++;
	}
	return $tabla_datos;

}

//-----------------------------------------------------------
/**
* Evalua si el texto es del tipo utf8
* OJO: ex tsdk_is_valid_utf8
*
* @param  string     $text 			Texto a evaluar
*/
function tsdk_is_valid_utf8($text) {
    return (bool)preg_match('//u', serialize($text));
}

//-----------------------------------------------------------
/**
* Reemplaza caracteres especiales
* OJO: ex tsdk_reemplazar_html
*
* @param  string     $mensaje 			Texto a reemplazar
*/
function tsdk_reemplazar_html($mensaje)
{
	//error_log("inicio de reemplazar");
	//error_log($mensaje);
    //$mensaje = utf8_encode($mensaje); //arreglado para mac
	//error_log("-------Detectando el tipo de enconding:");
    //error_log($mensaje);

    if (!tsdk_is_valid_utf8($mensaje))
    {
        $mensaje = utf8_encode($mensaje);
    }

	$mensaje = str_replace( array("á","Á"),
						    array("&aacute;","&Aacute;"),
							$mensaje);
	$mensaje = str_replace( array("é","É"),
						    array("&eacute;","&Eacute;"),
							$mensaje);
	$mensaje = str_replace( array("í","Í"),
						    array("&iacute;","&Iacute;"),
							$mensaje);
	$mensaje = str_replace( array("ó","Ó"),
						    array("&oacute;","&Oacute;"),
							$mensaje);
	$mensaje = str_replace( array("ú","Ú"),
						    array("&uacute;","&Uacute;"),
							$mensaje);
	$mensaje = str_replace( array("ñ","Ñ"),
						    array("&ntilde;","&Ntilde;"),
							$mensaje);
	$mensaje = str_replace("\"","&quot;",$mensaje);

	//error_log($mensaje);
	//error_log("fin de reemplazar");

	return $mensaje;
}

//-----------------------------------------------------------
/**
* Guarda la salida de un excel
*
* @param  object     $objWriter 			Objeto Excel a grabar
*/
function tsdk_SaveViaTempFile($objWriter){
	error_log("paso11");

    $filePath = '/tmp/' . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
	error_log("paso12:".$filePath);
	
	$objWriter->save($filePath);
	error_log("paso13");
	
	readfile($filePath);
	error_log("paso15");
	
	unlink($filePath);
	error_log("paso16");

}

//-----------------------------------------------------------
/**
* Graba en archivo log, imprime tambien un backtrace del pedido.
*
* @param  string 	$nombre_archivo 	Nombre del archivo log
* @param  string    $texto  			Texto a grabar en el archivo
*/
function tsdk_error_log($nombre_archivo,$texto)
{
	//sino se registra el nombre del archivo, el error se dirige al tracelog x defecto
    if (strcmp($nombre_archivo,"")==0)
    {
		if (!isset(tsdk_propiedades::$TRACELOG)){
			error_log("(No se definio tsdk_propiedades::TRACELOG) Mensaje:".$texto);
			return;
		}

		if ( tsdk_propiedades::$TRACELOG == ""){
			error_log("(No se definio tsdk_propiedades::TRACELOG) Mensaje:".$texto);
			return;
		}

    	tsdk_error_log(tsdk_propiedades::$TRACELOG,$texto); //llamada recursiva
		return;
    }

    //verificando que el nombre de archivo tenga el directorio donde grabar
    $solo_nombre = basename($nombre_archivo);
    if ( strcmp($solo_nombre,$nombre_archivo)==0 )
    {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                     "#x "."Al llamar a la funcion tsdk_error_log no ha agregado el directorio donde se grabará el archivo:".$nombre_archivo.
                     ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                            "\n"
                    );
            return;
    }

    ob_start(); //Iniciando la captura del buffer
    echo "\n----------\n".$texto."----------\n"; //escribiendo el texto

    debug_print_backtrace();  //generando trace

    $trace = ob_get_contents(); //obteniendo el contenido
    ob_end_clean();  //finalizando la captura

    //generando el texto a grabar
    $datos = "[ ".date("d.m.Y H:m:s")." ] ".$trace. "\n";

    //verificando si el archivo existe, sino existe lo crea
    if (!file_exists($nombre_archivo))
    {
            $controlador = fopen($nombre_archivo,"w");

            if ($controlador == false)
            {
                    //error creando el archivo
                    error_log("Error creando el archivo: [".$nombre_archivo."] Verifique los permisos.");

                    //print_r(error_get_last());
                    return;
            }

            fclose($controlador);
    }

    //grabando en el archivo log
    file_put_contents($nombre_archivo,$datos,FILE_APPEND);
}


//-----------------------------------------------------------
/**
* Escribe una variable en un archivo TRACE. El archivo trace se localiza en /tmp/tracelog_tsdk.
*
* @param  string     $nombre_variables1   Nombre de la primera variable
* @param  object     $variables1          Variable a imprimir
* @param  string     $archivo             Nombre del archivo donde escribir la variable
* @param  integer    $nrolinea            Nro de linea
*/
function tsdk_error_log_trace($nombre_variables1,$variables1,$archivo="",$nrolinea=0)
{
    //abriendo el archivo
	//$fp = fopen(PATHLOG.'/backtrace.log','a');
    //$fp = fopen("/tmp/tracelog_tsdk",'w');

	if (tsdk_propiedades::$TRACELOG == "")
	{
		error_log("No ha inicializado la variable: tsdk_propiedades::$TRACELOG");
		return;
	}

    $fp = fopen(tsdk_propiedades::$TRACELOG,'a+');

    //hallando fecha y hora
    $fecha = "[ ".date("d.m.Y H:m:s")." ] \n";
    fwrite($fp, $fecha);

	$mensaje = "************ [INICIO] ".$nombre_variables1." **************\n";
    fwrite($fp, $mensaje);

    if ($archivo != "")
    {
        $mensaje = "Archivo   : ".$archivo."\n";
        fwrite($fp, $mensaje);

        $mensaje = "Nro. Linea: ".$nrolinea."\n";
        fwrite($fp, $mensaje);
    }

	$mensaje = "\n".print_r($variables1,true)."\n";
	fwrite($fp, $mensaje);

	//escribiendo la separacion
	$mensaje = "\n";
    fwrite($fp, $mensaje);

	//cerrando el archivo
	fclose($fp);
}

//-----------------------------------------------------------
/**
* Crea un archivo con el nombre indicado y conteniendo el dato indicado.
*
* @param  string 	$nombre_archivo 	Ruta y nombre del archivo a crear
* @param  string    $datos  			Texto a grabar en el archivo
* @return integer 						Devuelve 0 si es OK, devuelve -1 si error.
*/
function tsdk_print_in_file($nombre_archivo,$datos)
{
	$controlador = fopen($nombre_archivo,"w");

	if ($controlador == false)
	{
		//error creando el archivo
		error_log("Error creando el archivo: [".$nombre_archivo."] Verifique los permisos.");

		//print_r(error_get_last());
		return -1;
	}

	//escribiendo en el archivo
	fwrite($controlador,$datos);

	//cerrando el archivo
	fclose($controlador);
	
	return 0;
}

//-----------------------------------------------------------
/**
* Envia correo electronico.
* Probado con zoho.com
* 
* @param  string 	$direccion_correo 	Direccion de correo a donde enviar mensaje
* @param  string    $subject  			Indica el subject a enviar
* @param  string    $cuerpo  			Indica el cuerpo del mensaje a enviar
* @return  								Devuelve 0 si es OK, devuelve 'string' con el mensaje de error en caso de error
*/
function tsdk_enviar_correo($direccion_correo, $subject, $cuerpo)
{
	$mail = new PHPMailer();

	//Luego tenemos que iniciar la validación por SMTP:
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
 
	$mail->Host = tsdk_propiedades::$SMTP_SERVER; // SMTP a utilizar. Por ej. smtp.elserver.com
	$mail->Username = tsdk_propiedades::$SMTP_USER; // Correo completo a utilizar
	$mail->Password = tsdk_propiedades::$SMTP_PASSWORD; // Contraseña

	// Enable TLS encryption over port 587
	$mail->SMTPSecure = tsdk_propiedades::$SMTP_SECURE;
	$mail->Port = tsdk_propiedades::$SMTP_PORT;
	$mail->Timeout = 20;

	//Con estas pocas líneas iniciamos una conexión con el SMTP. Lo que ahora deberíamos hacer, es configurar el mensaje a enviar, el //From, etc.
	$mail->From = tsdk_propiedades::$SMTP_FROM; // Desde donde enviamos (Para mostrar)
	$mail->FromName = tsdk_propiedades::$SMTP_FROMNAME;

	//Estas dos líneas, cumplirían la función de encabezado (En mail() usado de esta forma: “From: Nombre <correo@dominio.com>”) de //correo.
	$mail->AddAddress($direccion_correo); // Esta es la dirección a donde enviamos
	$mail->IsHTML(true); // El correo se envía como HTML
	$mail->Subject = $subject; // Este es el titulo de"l email.

	$mail->Body = $cuerpo; // Mensaje a enviar
	$mail->AltBody = $cuerpo;

	$exito = $mail->Send(); // Envía el correo.

	//También podríamos agregar simples verificaciones para saber si se envió:
	if($exito){
		return 0;
	}else{
		$mensaje = $mail->ErrorInfo;
		return $mensaje;
	}

	return 0;
}


//-----------------------------------------------------------
/**
 * Obtiene una cadena aleatoria de cierto tamaño
 *
 * @param  int        $len 			Tamaño del texto a obtener
 * @param  string     $chars         Texto de donde saca las letras
 */
function tsdk_rand_string($len, $chars = 'qwertyuiopasdfghjklzxcvbnm1234567890') {
	$string = '';
	for ($i = 0; $i < $len; $i++) {
		$pos = rand(0, strlen($chars) - 1);
		$string .= $chars{$pos};
	}
	return $string;
}

//-----------------------------------------------------------
/**
 * Convierte el valor recibido de un control HTML hacia un valor de mysql
 *
 * @param  string     $valor   Es el valor a convertir para mysql. Si el valor es "ON" se convierte en TRUE, sino en FALSE
 */
function tsdk_convert_checkbox_to_boolean($valor) 
{
	$salida = true;
	if (strtoupper(trim($valor)) == "ON")
	{
		$salida  = true;
	}else{
		$salida  = false;
	}
	
	return $salida;
}

//-----------------------------------------------------------
/**
 * Gestiona los errores fatales
 *
 */
/*function tsdk_gestor_shutdown_errores()
{
    $error = error_get_last();
//check if it's a core/fatal error, otherwise it's a normal shutdown
    if ($error !== NULL && in_array($error['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING,E_RECOVERABLE_ERROR)))		{
		error_log($error);
        die;
    }
}*/

//-----------------------------------------------------------
/**
 * Gestiona los errores de ejecucion, volcando los errores hacia un archivo
 *
 * @param  int        $errno     El numero de error
 * @param  string     $errstr    El texto del error
 * @param  string     $errfile   El archivo que contiene el error
 * @param  string     $errline   Es el numero de linea donde se produce el error
 */
function tsdk_gestor_errores($errno, $errstr, $errfile, $errline)
{
	$linea_error = "";
	
    if (!(error_reporting() & $errno)) {
        // Este código de error no está incluido en error_reporting
		$linea_error =  "ERROR-X. Error no manejado dentro de tsdk_gestor_errores. [".$errno."] ".$errstr."\n".
						"  Error en el archivo ".$errfile.", en la linea:".$errline.
						", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n";

		tsdk_error_log(tsdk_propiedades::$TRACELOG,
						"#x ".$linea_error.
						", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
						   "\n"
					   );
        return;
    }

    switch ($errno) {
    case E_USER_ERROR:
        $linea_error =  "USER_ERROR-X. [".$errno."] ".$errstr."\n".
						"  Error fatal en la línea ".$errline." en el archivo ".$errfile.
						", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n".
						"Abortando...\n";
		
		tsdk_error_log(tsdk_propiedades::$TRACELOG,
						"#x ".$linea_error.
						", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
						   "\n"
					   );
        exit(1);
        break;

    case E_USER_WARNING:
        $linea_error = "WARNING-X. [".$errno."] ".$errstr."\n".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;

    case E_USER_NOTICE:
        $linea_error = "NOTICE-X. [".$errno."] ".$errstr."\n".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;

    default:
        $linea_error = "ERROR-X. [".$errno."] ".$errstr.". ".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;
    }

	//escribiendo el error en archivo
	/*if (tsdk_propiedades::$FILE_ERROR == "")
	{
		error_log($linea_error);
		error_log("No ha inicializado la variable: tsdk_propiedades::$FILE_ERROR");
		return false;
	}*/

	//hallando la fecha y hora
	//$fecha = "[ ".date("d.m.Y H:m:s")." ] \n";
	
	//abriendo el archivo
	/*$archivo = fopen(tsdk_propiedades::$FILE_ERROR,'a'); 
	fwrite($archivo,"[".date("r")."] ".$linea_error); 
	fclose($archivo); */

	tsdk_error_log(tsdk_propiedades::$TRACELOG,
		 "#x ".$linea_error.
		 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
			"\n"
		);
			
    /* No ejecutar el gestor de errores interno de PHP */
    exit(1);
    return true;
}


//-----------------------------------------------------------
/**
 * Gestiona los errores de ejecucion, volcando los errores hacia un archivo. No genera trace.
 *
 * @param  int        $errno     El numero de error
 * @param  string     $errstr    El texto del error
 * @param  string     $errfile   El archivo que contiene el error
 * @param  string     $errline   Es el numero de linea donde se produce el error
 */
function tsdk_gestor_errores_lite($errno, $errstr, $errfile, $errline)
{
	$linea_error = "";
	
    if (!(error_reporting() & $errno)) {
        // Este código de error no está incluido en error_reporting
		$linea_error =  "ERROR-LITE. Error no manejado dentro de tsdk_gestor_errores. [".$errno."] ".$errstr."\n".
						"  Error en el archivo ".$errfile.", en la linea:".$errline.
						", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n";

		/*tsdk_error_log(tsdk_propiedades::$TRACELOG,
						"#x ".$linea_error.
						", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
						   "\n"
					   );*/
		error_log($linea_error);
    	exit(1);
        return true;
    }

    switch ($errno) {
    case E_USER_ERROR:
        $linea_error =  "USER_ERROR-LITE. [".$errno."] ".$errstr."\n".
						"  Error fatal en la línea ".$errline." en el archivo ".$errfile.
						", PHP " . PHP_VERSION . " (" . PHP_OS . ")\n".
						"Abortando...\n";
		
		/*tsdk_error_log(tsdk_propiedades::$TRACELOG,
						"#x ".$linea_error.
						", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
						   "\n"
					   );*/
		error_log($linea_error);

        exit(1);
        break;

    case E_USER_WARNING:
        $linea_error = "WARNING-LITE. [".$errno."] ".$errstr."\n".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;

    case E_USER_NOTICE:
        $linea_error = "NOTICE-LITE. [".$errno."] ".$errstr."\n".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;

    default:
        $linea_error = "ERROR-LITE. [".$errno."] ".$errstr.". ".
						"Archivo: ".$errfile.", en la linea: ".$errline."\n";
        break;
    }

	//escribiendo el error en archivo
	/*if (tsdk_propiedades::$FILE_ERROR == "")
	{
		error_log($linea_error);
		error_log("No ha inicializado la variable: tsdk_propiedades::$FILE_ERROR");
		return false;
	}*/

	//hallando la fecha y hora
	//$fecha = "[ ".date("d.m.Y H:m:s")." ] \n";
	
	//abriendo el archivo
	/*$archivo = fopen(tsdk_propiedades::$FILE_ERROR,'a'); 
	fwrite($archivo,"[".date("r")."] ".$linea_error); 
	fclose($archivo); */

	/*tsdk_error_log(tsdk_propiedades::$TRACELOG,
		 "#x ".$linea_error.
		 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
			"\n"
		);*/
		
	error_log($linea_error);

			
    /* No ejecutar el gestor de errores interno de PHP */
    exit(1);
    return true;
}

