<?php 
/**
 * Este archivo implementa las propiedades utilizadas en todas las clases de tsdk
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */
 
if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Propiedades
*
* Implementa las propiedades utilizadas por las funciones del TSDK
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_propiedades
{
	// ******************************
	// variables miembro
	// ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Es la ruta del archivo donde se guardara el trace
    * @var string
    */    
	public static 	$TRACELOG="";
	
	//variables para correo
    
    /**
    * Indica el servidor de correo (host)
    * @var string
    */
	public static 	$SMTP_SERVER;
    
    /**
    * Indica el usuario utilizado para hacer login al servidor de correo
    * @var string
    */
	public static 	$SMTP_USER;
    
    /**
    * Indica el password utilizado para hacer login al servidor de correo
    * @var string
    */
	public static 	$SMTP_PASSWORD;
    
    /**
    * Indica si la conexion al servidor de correo es segura
    * @var string
    */
	public static 	$SMTP_SECURE;
    
    /**
    * Indica el puerto al cual conectarse para hacer uso del servidor de correo
    * @var string
    */
	public static 	$SMTP_PORT;
	
    /**
    * De que cuenta se envia el correo
    * @var string
    */
    public static 	$SMTP_FROM;
    
    /**
    * Indica el nombre del usuario que enviará el correo
    * @var string
    */
	public static 	$SMTP_FROMNAME;

    /**#@-*/


	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @example "sample_tsdk_propiedades.php"
	*/
	function __construct()
	{
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}


}

?>
