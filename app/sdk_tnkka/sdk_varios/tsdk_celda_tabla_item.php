<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');


//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_celda_tabla_item
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa el valor que guarda el item de la coleccion
    * @var string
    */
	protected	$m_value = null;

    /**#@-*/
	
    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$propiedades=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$estilos=null;
	
    /**
    * Representa los eventos de la fila
    * @var coleccion
    */
	public	$eventos=null;
	
    /**#@-*/
	
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $valor          Se refiere al texto que aparece en la celda
    * @example "sample_tsdk_celda_tabla_item.construct.txt"
    */
	function __construct($valor=null)
	{
		if (is_null($valor))
			$this->m_value = "&nbsp;";
		else
			$this->m_value = $valor;
		
		//iniciando las propiedades
		$this->propiedades = new tsdk_propiedades_html();
		
		//iniciando los estilos
		$this->estilos = new tsdk_estilos();
		
		//iniciando los estilos
		$this->eventos = new tsdk_eventos();
		
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __toString()
    {
		return print_r($this->m_value,true);
    }
	
    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   Valor del item</br>
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "value":
			return $this->m_value;
			//break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   valor del item</br>
    *
    * @param	object	 $value        El valor a establecer en la propiedad
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){

        case "idventana":
            $this->eventos->idventana = $value;
            break;
            
        case "idcontrol":
            $this->eventos->idcontrol = $value;
            break;
            
		case "value":
			$this->m_value = $value;
			break;
		
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion inicializa las propiedades de la celda
    *
    * @access public
    */
	public function inicializar()
	{		
		//iniciando el valor de celda
		$this->m_value = "&nbsp;";
		
		//iniciando las propiedades
		$this->propiedades->delete_all();
		
		//iniciando los estilos
		$this->estilos->delete_all();
		
		//iniciando los estilos
		$this->eventos->delete_all();
	}
}

?>
