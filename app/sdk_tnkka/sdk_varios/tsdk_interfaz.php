<?php 
/**
 * Este archivo implementa las funciones y propiedades de las interfaces.
 * Una interfaz se encarga de atender los pedidos que el controlador le solicita, el controlador solo se encarga de recibir la peticion, evalua
 * si tiene permiso para su ejecucion y solicita a la interfaz su atencion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */
 
if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//funciones nuevas para excel
include_once(APPPATH."../../sdk_terceros/phpoffice_phpspreadsheet_1.10.1.0_require/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php");
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

//-----------------------------------------------------------
/**
* Interfaz
*
* Implementa las propiedades y metodos comunes utilizados por las interfaces
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_interfaz
{
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/ 

	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	protected  $m_basedatos=null;

	/**
	* Determina el controlador base que llama a la interfaz
	* @var tsdk_controlador_common
	*/
	public 	$m_controlador_base;

	/**
	* Determina quien es el manejador de base de datos ('mysql' o 'postgresql')
	* @var string
	*/
	//protected  $m_manejadorbd=null;

	/**
	* Objeto EXCEL
	* @var object
	*/
	//protected	$m_objPHPExcel;
	protected	$m_spreadsheet;

	/**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
 
    /**#@-*/

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//----------------------------------------------------------------------------------
	/**
	* Esta funcion valida si los controles indicados en la lista existen o no
	* @param  array 	$lista 		Arreglo conteniendo los nombre de los controles a validar. 
	* 								En la lista se ingresa el nombre del ID del control.
	*  								Estos controles los valida contra la lista $_POST
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
    */
    
	protected function existen_variables_post($lista)
	{
		foreach ($lista as $variablepost)
		{
			if (!isset($_POST[$variablepost]))
			{
				//tsdk_error_log_trace("----- VARIABLES POST DONDE SE BUSCA LOS ITEMS -----",$_POST);

                $this->m_mensaje=tsdk_reemplazar_html("No existe item: ".$variablepost);

				tsdk_error_log(tsdk_propiedades::$TRACELOG,
								 "#x Error en existen_variables_post. ". $this->m_mensaje.
								 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									"\n"
								);
				return false;
			}
        }
        
		return true;
    }
    
	//-----------------------------------------------------------
	/**
	* Esta funcion valida que los controles obligatorios no se encuentren vacios, asi como contengan el tipo de dato correcto.
	* @param  array 	$lista 		Arreglo de dos dimensiones. Es una lista de datos a validar.
	* 								Cada nodo tiene un arreglo, donde el primer campo es el ID del control a validar,
	* 								el segundo campo es el nombre, y el tercer campo es el tipo de dato. 
	* @param  object    $nombre     Es el nombre del control que tiene error. Este parametro es pasado por referencia.
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
	*/
	protected function validar_controles_obligatorios($lista, &$nombre)
	{
		foreach ($lista as $control )
		{
		  $nombrecontrol = $control[0];
		  $desc_control = $control[1];
		  $tipo_dato = $control[2];
		  $mensaje_personalizado = $control[3];

		  //validando que el control tenga un dato
		  //$valor = trim($this->input->post($nombrecontrol));
		  $valor = trim($_POST[$nombrecontrol]);

		  //validando el tipo de dato
		  switch ($tipo_dato){
			case tsdk_tipodato::tstring:
			  {
				break;
			  }
			case tsdk_tipodato::tstring_no_vacio:
			  {
				$cadena=trim($valor);
				if ($cadena=="")
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó datos en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
					
					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);

					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789+-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<=0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_igual_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor o igual a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				break;
			  }
			case tsdk_tipodato::tdouble_mayor_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = (double)($valor);
				if ($numero<=0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble_mayor_igual_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = (double)($valor);
				if ($numero<0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor o igual a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
				
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }

			case tsdk_tipodato::temail_o_vacio:
			  {
				//validando si es vacio
				if ( $valor == "")
				{
				  break; //OK
				}

				//validando que sea tipo email
				$valor = (string)($valor);
				$encuentros=preg_match("/^[0-9a-zA-Z]+([\._]?[[:alnum:]])*[[:alnum:]]@[0-9a-zA-Z]+([-]?[[:alnum:]])*[.][a-zA-Z]{2,4}$/",$valor); //validando
				if ($encuentros==0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un correo valido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }

			case tsdk_tipodato::tboolean:
			  {
				if ($valor != true && $valor != false)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un BOOLEAN en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			default:
			  {
				
				if ($mensaje_personalizado == "")
				{
					$this->m_mensaje=tsdk_reemplazar_html("No ingresó un TIPO DE DATO correcto, revisar programa !!!!");
				}else{
					$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
				}
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				echo "#x Error en validar_controles_obligatorios. Control:".$nombrecontrol.
				  ", descripcion:".$desc_control.
				  ", tipo de dato:".$tipo_dato.
				  ", valor:".$valor.
				  ". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
				  "\n";
				debug_print_backtrace();

				$nombre="";
				return false;

				break;
			  }
		  }
		}

		return true;
	}


    //-----------------------------------------------------------
    /**
	* Esta funcion crea el objeto EXCEL.
	* Nueva funcion creada en Phpspreadsheet
    *
	* @access protected
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_xxxxx.php"
    */
	protected function create_reporte_excel($tabladatos)
	{		
		// creando la hoja excel
		$this->m_spreadsheet = new Spreadsheet();

		//estilo negrita
		$estilo_negrita = array(
							'font' => array(
								'bold' => true
							)
						);
		$estilo_bordes = array(
							'borders' => array(
								'allBorders' => array(
									'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
								)
							)
						);

		// Set document properties
		$this->m_spreadsheet->getProperties()->setCreator('Test')
			->setLastModifiedBy('Test')
			->setTitle('Office 2007 XLSX Test Document')
			->setSubject('Office 2007 XLSX Test Document')
			->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
			->setKeywords('office 2007 openxml php')
			->setCategory('Test result file');

		// Activando la primera hoja
		$this->m_spreadsheet->setActiveSheetIndex(0);

		// Rename sheet
		$this->m_spreadsheet->getActiveSheet()->setTitle('Reporte');

		//colocando titulo de reporte
		$nrofila=1;
		$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila, $this->m_tituloreporte_excel);
		
		//combinando el titulo
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->mergeCells($columnainicial.'1:'.$columnafinal.'1');

		//colocando negrita
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->applyFromArray($estilo_negrita);

		//centrando el titulo
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.'1:'.$columnafinal.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		//colocando titulos de la tabla
		$nrofila=$nrofila+2;
		$columna='A';
		foreach ($this->m_titulos_reporte as $valor)
		{
			if ( strcasecmp($valor,"&nbsp;")!=0 )
				$this->m_spreadsheet->getActiveSheet()->SetCellValue( $columna.$nrofila, $valor); //colocando el titulo

			//estableciendo autosize
			$this->m_spreadsheet->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

			//aumentando a la siguiente columna
			$columna++;
		}

		//colocando negrita
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_negrita);

		//colocando bordes
		$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);

		//rellenando los datos
		foreach ($tabladatos->filas as $indice => $fila)
		{
			$nrofila++;

			//agregando valores de las columnas
			$columna='A';
			foreach ($this->m_campos_reporte as $valor)
			{
				if ( strcasecmp($valor,"&nbsp;")!=0 )
				{
					$this->m_spreadsheet->getActiveSheet()->SetCellValue( $columna.$nrofila, $fila->item($valor)->value );
				}
				
				$columna++;
			}

			//colocando bordes
			$columnainicial='A';
			$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
			$this->m_spreadsheet->getActiveSheet()->getStyle($columnainicial.$nrofila.':'.$columnafinal.$nrofila)->applyFromArray($estilo_bordes);

		}

		//-- creando la fila de mensajes
		$nrofila=$nrofila+2;
		if ($tabladatos->filas->count()==0)
		{
			$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila,"No existen items");
		}else{
			$this->m_spreadsheet->getActiveSheet()->SetCellValue('A'.$nrofila,"Nro de items: ".$tabladatos->filas->count() );
		}

		//combinando el mensaje
		$columnainicial='A';
		$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
		$this->m_spreadsheet->getActiveSheet()->mergeCells($columnainicial.$nrofila.':'.$columnafinal.$nrofila);

		//colocando autosize
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		//$this->m_spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion imprime el archivo EXCEL 
    * La salida del reporte siempre es hacia la pantalla
    * 
	* @access protected
	* @param  object  	$tabladatos 	Tabla de datos
    * @return none
    * 
    * @example "sample_tsdk_window_browse.php"
    */
	protected function print_excel_base()
	{
		/*
		//LAS SIGUIENTES SENTENCIAS SE ADECUARON PARA QUE FUNCIONEN CON SPREADSHEET
		// Se manda el archivo al navegador web, con el nombre que se indica, en formato 2007
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="Reporte.xlsx"');
		header('Cache-Control: max-age=0');
		$this->m_objWriter = PHPExcel_IOFactory::createWriter($this->m_objPHPExcel, 'Excel2007');
		
		//ob_end_clean(); //comentado al usarse en OSX

		//$this->m_objWriter->save('php://output'); //reemplazado por la siguiente funcion, para evitar un error en php 7
		tsdk_SaveViaTempFile($this->m_objWriter);
		*/

		// Redirect output to a client’s web browser (Xls)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Reporte.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = IOFactory::createWriter($this->m_spreadsheet, 'Xls');
		$writer->save('php://output');
		exit;

	}

	// ******************************
	// metodos públicos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @example "sample_tsdk_propiedades.php"
	*/
	public function __construct($controlador_base,$basedatos)
	{
		//estableciendo controlador base
		$this->m_controlador_base = $controlador_base;

		//estableciendo el objeto base de datos
		$this->m_basedatos = $basedatos;

		//inicializando el manejo de errores
		$gestor_errores_antiguo = set_error_handler("tsdk_gestor_errores");
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	public function __destruct()
	{
	}




}

?>
