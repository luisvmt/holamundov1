<?php 
/**
 * Este archivo implementa las propiedades utilizadas en todas las clases de tsdk
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades.php");

//-----------------------------------------------------------
/**
* Propiedades
*
* Implementa las propiedades utilizadas por las funciones del TSDK
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_parametros_controlador
{
	// ******************************
	// variables miembro
	// ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access private
    */
	
    /**
    * Representa la lista (arreglo) de los parametros del controlador
    * @var array
    */
	private 	$m_parametros=array();
		
    /**
    * Representa el cotrolador asociado
	* @var object
    */
	private 	$m_controlador;

    /**#@-*/
    

	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  object    $controlador      HAce referencia al controlador asociado
    * @example "sample_tsdk_parametros_controlador.construct.txt"
	*/
	function __construct($controlador=null)
	{
		$this->m_controlador = $controlador;
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a devolver
	* @example "sample_tsdk_parametros_controlador.php"
	*/
	public function __get($propiedad)
	{
		$is_post = false;
		//verificando si se ha obtenido variables por metodo post
		if ( count($_POST)>0 ) //no tiene variables post
		{
			$is_post = true;
		}

		if ($is_post) //recuperando variables post
		{
			return $this->m_controlador->input->post($propiedad);
		}

		//recuperando variables de session
		//verificando si existe la variable
		if (isset($this->m_parametros[$propiedad]))
			return $this->m_parametros[$propiedad];
		else{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get.".
				 "  Propiedad no encontrada: [".$propiedad."]".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
			return null;
		}
		return null;

	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
    * @access public
	* @param string $propiedad      La propiedad a establecer
	* @param string $value          El valor de la propiedad
	* @example "sample_tsdk_parametros_controlador.php"
	*/
	public function __set($propiedad, $value)
	{
		$this->m_parametros[$propiedad] = $value;
	}

	//-----------------------------------------------------------
	/**
	* Verifica si un parametro existe
	* 
    * @access public
	* @param string $propiedad      La propiedad a establecer
	* @example "sample_tsdk_parametros_controlador.php"
	*/
	public function exists_parametro($propiedad)
	{
		return isset($this->m_parametros[$propiedad]);
	}

	//-----------------------------------------------------------
	/**
	* Establece los parametros 
	* 
    * @access public
	* @param 	array 	$lista 		Lista de propiedades a establecer
	* @example "sample_tsdk_parametros_controlador.php"
	*/
	public function set_parametros($lista)
	{
		$this->m_parametros = $lista;
	}

	//-----------------------------------------------------------
	/**
	* Devuelve la lista de parametros de la clase
	* 
    * @access public
    * @return  array 	Lista de parametros de la clase
	* @example "sample_tsdk_parametros_controlador.php"
	*/
	public function get_parametros()
	{
		return $this->m_parametros;
	}

}

?>
