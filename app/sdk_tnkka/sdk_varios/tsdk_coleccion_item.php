<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');


//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_coleccion_item 
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa el valor que guarda el item de la coleccion
    * @var string
    */
	protected	$m_value = null;

	
    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  object    $valor          El valor que guardará el item de la coleccion
    * @example "sample_tsdk_coleccion_item.construct.php"
    */
	function __construct($valor=null)
	{
		$this->m_value = $valor;
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __toString()
    {
		return print_r($this->m_value,true);
    }
	
    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   Valor del item</br>
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "value":
			return $this->m_value;
			break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   valor del item</br>
    *
    * @param	object	 $value        El valor a establecer en la propiedad
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "value":
			$this->m_value = $value;
			break;
		
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}
	}


}

?>
