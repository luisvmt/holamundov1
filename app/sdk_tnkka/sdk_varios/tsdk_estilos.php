<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion de estilos
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
*/

class tsdk_estilos extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_coleccion.php"
    */
	function __construct()
	{
		parent::__construct();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
		parent::__destruct();
    }

    //-----------------------------------------------------------
    /**
    * Establece los estilos del control
    * @access public
    * @param array $lista		Arreglo con la lista de estilos a establecer
    * @return none
    * @example "sample_tsdk_control.php"
    */
    public function set_estilos($lista)
    { 
		if (is_array($lista))
		{
			foreach ($lista as $clave=>$valor)
			{
				$this->add($clave)->value=$valor;
			}
		}
    }

    //-----------------------------------------------------------
    /**
    * Devuelve la lista (arreglo) de estilos
    * @access public
    * @return string         Texto que representa la lista de estilos en formato html
    * @example "sample_tsdk_control.php"
    */
    public function get_estilos_text()
    {
        $salida = "";

        if ( $this->count() > 0) //si el total de estilos es mayor a cero
        {
            $salida .= "style=\"";
			
			//recorriendo cada elemento
			foreach ($this as $clave => $item) //cada item es del tipo tsdlk_item_coleccion
			{
                $salida = $salida.$clave;
                if (!is_null($item->value))
                {
                    $salida=$salida.":".$item->value.";";
                }
                $salida.=" ";
            }
            $salida.="\"";

        }

        return $salida;
    }

}
