<?php
/**
 * Este archivo implementa las funciones comunes para una vista Edicion
 *
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package varios
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_image.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_link.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_group.php");
include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_form.php");
include_once(APPPATH."../../sdk_tnkka/sdk_contenedores_html/tsdk_div.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php"); //funciones

include_once(APPPATH."../../sdk_terceros/excel/PHPExcel.php"); //excel
include_once(APPPATH."../../sdk_terceros/excel/PHPExcel/Writer/Excel2007.php"); // PHPExcel_Writer_Excel2007

//-----------------------------------------------------------
/**
* Ventana estandar - Windows Browse
*
* Implementa una ventana estandar que implementa una ventana edicion
*
* @author luisvmt@hotmail.com
* @package varios
*/ 

class tsdk_window_edicion
{
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa el objeto base de datos
	* @var object
	*/
	protected	$m_basedatos;

	/**
	* Determina el ID de la ventana
	* @var string
	*/
	protected	$m_idventana="";

	/**
	* Representa el ancho de la primera columna de la tabla que contiene la ventana
	* @var integer
	*/
	protected	$m_ancho_1racolumna;

	/**
	* Representa el ancho de la segunda columna de la tabla que contiene la ventana
	* @var integer
	*/
	protected	$m_ancho_2dacolumna;

	/**
	* Coleccion que contiene la lista de controles que componen la ventana edicion.
	* Esta variables es la creacion de una clase derivada de "tsdk_list_controls", esta variable la inicializa la clase derivada.
	* Por ejemplo: $this->m_listacontroles = new twindow_usuarios_edicion_controles($this->m_basedatos,"usuario")
	* @var array
	*/
	protected	$m_listacontroles;

	/**
	* Contiene el codigo javascript
	* @var string
	*/
	protected	$m_codigo_javascript;

	/**
	* Contiene el titulo de la ventana (insertar)
	* @var string
	*/
	protected	$m_titulo_insertar;

	/**
	* Contiene el titulo de la ventana (modificar)
	* @var string
	*/
	protected	$m_titulo_modificar;

	/**
	* Determina si se tiene permiso para modificar
	* @var boolean
	*/
	protected	$m_permiso_modificar;

	/**
	* Determina si se va a realizar una ventana modificar
	* @var string
	*/
	protected	$m_accion_ejecutar;

	/**
	* Titulo de la tabla
	* @var string
	*/
	protected	$m_titulo_tabla;

	/**
	* Objeto tabla que contiene toda la ventana
	* @var object
	*/
	protected	$m_tabla_general;
	
	/**
	* Numero de filas de la tabla general
	* @var integer
	*/
	protected	$m_nrofilas_tabla_general;
	
	/**
	* Numero de columnas de la tabla general
	* @var integer
	*/
	protected	$m_nrocolumnas_tabla_general;
	
	/**
	* Objeto tabla que contiene la ventana
	* @var object
	*/
	protected	$m_tabla;

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  	object 		$basedatos 		Base de datos
	* @example "sample_tsdk_window_edicion.php"
	*/
	function __construct($basedatos,$idventana)
	{
		$this->m_basedatos = $basedatos;
		$this->m_idventana = $idventana;

		$this->m_permiso_modificar=false;

		$this->m_titulo_tabla=array();
		$this->m_tabla=array();
		
		//creando la tabla general
		
		//----------------------------------------------------------------------------------------------
		//-- definiendo la tabla-general
		//-- es una tabla compuesta de: 2 encabezados, 1 fila de contenido, 2 filas de pies = 5 filas
		$this->m_nrofilas_tabla_general=5;
		$this->m_nrocolumnas_tabla_general=2;
		
		$this->m_tabla_general = new tsdk_tabla($this->m_idventana,"no-establecido",  //se modificara posteriormente
												$this->m_nrofilas_tabla_general,
												//$this->m_nrofilas_tabla_general,
												$this->m_nrocolumnas_tabla_general,
												array("border"=>"0"
													  )
												);

		$this->m_tabla_general->nro_header=2;
		$this->m_tabla_general->nro_footer=2;

		$this->m_tabla_general->set_propiedades_celda(0,0,array("class"=>"tvedicion_titulo_class",
																"colspan"=>"2",
																"align"=>"center"));  //titulo
		$this->m_tabla_general->set_propiedades_celda(1,0,array("style"=>"width: ".$this->m_ancho_1racolumna.";"));
		$this->m_tabla_general->set_propiedades_celda(1,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));

		$this->m_tabla_general->set_propiedades_celda(2,0,array("colspan"=>"2"));  //contenido

		$this->m_tabla_general->set_propiedades_celda(4,0,array("colspan"=>"2","style"=>"width: ".$this->m_ancho_1racolumna.";"));  //mensaje
		$this->m_tabla_general->set_propiedades_celda(4,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));  //mensaje

		//----------------------------------------------------------------------------------------------
		//-- modificando el footer de la tabla general
		//	
		$this->m_tabla_general->set_valores_fila($this->m_nrofilas_tabla_general - 2,array("(*) Campos obligatorios"));

	}

	//-----------------------------------------------------------
	/**
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5>
	*    <b>permiso_modificar</b>      :   Establece si se tiene permiso para modificar</br>
	*
	* @example "sample_tsdk_window_edicion.php"
	*/
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "permiso_modificar":
			return $this->m_permiso_modificar;
			break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en get. Propiedad no encontrada: ".
					$propiedad.". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			return null;
			break;
		}

	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5>
	*    <b>permiso_modificar</b>      :   Establece si se tiene permiso para modificar</br>
	*
	* @param string $value        El valor a establecer en la propiedad
	* @example "sample_tsdk_window_edicion.php"
	*/
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "permiso_modificar":
			$this->m_permiso_modificar = $value;
			break;
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x Error en set. Propiedad no encontrada: ".
					$propiedad.". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			break;
		}
	}

	//-----------------------------------------------------------
	/**
	* Devuelve el texto HTML que implementa la ventana
	* @access public
	* @return string                  Devuelve el texto HTML del control
	* @example "sample_tsdk_button.php"
	*/
	function get_html_edicion()
	{
		//----- creando control oculto para determinar si es insertar/modificar
		$control = new tsdk_textbox();  //es un texto oculto que se utilizara para determinar insertar/modificar
		//$control->set_id_ventana_control($this->m_listacontroles->idventana,"txt_accion");
		$control->config_as_text_hidden("");
		$this->m_listacontroles->add("txt_accion",$control);

//tsdk_error_log_trace("txt_accion",$this->m_listacontroles->get_html("txt_accion"),__FILE__,__LINE__);

		//definiendo el formulario
		$formulario = new tsdk_form($this->m_idventana, //idventana
									"form-edicion", //idcontrol
									array("method"=>"post")
									);
		$divrecordset = new tsdk_div($this->m_idventana,"divrecordset-edicion"); //array("id"=>"divrecordset_".$this->m_listacontroles->idventana));
		$divcontroles = new tsdk_div($this->m_idventana,"divcontroles-edicion"); //array("id"=>"divcontroles_".$this->m_listacontroles->idventana));

//tsdk_error_log_trace("----- this->m_listacontroles->idventana -----",$this->m_listacontroles->idventana);
//tsdk_error_log_trace("----- 	formulario -----",$formulario);
//tsdk_error_log_trace("----- 	formulario.html -----",$formulario->get_html());

		//-- estableciendo propiedades de los div
		$divrecordset->estilos->add("float",new tsdk_coleccion_item("left"));
		$divrecordset->estilos->add("margin-left",new tsdk_coleccion_item("20px"));

		$divcontroles->estilos->add("float",new tsdk_coleccion_item("left"));
		$divcontroles->estilos->add("margin-left",new tsdk_coleccion_item("20px"));

		//-----------------------------------------------
		//la tabla general ya se creó en el constructor
		//modificando el ID original de la tabla
		//-----------------------------------------------
		$this->m_tabla_general->propiedades->add("id")->value="tabla_".$this->m_listacontroles->idventana."_parent";
		
		//estableciendo titulo a tabla-general
		switch ($this->m_accion_ejecutar)
		{
			case "insertar":
				$this->m_listacontroles->set_value_item("txt_accion","0");  //insertar
				$this->m_tabla_general->set_valores_fila(0,array($this->m_titulo_insertar));
				if ($this->m_permiso_modificar == false)
				{
					$this->m_listacontroles->set_value_item('mensaje','No tiene permisos para crear un nuevo registro. Debe ser administrador');
				}else{
					$this->m_listacontroles->set_value_item('mensaje','');
				}
				break;

			case "modificar":
				$this->m_listacontroles->set_value_item("txt_accion","1"); //modificar
				$this->m_tabla_general->set_valores_fila(0,array($this->m_titulo_modificar));
				if ($this->m_permiso_modificar == false)
				{
					$this->m_listacontroles->set_value_item('mensaje','No tiene permisos para modificar los campos, solo los puede visualizar');
				}else{
					$this->m_listacontroles->set_value_item('mensaje','');
				}
//tsdk_error_log_trace("listacontroles",$this->m_listacontroles);

				break;
		}

		//estableciendo el mensaje
		$this->m_tabla_general->set_valores_fila($this->m_nrofilas_tabla_general - 1,array($this->m_listacontroles->get_html("mensaje")));

		//validando el numero de sub-tablas creadas
		if (count($this->m_tabla) <=1 )
		{
			$contenido_tabla_general="<div id='tabla_".$this->m_listacontroles->idventana."_accordion' ".
										"class='tjs_edicion_general_class1' >";
		}else{
			$contenido_tabla_general="<div id='tabla_".$this->m_listacontroles->idventana."_accordion' ".
										"class='tjs_edicion_general_class2' >";
		}

		//construyendo las tablas
		foreach ($this->m_tabla as $indice=>&$tabla)
		{
			//----------------------------------------------------------------------------------------------
			$tabla->nro_header=0;
			$tabla->nro_footer=0;

			$tabla->set_propiedades_celda(0,0,array("style"=>"width: ".$this->m_ancho_1racolumna.";"));
			$tabla->set_propiedades_celda(0,1,array("style"=>"width: ".$this->m_ancho_2dacolumna.";"));

			$contenido_tabla_general .= $this->m_titulo_tabla[$indice]."\n";
			$contenido_tabla_general .= "<div>".$tabla->get_html()."</div>";
		}
 
		//---------------------------------------------
		//estableciendo el contenido de la tabla-general
		$contenido_tabla_general .= "</div>";
		$this->m_tabla_general->set_valores_fila(2,array($contenido_tabla_general));

		//---------------------------------------------

		//-- añadiendo al divrecordset
		$divrecordset->content = "<br/>".$this->m_tabla_general->get_html();

		//-- añadiendo al divcontroles
		$textocontroles  = "</br></br></br>";

		if ($this->m_titulo_tabla[0]!="") //añadiendo enters para que los controles esten alineados
		{
			$textocontroles .= "</br></br>";
		}
		
		if ($this->m_permiso_modificar == true)
		{
			$textocontroles .= "</br></br>";
			$textocontroles .= $this->m_listacontroles->get_html("cmdgrabar");
			if ($this->m_accion_ejecutar == "modificar")
			{
				$textocontroles .= "</br></br>";
				$textocontroles .= $this->m_listacontroles->get_html("cmdclonar");
			}
		}else{
			//$textocontroles .= "</br></br>";
			$textocontroles .= "";
		}

		$textocontroles .= "</br></br>";
		$textocontroles .= $this->m_listacontroles->get_html("cmdcerrar");
		$textocontroles .= $this->m_listacontroles->get_html("txt_accion"); //control oculto que indica la accion a realizar insertar=0/modificar=1
//error_log("escribiendo control");
//error_log($this->m_listacontroles->get_html("txt_accion"));
		$divcontroles->content = $textocontroles;

		//--
		$formulario->content = $this->m_codigo_javascript.$divrecordset->get_html().$divcontroles->get_html();

//tsdk_error_log_trace("----- formulario2 -----",$formulario);

		return $formulario->get_html();

	}

	//-----------------------------------------------------------
	/**
	* Imprime en salida estandar el texto HTML que implementa la ventana
	* @access public
	* @return none
	* @example "sample_tsdk_button.php"
	*/
	public function print_html()
	{
		 echo $this->get_html();
	}


}

?>
