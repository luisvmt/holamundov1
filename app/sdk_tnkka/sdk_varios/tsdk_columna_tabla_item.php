<?php 
/**
 * Este archivo implementa la clase de item dentro de una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_estilos.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_eventos.php");

//-----------------------------------------------------------
/**
* Item de una coleccion
*
* Implementa la clase que representa un item de una coleccion
*/

class tsdk_columna_tabla_item 
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$propiedades=null;

    /**
    * Representa la coleccion de estilos
    * @var coleccion
    */
	public	$estilos=null;
	
	
    /**#@-*/
	
	// ******************************
    // metodos
    // ******************************

	
    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $texto          Se refiere al texto que aparece en el control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_columna_tabla_item.construct.php"
    */
	function __construct()
	{
		//$this->m_valor = $valor;
		
		//iniciando las propiedades
		$this->propiedades = new tsdk_propiedades_html();
		
		//iniciando los estilos
		$this->estilos = new tsdk_estilos();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __toString()
    {
		//return print_r($this->m_valor,true);
		return "";
    }
	
    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   Valor del item</br>
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		//case "value":
		//	return $this->m_valor;
		//	break;
		
		case "ancho":
		case "width":
			if (array_key_exists("width"))
				return $this->estilos->item("width")->value;
			else
				return null;
			break;
		
		case "alineacion-horizontal":
		case "text-align": //funciona para el div de la celda
			if (array_key_exists("text-align"))
				return $this->estilos->item("text-align")->value;
			else
				return null;
			break;
		
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>value</b>       :   valor del item</br>
    *
    * @param	object	 $value        El valor a establecer en la propiedad
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		//case "value":
		//	$this->m_valor = $value;
		//	break;
		
		case "ancho":
		case "width":
			$this->estilos->add("width")->value = $value;
			break;
		
		case "alineacion-horizontal":
		case "text-align": //funciona para el div de la celda
			$this->estilos->add("text-align")->value = $value;
			break;
		
		default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
		}
	}


}


