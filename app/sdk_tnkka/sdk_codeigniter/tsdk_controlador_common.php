<?php   
/**
 * Este archivo implementa la clase tsdk_basedatos_mysql
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controladores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

//Incluyendo la base de datos
include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/sdk_mysql/tsdk_basedatos_mysql.php");
include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/sdk_postgresql/tsdk_basedatos_postgresql.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_controlador.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//-----------------------------------------------------------
/**
* Controlador común
*
* Implementa la funcionalidad principal de los controladores del API
*
* Esta clase implementa la funcionalidad principal de los controladores
*
* @author luisvmt@hotmail.com
* @package controladores
*/
class tsdk_controlador_common extends CI_Controller
{
	// ******************************
	// variables miembro
	// ******************************


	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/ 

	/**
	* nombre del controlador
	* @var string
	*/
	protected  $m_nombre_controlador="";
	
	/**
	* id de session. Se obtiene con la funcion session_id();
	* @var string
	*/
	protected  $m_session_id="";
	
	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	protected  $m_basedatos=null;

	/**
	* Determina quien es el manejador de base de datos ('mysql' o 'postgresql')
	* @var string
	*/
	protected  $m_manejadorbd=null;

	/**
	* mensaje de error o exito de la clase
	* @var string
	*/
	protected  $m_mensaje="";

	/**
	* Objeto que contiene las variables pasadas al controlador
	* @var object
	*/
	protected  $m_parametros_controlador;

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

 
	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  string    $usuariobd  		Usuario para conexion a base de datos
	* @param  string    $contrasenabd       Contraseña para conexion a base de datos
	* @param  string    $servidorbd         Nombre o IP del servidor de base de datos
	* @param  string    $basedatos          Nombre de la base de datos
	* @param  integer   $puertobd           Numero de puerto TCP donde escucha la base de datos
	* @param  string    $esquema            Es el esquema a utilizar en la conexion
	* @param  string    $manejadorbd        Es el manejador a utilizar (mysql o postgresql)
	* @param  string    $controlador_raiz   Es el nombre del controlador donde se dirigirá sino se hizo login
	* @param  boolean   $validar_login      Indica si se debe validar si se hizo login
	* @param  boolean   $bd_is_persistent   Indica si la conexion a la base de datos será persistente
	*
	* @example "sample_tsdk_controlador_common_construct.txt"
	*/
	public function __construct($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$manejadorbd,
								$controlador_raiz="", $validar_login = true,$bd_is_persistent=true) //parametro: $validar_login = true
	{
		$respuesta_conexion = false;

		parent::__construct();

		//obteniendo los parametros
			//$parametros = func_get_args();
			//$validar_login = isset($parametros['0'])?$parametros['0']:true;

		//inicializando sessiones
		session_start();

		//inicializando variables
		$this->load->helper('url');
		$this->load->library('tsdk_login_library'); //libreria personal

		//obteniendo el id de session
		$this->m_session_id = session_id();

		//validando si el usuario esta loggeado
		if ($validar_login==true)
		{
			$this->tsdk_login_library->controlador_raiz = $controlador_raiz;

			$this->tsdk_login_library->check_is_logged();
		}

		//inicializando los parametros del controlador
		$this->m_parametros_controlador = new tsdk_parametros_controlador($this);

		//estableciendo el manejador de base de datos
		$this->m_manejadorbd = $manejadorbd;

		//conectando a la base de datos (mysql o postgresql)
		switch ($this->m_manejadorbd){
			case "mysql":
				$this->m_basedatos = new tsdk_basedatos_mysql();
				$respuesta_conexion = $this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$bd_is_persistent);
				break;
			case "postgresql":
				$this->m_basedatos = new tsdk_basedatos_postgresql();
				$respuesta_conexion = $this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$bd_is_persistent);
				break;
			default:
                $lineaerror = "Manejador de base de datos [ ".$manejadorbd." ] no reconocido.\n";
                //error_log($lineaerror);

                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x ".$lineaerror.
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );
                                                  
                break;
		}

		if (!$respuesta_conexion)
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				"#x Error en conexion a base de datos. Finalizando programa.".
				", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			die();
		}
		
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    public function __destruct()
    {
    	//error_log("Dentro de destructor del controlador tsdk");
        //$this->m_conexion = null;
    }

	//-----------------------------------------------------------
	/**
	* Implementa la funcion principal del controlador
	*
	*/
	protected function index()
	{
		//leyendo las variables post ... desde las variables de session

		/*** comentado temporalmente
		if (isset($_SESSION['session_post']))
		{
			$m_session_post = $_SESSION['session_post'];
		} else {
			$m_session_post = array();
		}

		//inicializando las variables que no existen
		if (!isset($m_session_post['mensaje']) )
		{
			$m_session_post['mensaje']="";
		}*/
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion inicializa las variables de session que se utilizaran posteriormente
	* 
	* @return none
	*/
	protected function inicializar_variables()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();
		}

		//verificando si existe el parametro mensaje
		if (! $this->m_parametros_controlador->exists_parametro("mensaje"))
		{
			$this->m_parametros_controlador->mensaje="";
		}

		//verificando si existe el parametro mensaje
		if (! $_SESSION['parametros_controlador']->exists_parametro("mensaje"))
		{
			$_SESSION['parametros_controlador']->mensaje="";
		}

	}

	//-----------------------------------------------------------
	/**
	* Esta funcion establecer los parametros del controlador.
	* Se debe llamar cada vez que se inicia el controlador (llamado por index o ajax)
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	protected function load_parametros_controlador()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x variable SESSION['parametros_controlador'] no existe".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
		}

		$lista = $_SESSION['parametros_controlador']->get_parametros();
		$this->m_parametros_controlador->set_parametros($lista);
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion redirecciona a otra pagina de la aplicacion. 
	* Guarda en una variable de session llamada parametros_controlador un objeto
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	public function redirect($url,$parametros_controlador)
	{
		//----------------------------
		//$this->session->set_userdata('session_post', $datos);
		/*if (!is_null($datos))
		  $_SESSION['session_post']=$datos;*/

		$_SESSION["parametros_controlador"] = $parametros_controlador;

		header('Location: '.$url);
	}

/*** comentado temporalmente


  //-----------------------------------------------------------------------
  //Inserta desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function insertar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//estableciendo valor de eliminar
	//$objeto->set_item_valor('eliminar',false);
	$objeto->eliminar=false;

	//ejecutando
	if ( $objeto->insertar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }

  //-----------------------------------------------------------------------
  //Modifica desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function modificar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos (primero se obtendra los valores)
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//obteniendo valores ... quizas algunos campos no se modifiquen
	if ( $objeto->obtener() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	//volviendo a establecer para modificar campos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	$respuesta=$objeto->modificar();
	if ( $respuesta < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return $respuesta;
  }

  //-----------------------------------------------------------------------
  //elimina desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function eliminar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	if ( $objeto->eliminar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }*/

} //fin de clase

