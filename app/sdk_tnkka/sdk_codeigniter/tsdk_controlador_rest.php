<?php   
/**
 * Este archivo implementa la clase tsdk_basedatos_mysql
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controladores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

//Incluyendo la base de datos
include_once(APPPATH."../../sdk_tnkka/sdk_mysql/tsdk_basedatos_mysql.php");
include_once(APPPATH."../../sdk_tnkka/sdk_postgresql/tsdk_basedatos_postgresql.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_controlador.php");
include_once(APPPATH."/libraries/REST_Controller.php");

//usando jwt
include_once(APPPATH."../../sdk_terceros/JWT/JWT.php");

//-----------------------------------------------------------
/**
* Controlador común que implementa REST
*
* Implementa la funcionalidad principal de los controladores del API REST
*
* Esta clase implementa la funcionalidad principal de los controladores
*
* @author luisvmt@hotmail.com
* @package controladores
*/
class tsdk_controlador_rest extends REST_Controller
{
	// ******************************
	// variables miembro
	// ******************************


	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* nombre del controlador
	* @var string
	*/
	//protected  $m_nombre_controlador="";
	
	/**
	* id de session. Se obtiene como una cadena aleatoria al momento de hacer login
	* @var string
	*/
	protected  $m_session_id="";
	
	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	protected  $m_basedatos=null;

	/**
	* mensaje de error o exito de la clase
	* @var string
	*/
	protected  $m_mensaje="";

	/**
	* Objeto que contiene las variables pasadas al controlador
	* @var object
	*/
	//protected  $m_parametros_controlador;

	/**#@-*/

	// ******************************
	// metodos
	// ******************************
 
 
	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  string    $usuariobd  		Usuario para conexion a base de datos
	* @param  string    $contrasenabd       Contraseña para conexion a base de datos
	* @param  string    $servidorbd         Nombre o IP del servidor de base de datos
	* @param  string    $basedatos          Nombre de la base de datos
	* @param  integer   $puertobd           Numero de puerto TCP donde escucha la base de datos
	* @param  boolean   $bd_is_persistent   Indica si la conexion a la base de datos es persistentEs
	*
	* @example "sample_tsdk_basedatos_mysql.php"
	*/
	public function __construct($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$manejadorbd,
								$bd_is_persistent=true) //parametro: $validar_login = true
	{
		parent::__construct();

		//obteniendo los parametros
			//$parametros = func_get_args();
			//$validar_login = isset($parametros['0'])?$parametros['0']:true;

		//inicializando sessiones
		//session_start();

		//inicializando variables
		//$this->load->helper('url');
		//$this->load->library('tsdk_login_library'); //libreria personal

		//obteniendo el id de session
		//$this->m_session_id = session_id();

		//validando si el usuario esta loggeado
		/*if ($validar_login==true)
		{
			//$this->tsdk_login_library->base_url = "http://adminfabystore_cancer01.tnkka.xyz/";
			$this->tsdk_login_library->controlador_raiz = $controlador_raiz;

			$this->tsdk_login_library->check_is_logged();

		}*/

		//inicializando los parametros del controlador
		$this->m_parametros_controlador = new tsdk_parametros_controlador($this);

		//estableciendo el manejador de base de datos
		$this->m_manejadorbd = $manejadorbd;

		//conectando a la base de datos (mysql o postgresql)
		switch ($this->m_manejadorbd){
			case "mysql":
				$this->m_basedatos = new tsdk_basedatos_mysql();
				$this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$bd_is_persistent);
				break;
			case "postgresql":
				$this->m_basedatos = new tsdk_basedatos_postgresql();
				$this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$esquema,$bd_is_persistent);
				break;
			default:
                $lineaerror = "Manejador de base de datos [ ".$manejadorbd." ] no reconocido.\n";
                //error_log($lineaerror);

                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x ".$lineaerror.
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );
                    
                break;
		}
		
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    public function __destruct()
    {
    	//error_log("Dentro de destructor del controlador tsdk");
        //$this->m_conexion = null;
    }

	//-----------------------------------------------------------
	/**
	* Implementa la funcion principal del controlador
	*
	*/
	protected function index()
	{
		//leyendo las variables post ... desde las variables de session

		/*** comentado temporalmente
		if (isset($_SESSION['session_post']))
		{
			$m_session_post = $_SESSION['session_post'];
		} else {
			$m_session_post = array();
		}

		//inicializando las variables que no existen
		if (!isset($m_session_post['mensaje']) )
		{
			$m_session_post['mensaje']="";
		}*/
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion inicializa las variables de session que se utilizaran posteriormente
	* 
	* @return none
	*/
	/*protected function inicializar_variables()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();
		}

		//verificando si existe el parametro mensaje
		if (! $this->m_parametros_controlador->exists_parametro("mensaje"))
		{
			$this->m_parametros_controlador->mensaje="";
		}

		//verificando si existe el parametro mensaje
		if (! $_SESSION['parametros_controlador']->exists_parametro("mensaje"))
		{
			$_SESSION['parametros_controlador']->mensaje="";
		}

	} */

	//-----------------------------------------------------------
	/**
	* Esta funcion establecer los parametros del controlador.
	* Se debe llamar cada vez que se inicia el controlador (llamado por index o ajax)
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	/*protected function load_parametros_controlador()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();

			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				 "#x variable SESSION['parametros_controlador'] no existe".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
		}

		$lista = $_SESSION['parametros_controlador']->get_parametros();
		$this->m_parametros_controlador->set_parametros($lista);
	}*/

	//-----------------------------------------------------------
	/**
	* Esta funcion redirecciona a otra pagina de la aplicacion. 
	* Guarda en una variable de session llamada parametros_controlador un objeto
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	/*protected function redirect($url,$parametros_controlador)
	{
	

		$_SESSION["parametros_controlador"] = $parametros_controlador;

		header('Location: '.$url);
	}*/

	//-----------------------------------------------------------
	/**
	* Esta funcion valida si los controles indicados en la lista existen o no
	* @param  array 	$lista 		Arreglo conteniendo los nombre de los controles a validar. 
	* 								En la lista se ingresa el nombre del ID del control.
	*  								Estos controles los valida contra la lista $_POST
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
	*/
	/*protected function existe_controles_post($lista)
	{
		foreach ($lista as $nombrecontrol)
		{
			if (!isset($_POST[$nombrecontrol]))
			{
				$this->m_mensaje=tsdk_reemplazar_html("No existe item: ".$nombrecontrol);
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				return false;
			}
		}
		return true;
	}*/

	//-----------------------------------------------------------------------
	//validando que el control no este vacio y que el valor sea del
	//tipo configurado
	//-----------------------------------------------------------------------

	//-----------------------------------------------------------
	/**
	* Esta funcion valida que los controles obligatorios no se encuentren vacios, asi como contengan el tipo de dato correcto.
	* @param  array 	$ista 		Arreglo de dos dimensiones. Es una lista de datos a validar.
	* 								Cada nodo tiene un arreglo, donde el primer campo es el ID del control a validar,
	* 								el segundo campo es el nombre, y el tercer campo es el tipo de dato. 
	* @param  object    $nombre     Es el nombre del control que tiene error. Este parametro es pasado por referencia.
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
	*/
	/*protected function validar_controles_obligatorios($lista, &$nombre)
	{
		foreach ($lista as $control )
		{
		  $nombrecontrol = $control[0];
		  $desc_control = $control[1];
		  $tipo_dato = $control[2];

		  //validando que el control tenga un dato
		  //$valor = trim($this->input->post($nombrecontrol));
		  $valor = trim($_POST[$nombrecontrol]);

		  //validando el tipo de dato
		  switch ($tipo_dato){
			case tsdk_tipodato::tstring:
			  {
				break;
			  }
			case tsdk_tipodato::tstring_no_vacio:
			  {
				$cadena=trim($valor);
				if ($cadena=="")
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó datos en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789+-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<=0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor a CERO en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_igual_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor o igual a CERO en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}

				break;
			  }
			case tsdk_tipodato::tdouble_mayor_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}

				$numero = (double)($valor);
				if ($numero<=0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor a CERO en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble_mayor_igual_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}

				$numero = (double)($valor);
				if ($numero<0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor o igual a CERO en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }

			case tsdk_tipodato::temail_o_vacio:
			  {
				//validando si es vacio
				if ( $valor == "")
				{
				  break; //OK
				}

				//validando que sea tipo email
				$valor = (string)($valor);
				$encuentros=preg_match("/^[0-9a-zA-Z]+([\._]?[[:alnum:]])*[[:alnum:]]@[0-9a-zA-Z]+([-]?[[:alnum:]])*[.][a-zA-Z]{2,4}$/",$valor); //validando
				if ($encuentros==0)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un correo valido en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }

			case tsdk_tipodato::tboolean:
			  {
				if ($valor != true && $valor != false)
				{
				  $this->m_mensaje=tsdk_reemplazar_html("No ingresó un BOOLEAN en [ ".$desc_control." ]");
				  error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				  $nombre=$nombrecontrol;
				  return false;
				}
				break;
			  }
			default:
			  {
				$this->m_mensaje=tsdk_reemplazar_html("No ingresó un TIPO DE DATO correcto, revisar programa !!!!");
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				echo "#x Error en validar_controles_obligatorios. Control:".$nombrecontrol.
				  ", descripcion:".$desc_control.
				  ", tipo de dato:".$tipo_dato.
				  ", valor:".$valor.
				  ". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
				  "\n";
				debug_print_backtrace();

				$nombre="";
				return false;

				break;
			  }
		  }
		}

		return true;
	}*/


/*** comentado temporalmente


  //-----------------------------------------------------------------------
  //Inserta desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function insertar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//estableciendo valor de eliminar
	//$objeto->set_item_valor('eliminar',false);
	$objeto->eliminar=false;

	//ejecutando
	if ( $objeto->insertar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }

  //-----------------------------------------------------------------------
  //Modifica desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function modificar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos (primero se obtendra los valores)
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//obteniendo valores ... quizas algunos campos no se modifiquen
	if ( $objeto->obtener() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	//volviendo a establecer para modificar campos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	$respuesta=$objeto->modificar();
	if ( $respuesta < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return $respuesta;
  }

  //-----------------------------------------------------------------------
  //elimina desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function eliminar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	if ( $objeto->eliminar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }*/

		
	//-----------------------------------------------------------
	/**
	* Esta funcion se encarga de generar un token (JWT).
	* @return string		Devuelve un string que identifica unicamente al cliente
	*/
	protected function get_aud()
	 {
        $aud = '';
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $aud = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $aud = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $aud = $_SERVER['REMOTE_ADDR'];
        }
        
        $aud .= @$_SERVER['HTTP_USER_AGENT'];
        $aud .= gethostname();
        
        return sha1($aud);
    }	
	
	//-----------------------------------------------------------
	/**
	* Esta funcion se encarga de generar un token (JWT).
	* @param  array     $datos_session		Arreglo donde se guarda los datos de la session
	* @param  string    $clave				Se refiere a la clave que se usará para crear el token
	* @return String				Devuelve el token como un texto
	*/
	protected function generar_token($datos_session=null, $clave)
	{
		$payload = array();
		
		$time=time();
		$payload["iat"]=$time;
		$payload["exp"]=$time+(60*60*24); //duracion de un dia
		$payload["datos"]=$datos_session;
		
		$texto_token = JWT::encode($payload, $clave);
		
		return $texto_token;
	}
			
	//-----------------------------------------------------------
	/**
	* Esta funcion se encarga de generar un token (JWT).
	* @param  string    $token		Es el token que deseamos validar
	* @param  string    $clave		Se refiere a la clave que se usará para crear el token
	* @return array					Los datos del token
	*/
	protected function validar_token($token, $clave)
	{
		//excepcion
		if ($token==="prueba")
		{
			return 1;
		}
		//fin-excepcion
		
		$decode = null; //objeto a decodificar
		
		if(empty($token))
        {
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Token no proporcionado.".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return null;
        }
        
		try{
			$decode = JWT::decode(
							$token,
							$clave,
							['HS256']
						);
		} catch (UnexpectedValueException $ex) {
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en descifrar token. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return null;
		}
        
        
        if($decode->datos->aud !== $this->get_aud())
        {
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Cliente inválido, o token incorrecto. Cliente en token:".$decode->datos->aud.". Cliente actual:".$this->get_aud().
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			return null;
        }
		
		return $decode;
	}
		
} //fin de clase

