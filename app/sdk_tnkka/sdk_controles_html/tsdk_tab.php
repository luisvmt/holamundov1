<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_tabs_coleccion.php");

//-----------------------------------------------------------
/**
* Control HTML - TAB
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_tab extends tsdk_control
{
	
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access private
    */

    /**
    * Indica el numero de tabs existente
    * @var integer
    */
    //private   $m_nrotabs=0;

    /**
    * Arreglo con los contenidos de los tabs
    * @var array
    */
    //private   $m_content=array();

    /**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
    
    /**
    * Representa la coleccion de los titulos del tab
    * @var coleccion
    */
    public  $tabs=null;
    
    /**
    * Representa la coleccion de los contenidos del tab
    * @var coleccion
    */
    //public  $contenidos=null;

    /**#@-*/
    

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_button.php"
    */
	function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
	{
		parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        $this->propiedades->add("class")->value="tsdk_tab";
        //$this->titulos = new tsdk_coleccion();
        //$this->contenidos = new tsdk_coleccion();
        $this->tabs = new tsdk_tabs_coleccion();
        
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_tabs</b>       :   Devuelve el numero de tabs presente en el control</br>
    * 
    * @example "sample_tsdk_tab.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "nro_tabs":
			//return $this->m_nrotabs;
            return $this->titulos->count();
			break;
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_tabs</b>       :   Establece el número de tabs que tendra el control. </br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_button.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		/*case "nro_tabs":
			$this->m_nrotabs=$value;
			for ($i=0;$i<$value;$i++)
			{
				if (!isset($this->m_titulos[$i]))
				{
					$this->m_titulos[$i]="";
				}
			}
			for ($i=0;$i<$value;$i++)
			{
				if (!isset($this->m_content[$i]))
				{
					$this->m_content[$i]="";
				}
			}
			break;*/
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion permite acceder directamente a la coleccion.
    * Tiene el mismo nombre que la coleccion
    *
    * @access public
    * @param  string    $indice     Indice del item al cual acceder
    * @return item
    * 
    * @example "sample_tsdk_tab.php"
    */
    public function &tabs($indice)
    {
        return $this->tabs->item($indice);
    }


/*
<ul class="tabs">
    <li><a href="#tab1">Gallery</a></li>
    <li><a href="#tab2">Submit</a></li>
</ul>

<div class="tsdk_tab_container">
    <div id="tab1" class="tsdk_tab_content">
        <!--Content-->
        contenido 1
    </div>
    <div id="tab2" class="tsdk_tab_content">
       <!--Content-->
       contenido 2
    </div>
</div>*/


    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_tab.php"
    */
	public function get_html()
	{
        //estableciendo el idventana y idcontrol
        foreach ($this->tabs as $clave => $item) //cada item es del tipo tsdlk_item_coleccion
        {
            $item->idventana = $this->idventana;
            $item->idcontrol = $this->idcontrol;
        }

        //imprimiendo DIV principal
        $salida  = "<div ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >\n";

        //recorriendo los tabs
        $salida .= "<ul>\n";

        $nrotab=0;
        foreach ($this->tabs as $clave => $item) //cada item es del tipo tsdk_tab_item
        {
            $salida .= "   <li><a href='#tab".($nrotab+1)."'>".$item->titulo."</a></li>\n";
            $nrotab++;
        }
        $salida .= "</ul>\n";

        //recorriendo e imprimiendo los contenidos
        $nrotab=0;
        foreach ($this->tabs as $clave => $item) //cada item es del tipo tsdlk_item_coleccion
        {
            $salida .= "   <div id='tab".($nrotab+1)."' class='tsdk_tab_content'>\n";
            $salida .= $item->contenido."\n";
            $salida .= "   </div>\n";
            $nrotab++;
        }
        
        //cerrando div principal
        $salida .= "</div>"; 

		return $salida;
	}

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_tab.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
