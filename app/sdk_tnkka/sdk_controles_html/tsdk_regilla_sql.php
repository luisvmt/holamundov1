<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_basedatos/tsdk_consulta_sql.php");

//-----------------------------------------------------------
/**
* Control HTML - Regilla SQL
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_regilla_sql extends tsdk_control
{
	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa la base de datos
	* @var object
	*/
	protected   $m_basedatos;

    /**
    * manejador de base de datos a utilizar
    * @var string
    */
    protected       $m_manejadorbd;

	/**
	* Representa el id de la ventana
	* @var object
	*/
	protected   $m_idventana;

	/**
	* Representa el id del control
	* @var object
	*/
	protected   $m_idcontrol;

	/**
	* Lista de titulos de la tabla
	* @var array
	*/
	protected   $m_columna_titulo=array(); 
	
	/**
	* Lista con los alineamientos de las columnas de la tabla
	* @var array
	*/
	protected   $m_columna_alineamiento=array();
	
	/**
	* Lista con los anchos de las columnas de la tabla
	* @var array
	*/
	protected   $m_columna_ancho=array();

	/**
	* Lista con las propiedades de cada columna de la tabla
	* @var array
	*/
	protected   $m_columna_propiedades=array();

	/**
	* Lista con los estilos de cada columna de la tabla
	* @var array
	*/
	protected   $m_columna_estilos=array();

	/**
	* Propiedad que determina si la columna se podrá ordenar
	* @var boolean
	*/
	protected   $m_permit_column_order;
	
	/**
	* Determina cual es la columna que tiene el orden. La primera columna es cero (0)
	* @var integer
	*/
	protected   $m_columna_orden; 
	
	/**
	* Determina cual es el sentido del orden, ascendente o descendente
	* @var string
	*/
	protected   $m_columna_sentido; 

	/**
	* Resultset con los datos de la base de datos
	* @var object
	*/
	protected   $m_resultset=null;
	
	/**
	* arreglo que contiene los campos con los cuales se creara el filtro de la consulta.
	* @var array
	*/
	//protected   $m_campos_filtro=array();
	
	/**
	* Determina el tamaño de la pagina, es decir el numero de lineas de datos que tiene la regilla (tabla)
	* @var integer
	*/
	protected   $m_tamano_pagina=0;
	
	/**
	* Determina el numero de registros devueltos en la regilla
	* @var integer
	*/
	protected   $m_numero_registros_regilla=0;
	
	/**
	* Determina el numero de pagina a mostrar. La primera pagina es uno (1)
	* @var integer
	*/
	protected   $m_numero_pagina=0;

	/**
	* Determina el numero de paginas que tiene la consulta.Este dato es de salida
	* @var integer
	*/
	protected   $m_totalpaginas=0;   
	
	/**
	* Determina el numero de registros que tiene la consulta
	* @var integer
	*/
	protected   $m_totalregistros=0; 
		
	/**
	* Propiedad que determina si se implementa el evento: fila onclick
	* @var boolean
	*/
	protected   $m_permit_fila_onclick;
	
	/**
	* Propiedad que determina si se implementa el evento: fila ondblclick
	* @var boolean
	*/
	protected   $m_permit_fila_ondblclick;
	
	/**#@-*/

	//-----------------------------------------------------------
	/**#@+
	* @access public
	*/

	/**
	* Objeto consulta de base de datos. Implementa la clase tsdk_consulta_sql
	* @access public
	* @var object
	*/
	public      $consulta;

	/**
	* Implementa la regilla/tabla del objeto
	* @var integer
	*/
	public   $regilla; 

	/**
	* Implementa un callback que se llama cada vez que se añade una fila a la tabla
	* @var function
	*
	* Una forma de llamar desde una clase externa: 
	* ...
	* $regilla_sql->funcion_after_add_fila = array($this,'after_add_fila');
	* ... 
	* public function after_add_fila() {
	*	...
	* }
	*
	*/
	public   $funcion_after_add_fila=null; 	

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  object     $basedatos         Es el objeto de base de datos que se utilizará en la regilla.
	* @example "sample_tsdk_regilla_sql.construct.txt"
	*/
	function __construct($idventana,$idcontrol,$basedatos,$manejadorbd)
	{
		//parent::__construct();

		//iniciando variables
		$this->m_basedatos = $basedatos;
		$this->m_manejadorbd = $manejadorbd;
		$this->m_idventana = $idventana;
		$this->m_idcontrol = $idcontrol;
		
		//creando objeto de base de datos
		$this->m_resultset = new tsdk_resultset($basedatos, $this->m_manejadorbd);

		//$this->nrofilas_header = $filas_cabecera;
		//$this->nrofilas_footer = $filas_pie;  

		//por defecto no permite hacer clic en el titulo para ordenar
		$this->m_permit_column_order = false; //no permite hacer clic en la columna

		//por defecto no se implementa los eventos de la fila
		$this->m_permit_fila_onclick = false; //no implementa evento click en la fila
		$this->m_permit_fila_ondblclick = false; //no implementa evento double-click en la fila

		//propiedades para la seleccion de filas
		//esto se implementa en javascript, debe implementarse la seleccion de la fila al momento de dar clic a una fila
		
		//propiedades para la consulta a la bd
		$this->consulta = new tsdk_consulta_sql($basedatos,$this->m_manejadorbd);

		//estableciendo el tipo de control
		$this->tipocontrol = tsdk_tipocontrol::tabla_sql;

		//iniciando la tabla
		//Tener en cuenta que necesita que se tenga implementado la hoja de estilo para la clase tsdk_tabla_class
		$this->regilla = new tsdk_tabla($this->m_idventana, 		//idventana
											$this->m_idcontrol,	//idcontrol
											0, //nrofilas
											0,	//nrocolumnas
											array(	"class"=>"tsdk_tabla_class",
													"border"=>"0"
												)
										);
		$this->regilla->nro_header = 1;
		$this->regilla->nro_footer = 0;		
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Get magic method
	*
	* Implementa la recuperacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	*
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>numero_pagina</b>         :   Numero de pagina actual</br>
	*    <b>tamano_pagina</b>         :   Tamano de pagina de la regilla (numero de filas)</br>
	*    <b>total_paginas</b>         :   Total de paginas de la consulta</br>
	*    <b>total_registros</b>       :   Total de registros de la consulta</br>
	*    <b>permit_column_order</b>   :   Permite ordenar las columnas</br>
	* 
	* @example "sample_tsdk_button.php"
	*/
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "numero_pagina":
			return $this->m_numero_pagina;
		case "tamano_pagina":
			return $this->m_tamano_pagina;
		case "total_paginas":
			return $this->m_totalpaginas;
		case "total_registros":
			return $this->m_totalregistros;
		case "columna_orden":
			return $this->m_columna_orden;
		case "columna_sentido":
			return $this->m_columna_sentido;
		case "permit_column_order":
			return $this->m_permit_column_order;
		case "permit_fila_onclick":
			return $this->m_permit_fila_onclick;
		case "permit_fila_ondblclick":
			return $this->m_permit_fila_ondblclick;
		case "numero_registros_regilla":
			return $this->m_numero_registros_regilla;
		default:
			return parent::__get($propiedad);
			/*tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en get. Propiedad no implementada: ".$propiedad.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			return null; */
		}
	}

	//-----------------------------------------------------------
	/**
	* Set magic method
	*
	* Implementa la modificacion de propiedades de la clase.
	* @access public
	* @param string $propiedad      La propiedad a devolver
	 *
	*    <h5><strong>Posibles valores:</strong></h5> 
	*    <b>numero_pagina</b>         :   Numero de pagina actual</br>
	*    <b>tamano_pagina</b>         :   Tamano de pagina de la regilla (numero de filas)</br>
	*    <b>permit_column_order</b>   :   Permite ordenar las columnas</br>
	* 
	* @param string $value        El valor a establecer en la propiedad
	* @example "sample_tsdk_button.php"
	*/
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "numero_pagina":
			$this->m_numero_pagina = $value;
			$this->consulta->numero_pagina = $value;
			break;
		case "tamano_pagina":
			$this->m_tamano_pagina = $value;
			$this->consulta->tamano_pagina = $value;
			break;
		case "permit_column_order":
			$this->m_permit_column_order=$value;
			break;
		case "permit_fila_onclick":
		    $this->m_permit_fila_onclick=$value;
		    break;
		case "permit_fila_ondblclick":
			$this->m_permit_fila_ondblclick=$value;
			break;
		default:
			parent::__set($propiedad,$value);
			/*tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en set. Propiedad no implementada: ".$propiedad.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);*/
		}
	}

	//-----------------------------------------------------------
	/**
	* Agrega una columna a la regilla
	* @access public
	* 
	* @param    string 		$titulo 		Titulo de la columna
	* @param    string 		$alineamiento 	Alineamiento de la columna
	* @param    string 		$ancho 		 	Ancho de la columna
	* @param    array 		$propiedades 	Propiedades de la columna
	* @param    array 		$estilos 		Estilos de la columna
	* @return   none
	* @example "sample_tsdk_regilla_sql.php"
	*/
	public function add_columna_regilla($titulo,$alineamiento,$ancho="",$propiedades=array(),$estilos=array() )
	{
		//luisdiaz
		//guardando los datos
		$this->m_columna_titulo[]=$titulo;
		$this->m_columna_alineamiento[]=$alineamiento;
		$this->m_columna_ancho[]=$ancho;
		$this->m_columna_propiedades=$propiedades;
		$this->m_columna_estilos=$estilos;
	}
	
	//-----------------------------------------------------------
	/**
	* Obtiene el número de registros de la consulta
	* @access private
	* 
	* @return   integer 		Numero de registros de la consulta
	*/
	private function get_nro_registros()
	{
		$consulta = $this->consulta->get_sentencia_sql_contador(); //solo devuelve consulta para contar los registros
		$tabladatos = $this->m_resultset->obtener_tabla_from_sql($consulta);
		return $tabladatos->get_valor_campo(0,"contador");
	}

	//-----------------------------------------------------------
	/**
	* Obtiene los datos de la consulta
	* @access private
	* 
	* @return   object 		Obtiene la consulta de base de datos. Devuelve el objeto 'tsdk_datos_sql'
	*/
	private function get_datos_bd()
	{
		$consulta = $this->consulta->get_sentencia_sql_paginada(); //devuelve consulta de una pagina
//error_log("xxx consulta:".$consulta);

		return $this->m_resultset->obtener_tabla_from_sql($consulta);
	}

	//-----------------------------------------------------------
	/**
	* Establece la columna a ordenar y el sentido
	* @access public
	* 
	* @param    integer     $nrocolumna	    Numero de columna a ordenar. La primera columna empieza en 0 (cero). 
	* @param    string      $sentido        Sentido a ordenar la columna
	* @return   none
	* @example "sample_tsdk_regilla_sql.php"
	*/
	public function set_orden($nrocolumna,$sentido)
	{
		//validando si la columna es numerico
		if (! is_numeric($nrocolumna)){
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				"#x Error en set_orden.".
				"  nrocolumna no es numerico (debe ser mayor o igual a cero): ".$nrocolumna.
				", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			return; //no se modifica  ningun valor
		}
			
		//validando los valores
		if ($nrocolumna<0)
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
				"#x Error en set_orden.".
				"  nrocolumna no valido (debe ser mayor o igual a cero): ".$nrocolumna.
				", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			return; //no se modifica  ningun valor
		}

		//estableciendo la columna a ordenar
		$this->m_columna_orden = $nrocolumna; 
		$this->m_columna_sentido = $sentido;

		//estableciendo en consulta
		//ojo: en sql la primera consulta empieza en 1
		$nrocolumna++;
		
		$this->consulta->order_by = array($nrocolumna." ".$sentido);
	}
	
	//-----------------------------------------------------------
	/**
	* Se encarga de crear el encabezado de la regilla
	* @access private
	* 
	* @return   none
	*/
	private function create_encabezado()
	{
		//eliminando datos anteriores
		$total_columnas = count($this->m_columna_titulo);
		$this->regilla->set_dimensiones(1,$total_columnas); //eliminando todos los datos

		//guardando el nombre del campo de base de datos
		//$this->m_columnas_camposbd[] = $campo_bd;

		for ($i=0; $i<$total_columnas; $i++)
		{ 
			$titulo = $this->m_columna_titulo[$i];

			if ($this->permit_column_order)
			{       
				//creando link para ordenar la columna
				$linkordenar = new tsdk_link($this->m_idventana,$this->m_idcontrol."_linkordenar");
				
				$linkordenar->propiedades->set_propiedades(array("class"=>"tsdk_tabla_linkordenar_class",
																 "href"=>"#"));

				//agregando el icono de orden
				if ( $i == $this->m_columna_orden && $this->m_columna_sentido!="") //significa que estamos en la columna ordenada
				{
					if ( strtoupper($this->m_columna_sentido)=="ASC" )
					{
						//$evento="columna_onclick";
						$parametros=tsdk_reemplazar_html(json_encode(array("nrocolumna"=>"$i","orden"=>"desc")));

						//modificando el titulo
						$titulo .= " &#9660;"; //flecha abajo
					}else{
						//$evento="columna_onclick";
						$parametros=tsdk_reemplazar_html(json_encode(array("nrocolumna"=>"$i","orden"=>"asc")));

						//modificando el titulo
						$titulo .= " &#9650;"; //flecha arriba
					}
				}else{
					//$evento="columna_onclick";
					$parametros=tsdk_reemplazar_html(json_encode(array("nrocolumna"=>"$i","orden"=>"asc")));
				}
				$linkordenar->eventos->add_evento_standard("onclick","event,this,'".$parametros."'");

				//convirtiendo en link
				$linkordenar->texto = $titulo;
				$titulo = $linkordenar->get_html();
			}
			
			//creando checkbox del encabezado
			//se añade como una columna adicional ... por revisar
		/*  $controlchk = new tsdk_textbox($this->idventana,"chktitulo");
			$controlchk->propiedades->set_propiedades(array( "type"=>"checkbox",
												//"id"=>$this->id."_chktitulo",
												//"name"=>$this->id."_chktitulo",
												"class"=>"tvbrowse_chktitulo_class"
											));
			$controlchk->set_eventos(array("onclick"=>$this->id."_chktitulo_onclick()"));

			//añadiendo el check del titulo
			if ( count($this->m_lista_acciones)>0 )
			{
				array_unshift( $titulos,$controlchk->get_html() ); //añadiendo el check del encabezado
			}else{
				array_unshift( $titulos, "" ); //añadiendo el check del encabezado
			}
	*/

			//estableciendo el alineamiento
			$this->regilla->set_estilos_celda(0,$i,array("text-align"=>$this->m_columna_alineamiento[$i]) );  

			//colocando el titulo de columna
			$this->regilla->set_valor_celda(0,$i,$titulo);

			//estableciendo el ancho de la columna
			if ($this->m_columna_ancho[$i] != "")
				$this->regilla->set_estilos_columna($i,array("width"=>$this->m_columna_ancho[$i]) );  

			//estableciendo las propiedades del titulo
			$this->regilla->set_propiedades_columna($i,$this->m_columna_propiedades );

			//Estableciendo los estilos del titulo
			$this->regilla->set_estilos_columna($i,$this->m_columna_estilos );  


		}           

	}

	//-----------------------------------------------------------
	/**
	* Permite establecer la funcion callback que se ejecutará cada vez que se añade una fila
	* @access public
	* @param    object   	$tabladatos   	Datos de la base de datos
	* @return   none
	*/
	/*public function set_callback_after_add_fila($funcion)
	{
		error_log("xxx xxx");
	}*/

	//-----------------------------------------------------------
	/**
	* Se encarga de crear la tabla a partir de los datos obtenidos
	* @access private
	* @param    object   	$tabladatos   	Datos de la base de datos. Es del tipo de objeto 'tsdk_datos_sql'
	* @return   none
	*/
	private function create_tabla($tabladatos, //tabla que contiene los datos a mostrar
									$habilitar_onclick_by_fila=false, //habilita el evento onclick
									$habilitar_ondblclick_by_fila=false)  //habilita el evento ondblclick
	{
		//rellenando los datos
		$nrofila = 1;
		$nrocolumna = 0;
		//error_log("xxx tamano-fila-nro-columnas:".$tabladatos->filas->item(0)->count());

		foreach ($tabladatos->filas as $fila)
		{
			//añadiendo una fila
			$this->regilla->add_fila();

			//inicializando valores de la fila      
			$valores_fila = array();

			//creando los parametros de la clave primaria como json
			$listaclave=array();

			//añadiendo nro fila
			$this->regilla->filas->item($nrofila)->propiedades->add("data-nrofila")->value=$nrofila;

			//añadiendo propiedad is_selected
			$this->regilla->filas->item($nrofila)->propiedades->add("data-is_selected")->value="false";

			//añadiendo la clave primaria
			if ( $fila->key_exists("clave_primaria"))
			{
				//convirtiendo la clave primaria en json
				$claveprimaria = explode(",", $fila->item('clave_primaria')->value ); //obtiene lista de claves primaria
//error_log("xxx claveprimaria:".$fila->item('clave_primaria')->value);

				foreach ($claveprimaria as $clave)
				{
					list($indice,$valor) = explode("=",$clave);
					$listaclave[$indice]=$valor;
				}
				$parametros_clave=tsdk_reemplazar_html(json_encode($listaclave));
				//$this->add_propiedades_fila($nrofila,array("data-clave"=>$parametros_clave));
				$this->regilla->filas->item($nrofila)->propiedades->add("data-clave")->value=$parametros_clave;


			}else{
				//$this->add_propiedades_fila($nrofila,array("data-clave"=>""));
				$this->regilla->filas->item($nrofila)->propiedades->add("data-clave")->value="";
			}

			//$parametros_clave=tsdk_reemplazar_html(json_encode($listaclave));

			/*
			//creando checkbox de la fila
			$controlchk = new tsdk_textbox($this->idventana,"chkfila".$nrofila);
			$controlchk->propiedades->set_propiedades(array( "type"=>"checkbox",
												//"id"=>$this->m_id."_chkfila".$nrofila,
												//"name"=>$this->m_id."_chkfila".$nrofila,
												"class"=>"tvbrowse_chkfila_class",
												"value"=>$parametros_clave));

			//estableciendo los datos de la fila
			if ( count($this->m_lista_acciones)>0 )
			{
				$valores_fila=array( $controlchk->get_html() ); //estableciendo primera columna
			}else{
				$valores_fila=array("");
			}*/

			//error_log("xxx totalfilas:".$this->regilla->filas->count() );
			//error_log("xxx nrofila:".$nrofila);
			//error_log("xxx tamano-fila:".$fila->count());

			//estableciendo la clase de la fila
			$this->regilla->filas->item($nrofila)->propiedades->add("class")->value="tsdk_tabla_noselected_class";

			//estableciendo eventos: click en la fila
			if ($this->m_permit_fila_onclick){
				$parametros=$nrofila;
				$this->regilla->filas->item($nrofila)->eventos->add("onclick")->value = "fila_onclick(event,this,'".$parametros."')";
			}
			
			//estableciendo eventos: dblclick en la fila
			if ($this->m_permit_fila_ondblclick){
				$parametros=$nrofila;
				$this->regilla->filas->item($nrofila)->eventos->add("ondblclick")->value = "fila_ondblclick(event,this,'".$parametros."')";
			}

			//agregando valores de las columnas
			$nrocolumna=0;

			foreach ($fila as $clave=>$valor)
			{
				if ($clave != "clave_primaria")
				{
					$valores_fila[] = tsdk_reemplazar_html($valor);

					//estableciendo el alineamiento
					$this->regilla->set_estilos_celda($nrofila,$nrocolumna,
												array("text-align"=>$this->m_columna_alineamiento[$nrocolumna]) 
											); 

//error_log("nrofila:".$nrofila." nrocolumna:".$nrocolumna." total-columnas:".count($this->m_columna_titulo)." align:".$this->m_columna_alineamiento[$nrocolumna]." valor:".$valor);

					$nrocolumna++; 
				}else{
//error_log("OJO nrofila:".$nrofila." nrocolumna:".$nrocolumna." total-columnas:".count($this->m_columna_titulo)." align:".$this->m_columna_alineamiento[$nrocolumna]." valor:".$valor);

				}

				if ($nrocolumna>=count($this->m_columna_titulo)) //si la consulta tiene mas columnas que la tabla
					break;          
			}

			//estableciendo los valores de la fila
			$this->regilla->set_valores_fila($nrofila,$valores_fila);

			//llamando a un callback, para que modifiquen los valores de la fila ... si lo considera el programador.
			if ( !is_null($this->funcion_after_add_fila))
			{
				call_user_func_array( $this->funcion_after_add_fila, array($nrofila,&$this->regilla) );
			}

			$nrofila++;
		}
	}

	//-----------------------------------------------------------
	/**
	* Configura la regilla como una tabla SQL.
	* Tener en cuenta que necesita que se tenga implementado la hoja de estilo para la clase tsdk_tablasql_class
	* @access public
	* 
	* @return   none
	* @example "sample_tsdk_regilla_sql.php"
	*/
	/*public function config_as_tablasql()
	{
		//$this->propiedades->border = 0; //borde por defecto
		
		$this->propiedades->add("class")->value="tsdk_tablasql_class";
		$this->propiedades->add("border")->value="1";
		
		$this->set_dimensiones(1,0,true);

		$this->nro_header = 1;
		$this->nro_footer = 0;  

	}*/

	//-----------------------------------------------------------
	/**
	* Esta funcion crea la tabla según los parametros ingresados y la puebla de datos.
	* Se crea primero para que posteriormente esta tabla se pueda modificar luego de creada.
	* @access public
	* 
	* @return   string  	codigo HTML
	* @example "sample_tsdk_regilla_sql.php"
	*/
	public function make_tabla()
	{

		//verificando valores por defecto
		if ($this->m_numero_pagina <= 0)
		{
			$this->numero_pagina = 1;   //por defecto se recupera la primera pagina
			$this->tamano_pagina = 100; //por defecto se recupera los 100 primeros registros
		}

		//recuperando el numero de registros
		$this->m_totalregistros = $this->get_nro_registros();
//error_log("xxx 1 total-registros:".$this->m_totalregistros);

		//calculando total de paginas
		if ($this->m_tamano_pagina>0) //si es igual a cero, significa todos los registros
		{
			$this->m_totalpaginas = ($this->m_totalregistros % $this->m_tamano_pagina == 0)?$this->m_totalregistros/$this->m_tamano_pagina:
																							  floor($this->m_totalregistros/$this->m_tamano_pagina) + 1;

			//validando el nro de pagina, corrigiendo
			if ($this->m_numero_pagina > $this->m_totalpaginas)
			{
				$this->numero_pagina=1;
			}
		}else{
			$this->m_totalpaginas=0;
			$this->numero_pagina=1;
		}
//error_log("xxx 2 total-paginas:".$this->m_totalpaginas);
//error_log("xxx 3 m_numero_pagina:".$this->m_numero_pagina);

		//-------------------------------
		//recuperando datos de la tabla
		$tabladatos = $this->get_datos_bd();

		if ($tabladatos == null)
		{
			return; //no se imprime el error en la pagina, solo en el log
		}

		//devolviendo el numero de registros que tiene la regilla
		$this->m_numero_registros_regilla = $tabladatos->filas->count();
//error_log("xxx 4 nro registros tabla datos:".$tabladatos->filas->count());
//error_log("xxx 4.1 nro columnas tabla datos:".$tabladatos->filas->item(0)->count());
		
		//--------------------------------
		//creando nuevamente el encabezado
		$this->create_encabezado();

		//--------------------------------
		//rellena datos a la tabla
		$this->create_tabla($tabladatos);

		//--------------------------------
		//adiciona propiedades
		$this->regilla->propiedades->add("width")->value="100%";

		//falta poner barras de desplazamiento
		
		//luis agregar estilo de ancho al div
		//quizas style="height:30%;background:red"

		//$this->m_tabla->add_propiedades_columna(0,array("width"=>"5%"));

	} 

	//-----------------------------------------------------------
	/**
	* Obtiene el codigo HTML que crea la regilla SQL
	* Tener en cuenta que necesita que se tenga implementado la hoja de estilo para la clase tsdk_tablasql_class
	* @access public
	* 
	* @return   string  	codigo HTML
	* @example "sample_tsdk_regilla_sql.php"
	*/
	public function get_html($make = true //fuerza que la tabla se vuelva a crear
								) 
	{
		if ($make){
			$this->make_tabla();
		}

		return $this->regilla->get_html();
	}

	//-----------------------------------------------------------
	/**
	* Imprime en la salida estandar el codigo HTML que crea la regilla SQL
	* Tener en cuenta que necesita que se tenga implementado la hoja de estilo para la clase tsdk_tablasql_class
	* @access public
	* 
	* @return   none
	* @example "sample_tsdk_regilla_sql.php"
	*/
	public function print_html() 
	{
		echo $this->get_html();
	}

}

?>
