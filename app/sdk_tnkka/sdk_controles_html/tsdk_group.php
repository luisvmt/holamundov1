<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - button
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/
class tsdk_group extends tsdk_control
{
// ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Es el texto que acompaña al control. Es utilizado como titulo del grupo
    * @var string
    */
    protected	$m_texto="";
	
    /**#@-*/


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $texto          Se refiere al texto que aparece en el control. Es utilizado como titulo del grupo
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_button.php"
    */
    function __construct($texto,$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
		$this->texto = $texto;
        $this->tipocontrol = tsdk_tipocontrol::group;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }


//<fieldset>
//  <legend>Datos personales</legend>
//
//  Nombre <br/>
//  <input type="text" name="nombre" value="" />
//  <br/>
//  Apellidos <br/>
//  <input type="text" name="apellidos" value="" />
//  <br/>
//  DNI <br/>
//  <input type="text" name="dni" value="" size="10" maxlength="9" />
//</fieldset>

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>texto</b>        :   Devuelve el texto del control</br>
    * 
    * @example "sample_tsdk_span.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "texto":
			return $this->m_texto;
			break;
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>texto</b>        :   Establece el texto del control</br>
    * 
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_span.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "texto":
			$this->m_texto=$value;
			break;
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_group.php"
    */
    public function get_html()
    {
        $salida = "<fieldset ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >";
        $salida .= "<legend>".$this->m_texto."</legend>";
        $salida .= $this->m_content;
        $salida .= "</fieldset>";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_group.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
