<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - menu_item
*
* Implementa la clase que representa un button de html
*/


class tsdk_menu_item //extends tv_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Es el id del menu
    * @var string
    */    
    protected     $m_idmenu;
    
    /**
    * Es el id del menu parent
    * @var string
    */    
    protected     $m_iditem_parent;

    /**
    * Es el id del menu 
    * @var string
    */    
    protected     $m_id;

    /**
    * Texto a mostrar
    * @var string
    */    
    protected     $m_texto;

    /**
    * Si es true, crea una llamada al evento onclick. Caso contrario no la crea
    * @var boolean
    */    
    protected     $m_show_event;

    /**
    * lista de subitems del menu
    * @var array
    */    
    protected     $m_subitems;

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string     $id            Id del menu
    * @param  string     $texto         Es el texto a mostrar en el menu
    * @param  boolean.   $show_event    Determina si se muestra los eventos
    * @example "sample_tsdk_menu_item.php"
    */
    function __construct($id="",$texto="",$show_event=false)
    {
        $this->m_id=$id;
        $this->m_texto=$texto;
        $this->m_subitems = array();
        $this->m_show_event=$show_event;

    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>id</b>             :   id del control</br>
    *    <b>texto</b>          :   texto a mostrar</br>
    *    <b>show_event</b>     :   muestra los eventos onclick</br>
    *    <b>id_menu</b>        :   id del menu</br>
    *    <b>id_item_parent</b>    :   id del item parent</br>
    * @example "sample_tsdk_menu_item.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "id":
            return $this->m_id;
            break;
        case "texto": //debe ser la union de idventana_idcontrol
            return $this->m_texto;
            break;
        case "show_event": //debe ser la union de idventana_idcontrol
            return $this->m_show_event;
            break;
        case "id_menu":
            return $this->m_idmenu;
            break;
        case "id_item_parent":
            return $this->m_iditem_parent;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en get. Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>id</b>             :   id del control</br>
    *    <b>texto</b>          :   texto a mostrar</br>
    *    <b>show_event</b>     :   muestra los eventos onclick</br>
    *    <b>id_menu</b>        :   id del menu</br>
    *    <b>id_item_parent</b>    :   id del item parent</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_menu_item.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "id":  
            $this->m_id = $value;
            break;
        case "texto":  
            $this->m_texto = $value;
            break;
        case "show_event":
            $this->m_show_event = $value;
            break;
        case "id_menu":
            $this->m_idmenu = $value;

            //estableciendo para cada submenu
            foreach ($this->m_subitems as $clave=>$nodo)
            {
                $nodo->id_menu = $value;
            }
            break;
        case "id_item_parent":
            $this->m_iditem_parent = $value;

            //estableciendo para cada submenu
            foreach ($this->m_subitems as $clave=>$nodo)
            {
                $nodo->id_item_parent = $value;
            }
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en set. Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Añade un subitem al menu
    *
	* @access public
    * @param  object    $subitem          subitem a añadir
    * @return none
    * 
    * @example "sample_tsdk_menu_item.php"
    */
    public function add_subitem($subitem)
    {
        $subitem->id_item_parent = $this->m_id;
        $this->m_subitems[] = $subitem;
    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_menu_item.php"
    */
    public function get_html()
    {
        $salida  = "  <li id='".$this->m_id."'>";
        $salida .= "<a href='#' id='".$this->m_id."_a' ";

        //creando el evento onclick
        //el evento onclick es de la forma: idmenu_iditem_onclick(idmenu,idparent,iditem)
        if ($this->m_show_event)
        {
            $salida .= " onclick=\"";
            $salida .= $this->m_idmenu."_".$this->m_id."_onclick('".$this->m_idmenu."','".$this->m_iditem_parent."','".$this->m_id."');\" ";
        }
        $salida .= ">";
        $salida .= $this->m_texto;
        $salida .= "</a>";

        //imprimiendo subitems
        if ( count($this->m_subitems)>0 )
        {
            $salida .= "\n";
            $salida .= "    <ul id='".$this->m_id."_ul' class='".$this->m_idmenu."_ul' >\n";
        }
        foreach ($this->m_subitems as $clave=>$nodo)
        {
            $salida .= "      ".$nodo->get_html();
        }
        if ( count($this->m_subitems)>0 )
        {
            $salida .= "    </ul>\n";
        }

        //cerrando item
        $salida .= "  </li>\n";

        return $salida;
        
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_menu_item.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }

}

?>
