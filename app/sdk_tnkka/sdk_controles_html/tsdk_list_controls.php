<?php 
/**
 * Este archivo implementa una clase que implementa una coleccion
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");

//-----------------------------------------------------------
/**
* Lista de objetos - coleccion
*
* Implementa una clase que administra una coleccion
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_list_controls extends tsdk_coleccion 
{	
    // ******************************
    // variables miembro
    // ******************************
    
    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * id de la ventana que contiene al control
    * @var string
    */
    protected       $m_idventana;

    /**
    * manejador de base de datos a utilizar
    * @var string
    */
    protected       $m_manejadorbd;

    /**
    * mensaje de error o exito de la clase
    * @var string
    */
    protected       $m_mensaje;

    /**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    
    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Se refiere al id de la ventana que contiene el control
    * @example "sample_tsdk_list_controls.php"
    */
    public function __construct($idventana="")
    {
        $this->m_idventana = $idventana;
        parent::__construct();
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    public function __destruct()
    {
		parent::__destruct();
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    idventana   :   id de la ventana que contiene todos los controles
    *    nro_items   :   nro de controles que contiene la lista
    *    mensaje     :   mensaje de la clase
    *
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "idventana":
            return $this->m_idventana;
            break;
        case "mensaje":
            return $this->m_mensaje;
            break;

        default:
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                 "#x Error en get. Propiedad no encontrada: ".$propiedad.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificación de las propiedades de la clase
    *
    * @param string $propiedad      La propiedad a establecer.
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    idventana   :   id de la ventana que contiene todos los controles
    *    mensaje     :   mensaje de la clase
    *
    * @param string $value          El valor a establecer
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "mensaje":
            $this->m_mensaje=$value;
            break;
        case "idventana":
            $this->m_idventana=$value;
            break;
        default:
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                 "#x Error en set. Propiedad no encontrada: ".$propiedad.
                 //", Value: ".$value.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Busca un control en base a la clave proporcionada.
    * Busca en la lista de controles y en la de subcontroles de cada control
    *
    * @param  string    $indice     El texto que identifica al control (clave)
    * @return object                El control encontrado. Si no lo encuentra devuelve null
    * @example "sample_tsdk_list_controls.php"
    */
    public function &find_item($indice)
    {
        //recorriendo los subcontroles
        foreach ($this as $clave=>$item)
        {
            if ($clave == $indice)
            {
                return $item;
            }else{
                //buscando en los subcontroles de este item
                $control = $item->subcontroles->find_item($indice);
                if (!is_null($control))
                {
                    return $control;
                }
            }
        }

        //significa que no encontro el item dentro del foreach
        $control=null;
        return $control;
    }

    //-----------------------------------------------------------
    /**
    * Recupera una lista de controles que tienen una caracteristica en comun
    * La lista (array) devuelto es por referencia
    *
    * @param    bool        $is_control     Si es true, devuelve la lista de objetos que son controles
    * @param    bool        $has_sql        Si es true, devuelve la lista de objetos que tienen la propiedad sql
    * @return object                El control encontrado. Si no lo encuentra devuelve null
    * @example "sample_tsdk_list_controls.php"
    */
    public function &find_array($is_control=false,$has_sql=false)
    {
        $arreglo = array();

        //creando arreglo unidimensional de todos los controles
        if ($is_control==true)
        {

            //foreach ($this as $clave => & $valor) //revisar luis
            //foreach ($this as $clave => $valor)
            foreach ($this->m_lista as $clave => & $valor) //recorriendo la coleccion, pero como arreglo para poder usar valor por referencia
            {
                $arreglo[$clave]=&$valor;

//tsdk_error_log_trace("---- debug control.clave:".$clave." ------",$valor);

                //buscando en los subcontroles
                $arreglo2 = &$valor->subcontroles->find_array($is_control,$has_sql);

                foreach ($arreglo2 as $clave2 => &$valor2)
                {
                    $arreglo[$clave2] = &$valor2;
                }

            }
        }

        //creando arreglo con controles que tienen sql
        if ($has_sql==true)
        {
            //foreach ($this as $clave => & $valor) //revisar luis
            //foreach ($this as $clave => $valor)
            foreach ($this->m_lista as $clave => & $valor) //recorriendo la coleccion, pero como arreglo para poder usar valor por referencia
            {
                if ($valor->sql != "")
                {
                    $arreglo[$clave]=&$valor;
                }

                //buscando en los subcontroles
                //$arreglo2 = &$valor->m_subcontroles->find_array($has_sql,$is_control);
                $arreglo2 = &$valor->subcontroles->find_array($is_control,$has_sql);

                foreach ($arreglo2 as $clave2 => &$valor2)
                {
                    $arreglo[$clave2] = &$valor2;
                }

            }
        }

        //retornando la salida
        return $arreglo;
    }

    //-----------------------------------------------------------
    /**
    * Busca dentro de la lista de controles el control referenciado.
    * Establecer su valor de acuerdo al tipo de control buscado
    * No devuelve ningun valor
    *
    * @param    string      $indice         Es el nombre del control buscado
    * @param    string      $valor          Es el valor a establecer
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function set_value_item($indice,$valor)
    {
        $control = $this->find_item($indice);
        if (is_null($control))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                 "#x Error en set_value_item. Indice [".$indice."] no encontrado. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            return null;
        }

        //estableciendo el valor del control
        switch ($control->tipocontrol ){
        case tsdk_tipocontrol::textbox:
            $control->value=$valor;
            break;

        case tsdk_tipocontrol::textarea:
        case tsdk_tipocontrol::span:
            $control->texto = $valor;
            break;

        case tsdk_tipocontrol::checkbox:
            if ($valor == "1" || $valor == true)
            {
                $control->set_propiedad('checked',null);
            }else{
                $control->del_propiedad("checked");
            }
            break;

        case tsdk_tipocontrol::combobox:
        case tsdk_tipocontrol::listbox:
            foreach ($control->m_listaitems as &$item)
            {
                if ($item->value == $valor)
                {
                    $item->selected = true;
                }else{
                    $item->selected = false;
                }
            }
            break;

        default:
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                     "#x Error en set_value_item. Indice: [".$indice."]".
                     ", Valor: [".$valor."]".
                     ", Tipo de control: [".$this->m_listaitems[$indice]->tipocontrol."]".
                     ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Ejecuta el sql que tiene cada control, el resultado lo guarda en cada control respectivamente
    *
    * @param    object      $conexion           Es la conexion a la base de datos
    * @param    object      $control            Es el control donde ejecutar el sql
    * @return   int                             Devuelve 0 si es OK, -1 en caso de error
    * @example "sample_tsdk_list_controls.php"
    */
    protected function exec_sql_into_control($conexion,&$control)
    {
        $objeto_bd = new tsdk_resultset( $conexion, $this->m_manejadorbd );

        $tabladatos = $objeto_bd->obtener_tabla_from_sql( $control->sql ); //retorna un objeto tsdk_datos_sql

        if ( is_null($tabladatos) )
        {
            return -1;
        }

        //añadiendo valores
        switch ( $control->tipocontrol ){
        case tsdk_tipocontrol::textbox:
            break;

        case tsdk_tipocontrol::checkbox:
            break;

        case tsdk_tipocontrol::combobox:
        case tsdk_tipocontrol::listbox:

            //buscando el item del combobox/listbox
            foreach ($tabladatos->filas as $fila)
            {
                $id=$fila->item('id')->value;
                $texto=$fila->item('texto')->value;

                $control->add_item( new tsdk_lista_item( $id,$texto) );
            }
            break;
        }

        return 0;
    }

    //-----------------------------------------------------------
    /**
    * Recupera el html del control indicado
    *
    * @param    string      $indice             Es el nombre del control buscado
    * @return   string                          Devuelve el codigo html
    * @example "sample_tsdk_list_controls.php"
    */
    public function get_html($indice)
    {
        $control = $this->find_item($indice);

        if ( is_null($control))
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                            "#x Control no encontrado en tsdk_list_controls: '".$indice."'".
                            ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                               "\n"
                           );
            return null;
        }

        //devolviendo el html del control
        return $control->get_html();
    }

    //-----------------------------------------------------------
    /**
    * Imprime en la salida estandar el html del control indicado
    *
    * @param    string      $idcontrol          Es el nombre del control buscado
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function print_html($idcontrol)
    {
        echo $this->get_html($idcontrol);
    }

    //-----------------------------------------------------------------------------------------------------
    //carga los controles html desde un recordset
    //$listaitems es de la forma  clave=>valor
    //                              clave:nombre del campo del recordset, nombre del item (control html)
    //                              valor:tipo de item: textbox,checkbox,combobox
    //------------------------------------------------------------------------------
    //public function load_items_from_recordset( $recordset,$listaitems )

    //-----------------------------------------------------------
    /**
    * Modifica el valor de la lista de controles desde un recordset
    *
    * @param    array       $listaitems         Lista de items a modificar
    * @param    object      $recordset          Es el recordset con los datos
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function load_values_from_recordset (& $listacontroles, $recordset)
    {
//tsdk_error_log_trace("---- debug listacontroles 1 ------",$listacontroles);
//tsdk_error_log_trace("---- debug recordset 1 ------",$recordset);

//error_log("paso_b1");

        //grabando en los controles
        foreach ($listacontroles as $clave=>& $control)
        {
//error_log("paso_b2. Clave:".$clave);
//error_log("paso_b2. TipoControl:".$control->tipocontrol);
//tsdk_error_log_trace("---- valor de control. Clave:".$clave." ------",$control);

            //validando si el control debe ser llenado con un valor de base de datos
            if ($control->is_field_database == true) 
            {
                //estableciendo el valor del control
                switch ($control->tipocontrol ){
                    case tsdk_tipocontrol::textbox:
                        
                        if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
                        {
                            //$control->value = $recordset->get_item_valor($clave);
                            $control->value = $recordset->campos->item($clave)->value;
                        }else{
                            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                                            "#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
                                            ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                                            "\n"
                                        );
                            $control->value = "CAMPO NO ENCONTRADO";
                        }
                            
                        break;

                    case tsdk_tipocontrol::textarea:
                                        
                        if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
                        {
                            //$control->texto = $recordset->get_item_valor($clave);
                            $control->texto = $recordset->campos->item($clave)->value;
                        }else{
                            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                                            "#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
                                            ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                                            "\n"
                                        );
                            $control->texto = "CAMPO NO ENCONTRADO";
                        }
                        break;

                    case tsdk_tipocontrol::checkbox:
                        
                        if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
                        {
                            //if ($recordset->get_item_valor($clave) == "1")
                            if ($recordset->campos->item($clave)->value == "1")
                            {
                                $control->checked = true;
                            }else{
                                $control->checked = false;
                            }
                        }else{
                            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                                            "#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
                                            ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                                            "\n"
                                        );
                            //$control->texto = "CAMPO NO ENCONTRADO";
                            
                        }
                        break;

                    case tsdk_tipocontrol::combobox:
                    case tsdk_tipocontrol::listbox:
                    
                        if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
                        {
                            //error_log("nro.items:".count($this->m_listaitems[$clave]->m_listaitems) );
                            //buscando el item del combo
                            /*foreach ($control->get_items() as &$item)
                            {
                                if ($item->value == $recordset->campos->item($clave)->value )
                                {
                                    $item->selected = true;
                                }else{
                                    $item->selected = false;
                                    //error_log("  x no igual");
                                }
                            }*/
                            $control->select_item($recordset->campos->item($clave)->value);
                        }else{
                            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                                            "#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
                                            ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                                            "\n"
                                        );
                            //$control->texto = "CAMPO NO ENCONTRADO";
                            $control->add_item( new tsdk_lista_item("-","CAMPO NO ENCONTRADO") );

                        }
                        break;
                    }
            }
            

        }

//error_log("paso_b3");
//tsdk_error_log_trace("---- debug listacontroles 2 ------",$listacontroles);

    }

    //-----------------------------------------------------------
    /** 
     * Añade un elemento a la coleccion
     * @param  string    $idcontrol     Es la clave del elemento a añadir
     * @param  object    $control       Es el objeto a añadir a la coleccion
     *  
     * @return object   Una referencia al objeto añadido
     * @example "sample_tsdk_tabs_coleccion.add.txt"
     */
    public function & add($idcontrol=null,$control=null)
    {
        if (is_null($idcontrol))
        {
            $idcontrol = "control_".strval($this->count());
        }

        if ($idcontrol=="")
        {
            $idcontrol = "control_".strval($this->count());
        }

        if (is_null($control))
        {
            //se obliga que sea un objeto con un valor
            //no puede ser nulo
            $mensaje_error = "El objeto a agregar no puede ser nulo.";
            
            tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
                 "#x ".$mensaje_error.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );

            return null;
        }else{
            //se obtiene el idcontrol establecido
            $temporal = $control->idcontrol;
            if ($temporal != ""){
                $idcontrol = $temporal;
            }
        }

        //estableciendo id de ventana y control
        $control->set_id_ventana_control($this->m_idventana,$idcontrol);

        //añadiendo el control a la lista
        return parent::add($idcontrol,$control); //llamando al padre
    }
    

}
