<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - button
*
* Implementa la clase que representa un button de html
*
*/

class tsdk_button extends tsdk_control
{
	// ******************************
	// variables miembro
	// ******************************

	//No tiene


	// ******************************
	// metodos
	// ******************************

	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	 * 
	* @access public
	* @param  string    $idventana      Es el ID de la ventana que contiene el control
	* @param  string    $idcontrol      Es el ID del control 
	* @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
	* @param  array     $estilos        Es un arreglo con la lista de estilos del control
	* @param  array     $eventos        Es un arreglo con la lista de eventos del control
	* @example "sample_tsdk_button.construct.txt"
	*/
	function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
	{
		parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

		//modificando el valor (texto)
		//$this->propiedades->add("value")->value = $texto;
		$this->propiedades->add("type")->value = "button";

		//iniciando variables
		$this->tipocontrol = tsdk_tipocontrol::button;
	}

	//-----------------------------------------------------------
	/** 
	* Implementa el destructor de la clase
	*
	*/
	function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* Implementa la configuracion de un control estandar
	*
	* @access public
	* @param  string    $valor          Se refiere al texto que aparece en el control
	* @param  int       $tabindex       Se refiere a la posicion del control en la ventana html
	* @return none
	* 
	* @example "sample_tsdk_button.php"
	*/
	public function config_as_button_standard($valor,$tabindex=0)
	{
		$this->propiedades->add("type")->value = "button";
		$this->propiedades->add("value")->value = $valor;
		$this->propiedades->add("tabindex")->value = $tabindex;
		//$this->set_estilos(array("width"=>$ancho));
	}

	//-----------------------------------------------------------
	/**
	* Devuelve el texto HTML que implementa el control
	* @access public
	* @return string                  Devuelve el texto HTML del control
	* @example "sample_tsdk_button.php"
	*/
	public function get_html()
	{
		//tsdk_error_log_trace("prueba",$this);

		$salida = "<input ";
		$salida .= $this->propiedades->get_propiedades_text();
		$salida .= " ".$this->estilos->get_estilos_text();
		$salida .= " ".$this->eventos->get_eventos_text();
		$salida .= " />";

 		return $salida;
	}

	//-----------------------------------------------------------
	/**
	* Imprime en salida estandar el texto HTML que implementa el control
	* @access public
	* @return none
	* @example "sample_tsdk_button.php"
	*/
	public function print_html()
	{
		 echo $this->get_html();
	}

}

?>
