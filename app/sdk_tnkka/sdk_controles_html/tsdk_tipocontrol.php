<?php 
/**
 * Este archivo implementa una clase que representa los tipos de control HTML existentes
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

//-----------------------------------------------------------
/**
* Tipo de control
*
* Implementa una clase que permite establecer el comportamiento que se tendra
* en un tipo de control dentro de la aplicación. OBS: Es distinto al valor type de input
*
* @author luisvmt@hotmail.com
* @package controles
*/
 
class tsdk_tipocontrol
{
    // ******************************
    // variables miembro
    // ******************************

    const   textbox=0;
    const   textarea=1;
    const   checkbox=2;
    const   combobox=3;
    const   listbox=4;
    const   button=5;
    const   span=6;
    const   form=7;
    const   file=8;
    const   tabla=9;
    const   image=10;
    const   group=11;
    const   link=12;
    const   div=13;
    const   contenedor=14;
    const   lista_item=15;
    const   text=16;
    const   tab=17;
    const   dialog=18;
    const   parrafo=19;
    const   tab_item=20;
    const   tabla_sql=21;

    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    */
    function __construct()
    {

    }

    //-----------------------------------------------------------
    /**
    * Implementa el destructor de la clase
    */
    function __destruct()
    {

    }

    //-----------------------------------------------------------
    /**
    * Devuelve el tipo de control (string)
    *
    * @param  string    $tipo           Se refiere al tipo de control
    * @return string                    Devuelve el texto que representa al tipo de control
    */
    public static function get_as_text($tipo)
    {
        switch ($tipo)
        {
            case tsdk_tipocontrol::textbox:
                return "textbox";
            case tsdk_tipocontrol::textarea:
                return "textarea";
            case tsdk_tipocontrol::checkbox:
                return "checkbox";
            case tsdk_tipocontrol::combobox:
                return "combobox";
            case tsdk_tipocontrol::listbox:
                return "listbox";
            case tsdk_tipocontrol::button:
                return "button";
            case tsdk_tipocontrol::span:
                return "span";
            case tsdk_tipocontrol::form:
                return "form";
            case tsdk_tipocontrol::file:
                return "file";
            case tsdk_tipocontrol::tabla:
                return "tabla";
            case tsdk_tipocontrol::image:
                return "image";
            case tsdk_tipocontrol::group:
                return "group";
            case tsdk_tipocontrol::link:
                return "link";
            case tsdk_tipocontrol::div:
                return "div";
            case tsdk_tipocontrol::contenedor:
                return "contenedor";
            case tsdk_tipocontrol::lista_item:
                return "lista_item";
            case tsdk_tipocontrol::text:
                return "text";
            case tsdk_tipocontrol::dialog:
                return "dialog";
            case tsdk_tipocontrol::tab:
                return "tab";
            case tsdk_tipocontrol::tab_item:
                return "tab_item";
            case tsdk_tipocontrol::tabla_sql:
                    return "tabla_sql";
            default:
                return "Tipo de Control no reconocido";
        }
        
    }
}

?>
