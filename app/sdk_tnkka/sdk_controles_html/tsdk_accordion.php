<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - accordion
*
* Implementa la clase que representa un accordion de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_accordion
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access private
    */

    /**
    * Es el DIV principal del accordion
    * @var object
    */    
    private     $m_divprincipal;

    /**
    * Es el arreglo que contiene los titulos de los paneles
    * @var array
    */    
    private     $m_titulo_panel;

    /**
    * Es el arreglo de los contenidos del panel
    * @var array
    */  
    private     $m_content_panel;

    /**
    * Es el id del control. Es la union de idventana_idcontrol
    * @var string
    */  
    private     $m_id;
    
    /**
    * Es el nombre del control. Es la union de idventana_idcontrol
    * @var string
    */  
    private     $m_name;

    /**
    * Es el id de la ventana que contiene al control
    * @var string
    */  
    private     $m_idventana;
    
    /**
    * Es el id del control
    * @var string
    */  
    private     $m_idcontrol;
    
    /**#@-*/

    // ******************************
    // metodos
    // ******************************


    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el id de la ventana que contiene al control
    * @param  string    $idcontrol      Es el id del control
    *
    * @example "sample_tsdk_accordion.construct.txt"
    */
    function __construct($idventana,$idcontrol)
    {

        //iniciando variables
        $this->idventana=$idventana; //automaticamente establece el valor de id
        $this->idcontrol=$idcontrol;
        //$this->m_id = $id;
        //$this->m_divprincipal = new tsdk_div(array("id"=>"accordion_".$this->m_id));
        //$this->m_divprincipal = new tsdk_div(array("id"=>$this->id));
        $this->m_divprincipal = new tsdk_div($idventana,
                                            $idcontrol."-divprincipal"
                                            );

        $this->m_titulo_panel=array();
        $this->m_content_panel=array();

        //$this->tipocontrol = tsdk_tipocontrol::accordion;
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_paneles</b>    :   Devuelve el numero de paneles que contiene el accordion</br>
    *    <b>id</b>             :   Devuelve el id del control (la union de idventana_idcontrol)</br>
    *    <b>name</b>           :   Devuelve el valor de name del control (la union de idventana_idcontrol)</br>
    *    <b>idcontrol</b>      :   Devuelve el id del control</br>
    *    <b>idventana</b>      :   Devuelve el id de la ventana que contiene el control</br>
    * @example "sample_tsdk_accordion.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "nro_paneles":
            return count ($this->m_content_panel);
            break;
        case "id": //debe ser la union de idventana_idcontrol
            return $this->m_id;
            break;
        case "name": //debe ser la union de idventana_idcontrol
            return $this->m_name;
            break;
        case "idcontrol":
            return $this->m_idcontrol;
            break;
        case "idventana":
            return $this->m_idventana;
            break;
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
	* @access public
    *
    * Implementa la modificacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    id             :   Establece el id del control (la union de idventana_idcontrol)</br>
    *    name           :   Establece el valor de name del control (la union de idventana_idcontrol)</br>
    *    idcontrol      :   Establece el id del control</br>
    *    idventana      :   Establece el id de la ventana que contiene el control</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_accordion.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "name": //debe ser la union de idventana_idcontrol
        case "id":  //debe ser la union de idventana_idcontrol
            $lista = explode ("_",$value);

            $this->m_idventana = $lista[0];
            if (count($lista)>=2)
                $this->m_idcontrol = $lista[1];
            else
                $this->m_idcontrol="";

            $this->m_id = $value;
            $this->m_name = $value;
            break;
        case "idcontrol":
            $this->m_idcontrol = $value;
            
            //actualizando el id y name del control
            $this->m_id = $this->m_idventana."_".$this->m_idcontrol;
            $this->m_name = $this->m_idventana."_".$this->m_idcontrol;
            break;
        case "idventana":
            $this->m_idventana = $value;

            //actualizando el id y name del control
            $this->m_id = $this->m_idventana."_".$this->m_idcontrol;
            $this->m_name = $this->m_idventana."_".$this->m_idcontrol;
            break;
        //case "nro_paneles":
        //  $this->m_nro_paneles = $value;
        //  break;
        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece la clase del control. Es utilizado para establecer los estilos del control
	* @access public
    * @param    string  $clase          Se refiere al nombre de clase
    * @return   none
    * @example "sample_tsdk_accordion.php"
    */
    public function set_clase($clase)
    {
        $this->m_divprincipal->propiedades->add("class")->value=$clase;

    }

    //-----------------------------------------------------------
    /**
    * Establece la titulo de un panel
	* @access public
    * @param    integer     $nropanel          Numero de panel a modificar. El primer panel empieza en cero (0)
    * @param    string      $titulo            Es el titulo del panel
    * @return   none
    * @example "sample_tsdk_accordion.php"
    */
    public function set_titulo_panel($nropanel,$titulo)
    {
        $this->m_titulo_panel[$nropanel]=$titulo;
    }

    //-----------------------------------------------------------
    /**
    * Obtiene el titulo del panel
	* @access public
    * @param    integer     $nropanel          Numero de panel a obtener. El primer panel empieza en cero (0)
    * @return   string                          El titulo del panel solicitado
    * @example "sample_tsdk_accordion.php"
    */
    public function get_titulo_panel($nropanel)
    {
        return $this->m_titulo_panel[$nropanel];
    }

    //-----------------------------------------------------------
    /**
    * Establece el texto HTML del panel
	* @access public
    * @param    integer     $nropanel          Numero de panel a modificar. El primer panel empieza en cero (0)
    * @param    string      $contenido         Es el texto HTML que tendra el panel
    * @return   none
    * @example "sample_tsdk_accordion.php"
    */
    public function set_content_panel($nropanel,$contenido)
    {
        $this->m_content_panel[$nropanel]=$contenido;
    }

    //-----------------------------------------------------------
    /**
    * Añade texto HTML del panel
	* @access public
    * @param    integer     $nropanel          Numero de panel a modificar. El primer panel empieza en cero (0)
    * @param    string      $contenido         Es el texto HTML a añadir
    * @return   none
    * @example "sample_tsdk_accordion.php"
    */
    public function add_content_panel($nropanel,$contenido)
    {
        $this->m_content_panel[$nropanel] .= $contenido;
    }

    //-----------------------------------------------------------
    /**
    * Obtiene el texto HTML del panel
	* @access public
    * @param    integer     $nropanel       Numero de panel a obtener. El primer panel empieza en cero (0)
    * @return   string                      Texto HTML del panel
    * @example "sample_tsdk_accordion.php"
    */
    public function get_content_panel($nropanel)
    {
        return $this->m_content_panel[$nropanel];
    }

    //-----------------------------------------------------------
    /**
    * Obtiene el texto HTML del control
	* @access public
    * @return   string                      Texto HTML del control
    * @example "sample_tsdk_accordion.php"
    */
    public function get_html()
    {
        if ( count($this->m_content_panel)==0 )
            return;

        //imprimiendo el acordeon
        foreach($this->m_content_panel as $indice=>$texto_panel)
        {
            //estableciendo el titulo del panel
            if (isset($this->m_titulo_panel[$indice])){
                //$this->m_divprincipal->add_content($this->m_titulo_panel[$indice]."\n");
                $this->m_divprincipal->content = $this->m_divprincipal->content . $this->m_titulo_panel[$indice]."\n";
            }

            //estableciendo el contenido del panel
            $panel = new tsdk_div();
            $panel->content = $texto_panel;

            //$this->m_divprincipal->add_content($panel->get_html()."\n");
            $this->m_divprincipal->content = $this->m_divprincipal->content . $panel->get_html()."\n";
        }

        return $this->m_divprincipal->get_html();
    }

    //-----------------------------------------------------------
    /**
    * Escribe en la salida estandar el texto HTML del control
	* @access public
    * @return   none
    * @example "sample_tsdk_accordion.php"
    */
    public function print_html()
    {
        echo $this->get_html();
    }
}

?>
