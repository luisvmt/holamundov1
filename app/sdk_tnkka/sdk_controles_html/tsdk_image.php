<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - image
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_image extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //No tiene


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_image.php"
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        $this->tipocontrol = tsdk_tipocontrol::image;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }
    
    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_image.php"
    */
    public function get_html()
    {
        //<img src="smiley.gif" alt="Smiley face" width="42" height="42">

        $salida = "<img ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " />";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_image.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }

}

?>
