<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_lista_item.php");

//-----------------------------------------------------------
/**
* Control HTML - TextArea
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_textarea extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Indica el texto que se encuentra en el control
    * @var string
    */
    protected	$m_texto="";

    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_textarea.php"
    */
	function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
	{
	   parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

		//iniciando variables
		//$this->m_listaitems = array();
		$this->tipocontrol = tsdk_tipocontrol::textarea;
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>texto</b>        :   Devuelve el texto del control</br>
    * 
    * @example "sample_tsdk_span.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "texto":
			return $this->m_texto;
			break;
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>texto</b>        :   Establece el texto del control</br>
    * 
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_span.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "texto":
			$this->m_texto=$value;
			break;
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Implementa la configuracion de un control estandar
    *
    * @access public
    * @param  string    $valor          Se refiere al texto que aparece en el control
    * @param  int       $tabindex       Se refiere a la posicion del control en la ventana html
    * @param  int       $nrofilas       Nro de filas que tendra el control
    * @param  int       $size           Se refiere al ancho en caracteres del control
    * @param  int       $maxlength      Es el número de caracteres que puede guardar el control
    * @param  int       $ancho          Es el ancho en porcentaje del control

    * @return none
    * 
    * @example "sample_tsdk_textarea.php"
    */
    public function config_as_text($valor,$tabindex,$nrofilas=CONTROL_TEXTAREA_NROFILAS,$size=CONTROL_SIZE,$maxlength=CONTROL_MAX_LENGTH,$ancho=CONTROL_WIDTH_PERCENTAJE)
    {
		$this->propiedades->add("type",new tsdk_coleccion_item("text"));
		$this->propiedades->add("value",new tsdk_coleccion_item($valor));
		$this->propiedades->add("size",new tsdk_coleccion_item($size));
		$this->propiedades->add("maxlength",new tsdk_coleccion_item($maxlength));
		$this->propiedades->add("tabindex",new tsdk_coleccion_item($tabindex));
		$this->propiedades->add("rows",new tsdk_coleccion_item($nrofilas));

		$this->estilos->add("width",new tsdk_coleccion_item($ancho));
		
    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
    * @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_textarea.php"
    */
	public function get_html()
	{
		//$this->m_propiedad["aass"]=NULL;
		//<textarea name="textarea" style="width:250px;height:150px;"></textarea>

		$salida = "<textarea ";
		$salida .= $this->propiedades->get_propiedades_text();
		$salida .= " ".$this->estilos->get_estilos_text();
		$salida .= " ".$this->eventos->get_eventos_text();
		$salida .= " >";
		$salida .= $this->m_texto;
		$salida .= "</textarea>";

		/*echo "<pre>";
		print_r ($this->m_propiedad);
		if ( !is_null($this->m_propiedad["required"]) )
			echo "no es nulo";
		else
			echo "es nulo";
		echo "</pre>";*/

		return $salida;

	}
	
    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
    * @access public
    * @return none
    * @example "sample_tsdk_textarea.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
