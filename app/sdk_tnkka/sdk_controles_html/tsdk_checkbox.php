<?php 
/**
 * Este archivo implementa la clase base de todos los objetos html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
//-----------------------------------------------------------
/**
* Control HTML - checkbox
*
* Implementa la clase que representa un checkbox
*
* Esta clase implementa la clase que representa un checkbox
*
* @author luisvmt@hotmail.com
* @package controles
*/ 

class tsdk_checkbox extends tsdk_button
{

    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Indica el texto que se colocará en el control
    * @var string
    */
    protected	$m_texto="";

    /**#@-*/
    
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    *
    * @example "sample_tsdk_checkbox.construct.txt"
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
        //llamando al padre
        parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        //estableciendo la propiedad a checkbox
        $this->type = "checkbox";
        $this->tipocontrol = tsdk_tipocontrol::checkbox;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>checked</b>      :   Indica si el control tiene el check seleccionado</br>
    *    <b>type</b>         :   Indica el tipo del control</br>
    *    <b>value</b>        :   Indica el valor del control</br>
    *    <b>texto</b>        :   Indica el texto asociado al control</br>
    * @example "sample_tsdk_checkbox.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "checked":
			if ($this->propiedades->key_exists($propiedad))
            {
                return true;
            }else{
                return false;
            }
            break;
        case "type":
        case "value":
			if ($this->propiedades->key_exists($propiedad))
            {
				return $this->propiedades->item($propiedad)->value;
            }else{
                return null;
            }
            break;
        case "texto":
            return $this->m_texto;
            break;
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>checked</b>      :   Indica si el control tiene el check seleccionado</br>
    *    <b>type</b>         :   Indica el tipo del control</br>
    *    <b>value</b>        :   Indica el valor del control</br>
    *    <b>texto</b>        :   Indica el texto asociado al control</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_checkbox.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "checked":
            if ($value==true){
				$this->propiedades->add($propiedad)->value=null;
            }else{
				if ($this->propiedades->key_exists($propiedad))
                {
					$this->propiedades->delete($propiedad);
                }
            }  
            break;
        case "texto":
            $this->m_texto = $value;
            break;
        case "type":
        case "value":
			$this->propiedades->add($propiedad)->value = $value;
            break;
        default:
            parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_checkbox.php"
    */
    public function get_html()
    {
        //forzando el tipo de control
        $this->type = "checkbox";
        $this->tipocontrol = tsdk_tipocontrol::checkbox;
        
        //devolviendo el texto html
        $salida = "<label><input ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " />";
        $salida .= $this->m_texto."</label>";

        return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_checkbox.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
