<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - lista_item
*
* Implementa un item de los siguientes controles: tsdk_combobox, tsdk_listbox
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_lista_item extends tsdk_control
{
	// ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Indica el texto que se colocará en el item
    * @var string
    */
    protected	$m_texto="";

    /**#@-*/
	

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
	* @param  string    $valor          Es el valor del item
    * @param  string    $texto          Es el texto que se muestra en el item
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_button.php"
    */
    function __construct($valor="",$texto="",$propiedades=null,$estilos=null,$eventos=null)
    {
        parent::__construct("","",$propiedades,$estilos,$eventos);

		$this->m_texto = $texto;
		$this->propiedades->add("value")->value = $valor;
        $this->tipocontrol = tsdk_tipocontrol::lista_item;
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>selected</b>    :   Determina si el item esta seleccionado</br>
    *    <b>value</b>       :   Valor del item</br>
    *    <b>label</b>           :   Texto del item</br>
    * @example "sample_tsdk_lista_item.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "selected":
			if ($this->propiedades->key_exists($propiedad))
            {
                return true;
            }else{
                return false;
            }
            break;
        case "value":
			if ($this->propiedades->key_exists($propiedad))
            {
				return $this->propiedades->item($propiedad)->value;
            }else{
                return null;
            }
            break;
        case "label":
            return $this->m_texto;
            break;
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>selected</b>    :   Determina si el item esta seleccionado</br>
    *    <b>value</b>       :   Valor del item</br>
    *    <b>label</b>           :   Texto del item</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_lista_item.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "selected":
            if ($value==true){
				$this->propiedades->add($propiedad)->value = null;
            }else{
				if ($this->propiedades->key_exists($propiedad))
                {
					$this->propiedades->delete($propiedad);
                }
            }
            break;
        case "disabled": //deshabilita el item
            if ($value==true){
				$this->propiedades->add($propiedad)->value = null;
            }else{
				if ($this->propiedades->key_exists($propiedad))
                {
					$this->propiedades->delete($propiedad);
                }
            }
            break;
        case "enabled": //creado por luis diaz, habilita el item
            if ($value==true){
				if ($this->propiedades->key_exists("disabled"))
                {
					$this->propiedades->delete("disabled");
                }
            }else{
				$this->propiedades->add("disabled")->value = null;
            }
            break;
        case "value":
			$this->propiedades->add($propiedad)->value = $value;
            break;
        case "label":
            $this->m_texto=$value;
            break;
        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece el valor, texto, propiedades, estilos y eventos de un item
    *
	* @access public
    * @param  string    $valor          Es el valor del item
    * @param  string    $texto          Es el texto que se muestra en el item
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del item
    * @param  array     $estilos        Es un arreglo con la lista de estilos del item
    * @param  array     $eventos        Es un arreglo con la lista de eventos del item
    * @return none
    * 
    * @example "sample_tsdk_button.php"
    */
    public function set_values($valor="",$texto="",$propiedades=null,$estilos=null,$eventos=null)
    {
		$this->propiedades->add("value")->value = $valor;
        $this->m_texto=$texto;
		$this->propiedades->set_propiedades($propiedades);
		$this->estilos->set_estilos($estilos);
		$this->eventos->set_eventos($eventos);
    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el item
	* @access public
    * @return string                  Devuelve el texto HTML del item
    * @example "sample_tsdk_lista_item.php"
    */
    public function get_html()
    {
         $salida = "<option ";
         $salida .= "value='".$this->propiedades->item("value")->value."' ";
         $salida .= $this->propiedades->get_propiedades_text();
         $salida .= " ".$this->estilos->get_estilos_text();
         $salida .= " ".$this->eventos->get_eventos_text();
         $salida .= " >";
         $salida .= $this->m_texto;
         $salida .= "</option>\n";

         return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el item
	* @access public
    * @return none
    * @example "sample_tsdk_button.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
