<?php 
/**
 * Este archivo implementa la clase base de todos los objetos html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */
 
if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_filas_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_fila_tabla_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_columnas_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_columna_tabla_item.php");

//-----------------------------------------------------------
/**
* Control HTML
*
* Implementa la clase base de todos los objetos que representan objetos html.
*
* Esta clase implementa la clase base de todos los objeto HTML,
*
* @author luisvmt@hotmail.com
* @package controles
*/ 

class tsdk_tabla extends tsdk_control
{

	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access private
	*/

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa el numero de filas de la cabecera de la tabla. 
	* @var integer
	*/
	protected   $m_nrofilas_header=0;

	/**
	* Representa el numero de filas del resumen 6+(footer) de la tabla. 
	* @var integer
	*/
	protected   $m_nrofilas_footer=0;
	
	/**
	* Arreglo donde guarda el contenido de cada celda. 
	* @var array
	*/
	protected	$m_valor_celda=array();

	/**
	* Arreglo donde guarda las propiedades de cada celda
	* @var array
	*/
	protected	$m_propiedades_celda=array();

	/**
	* Arreglo donde se guarda los estilos de las celdas
	* @var array
	*/
	protected	$m_estilos_celda=array();

	/**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de filas de la tabla
    * @var coleccion
    */
	public	$filas=null;
	
	/**
    * Representa la coleccion de columnas de la tabla
    * @var coleccion
    */
	public	$columnas=null;


    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  integer    $nrofilas             Números de filas de la tabla
    * @param  integer    $nrocolumnas          Números de columnas de la tabla
    * @param  array      $propiedades          Arreglo con la lista de  propiedades del control
    * @param  array      $estilos              Arreglo con la lista de estilos del control
    * @param  array      $eventos              Arreglo con la lista de eventos del control
    * @example "sample_tsdk_tabla.php"
    */
	function __construct($idventana="",$idcontrol="",$nrofilas=0, $nrocolumnas=0, $propiedades=null, $estilos=null, $eventos=null)
	{
	   parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

	   //iniciando variables
	   $this->filas = new tsdk_filas_tabla();
	   $this->columnas = new tsdk_columnas_tabla();

	   $this->set_dimensiones($nrofilas,$nrocolumnas);
	   $this->tipocontrol = tsdk_tipocontrol::tabla;
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_header</b>        :   Devuelve el numero de filas del header</br>
    *    <b>nro_footer</b>        :   Devuelve el numero de filas del resumen</br>
    *    <b>nro_filas</b>         :   Devuelve el numero de filas de la tabla</br>
    *    <b>nro_columnas</b>      :   Devuelve el numero de columnas de la tabla</br>
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "nro_header":
			return $this->m_nrofilas_header;
			break;
		
		case "nro_footer":
			return $this->m_nrofilas_footer;
			break;
		
		case "nro_filas":
			//return $this->m_nrofilas;
			return $this->filas->count();
			//break;
		
		case "nro_columnas":
			//return $this->m_nrocolumnas;
			return $this->columnas->count();
			//break;
		
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_header</b>        :   Establece el numero de filas del header</br>
    *    <b>nro_footer</b>        :   Establece el numero de filas del resumen</br>
    *    <b>border</b>            :   Establece el borde de la tabla</br>
    * 
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_tabla.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "nro_header":
			$this->m_nrofilas_header=$value;
			break;
		
		case "nro_footer":
			$this->m_nrofilas_footer=$value;
			break;
		
		case "border":
			$this->propiedades->add("border")->value=$value;
			break;
		
		default:
			return parent::__set($propiedad,$value);
			//break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Esta funcion permite acceder directamente a la coleccion.
    * Tiene el mismo nombre que la coleccion
    *
    * @access public
    * @param  string    $indice     Indice del item al cual acceder
    * @return item
    * 
    * @example "sample_tsdk_tabla.php"
    */
    public function &filas($indice)
    {
        return $this->filas->item($indice);
    }

    //-----------------------------------------------------------
    /**
    * Añade una fila a la tabla
    *
	* @access public
    * @return integer  		Devuelve el indice de la fila añadida (basado en 0)
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function add_fila()
	{
		$this->set_dimensiones($this->filas->count()+1,$this->columnas->count(),false);  //no elimina valores presentes

		//devolviendo el indice
		return $this->filas->count()-1; //devuelve el indice de la fila añadida
	}

    //-----------------------------------------------------------
    /**
    * Añade una columna a la tabla
    *
	* @access public
    * @return integer  		Devuelve el indice de la columna añadida (basado en 0)
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function add_columna()
	{
		$this->set_dimensiones($this->filas->count(),$this->columnas->count()+1,false);  //no elimina valores presentes
		return $this->columnas->count()-1; //devuelve el indice de la columna añadida
	}

    //-----------------------------------------------------------
    /**
    * Modifica las dimensiones (filas y columnas) de la tabla
    * 
	* @access public
    * @param  integer 	$nrofilas 		El nuevo numero de filas 
    * @param  integer 	$nrocolumnas	El nuevo numero de columnas 
    * @param  boolean 	$clear_celdas	Si es true, elimina el contenido de todas las celdas de la tabla. 
    *                                   Si es false, mantiene el contenido de las celdas.
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_dimensiones($nrofilas,$nrocolumnas,$clear_celdas=true)
	{
		if ($nrofilas < $this->filas->count())
		{
			//borrando filas mayores a lo establecido
			while ($nrofilas < $this->filas->count() )
			{
				$this->filas->delete($this->filas->count()-1); //borra el ultimo
			}
			
		}elseif ($nrofilas > $this->filas->count()) 
		{
			//añadiendo filas faltantes
			while ($nrofilas > $this->filas->count() )
			{
				$this->filas->add(null,new tsdk_fila_tabla_item() ); //añade un item al final
			}
			
		}
				
		//borrando columnas mayores a lo establecido
		if ($nrocolumnas < $this->columnas->count() ) //debemos borrar columnas
		{
			while ($nrocolumnas < $this->columnas->count())
			{
				$this->columnas->delete($this->columnas->count()-1); //borra el ultimo
			}
		}elseif ($nrocolumnas > $this->columnas->count()) //debemos añadir columnas
		{
			while ($nrocolumnas > $this->columnas->count() )
			{
				$this->columnas->add(null,new tsdk_columna_tabla_item() ); //añade un item al final
			}
		}
		
		//iniciando las celdas
		if ($clear_celdas)
		{
			$this->clear_cells();
		}else{
			
			//Las filas faltantes ya fueron creadas
			for ($i=0; $i<$nrofilas; $i++) 
			{
				/*if (! $this->filas->key_exists($i)) //si no existe, se crea la fila, con sus estilos y propiedades
				{
					$this->filas->add($i,new tsdk_fila_tabla_item());
				}*/
				
				//creando las celdas que no existan de la fila
				$this->filas->item($i)->create_celdas($nrocolumnas);
			}
			
			//creando las columnas
			/*for ($j=0; $j<$nrocolumnas; $j++)
			{
				//si no existe, se crea
				if (! $this->columnas->key_exists($j)) //si no existe, se crea la columna, con sus estilos y propiedades
				{
					$this->columnas->add($j,new tsdk_columna_tabla_item());
				}
			}*/
		}

		//estableciendo idventana,idcontrol a las filas
		if ($this->filas->count()>0 && $this->columnas->count()>0)
		{
			for ($i=0; $i<$this->filas->count(); $i++)
			{
				$this->filas->item($i)->idventana = $this->idventana;
				$this->filas->item($i)->idcontrol = $this->idcontrol;

				//estableciendo idventana,idcontrol a las celdas
				for ($j=0; $j<$this->columnas->count(); $j++)
				{
					$this->filas->item($i)->celdas->item($j)->idventana = $this->idventana;
					$this->filas->item($i)->celdas->item($j)->idcontrol = $this->idcontrol;
				}
			}
		}

	}

    //-----------------------------------------------------------
    /**
    * Elimina el contenido de todas las celdas de la tabla
    * 
	* @access public
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function clear_cells()
	{
		for ($i=0; $i < $this->filas->count(); $i++)
		{
			$this->filas->item($i)->propiedades->delete_all();
			$this->filas->item($i)->estilos->delete_all();
			$this->filas->item($i)->eventos->delete_all();

			for ($j=0; $j<$this->columnas->count(); $j++)
			{
				if ($i==0)
				{
					$this->columnas->item($j)->propiedades->delete_all();
					$this->columnas->item($j)->estilos->delete_all();
				}

				if ($this->filas->item($i)->celdas->key_exists($j))
				{
					$this->filas->item($i)->celdas->item($j)->inicializar(); //limpia la celda
				}else{
					$this->filas->item($i)->celdas->add($j,new tsdk_celda_tabla_item()); //añadiendo la celda
				}
			}
		}
	}

    //-----------------------------------------------------------
    /**
    * Crea la fila y columna en caso falte.
    * 
	* @access private
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nrocolumna 	Numero de columna de la celda a modificar (basado en 0)
    * @return integer					Devuelve 0 si es OK, -1 en caso de error (si numero de fila o columna sobrepasa lo establecido)
    * 
    * @example "sample_tsdk_tabla.php"
    */
	protected function verificar_celda($nrofila,$nrocolumna) //estan basados en 0
	{
		//verificando el numero de fila
		if ($nrofila > $this->filas->count()-1)
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en create_celda. El numero de fila es mayor a las filas creadas en la tabla. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			return -1;
		}
		
		//verificando el numero de columna
		if ($nrocolumna > $this->columnas->count()-1)
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en create_celda. El numero de columna es mayor a las columnas creadas en la tabla. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
			return -1;
		}

		//creando las celdas de la fila en caso necesiten crearse
		$this->filas->item($nrofila)->create_celdas($nrocolumna+1);
	}

    //-----------------------------------------------------------
    /**
    * Establece el valor de una celda
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nrocolumna 	Numero de columna de la celda a modificar (basado en 0)
    * @param  string  	$valor          Contenido de la celda
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_valor_celda($nrofila,$nrocolumna,$valor)
	{
		if ( $this->verificar_celda($nrofila,$nrocolumna)>=0)
		{
			$this->filas->item($nrofila)->celdas->item($nrocolumna)->value = $valor;
		}
	}

    //-----------------------------------------------------------
    /**
    * Concatena el valor de una celda
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nrocolumna 	Numero de columna de la celda a modificar (basado en 0)
    * @param  string  	$valor          Contenido de la celda
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function add_valor_celda($nrofila,$nrocolumna,$valor)
	{
		if ( $this->verificar_celda($nrofila,$nrocolumna)>=0)
		{
			$this->filas->item($nrofila)->celdas->item($nrocolumna)->value .= $valor;
		}
	}
 
    //-----------------------------------------------------------
    /**
    * Establece las propiedades de la celda
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nrocolumna 	Numero de columna de la celda a modificar (basado en 0)
    * @param  array  	$propiedades    Array con la lista de propiedades
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_propiedades_celda($nrofila,$nrocolumna,$propiedades)
	{
		if ( $this->verificar_celda($nrofila,$nrocolumna)>=0)
		{
			$this->filas->item($nrofila)->celdas->item($nrocolumna)->propiedades->set_propiedades($propiedades);
		}
	}

    //-----------------------------------------------------------
    /**
    * Establece estilos a la celda
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nrocolumna 	Numero de columna de la celda a modificar (basado en 0)
    * @param  array  	$estilos        Array con la lista de estilos
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_estilos_celda($nrofila,$nrocolumna,$estilos)
	{
		//$this->m_estilos_celda[$nrofila][$nrocolumna]=$estilos;
				
		if ( $this->verificar_celda($nrofila,$nrocolumna)>=0)
		{
			$this->filas->item($nrofila)->celdas->item($nrocolumna)->estilos->set_estilos($estilos);
		}
	}

    //-----------------------------------------------------------
    /**
    * Establece los valores de las celdas de una fila
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila a modificar (basado en 0)
    * @param  array  	$valores        Array con el contenido de la fila
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_valores_fila($nrofila,$valores)
	{
		if (is_array($valores))
		{
			foreach ($valores as $clave => $valor)
			{
				//$this->m_valor_celda[$nrofila][$clave]=$valor;

				if ( $this->verificar_celda($nrofila,$clave)>=0)
				{
					$this->filas->item($nrofila)->celdas->item($clave)->value = $valor;
				}
			}
		}else{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x El valor ingresado no es un arreglo. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
		}
	}

    //-----------------------------------------------------------
    /**
    * Añade valores a las celdas de una fila
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila a modificar (basado en 0)
    * @param  array  	$valores        Array con el contenido de la fila
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function add_valores_fila($nrofila,$valores)
	{
		if (is_array($valores))
		{
			foreach ($valores as $clave => $valor)
			{
				//$this->m_valor_celda[$nrofila][$clave]=$valor;

				if ( $this->verificar_celda($nrofila,$clave)>=0)
				{
					$this->filas->item($nrofila)->celdas->item($clave)->value .= $valor;
				}
			}
		}else{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x El valor ingresado no es un arreglo. ".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
		}
	}
	
    //-----------------------------------------------------------
    /**
    * Establece estilos a una fila
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila a modificar (basado en 0)
    * @param  array  	$estilos        Array con los estilos de la fila
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_estilos_fila($nrofila,$estilos)
	{
		//$this->m_estilos_fila[$nrofila]=$estilos;
		$this->filas->item($nrofila)->estilos->set_estilos($estilos);
	}

    //-----------------------------------------------------------
    /**
    * Establece propiedades a una fila
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila a modificar (basado en 0)
    * @param  array  	$propiedades    Array con las propiedades de la fila
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_propiedades_fila($nrofila,$propiedades)
	{
		//$this->m_propiedades_fila[$nrofila]=$propiedades;
		$this->filas->item($nrofila)->propiedades->set_propiedades($propiedades);
	}

    //-----------------------------------------------------------
    /**
    * Establece los eventos de la fila
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila a modificar (basado en 0)
    * @param  array  	$eventos        Array con los eventos de la fila
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_eventos_fila($nrofila,$eventos)
	{
		//$this->m_eventos_fila[$nrofila]=$eventos;
		$this->filas->item($nrofila)->eventos->set_eventos($eventos);
	}

    //-----------------------------------------------------------
    /**
    * Establece estilos a una columna
    * 
	* @access public
    * @param  integer 	$nrocolumna 		Numero de columna a modificar (basado en 0)
    * @param  array  	$estilos            Array con los estilos de la columna
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_estilos_columna($nrocolumna,$estilos)
	{
		//$this->m_estilos_columna[$nrocolumna]=$estilos;
		$this->columnas->item($nrocolumna)->estilos->set_estilos($estilos);
	}

    //-----------------------------------------------------------
    /**
    * Establece propiedades a una columna
    * 
	* @access public
    * @param  integer 	$nrocolumna 		Numero de columna a modificar (basado en 0)
    * @param  array  	$propiedades        Array con las propiedades de la columna
    * @return none
    * 
    * @example "sample_tsdk_tabla.php"
    */
	public function set_propiedades_columna($nrocolumna,$propiedades)
	{
		//$this->m_propiedades_columna[$nrocolumna]=$propiedades;
		$this->columnas->item($nrocolumna)->propiedades->set_propiedades($propiedades);
	}

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_tabla.php"
    */
	public function get_html()
	{
		//inicializando variables
		if (!($this->m_nrofilas_header >= 0)) {
			$this->m_nrofilas_header = 0;
		}

		if (!($this->m_nrofilas_footer >= 0)) {
			$this->m_nrofilas_footer = 0;
		}

		$salida = "";

		//imprimiendo el encabezado
		$salida = "<table ";
		$salida .= $this->propiedades->get_propiedades_text();
		$salida .= " ".$this->estilos->get_estilos_text();
		$salida .= " ".$this->eventos->get_eventos_text();
		$salida .= " >\n";

		//imprimiendo las celdas
		if ($this->filas->count()>0 && $this->columnas->count()>0)
		{
			for ($i=0; $i<$this->filas->count(); $i++)
			{
				//iniciando encabezado
				if ($this->m_nrofilas_header>0 && $i==0) //$i==$this->m_nrofilas_header-1)
				{
					$salida .= "<thead>\n";
				}

				//iniciando pie
				if ($this->m_nrofilas_footer>0 && $i==$this->filas->count() - $this->m_nrofilas_footer)
				{
					$salida .= "<tfoot>\n";
				} 

				//iniciando body
				if ($i==$this->m_nrofilas_header)
				{
					$salida .= "<tbody>\n";
				}

				//imprimiendo las filas
				$salida .= "<tr ";
				
				$salida .= $this->filas->item($i)->propiedades->get_propiedades_text();
				$salida .= $this->filas->item($i)->estilos->get_estilos_text();
				$salida .= $this->filas->item($i)->eventos->get_eventos_text();
				
				$salida.=">\n";

				//imprimiendo las columnas
				for ($j=0; $j<$this->columnas->count(); $j++)
				{
					//validando y creando la celda
					if ( $this->verificar_celda($i,$j)<0 )
					{
						break; //error
					}

					//verificando si la celda tiene las propiedades y estilos de la columna. Si no los tiene
					//los copia de la columna. Prevalece las propiedades y estilos de la celda
					
					foreach ($this->columnas->item($j)->propiedades as $clave=>$valor)
					{
						if (! $this->filas->item($i)->celdas->item($j)->propiedades->key_exists($clave) ) //si no existe se actualiza
						{
							$this->filas->item($i)->celdas->item($j)->propiedades->add($clave)->value = $valor;
						}
					}
					
					foreach ($this->columnas->item($j)->estilos as $clave=>$valor)
					{
						if (! $this->filas->item($i)->celdas->item($j)->estilos->key_exists($clave) ) //si no existe se actualiza
						{
							$this->filas->item($i)->celdas->item($j)->estilos->add($clave)->value = $valor;
						}
					}
					
					//verificando si la celda se debe mostrar
					//if ( !array_key_exists('ocultar_x_span',$this->m_propiedades_celda[$i][$j]) )
					if ( ! $this->filas->item($i)->celdas->item($j)->propiedades->key_exists("ocultar_x_span"))
					{
						//imprimiendo el tag de encabezado o tag normal
						if ( $this->m_nrofilas_header>0 && $i<$this->m_nrofilas_header)
						{
							$salida .= "   <th ";
						}else{
							$salida .= "   <td ";
						}

						//if ($i==0)
						//{
							//error_log("paso1-".$j);

							//imprimiendo las propiedades de la columna
							/*if (is_array($this->m_propiedades_columna[$j]) )
							{
								foreach($this->m_propiedades_columna[$j] as $clave=>$valor)
								{
									$salida = $salida.$clave;
									if (!is_null($valor))
									{
										$salida=$salida."=\"".$valor."\"";
									}
									$salida.=" ";
								}
							}

							//imprimiendo las estilos de la columna
							if (is_array($this->m_estilos_columna[$j]) )
							{
								if( count($this->m_estilos_columna[$j])>0 )
								{
									$salida .= "style=\"";
									foreach($this->m_estilos_columna[$j] as $clave=>$valor)
									{
										$salida = $salida.$clave;
										if (!is_null($valor))
										{
											$salida=$salida.":".$valor.";";
										}
										$salida.=" ";
									}
									$salida.="\"";
								}
							}*/
							
							//$salida .= $this->filas->item($i)->celdas->item($j)->propiedades->get_propiedades_text();
							//$salida .= $this->filas->item($i)->celdas->item($j)->estilos->get_estilos_text();
						//}

						//imprimiendo las propiedades de la celda
						//if ( is_array($this->m_propiedades_celda[$i][$j]) )
						//{
							//foreach($this->m_propiedades_celda[$i][$j] as $clave=>$valor)
							foreach($this->filas->item($i)->celdas->item($j)->propiedades as $clave=>$item)
							{
								if ( strcasecmp (trim($clave),"colspan")==0)
								{
									//ocultando demas celdas
									for ($k=1;$k<$item->value;$k++)
									{
										//$this->m_propiedades_celda[$i][$j+$k]["ocultar_x_span"]=true;
										if ( $this->verificar_celda($i,$j+$k)>=0 )
										{
											$this->filas->item($i)->celdas->item($j+$k)->propiedades->add("ocultar_x_span")->value = true;
										}
									}
								}

								if ( strcasecmp (trim($clave),"rowspan")==0)
								{
									//ocultando demas celdas
									for ($k=1;$k<$item->value;$k++)
									{
										//$this->m_propiedades_celda[$i+$k][$j]["ocultar_x_span"]=true;
										if ( $this->verificar_celda($i+$k,$j)>=0 )
										{
											$this->filas->item($i+$k)->celdas->item($j)->propiedades->add("ocultar_x_span")->value = true;
										}
									}
								}

								//imprimiendo
								$salida .= $this->filas->item($i)->celdas->item($j)->propiedades->get_propiedades_text();
							}
						//}

						$salida .=">"; //se imprime solo las propiedades de la celda.
										//los estilos se imprimen con el div de la celda

						//impriendo el valor de la celda y los estilos del div de la celda
						//agregando un estilo a la celda
						$this->filas->item($i)->celdas->item($j)->estilos->add("word-wrap")->value = "break-word";
												
						//inicio de la celda
						$salida .= "<div ";
						
						//añadiendo estilos a la celda
						$salida .= $this->filas->item($i)->celdas->item($j)->estilos->get_estilos_text();
						$salida .= " >";
						
						//añadiendo el valor de la celda
						//$salida .= $this->m_valor_celda[$i][$j];
						$salida .= $this->filas->item($i)->celdas->item($j)->value;
						$salida .= "</div>";

						//cerrando la celda
						$salida .= "</td>\n";
					}
				}

				//cerrando la fila
				$salida .= "</tr>\n";

				//cerrando el encabezado
				if ($this->m_nrofilas_header>0 && $i==$this->m_nrofilas_header-1)
				{
					$salida .= "</thead>\n";
				}

				//cerrando pie
				if ($this->m_nrofilas_footer>0 && $i==$this->filas->count()-1)
				{
					$salida .= "</tfoot>\n";
				}

				//cerrando body
				if ($i==$this->filas->count() - $this->m_nrofilas_footer - 1)
				{
					$salida .= "</tbody>\n";
				}

			}
		}

		//finalizando la tabla
		$salida .= "</table>";

		return $salida;

	}

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_tabla.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}


/*

<table width="430" border="0">
  <tr>
	<td width="137">usuario:</td>

	<td width="144"><?php echo $txtusuario->get_html(); ?></td>
	<td width="135"><input type="submit" name="cmdaceptar" id="cmdaceptar" value="Aceptar" /></td>
	</tr>
  <tr>
	<td>contrase�a:</td>
	<td><?php echo $txtpassword->get_html(); ?></td>
	<td>&nbsp;</td>
	</tr>
  <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>
  <tr>
	<td colspan="3">Mensaje</td>
	</tr>
  <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>
</table>*/


