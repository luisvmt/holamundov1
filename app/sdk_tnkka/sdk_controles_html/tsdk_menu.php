<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - menu_item
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_menu //extends tv_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Es el id del menu
    * @var string
    */    
    protected     $m_id;
    
    /**
    * Es la lista de items. Cada item tiene 2 valores: id y texto
    * @var array
    */    
    protected     $m_items;

    /**
    * Es el color1
    * @var string
    */    
    protected     $m_color1;

    /**
    * Es el color2
    * @var string
    */    
    protected     $m_color2;

    /**
    * Es el color3
    * @var string
    */    
    protected     $m_color3;

    /**
    * Es el color4
    * @var string
    */    
    protected     $m_color4;

    /**
    * Es el color5
    * @var string
    */    
    protected     $m_color5;

    /**
    * Es el color6
    * @var string
    */    
    protected     $m_color6;

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string     $id            Id del menu
    * @param  string     $color1        Es el color a mostrar en el menu
    * @param  string     $color2        Es el color a mostrar en el menu
    * @param  string     $color3        Es el color a mostrar en el menu
    * @param  string     $color4        Es el color a mostrar en el menu
    * @param  string     $color5        Es el color a mostrar en el menu
    * @param  string     $color6        Es el color a mostrar en el menu
    * @example "sample_tsdk_menu_item.php"
    */
   function __construct($id="",$color1="A9A9F5",$color2="CED8F6",$color3="F5D0A9",$color4="F5D0A9",$color5="0079b2",$color6="c0c0c0")
   {
       //parent::__construct($propiedades,$estilos,$eventos);

       //iniciando variables
       $this->m_items=array();
       $this->m_id=$id;
       $this->m_color1=$color1;
       $this->m_color2=$color2;
       $this->m_color3=$color3;
       $this->m_color4=$color4;
       $this->m_color5=$color5;
       $this->m_color6=$color6;

   }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>id</b>             :   id del control</br>
    *    
    * @example "sample_tsdk_menu_item.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "id":
            return $this->m_id;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en get. Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>id</b>             :   id del control</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_menu_item.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "id":  
            $this->m_id = $value;
            break;
        
        default:
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en set. Propiedad no encontrada: ".$propiedad.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Añade un item al menu
    *
	* @access public
    * @param  object    $item          item a añadir
    * @return none
    * 
    * @example "sample_tsdk_menu_item.php"
    */
	public function add_item($item)
	{
		$item->id_menu = $this->m_id;
		$this->m_items[] = $item;
	}

    //-----------------------------------------------------------
    /**
    * Obtiene HTML_01 de estilo horizontal del menu
    *
	* @access public
    * @return   string          Codigo HTML
    * 
    * @example "sample_tsdk_menu.php"
    */
   public function get_estilo_horizontal_01()
   {
      $salida  = "<style type=\"text/css\">"."\n";

      $salida .= "  #".$this->m_id." li{"."\n";
      $salida .= "  float:left;"."\n";
      $salida .= "  position:relative;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." a{"."\n";
      //$salida .= "  font-family:'Helvetica', Arial, sans-serif;"."\n";
      //$salida .= "  font-family: Verdana, Geneva, sans-serif;"."\n";
      $salida .= "  font-family:Verdana,Arial,sans-serif;"."\n";
      //$salida .= "  font-size:90%;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." ul{"."\n";
      $salida .= "  left:-9999px;"."\n";      // Escondemos inicialmente los submen�s
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." li:hover ul{"."\n";
      $salida .= "  left:0;"."\n"; // Submen�s visibles
      $salida .= "  }"."\n";

      $salida .= "</style>"."\n";

      return $salida;
   }

    //-----------------------------------------------------------
    /**
    * Obtiene HTML_02 de estilo horizontal del menu
    *
	* @access public
    * @return   string          Codigo HTML
    * 
    * @example "sample_tsdk_menu.php"
    */
   public function get_estilo_horizontal_02()
   {
      $salida = "<style type=\"text/css\">"."\n";

      $salida .= "#".$this->m_id."{"."\n";
      $salida .= "  border:none;"."\n";
      $salida .= "  border:0px;"."\n";
      $salida .= "  margin:0px;"."\n";
      $salida .= "  padding:0px;"."\n";
      $salida .= "  font-family:Verdana,Arial,sans-serif;"."\n";
      //$salida .= "  font-family:'Helvetica', Arial, sans-serif;"."\n";
      //$salida .= "  font-family: Verdana, Geneva, sans-serif;"."\n";
      //$salida .= "  font-size:90%;"."\n";

      //$salida .= "    font-weight:bold;"."\n";
      $salida .= "  color:#000;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div ul{"."\n";
      $salida .= "  background: #".$this->m_color1.";"."\n";   /*fondo de menu*/
      $salida .= "  height:25px;"."\n"; /*originalmente 43*/
      $salida .= "  list-style:none;"."\n";
      $salida .= "  margin:0;"."\n";
      $salida .= "  padding:0;"."\n";
      $salida .= "  border: 1px solid #".$this->m_color5.";"."\n";  /*borde de menu*/
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li{"."\n";
      $salida .= "  float:left;"."\n";
      $salida .= "  padding:0px 8px 0px 8px;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li a{"."\n";
      $salida .= "  color:#".$this->m_color3.";"."\n";     /*color de texto menu - normal*/
      $salida .= "  display:block;"."\n";
      //$salida .= "    font-weight:bold;"."\n";
      $salida .= "  line-height:25px;"."\n";  /*inicial 43*/
      $salida .= "  padding:0px 25px;"."\n";
      $salida .= "  text-align:center;"."\n";
      $salida .= "  text-decoration:none;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li a:hover{"."\n";
      $salida .= "  color:#".$this->m_color4.";"."\n";  /*color de hover de menu superior*/
      $salida .= "  text-decoration:none;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li ul{"."\n";
      $salida .= "  background:#".$this->m_color6.";"."\n";   /*FONDO DE MENU DESPLEGABLE*/
      $salida .= "  border-left:1px solid #".$this->m_color5.";"."\n";   /*COLOR DE BORDE*/
      $salida .= "  border-right:1px solid #".$this->m_color5.";"."\n";
      $salida .= "  border-bottom:1px solid #".$this->m_color5.";"."\n";
      $salida .= "  display:none;"."\n";
      $salida .= "  height:auto;"."\n";
      $salida .= "  position:absolute;"."\n";
      $salida .= "  width:225px;"."\n";
      $salida .= "  z-index:200;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li:hover ul{"."\n";
      $salida .= "  display:block;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li li {"."\n";
      $salida .= "  display:block;"."\n";
      $salida .= "  float:none;"."\n";
      $salida .= "  padding:0px;"."\n";
      $salida .= "  width:225px;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li ul a{"."\n";
      $salida .= "  display:block;"."\n";
      //$salida .= "    /*font-size:12px;"."\n";*/
      $salida .= "  font-style:normal;"."\n";
      $salida .= "  padding:0px 10px 0px 15px;"."\n";
      $salida .= "  text-align:left;"."\n";
      //$salida .= "    font-size:90%;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div li ul a:hover{"."\n";
      $salida .= "  background:#".$this->m_color2.";"."\n";   /*color de fondo hover de submenu*/
      $salida .= "  color:#".$this->m_color4.";"."\n"; /*color de texto de hover en submenu*/
      //$salida .="   opacity:1.0;"."\n";
      $salida .= "}"."\n";

      $salida .= "#".$this->m_id."_div p{"."\n";
      $salida .= "  clear:left;"."\n";
      $salida .= "}"."\n";

      $salida .= "</style>"."\n";

      return $salida;
   }


    //-----------------------------------------------------------
    /**
    * Obtiene HTML de estilo horizontal del menu
    *
	* @access public
    * @return   string          Codigo HTML
    * 
    * @example "sample_tsdk_menu.php"
    */
	public function get_estilo_horizontal()
	{
		return $this->get_estilo_horizontal_01().$this->get_estilo_horizontal_02();
	}

    //-----------------------------------------------------------
    /**
    * Obtiene HTML de estilo horizontal del menu
    *
	* @access public
    * @return   string          Codigo HTML
    * 
    * @example "sample_tsdk_menu.php"
    */
	public function print_estilo_horizontal()
	{
		echo $this->get_estilo_horizontal();
	}

/*
   function print_estilo_horizontal()
   {
      $salida = "<style type=\"text/css\">"."\n";

      $salida .= "  #menuprincipal_div{"."\n";
      //background:#CED8F6;   //color de fondo
      $salida .= "    margin-left:20px;"."\n";
      $salida .= "    margin-bottom:0px;"."\n";
      $salida .= "    float:left;"."\n";
      $salida .= "    width:100%;"."\n";
      $salida .= "    -moz-border-radius:10px;"."\n";
      $salida .= "    -webkit-border-radius:10px;"."\n";
      $salida .= "    border-radius:10px;"."\n";
      $salida .= "    -webkit-box-shadow:1px 1px 3px #888;"."\n";
      $salida .= "    -moz-box-shadow:1px 1px 3px #888;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal, #menuprincipal ul, #menuprincipal ul li,"."\n";
      $salida .= "    #menuprincipal ul li ul, #menuprincipal ul li ul li {"."\n";
      $salida .= "    margin:0;"."\n";
      $salida .= "    padding:0;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal{"."\n";
      $salida .= "  list-style:none;"."\n";
      //$salida .= "  font-weight:bold;"."\n";
      //margin-left:20px;
      //$salida .= "  margin-right:5px;
      $salida .= "  margin-top:10px;"."\n";
      $salida .= "  margin-bottom:10px;"."\n";
      $salida .= "  float:left;"."\n";
      $salida .= "  width:100%;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal li{"."\n";
      $salida .= "  float:left;"."\n";
      $salida .= "  margin-right:10px;"."\n";  //separacion horizontal
      $salida .= "  position:relative;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal a{"."\n";
      $salida .= "  display:block;"."\n";
      $salida .= "  padding:5px;"."\n";
      $salida .= "  padding-left: 15px;"."\n";
      $salida .= "  padding-right: 15px;"."\n";
      $salida .= "  color:#".$this->m_color3.";"."\n";        //color de texto
      //background:#687EAB;   //color de fondo
      //text-decoration:none;
      //text-shadow:0.5px 0.5px #DF7401;
      $salida .= "  text-decoration:underline;"."\n";
      $salida .= "  font-family:'Helvetica', Arial, sans-serif;"."\n";
      //$salida .= "  font-size:88%;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal a:hover{"."\n";
      $salida .= "  color:#".$this->m_color4.";"."\n";          //color de link, al pasar el mouse
      $salida .= "  background:#111;"."\n";
      $salida .= "  text-decoration:underline;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal ul{"."\n";
      // Color de fondo de la web -para IE-
      $salida .= "  background:white;"."\n";
      $salida .= "  list-style:none;"."\n";
      $salida .= "  position:absolute;"."\n";

      // Escondemos inicialmente los submen�s
      $salida .= "  left:-9999px;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal ul li{"."\n";
      // Separaci�n vertical entre elementos
      $salida .= "  padding-top:1px;"."\n";
      $salida .= "  float:none;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal ul a{"."\n";
      // Separa los elementos en diferentes l�neas
      $salida .= "  white-space:nowrap;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal li:hover ul{"."\n";
      $salida .= "  left:0;"."\n"; // Submen�s visibles
      $salida .= "  }"."\n";

      // Para que cuando se mueva el cursor por los
      //elementos del submen� los elementos permanecen visibles
      $salida .= "  #menuprincipal li:hover a{"."\n";
      $salida .= "  background:#".$this->m_color2.";"."\n";             // EFF2FB color de fondo al pasar por encima
      $salida .= "  text-decoration:underline;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal li:hover ul a{"."\n";
      $salida .= "  text-decoration:none;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #menuprincipal li:hover ul li a:hover{"."\n";
      $salida .= "  background:#".$this->m_color1.";"."\n";             // color de fondo al pasar por encima
      $salida .= "  }"."\n";
      $salida .= "</style>"."\n";

      return $salida;
      
   }
  */


    //-----------------------------------------------------------
    /**
    * Obtiene HTML de estilo vertical del menu
    *
	* @access public
    * @return   string          Codigo HTML
    * 
    * @example "sample_tsdk_menu.php"
    */
   public function get_estilo_vertical()
   {
      $salida = "<style type=\"text/css\">"."\n";

      $salida .= "  #".$this->m_id."_div{"."\n";
      $salida .= "    style='margin-top: 20px;'"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id."{"."\n";
      $salida .= "    -moz-border-radius:5px;"."\n";
      $salida .= "    -webkit-border-radius:5px;"."\n";
      $salida .= "    border-radius:5px;"."\n";
      $salida .= "    -webkit-box-shadow:1px 1px 3px #888;"."\n";
      $salida .= "    -moz-box-shadow:1px 1px 3px #888;"."\n";
      $salida .= "  }"."\n";
      $salida .= "  #".$this->m_id." li{border-bottom:1px solid #FFF;}"."\n";
      $salida .= "  #".$this->m_id." ul li, #".$this->m_id." li:last-child{border:none} "."\n";

      $salida .= "  #".$this->m_id." a{"."\n"; //item
      $salida .= "    display:block;"."\n";
      $salida .= "    color:#".$this->m_color3.";"."\n";
      $salida .= "    text-decoration:none;"."\n";
      $salida .= "    font-family:Verdana,Arial,sans-serif;"."\n";
      //$salida .= "      font-size:88%;"."\n";
      $salida .= "    padding:3px 5px;"."\n";
      $salida .= "    text-shadow:0.5px 0.5px #DF7401;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." ul a{"."\n";  //subitem
      $salida .= "    display:block;"."\n";
      $salida .= "    color:#".$this->m_color4.";"."\n";
      $salida .= "    text-decoration:none;"."\n";
      $salida .= "    font-family:Verdana,Arial,sans-serif;"."\n";
      //$salida .= "      font-size:88%;"."\n";
      $salida .= "    padding:3px 5px;"."\n";
      //$salida .= "      text-shadow:0.5px 0.5px #DF7401;"."\n";
      $salida .= "    text-shadow: none;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." a:hover{"."\n";
      $salida .= "    color:#F9B855;"."\n";
      $salida .= "    -webkit-transition: color 0.2s linear;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  #".$this->m_id." ul a{background-color:#".$this->m_color2.";}"."\n";

      $salida .= "  #".$this->m_id." ul a:hover{"."\n";
      $salida .= "    background-color:#FFF;"."\n";
      $salida .= "    color:#2961A9;"."\n";
      $salida .= "    text-shadow:none;"."\n";
      $salida .= "    -webkit-transition: color, background-color 0.2s linear;"."\n";
      $salida .= "  }"."\n";

      $salida .= "  .".$this->m_id."_ul {"."\n";
      $salida .= "    display:block;"."\n";
      $salida .= "    background-color:#".$this->m_color1.";"."\n";
      $salida .= "    margin:0;"."\n";
      $salida .= "    padding:0;"."\n";
      $salida .= "     width:130px;"."\n";
      $salida .= "    list-style:none;"."\n";
      $salida .= "  }"."\n";
      $salida .= "  #".$this->m_id." ul{background-color:#".$this->m_color2.";}"."\n";
      $salida .= "  #".$this->m_id." li ul {display:none;}"."\n";
      $salida .= "</style>"."\n";

      return $salida;
      
   }


    //-----------------------------------------------------------
    /**
    * Obtiene HTML del javascript para manejo del menu
    *
	* @access public
    * @return   string          Codigo JAVASCRIPT
    * 
    * @example "sample_tsdk_menu.php"
    */
   public function get_javascript()
   {
      $salida  = "<script type=\"text/javascript\" charset=\"utf-8\">"."\n";
      $salida .= "  $(function(){"."\n";
      $salida .= "    $('#".$this->m_id." li a').click(function(event){"."\n";
      $salida .= "      var elem = $(this).next();"."\n";
      $salida .= "      if(elem.is('ul')){"."\n";
      $salida .= "        event.preventDefault();"."\n";
      $salida .= "        $('#".$this->m_id." ul:visible').not(elem).slideUp();"."\n";
      $salida .= "        elem.slideToggle();"."\n";
      $salida .= "      }"."\n";
      $salida .= "    });"."\n";
      $salida .= "  });"."\n";
/*
      $salida .= "function seleccionar_menu(idmenu,iditem_parent,iditem) "."\n";
      $salida .= "{"."\n";
      $salida .= "  var texto='';"."\n";
      $salida .= "  texto =  $('#'+iditem+'_a').text() + ' <-- (*)';"."\n";

      $salida .= "  $('#'+iditem_parent+'_ul').slideToggle();"."\n"; //mostrando el item parent
      $salida .= "  $('#'+iditem+'_a').text( texto );"."\n"; //cambiando el texto del link
      $salida .= "}"."\n";*/

      $salida .= "function seleccionar_menu(idmenu,iditem_parent,iditem) "."\n";
      $salida .= "{"."\n";
      $salida .= "  var texto='';"."\n";
      $salida .= "  texto =  '[ ' + $('#'+iditem+'_a').text() + ' ]';"."\n";

      $salida .= "  $('#'+iditem_parent+'_ul').slideToggle();"."\n"; //mostrando el item parent
      $salida .= "  $('#'+iditem+'_a').text( texto );"."\n"; //cambiando el texto del link
      $salida .= "}"."\n";



      $salida .= "</script>"."\n";

      return $salida;
      
   }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_menu_item.php"
    */
   public function get_html()
   {
      $salida = "<div id='".$this->m_id."_div' >";
      $salida .= "<ul id='".$this->m_id."' class='".$this->m_id."_ul' >\n";

      foreach ($this->m_items as $clave => $nodo)
      {
         $salida .= $nodo->get_html();
      }
      $salida .= "</ul>\n";
      $salida .= "</div>";

      return $salida;
      
   }
   
    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_menu_item.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }

}

?>
