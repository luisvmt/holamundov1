<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");


//-----------------------------------------------------------
/**
* Control HTML - TextBox
*
* Implementa la clase que representa un textbox de html
* Esta clase implementa: textbox, password, checkbox
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_textbox extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //No tiene


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_button.php"
    */
   function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
   {
       parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

	   //iniciando variables
	   $this->tipocontrol = tsdk_tipocontrol::textbox;
   }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>autofocus</b>       :   Propiedad estandar HTML</br>
    *    <b>readonly</b>        :   Propiedad estandar HTML</br>
    *    <b>required</b>        :   Propiedad estandar HTML</br>
    *    <b>value</b>           :   Propiedad estandar HTML</br>
    *    <b>autocomplete</b>    :   Propiedad estandar HTML. Permite autocompletar conforme se escribe en el control.</br>
    *    <b>height</b>          :   Propiedad estandar HTML</br>
    *    <b>max</b>       :   Propiedad estandar HTML</br>
    *    <b>type</b>      :   Propiedad estandar HTML. Devuelve el tipo de control HTML</br>
    * @example "sample_tsdk_textbox.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "autofocus":
		case "readonly":
		case "required":
			
			return $this->propiedades->key_exists($propiedad);
			break;
		
		case "value":
		case "autocomplete": //Permite autocompletar conforme se escribe en el control
        case "height":
        case "maxlength": //establece el tamaño maximo de caracteres que se puede ingresar al control
        case "min": //establece el valor minimo que se puede utilizar en el control
		case "max": //establece el valor maximo que se puede utilizar en el control
		case "type":
			
			if ($this->propiedades->key_exists($propiedad))
			{
				return $this->propiedades($propiedad)->value;
			}else{
				return null;
			}
			break;
			
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>autofocus</b>       :   Propiedad estandar HTML</br>
    *    <b>readonly</b>        :   Propiedad estandar HTML</br>
    *    <b>required</b>        :   Propiedad estandar HTML</br>
    *    <b>value</b>           :   Propiedad estandar HTML</br>
    *    <b>autocomplete</b>    :   Propiedad estandar HTML. Permite autocompletar conforme se escribe en el control.</br>
    *    <b>height</b>          :   Propiedad estandar HTML</br>
    *    <b>max</b>       :   Propiedad estandar HTML</br>
    *    <b>type</b>      :   Propiedad estandar HTML. Establece el tipo de control HTML</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_button.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "autofocus":
		case "readonly":
		case "required":
			if ($value==true){
				$this->propiedades->add($propiedad)->value = null;
			}else{
				if ($this->propiedades->key_exists($propiedad))
				{
					$this->propiedades->delete($propiedad);
				}
			}
			break;
		case "value":
		case "autocomplete": //Permite autocompletar conforme se escribe en el control
        case "height":
        case "maxlength": //establece el tamaño maximo de caracteres que se puede ingresar al control
        case "min": //establece el valor minimo que se puede utilizar en el control
        case "max": //establece el valor maximo que se puede utilizar en el control
        case "type":
			$this->propiedades->add($propiedad)->value = $value;
			break;
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Configura el textbox como un textbox oculto. Util para el paso de datos entre javascript y php
    * @access public
    * @param string $valor      Es el texto a colocar en el textobox
    * @return none
    * @example "sample_tsdk_button.php"
    */
	public function config_as_text_hidden($valor)
	{
		$this->propiedades->add("type")->value="hidden";
		$this->propiedades->add("value")->value=$valor;
		
	}

    //-----------------------------------------------------------
    /**
    * Configura el textbox como un textbox Deshabilitado.
    * @access public
    * @param 	string 		$valor      Es el texto a colocar en el textobox
    * @param 	integer		$tabindex   Es el tabindex del control
    * @param 	integer		$size       Es el tamaño del texbox
    * @param 	integer		$maxlength  Tamaño maximo del texto del textbox
    * @param 	integer		$ancho      Tamaño del control
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
	public function config_as_text_disabled($valor,$tabindex,$size=CONTROL_SIZE,$maxlength=CONTROL_MAX_LENGTH,$ancho=CONTROL_WIDTH_PERCENTAJE)
	{

		$this->propiedades->add("type")->value="text";
		$this->propiedades->add("value")->value=$valor;
		$this->propiedades->add("size")->value=$size;
		$this->propiedades->add("maxlength")->value=$maxlength;
		$this->propiedades->add("tabindex")->value=$tabindex;
		$this->propiedades->add("readonly")->value=null;

		$this->estilos->add("width",new tsdk_coleccion_item($ancho));
		$this->estilos->add("background-color",new tsdk_coleccion_item(CONTROL_BACKGROUND_DISABLED));

	}

    //-----------------------------------------------------------
    /**
    * Configura el textbox como un textbox estandar.
    * @access public
    * @param 	string 		$valor      Es el texto a colocar en el textobox
    * @param 	integer		$tabindex   Es el tabindex del control
    * @param 	integer		$size       Es el tamaño del texbox
    * @param 	integer		$maxlength  Tamaño maximo del texto del textbox
    * @param 	integer		$ancho      Tamaño del control
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function config_as_text($valor,$tabindex,$size=CONTROL_SIZE,$maxlength=CONTROL_MAX_LENGTH,$ancho=CONTROL_WIDTH_PERCENTAJE)
    {
		$this->propiedades->add("type",new tsdk_coleccion_item("text"));
		$this->propiedades->add("value",new tsdk_coleccion_item($valor));
		$this->propiedades->add("size",new tsdk_coleccion_item($size));
		$this->propiedades->add("maxlength",new tsdk_coleccion_item($maxlength));
		$this->propiedades->add("tabindex",new tsdk_coleccion_item($tabindex));

		$this->estilos->add("width",new tsdk_coleccion_item($ancho));
    }

    //-----------------------------------------------------------
    /**
    * Configura el textbox como un textbox para almacenar password.
    * @access public
    * @param 	string 		$valor      Es el texto a colocar en el textobox
    * @param 	integer		$tabindex   Es el tabindex del control
    * @param 	integer		$size       Es el tamaño del texbox
    * @param 	integer		$maxlength  Tamaño maximo del texto del textbox
    * @param 	integer		$ancho      Tamaño del control
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function config_as_password($valor,$tabindex,$size=CONTROL_SIZE,$maxlength=CONTROL_MAX_LENGTH,$ancho=CONTROL_WIDTH_PERCENTAJE)
    {
		$this->propiedades->add("type",new tsdk_coleccion_item("password"));
		$this->propiedades->add("value",new tsdk_coleccion_item($valor));
		$this->propiedades->add("size",new tsdk_coleccion_item($size));
		$this->propiedades->add("maxlength",new tsdk_coleccion_item($maxlength));
		$this->propiedades->add("tabindex",new tsdk_coleccion_item($tabindex));

		$this->estilos->add("width",new tsdk_coleccion_item($ancho));

    }

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
    * @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_textarea.php"
    */
	public function get_html()
	{
		$salida = "";

		//<input name="txtusuario" type="text" id="txtusuario" value="aaa" size="12" maxlength="50" />

		$salida = "<input ";
		$salida .= $this->propiedades->get_propiedades_text();
		$salida .= " ".$this->estilos->get_estilos_text();
	    $salida .= " ".$this->eventos->get_eventos_text();
		$salida .= " />";

		//error_log("***********".$salida."...............");

		return $salida;
	}

    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
    * @access public
    * @return none
    * @example "sample_tsdk_textarea.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }

    //-----------------------------------------------------------
    /**
    * Configura el evento onchange standard.
    * @access public
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function add_evento_onchange()
    {
        $this->eventos->add_evento_standard("onchange","event,this");
    }

    //-----------------------------------------------------------
    /**
    * Configura el evento keypress standard.
    * @access public
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function add_evento_onkeypress()
    {
        $this->eventos->add_evento_standard("onkeypress","event,this");
    }

    //-----------------------------------------------------------
    /**
    * Configura el evento keydown standard.
    * @access public
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function add_evento_onkeydown()
    {
        $this->eventos->add_evento_standard("onkeydown","event,this");
    }

    //-----------------------------------------------------------
    /**
    * Configura el evento keyup standard.
    * @access public
    * @return   none
    * @example "sample_tsdk_textbox.php"
    */
    public function add_evento_onkeyup()
    {
        $this->eventos->add_evento_standard("onkeyup","event,this");
    }

}

?>
