<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_lista_item.php");

//-----------------------------------------------------------
/**
* Control HTML - list combo box
*
* Implementa la clase que representa un button de html
*/

class tsdk_list_combo_box extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Es el arreglo (lista) de items 
    * @var object
    */    
    protected  $m_listaitems;

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_button.php"
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
       parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        $this->m_listaitems = array();
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>autofocus</b>    :   Establece si el item se selecciona automaticamnte</br>
    *    <b>multiple</b>     :   Permite multiple selecciones</br>
    *    <b>required</b>     :   Establece si el item es requerido</br>
    *    <b>form</b>      :   Formulario</br>
    *    <b>name</b>      :   Nombre del control</br>
    *    <b>size</b>      :   Tamaño del control</br>
    *    <b>value</b>      :   Valor del item</br>
    * @example "sample_tsdk_accordion.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "autofocus":
        case "multiple":
        case "required":
			if ($this->propiedades->key_exists($propiedad))
            {
                return true;
            }else{
                return false;
            }
            break;
        case "form":
        case "name":
        case "size":
			if ($this->propiedades->key_exists($propiedad))
            {
				return $this->propiedades->item($propiedad)->value;
            }else{
                return null;
            }
            break;

        case "value":

            foreach ($this->m_listaitems as &$item)
            {
                if ($item->selected == true)
                {
                    return $item->value;
                }
                return null;
            }
            break;

        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>autofocus</b>    :   Establece si el item se selecciona automaticamnte</br>
    *    <b>multiple</b>     :   Permite multiple selecciones</br>
    *    <b>required</b>     :   Establece si el item es requerido</br>
    *    <b>form</b>      :   Formulario</br>
    *    <b>name</b>      :   Nombre del control</br>
    *    <b>size</b>      :   Tamaño del control</br>
    *    <b>value</b>      :   Valor del item</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_list_combo_box.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "autofocus":
        case "multiple":
        case "required":
            if ($value==true){
				$this->propiedades->add($propiedad)->value = null;
            }else{
				if ($this->propiedades->key_exists($propiedad))
                {
					$this->propiedades->delete($propiedad);
                }
            }
            break;
        case "form":
        case "name":
        case "size":
			$this->propiedades->add($propiedad)->value = $value;
            break;

		case "value":
            foreach ($this->m_listaitems as &$item)
            {
                if ($item->value == $value)
                {
                    $item->selected = true;
                }else{
                    $item->selected = false;
                }
            }
            break;

        case "readonly":  //esta propiedad es creada por luis diaz
                          //la propiedad readonly de HTML no funciona en controles select, por eso se deshabilita los items que no están seleccionados

            if ($value == true){ //--- deshabilitando todos los items no seleccionados 

				foreach ($this->m_listaitems as &$item)
				{
					if ($item->selected != true)
					{
						$item->enabled = false;
					}
                }
                
            }else{ //--- habilitando todos los items

				foreach ($this->m_listaitems as &$item)
				{
					$item->enabled = true;
				}
            }
            break;

        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Agrega un item a la lista
    *
	* @access public
    * @param  object    $item          Item a agregar
    * @return none
    * 
    * @example "sample_tsdk_button.php"
    */
    public function add_item($item)
    {
        $this->m_listaitems[]=$item;
    }

    //-----------------------------------------------------------
    /**
    * devolviendo por referencia lista de items
	* @access public
    * @return   array                          Devuelve la lista de items
    * @example "sample_tsdk_list_combo_box.php"
    */
    public function &get_items()
    {
        return $this->m_listaitems;
    }

    //-----------------------------------------------------------
    /**
    * Seleccionando un item del combobox
	* @access  public
    *
    * @param   string    $item       El item a seleccionar
    * @return  none
	* @example "sample_tsdk_list_combo_box.select_item.txt"
    */
	public function select_item($item)
	{
		$this->value = $item;
	}
    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_list_combo_box.php"
    */
    public function get_html()
    {
        //$this->m_propiedad["aass"]=NULL;

        $salida = "<select ";
        $salida .= $this->propiedades->get_propiedades_text();
        $salida .= " ".$this->estilos->get_estilos_text();
        $salida .= " ".$this->eventos->get_eventos_text();
        $salida .= " >\n";

        //imprimiendo valores
        foreach ($this->m_listaitems as $item)
        {
           $salida .= $item->get_html();
        }

        $salida .= "</select>";

        return $salida;
    }


    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_list_combo_box.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

?>
