<?php
/**
 * Este archivo implementa la clase base de todos los objetos html
 *
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_list_controls.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_estilos.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_eventos.php");



//-----------------------------------------------------------
/**
* Control HTML
*
* Implementa la clase base de todos los objetos que representan objetos html.
*
* Esta clase implementa la clase base de todos los objeto HTML,
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Representa la consulta sql con la cual se llena los datos del control
    * @var string
    */
    protected   $m_sql=""; //consulta sql que permite la carga de datos

    /**
    * Representa el tipo de control. Es un valor de la clase tsdk_tipocontrol
    * @var tsdk_tipocontrol
    */
    protected   $m_tipocontrol=-1; //tipo de control

    /**
    * Representa el id del control
    * @var string
    */
    protected   $m_idcontrol="";

    /**
    * Representa el id de la ventana que contiene el control
    * @var string
    */
    protected   $m_idventana="";

    /**
    * Representa el manejador de eventos, es decir la funcion que atiende el evento
    * @var string
    */
    protected   $m_handler_eventos="";

    /**
    * Este campo indica si el control será llenado desde un campo de base de datos
    * @var string
    */
    protected   $m_is_field_database=true;

    /**
    * Representa el arreglo (lista) de subcontroles
    * @access protected
    * @var array
    */
    //protected      $m_subcontroles=null;
	
    /**#@-*/
	
    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
     * Representa la coleccion de estilos
     * @var tsdk_propiedades_html
     */
	public	$propiedades=null;

    /**
    * Representa la coleccion de estilos
    * @var tsdk_estilos
    */
	public	$estilos=null;

    /**
    * Representa la coleccion de estilos
    * @var tsdk_eventos
    */
	public	$eventos=null;
	
    /**
    * Representa la coleccion de subcontroles
    * @var tsdk_eventos
    */
    public  $subcontroles=null;

    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_control.construct.txt"
    */
    function __construct($idventana,$idcontrol,$propiedades,$estilos,$eventos)
    {
        //iniciando lista de controles (subcontroles)
        //$this->m_subcontroles=new tsdk_list_controls();
        $this->subcontroles=new tsdk_list_controls();
		
		//iniciando las propiedades
		$this->propiedades = new tsdk_propiedades_html();
		
		//iniciando los estilos
		$this->estilos = new tsdk_estilos();
		
		//iniciando los estilos
		$this->eventos = new tsdk_eventos();
		
        //iniciando variables
        //$this->m_texto = $texto;
		
        //OJO: si dentro de la lista de propiedades existe el ID y NAME, estos no se tomaran en cuenta
		$this->m_idventana = $idventana;
		$this->m_idcontrol = $idcontrol;

        //iniciando variables
        if (is_array($propiedades))
		{
			foreach ($propiedades as $clave => $valor)
			{
				$this->propiedades->add($clave,new tsdk_coleccion_item($valor));
			}	
		}
		
        //iniciando variables
        if (is_array($estilos))
		{
			foreach ($estilos as $clave => $valor)
			{
				$this->estilos->add($clave,new tsdk_coleccion_item($valor));
			}	
		}

        //iniciando variables
        if (is_array($eventos))
		{
			foreach ($estilos as $clave => $valor)
			{
				$this->eventos->add($clave,new tsdk_coleccion_item($valor));
			}	
		}
		
        //estableciendo las propiedades
        $this->idventana = $this->m_idventana;
        $this->idcontrol = $this->m_idcontrol;

		//estableciendo el idventana
        $this->subcontroles->idventana = $this->m_idventana;
        
        //inicializando valor
        $this->m_is_field_database = true;
    }

    //-----------------------------------------------------------
    /**
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>idcontrol</b>      :   Devuelve el id del control</br>
    *    <b>idventana</b>      :   Devuelve el id de la ventana que contiene el control</br>
    *    <b>handler_eventos</b>      :   Devuelve la funcion que atenderá de los eventos</br>
    *    <b>texto</b>                :   Devuelve el texto del control. Depende del control para determinar que representa el texto</br>
    *    <b>tipocontrol</b>          :   Devuelve el tipo de control</br>
    * @example "sample_tsdk_button.php"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "idcontrol":
            return $this->m_idcontrol;
            break;
		
        case "idventana":
            return $this->m_idventana;
            break;
        
        //case "texto":
        //    return $this->m_texto;
        //    break;
        case "sql":
            return $this->m_sql;
            break;
        case "tipocontrol":
            return $this->m_tipocontrol;
            break;
        case "is_field_database":
            return $this->m_is_field_database;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
	             "#x Error en get. ".
                 ", tipo_control: [".tsdk_tipocontrol::get_as_text($this->m_tipocontrol)."]".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    <b>idcontrol</b>      :   Establece el id del control</br>
    *    <b>idventana</b>      :   Establece el id de la ventana que contiene el control</br>
    *    <b>handler_eventos</b>      :   Establece el prefijo de los eventos</br>
    *    <b>texto</b>                :   Establece el texto del control. Depende del control para determinar que representa el texto</br>
    *    <b>tipocontrol</b>          :   Establee el tipo de control</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_button.php"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "disabled":
            if ($value==true){
				$this->propiedades->add($propiedad,new tsdk_coleccion_item(null));
            }else{
				if ($this->propiedades->key_exists($propiedad)) //ojo no usar isset
                {
					$this->propiedades->delete($propiedad);
                }
            }
            break;

        case "readonly": //no todos los controles soportan esta propiedad, por ejemplo 
            if ($value==true){
                $this->propiedades->add($propiedad,new tsdk_coleccion_item(null));
            }else{
                if ($this->propiedades->key_exists($propiedad)) //ojo no usar isset
                {
                    $this->propiedades->delete($propiedad);
                }
            }
            break;
            
        case "enabled": //propiedad creada por luis diaz
            if ($value==true){
				if ($this->propiedades->key_exists('disabled'))
                {
					$this->propiedades->delete('disabled');
                }
            }else{
				$this->propiedades->add('disabled',new tsdk_coleccion_item(null));
            }
            break;

        case "title": //implementa el tooltip
        case "tabindex":
			$this->propiedades->add($propiedad,new tsdk_coleccion_item($value));
            break;
				
        case "idcontrol":
            $this->m_idcontrol = $value;

			//actualizando el id y name del control
			$this->propiedades->add("id")->value   = $this->m_idventana."_".$this->m_idcontrol;
			$this->propiedades->add("name")->value = $this->m_idventana."_".$this->m_idcontrol;
			
			//actualizando en los eventos
			$this->eventos->idcontrol = $value;
            break;
		
        case "idventana":
            $this->m_idventana = $value;
            $this->subcontroles->idventana = $value;

			//actualizando el id y name del control
			$this->propiedades->add("id")->value   = $this->m_idventana."_".$this->m_idcontrol;
			$this->propiedades->add("name")->value = $this->m_idventana."_".$this->m_idcontrol;
			
			//actualizando en los eventos
			$this->eventos->idventana = $value;
            break;
		
        //case "texto":
        //    $this->m_texto = $value;
        //    break;
        case "sql":
            $this->m_sql=$value;
            break;
        case "tipocontrol":
            $this->m_tipocontrol = $value;
            break;
        case "is_field_database":
            $this->m_is_field_database = $value;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	"#x Error en set. ".
                 ", tipo_control: [".tsdk_tipocontrol::get_as_text($this->m_tipocontrol)."]".
                 ", Propiedad no encontrada: [".$propiedad."]".
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * Establece el valor de idventana, idcontrol, id y name
    * @access public
    * @param string $idventana      Se refiere al id de la ventana que contiene el control
    * @param string $idcontrol      Se refiere al id del control
    * @return none
    * @example "sample_tsdk_button.php"
    */
    public function set_id_ventana_control($idventana,$idcontrol)
    {
        //error_log("xxx. idventana:".$idventana." - idcontrol:".$idcontrol);

        $this->m_idventana=$idventana;
        $this->m_idcontrol=$idcontrol;
        $this->subcontroles->idventana = $idventana;

		//actualizando el id y name del control
		$this->propiedades->add("id")->value = $this->m_idventana."_".$this->m_idcontrol;
		$this->propiedades->add("name")->value = $this->m_idventana."_".$this->m_idcontrol;
		
		//actualizando el id y name de los eventos
		$this->eventos->idcontrol = $this->m_idcontrol;
		$this->eventos->idventana = $this->m_idventana;
    }
 
    //-----------------------------------------------------------
    /**
    * agrega un item a la lista de subcontroles.
    * Se necesita que previamente se halla establecido el idventana del control
    * @access public
    * @param    string          $idcontrol      Es el nombre a buscar en la lista de controles
    * @param    object          $control        Se refiere al control a añadir
    * @return   object                          Devuelve el control añadido. Si no se ha establecido el
    *                                           idventana, devuelve nulo
    * @example "sample_tsdk_control.php"
    */
    public function &add_control($idcontrol, &$control)
    {
        if ($this->m_idventana=="")
        {
            return null;
        }

        $control->set_id_ventana_control($this->m_idventana,$idcontrol);

        //$this->get_subcontroles()->m_listaitems[$idcontrol]=$control;
        return $this->subcontroles->add_item($idcontrol,$control);

        //return $this->subcontroles->m_listaitems[$idcontrol];
    }

    //-----------------------------------------------------------
    /**
    * devolviendo por referencia lista de subcontroles
    * @access public
    * @return   array                          Devuelve la lista de subcontroles
    * @example "sample_tsdk_control.php"
    */
    //public function &get_subcontroles()
    //{
    //    return $this->subcontroles;
    //}

    //-----------------------------------------------------------
    /**
    * Obteniendo el HTML de los subcontroles
    * @access public
    * @return string            Texto HTML de los subcontroles
    * @example "sample_tsdk_control.php"
    */
    public function get_html_subcontroles()
    {
         $salida = " ";

         //foreach ($this->subcontroles->get_items() as $clave => $control)
         //{
         //    $salida .= $control->get_html();
         //    $salida .= " ";
         //}

         foreach ($this->subcontroles as $clave=>$control)
         {
             $salida .= $control->get_html();
             $salida .= " ";
         }

         return $salida;
    }

    //-----------------------------------------------------------
    /**
    * Imprimiendo en la salida estandar el HTML de los subcontroles
    * @access public
    * @return none
    * @example "sample_tsdk_control.php"
    */
    public function print_html_subcontroles()
    {
        echo $this->get_html_subcontroles();
    }
}

?>
