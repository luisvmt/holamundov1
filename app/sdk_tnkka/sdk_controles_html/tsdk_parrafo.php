<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");

//-----------------------------------------------------------
/**
* Control HTML - span
*
* Implementa la clase que representa un button de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_parrafo extends tsdk_control
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Indica el texto que se colocará en el parrafo
    * @var string
    */
    protected	$m_texto="";

    /**#@-*/




    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $texto          Es el texto del parrafo
	* @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    * @example "sample_tsdk_span.php"
    */
	function __construct($texto,$idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
	{
	   parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

	   //iniciando variables
	   $this->m_texto = $texto;
	   $this->tipocontrol = tsdk_tipocontrol::parrafo;
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>align</b>      	 :   Propiedad estandar HTML</br>
    *    
    * @example "sample_tsdk_parrafo.php"
    */
	public function __get($propiedad)
	{
		switch ($propiedad){
		case "align":
			if ($this->propiedades->key_exists($propiedad))
			{
				return $this->propiedades->item($propiedad)->value;
			}else{
				return null;
			}
			break;
		case "texto":
			return $this->m_texto;
			break;
		default:
			return parent::__get($propiedad);
			break;
		}

	}

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>align</b>       	:   Propiedad estandar HTML</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_parrafo.php"
    */
	public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "align":
			$this->propiedades->add($propiedad)->value = $value;
			break;
		case "texto":
			$this->m_texto=$value;
			break;
		default:
			return parent::__set($propiedad,$value);
			break;
		}
	}

    //-----------------------------------------------------------
    /**
    * Configura el span como mensaje
    *
	* @access public
    * @param  string    $color          Se refiere al color del texto
    * @param  int       $tamano_letra   Se refiere al tamaño de la letra
    * @return none
    * 
    * @example "sample_tsdk_span.php"
    */
    /*public function config_as_mensaje($color=CONTROL_COLOR_MENSAJE,$tamano_letra=CONTROL_TAMANO_LETRA_MENSAJE)
    {
		$this->propiedades->set_propiedades(array("type"=>"span"));
		$this->set_estilos(array("color"=>$color,
									   "font-size"=>$tamano_letra));
									 
    }*/

    //-----------------------------------------------------------
    /**
    * Devuelve el texto HTML que implementa el control
	* @access public
    * @return string                  Devuelve el texto HTML del control
    * @example "sample_tsdk_span.php"
    */
	public function get_html()
	{
		 $salida = "<p ";
		 $salida .= $this->propiedades->get_propiedades_text();
		 $salida .= " ".$this->estilos->get_estilos_text();
		 $salida .= " ".$this->eventos->get_eventos_text();
		 $salida .= " >";
		 $salida .= $this->m_texto;
		 $salida .= "</p>";

		 return $salida;

	}
	
    //-----------------------------------------------------------
    /**
    * Imprime en salida estandar el texto HTML que implementa el control
	* @access public
    * @return none
    * @example "sample_tsdk_span.php"
    */
    public function print_html()
    {
         echo $this->get_html();
    }
}

//<span style="color:#FF0000; font-size:15px;">Mensaje</span>

?>
