<?php 
/**
 * Este archivo implementa la clase de un objeto html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_lista_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_list_combo_box.php");

//-----------------------------------------------------------
/**
* Control HTML - listbox
*
* Implementa la clase que representa un combobox de html
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_listbox extends tsdk_list_combo_box
{

    // ******************************
    // variables miembro
    // ******************************

    //No tiene


    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Es el ID de la ventana que contiene el control
    * @param  string    $idcontrol      Es el ID del control
    * @param  array     $propiedades    Es un arreglo con la lista de propiedades HTML del control
    * @param  array     $estilos        Es un arreglo con la lista de estilos del control
    * @param  array     $eventos        Es un arreglo con la lista de eventos del control
    *
    * @example "sample_tsdk_listbox.txt"
    */
    function __construct($idventana="",$idcontrol="",$propiedades=null,$estilos=null,$eventos=null)
    {
       parent::__construct($idventana,$idcontrol,$propiedades,$estilos,$eventos);

        //iniciando variables
        $this->tipocontrol = tsdk_tipocontrol::listbox;
        $this->size=5; //por defecto se coloca un ancho de 3 items
    }

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>      :   Por implementar</br>
    * @example "sample_tsdk_listbox.txt"
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        /*
        case "value":

            foreach ($this->m_listaitems as &$item)
            {
                if ($item->selected == true)
                {
                    return $item->value;
                }
                return null;
            }
            break;
            */
        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>none</b>      :   Por implementar</br>
    *
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_listbox.txt"
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        /*
        case "value":
            foreach ($this->m_listaitems as &$item)
            {
                if ($item->value == $value)
                {
                    $item->selected = true;
                }else{
                    $item->selected = false;
                }
            }
            break;
            */
        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }


    //-------------------------------------------------------------------------------
    // Ejemplo de como se utiliza el combo fijo
    //-------------------------------------------------------------------------------
    /*
        $control = new tsdk_combobox();
        $control->config_as_combofijo($tabindex++);
        $control->add_item( new tsdk_lista_item("usuarios","Usuarios") );
        $control->add_item( new tsdk_lista_item("distrito","Distrito") );
        $control->add_item( new tsdk_lista_item("all","Todos",array("selected"=>null)) );
        $this->add_item("objeto",$control);
    */

    //-----------------------------------------------------------
    /**
    * Configura el control como un listbox fijo
    * @param integer $tabindex      tabindex del control 
    * @param integer $alto          alto del listbox
    * @param integer $ancho         ancho del control 
    * 
    * @return none
    * @example "sample_tsdk_listbox.php"
    */
    public function config_as_listfijo($tabindex,$alto=5,$ancho=CONTROL_WIDTH_PERCENTAJE)
    {
      $this->propiedades->set_propiedades(array("tabindex"=>$tabindex,
                                    "size"=>$alto
                                     ));
      $this->set_estilos(array("width"=>$ancho));
    }

    //-----------------------------------------------------------
    /**
    * Configura el control como un listbox dinamico con sql
    * @param integer $tabindex      tabindex del control 
    * @param string  $sql           consulta sql del control
    * @param integer $alto          alto del listbox
    * @param integer $ancho         ancho del control 
    * 
    * @return none
    * @example "sample_tsdk_listbox.php"
    */
    public function config_as_listsql($tabindex,$sql,$alto=5,$ancho=CONTROL_WIDTH_PERCENTAJE)
    {
      $this->propiedades->set_propiedades(array("tabindex"=>$tabindex,
                                    "size"=>$alto
                                     ));
      $this->set_estilos(array("width"=>$ancho));
      $this->sql=$sql;
    }


}

?>
