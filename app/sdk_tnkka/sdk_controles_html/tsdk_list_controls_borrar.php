<?php
/**
 * Este archivo implementa una lista de controles
 *
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
    exit(__FILE__.': No direct script access allowed');


//include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
//include_once ("../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
//include_once(APPPATH."../../objetos/tapi/tapi_recordset.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//-----------------------------------------------------------
/**
* Lista de controles HTML. Otros objetos son derivados de este control
*
* Implementa una lista de objetos html.
*
* Esta clase implementa una lista de objetos HTML
*
* @author luisvmt@hotmail.com
* @package controles
*/

class tsdk_list_controls
{
    // ******************************
    // variables miembro
    // ******************************

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * id de la ventana que contiene al control
    * @var string
    */
    protected       $m_idventana;

    /**
    * mensaje de error o exito de la clase
    * @var string
    */
    protected       $m_mensaje;

    /**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access protected
    */

    /**
    * Implementa un arreglo con la lista de controles
    * @var array
    */
    protected          $m_listaitems; //guarda la lista de controles

    /**#@-*/

    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @param  string    $idventana      Se refiere al id de la ventana que contiene el control
    * @example "sample_tsdk_list_controls.php"
    */
    function __construct($idventana="")
    {
        $this->m_idventana = $idventana;
        $this->m_listaitems=array();
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    idventana   :   id de la ventana que contiene todos los controles
    *    nro_items   :   nro de controles que contiene la lista
    *    mensaje     :   mensaje de la clase
    *
    */
    public function __get($propiedad)
    {
        switch ($propiedad){
        case "idventana":
            return $this->m_idventana;
            break;
        case "nro_items":
            return count($this->m_listaitems);
            break;
        case "mensaje":
            return $this->m_mensaje;
            break;

        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en get. Propiedad no encontrada: ".$propiedad.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificación de las propiedades de la clase
    *
    * @param string $propiedad      La propiedad a establecer.
    *
    *    <h5><strong>Posibles valores:</strong></h5>
    *    idventana   :   id de la ventana que contiene todos los controles
    *    mensaje     :   mensaje de la clase
    *
    * @param string $value          El valor a establecer
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "mensaje":
            $this->m_mensaje=$value;
            break;
        case "idventana":
            $this->m_idventana=$value;
            break;
        default:
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
            	 "#x Error en set. Propiedad no encontrada: ".$propiedad.
                 //", Value: ".$value.
                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );
            break;
        }
    }

    //-----------------------------------------------------------
    /**
    * devolviendo por referencia lista de subcontroles
    * @return   array                          Devuelve la lista de subcontroles
    * @example "sample_tsdk_list_controls.php"
    */
    public function &get_items()
    {
        return $this->m_listaitems;
    }

    //-----------------------------------------------------------
    /**
    * Busca un control en base a la clave proporcionada.
    * Busca en la lista de controles y en la de subcontroles de cada control
    *
    * @param  string    $indice     El texto que identifica al control (clave)
    * @return object                El control encontrado. Si no lo encuentra devuelve null
    * @example "sample_tsdk_list_controls.php"
    */
    public function &find_item($indice)
    {
        foreach ($this->m_listaitems as $clave => & $valor)
        {
            if ($clave == $indice)
            {
                return $valor;
            }else{
                $control = &$valor->get_subcontroles()->find_item($indice);
                if ( !is_null($control) )
                {
                    return $control;
                }
            }
        }

        //significa que no encontro el item dentro del foreach
        $control=null;
        return $control;
    }

    //-----------------------------------------------------------
    /**
    * Recupera una lista de controles que tienen una caracteristica en comun
    * La lista (array) devuelto es por referencia
    *
    * @param    bool        $is_control     Si es true, devuelve la lista de objetos que son controles
    * @param    bool        $has_sql        Si es true, devuelve la lista de objetos que tienen la propiedad sql
    * @return object                El control encontrado. Si no lo encuentra devuelve null
    * @example "sample_tsdk_list_controls.php"
    */
    public function &find_array($is_control=false,$has_sql=false)
    {
        $arreglo = array();

        //creando arreglo unidimensional de todos los controles
        if ($is_control==true)
        {

            foreach ($this->m_listaitems as $clave => & $valor)
            {
                $arreglo[$clave]=&$valor;

                //buscando en los subcontroles
                $arreglo2 = &$valor->get_subcontroles()->find_array($is_control,$has_sql);

                foreach ($arreglo2 as $clave2 => &$valor2)
                {
                    $arreglo[$clave2] = &$valor2;
                }

            }
        }

        //creando arreglo con sql
        if ($has_sql==true)
        {
            foreach ($this->m_listaitems as $clave => & $valor)
            {
                if ($valor->sql != "")
                {
                    $arreglo[$clave]=&$valor;
                }

                //buscando en los subcontroles
                //$arreglo2 = &$valor->m_subcontroles->find_array($has_sql,$is_control);
                $arreglo2 = &$valor->get_subcontroles()->find_array($is_control,$has_sql);

                foreach ($arreglo2 as $clave2 => &$valor2)
                {
                    $arreglo[$clave2] = &$valor2;
                }

            }
        }

        //retornando la salida
        return $arreglo;
    }

    //-----------------------------------------------------------
    /**
    * Busca dentro de la lista de controles el control referenciado.
    * Establecer su valor de acuerdo al tipo de control buscado
    * No devuelve ningun valor
    *
    * @param    string      $indice         Es el nombre del control buscado
    * @param    string      $valor          Es el valor a establecer
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function set_value_item($indice,$valor)
    {
        //estableciendo el valor del control
        switch ($this->m_listaitems[$indice]->tipocontrol ){
        case tsdk_tipocontrol::textbox:
            $this->m_listaitems[$indice]->value=$valor;
            break;

        case tsdk_tipocontrol::textarea:
        case tsdk_tipocontrol::span:
            $this->m_listaitems[$indice]->texto = $valor;
            break;

        case tsdk_tipocontrol::checkbox:
            if ($valor == "1" || $valor == true)
            {
                $this->m_listaitems[$indice]->set_propiedad('checked',null);
            }else{
                $this->m_listaitems[$indice]->del_propiedad("checked");
            }
            break;

        case tsdk_tipocontrol::combobox:
        case tsdk_tipocontrol::listbox:
            foreach ($this->m_listaitems[$indice]->m_listaitems as &$item)
            {
                if ($item->value == $valor)
                {
                    $item->selected = true;
                }else{
                    $item->selected = false;
                }
            }
            break;

        default:
	        tsdk_error_log(tsdk_propiedades::$TRACELOG,
		             "#x Error en set_value_item. Indice: [".$indice."]".
	                 ", Valor: [".$valor."]".
	                 ", Tipo de control: [".$this->m_listaitems[$indice]->tipocontrol."]".
	                 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
	                    "\n"
	                );
        	break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Agrega un item a la lista de items. Añade el nombre y idventana del item
    * No devuelve el item añadido
    *
    * @param    string      $idcontrol      Es el id del control a añadir
    * @param    object      $control        Es el control/item a añadir
    * @return   object                      El item añadido
    * @example "sample_tsdk_list_controls.php"
    */
    public function &add_item($idcontrol, &$control)
    {
        $control->set_id_ventana_control($this->m_idventana,$idcontrol);

        $this->m_listaitems[$idcontrol]=$control;

//tsdk_error_log_trace($idcontrol." - idventana",$this->m_idventana,__FILE__,__LINE__);
//tsdk_error_log_trace($idcontrol." - idcontrol",$idcontrol,__FILE__,__LINE__);
//tsdk_error_log_trace($idcontrol." - HTML",$this->m_listaitems[$idcontrol]->get_html(),__FILE__,__LINE__);

        return $this->m_listaitems[$idcontrol];
    }

    //-----------------------------------------------------------
    /**
    * Agrega un estilo al item
    *
    * @param    string      $indice         Es el nombre del item buscado
    * @param    string      $nombreestilo   Es el estilo a añadir
    * @param    string      $valorestilo    Es el valor del estilo a añadir
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    function add_estilo($indice,$nombreestilo,$valorestilo)
    {
		$this->m_listaitems[$indice]->estilos->add($nombreestilo, new tsdk_coleccion_item($valorestilo));

    }

    //-----------------------------------------------------------
    /**
    * Agrega un evento al item
    *
    * @param    string      $indice             Es el nombre del item buscado
    * @param    string      $nombreevento       Es el evento a añadir
    * @param    string      $nombrefuncion      Es la funcion que atenderá el evento
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    function add_evento($indice,$nombreevento,$nombrefuncion)
    {
        //$this->m_listaitems[$indice]->set_evento($nombreevento,$this->m_idventana."_".$indice."_".$nombrefuncion);
		$this->m_listaitems[$indice]->eventos->add($nombreevento)->value = $this->m_idventana."_".$indice."_".$nombrefuncion;
    }

    //-----------------------------------------------------------
    /**
    * Ejecuta el sql que tiene cada control, el resultado lo guarda en cada control respectivamente
    *
    * @param    object      $conexion           Es la conexion a la base de datos
    * @param    object      $control            Es el control donde ejecutar el sql
    * @return   int                             Devuelve 0 si es OK, -1 en caso de error
    * @example "sample_tsdk_list_controls.php"
    */
    function exec_sql_into_control($conexion,&$control)
    {
        $objeto_bd = new tsdk_resultset( $conexion );

        $tabladatos = $objeto_bd->obtener_tabla_from_sql( $control->sql ); //retorna un objeto tsdk_datos_sql

        if ( is_null($tabladatos) )
        {
            return -1;
        }

        //añadiendo valores
        switch ( $control->tipocontrol ){
        case tsdk_tipocontrol::textbox:
            break;

        case tsdk_tipocontrol::checkbox:
            break;

        case tsdk_tipocontrol::combobox:
        case tsdk_tipocontrol::listbox:

            //buscando el item del combobox/listbox
            foreach ($tabladatos->filas as $fila)
            {
                $id=$fila->item('id')->value;
                $texto=$fila->item('texto')->value;

                $control->add_item( new tsdk_lista_item( $id,$texto) );
            }
            break;
        }

        return 0;
    }

    //-----------------------------------------------------------
    /**
    * Recupera el html del control indicado
    *
    * @param    string      $idcontrol          Es el nombre del control buscado
    * @return   string                          Devuelve el codigo html
    * @example "sample_tsdk_list_controls.php"
    */
    public function get_html($idcontrol)
    {
        if (!isset($this->m_listaitems[$idcontrol]))
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
							"#x Item no creado en tv_list_controls: '".$idcontrol."'".
							", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
							   "\n"
						   );
		}

        //imprimiendo el control
        return $this->m_listaitems[$idcontrol]->get_html();
    }

    //-----------------------------------------------------------
    /**
    * Imprime en la salida estandar el html del control indicado
    *
    * @param    string      $idcontrol          Es el nombre del control buscado
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function print_html($idcontrol)
    {
        echo $this->get_html($idcontrol);
    }

    //-----------------------------------------------------------------------------------------------------
    //carga los controles html desde un recordset
    //$listaitems es de la forma  clave=>valor
    //                              clave:nombre del campo del recordset, nombre del item (control html)
    //                              valor:tipo de item: textbox,checkbox,combobox
    //------------------------------------------------------------------------------
    //public function load_items_from_recordset( $recordset,$listaitems )

    //-----------------------------------------------------------
    /**
    * Modifica el valor de la lista de controles desde un recordset
    *
    * @param    array       $listaitems         Lista de items a modificar
    * @param    object      $recordset          Es el recordset con los datos
    * @return   none
    * @example "sample_tsdk_list_controls.php"
    */
    public function load_values_from_recordset (& $listaitems, $recordset)
    {
    //error_log("paso_b1");
        //grabando en los controles
        foreach ($listaitems as $clave=>& $control)
        {
    //error_log("paso_b2");

            //estableciendo el valor del control
            switch ($control->tipocontrol ){
            case tsdk_tipocontrol::textbox:
				
				if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
				{
					//$control->value = $recordset->get_item_valor($clave);
					$control->value = $recordset->campos->item($clave)->value;
				}else{
					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									"#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
									", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									   "\n"
								   );
					$control->value = "CAMPO NO ENCONTRADO";
				}
					
                break;

            case tsdk_tipocontrol::textarea:
                				
				if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
				{
					//$control->texto = $recordset->get_item_valor($clave);
					$control->texto = $recordset->campos->item($clave)->value;
				}else{
					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									"#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
									", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									   "\n"
								   );
					$control->texto = "CAMPO NO ENCONTRADO";
				}
                break;

            case tsdk_tipocontrol::checkbox:
				
				if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
				{
					//if ($recordset->get_item_valor($clave) == "1")
					if ($recordset->campos->item($clave)->value == "1")
					{
						$control->checked = true;
					}else{
						$control->checked = false;
					}
				}else{
					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									"#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
									", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									   "\n"
								   );
					//$control->texto = "CAMPO NO ENCONTRADO";
					
				}
                break;

            case tsdk_tipocontrol::combobox:
            case tsdk_tipocontrol::listbox:
			
				if ($recordset->campos->key_exists($clave)) //validando si la clave existe en la lista de campos
				{
					//error_log("nro.items:".count($this->m_listaitems[$clave]->m_listaitems) );
					//buscando el item del combo
					/*foreach ($control->get_items() as &$item)
					{
						if ($item->value == $recordset->campos->item($clave)->value )
						{
							$item->selected = true;
						}else{
							$item->selected = false;
							//error_log("  x no igual");
						}
					}*/
					$control->select_item($recordset->campos->item($clave)->value);
				}else{
					tsdk_error_log(tsdk_propiedades::$TRACELOG,
									"#x Error en load_values_from_recordset. Campo de recordset no encontrado: ".$clave.
									", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									   "\n"
								   );
					//$control->texto = "CAMPO NO ENCONTRADO";
					$control->add_item( new tsdk_lista_item("-","CAMPO NO ENCONTRADO") );

				}
                break;
            }

        }

//error_log("paso_b3");

    }
}

?>
