<?php

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_parrafo.php");

include_once("../objetos/configuracion_app/constantes.php");

//-----------------------------------------------------------
/**
 * Escribe en el archivo log del web server
 *
 * @param  string    $texto          Texto a escribir
 * @param  string    $identificador  Identificador del mensaje de error, se utiliza para distinguir el mensaje a grabar
 */
/* function global_error_log($texto,$identificador="")
  {
  //escribiendo en archivo log
  if ($identificador != "")
  {
  $texto="[".$identificador."]".$texto;
  }

  error_log($texto);
  } */

//-----------------------------------------------------------
/**
 * Escribe una variable en un archivo TRACE
 * OJO: ex error_log_trace
 *
 * @param  array     $variables1          Arreglo con las variables a escribir en el trace (1)
 * @param  array     $variables2          Arreglo con las variables a escribir en el trace (2)
 */
/*
  function global_error_log_trace($nombre_variables1,$variables1,$nombre_variables2="",$variables2=array())
  {
  //abriendo el archivo
  //$fp = fopen(PATHLOG.'/backtrace.log','a');
  $fp = fopen(TRACELOG,'a+');

  //hallando fecha y hora
  $fecha   = date("d.m.y - H.i.s ");
  fwrite($fp, $fecha);

  $codigo="variable_1";
  $mensaje = "[ ".$codigo."] ************ ".$nombre_variables1." **************\n";
  fwrite($fp, $mensaje);

  $mensaje = "\n".print_r($variables1,true);
  fwrite($fp, $mensaje);

  $mensaje = "[ ".$codigo."] ***************************************************\n";
  fwrite($fp, $mensaje);

  if ($nombre_variables2 != "")
  {
  $codigo="variable_2";
  $mensaje = "[ ".$codigo."] ************ ".$nombre_variables2." **************\n";
  fwrite($fp, $mensaje);

  $mensaje = "\n".print_r($variables2,true);
  fwrite($fp, $mensaje);

  $mensaje = "[ ".$codigo."] ***************************************************\n";
  fwrite($fp, $mensaje);
  }

  //escribiendo la separacion
  $mensaje = "\n";
  fwrite($fp, $mensaje);

  //cerrando el archivo
  fclose($fp);
  } */

//-----------------------------------------------------------
/**
 * Valida un permiso sobre un objeto. Obtiene el codigo del usuario de la variable de session
 *
 * @param  object     $basedatos          Objeto base de datos
 * @param  string     $objeto             Objeto donde validar permiso
 * @param  string     $accion             Accion a validar si tiene permiso.
 *										  Las posibles acciones son:
 *											'browse', 'insertar', 'modificar', 'clonar', 'eliminar'
 *										  Si el tipo de usuario es 'A' (Administrador) esta permitido todas las opciones  
 */
function global_has_permiso($basedatos, $objeto, $accion) 
{
	//iniciando la variable de session
	$_SESSION["tipo_usuario"]=$_SESSION["tipo_usuario"]."";
	
	//validando si es administrador
	if (strtoupper($_SESSION["tipo_usuario"]) == "A") {
		return true;
	}

	//validando si el menu debe ser habilitado, se consulta con los valores establecidos en constantes.php
	$permitido = false;
	
	$global_permisos = $GLOBALS['global_permisos'];
	
	if (array_key_exists($_SESSION["tipo_usuario"],$global_permisos)) //si se ha creado la lista de permisos para el tipo de usuario
	{
		$texto_opcion = $objeto."_".$accion;
	
		if (array_key_exists($texto_opcion, $global_permisos[$_SESSION["tipo_usuario"]]))
		{
			return ($global_permisos[$_SESSION["tipo_usuario"]][$texto_opcion]==1)?true:false;
		}
	}
	
	return $permitido;
}


//-----------------------------------------------------------
/**
 * Valida un permiso sobre un objeto. Obtiene el codigo del usuario de la variable de session
 *
 * @param  object     $basedatos          Objeto base de datos
 * @param  string     $objeto             Objeto donde validar permiso
 * @param  string     $accion             Accion a validar si tiene permiso.
 *										  Las posibles acciones son:
 *											'browse', 'insertar', 'modificar', 'clonar', 'eliminar'
 *										  Si el tipo de usuario es 'A' (Administrador) esta permitido todas las opciones  
 */
function global_habilitar_menu($nombre_menu) 
{
	//convirtiendo la variable SESSION
	if (!isset($_SESSION["tipo_usuario"]))
	{
		$_SESSION["tipo_usuario"]="";
	}

	$_SESSION["tipo_usuario"]=$_SESSION["tipo_usuario"]."";
	
	//validando si es administrador
	if (strtoupper($_SESSION["tipo_usuario"]) == "A") {
		return true;
	}
	
	//validando si el menu debe ser habilitado, se consulta con los valores establecidos en constantes.php
	$permitido = false;
	
	$global_menu_opciones = $GLOBALS['global_menu_opciones'];
	if (array_key_exists($_SESSION["tipo_usuario"],$global_menu_opciones)) //si se ha creado la lista de menus para el tipo de usuario
	{
		if (in_array($nombre_menu,$global_menu_opciones[$_SESSION["tipo_usuario"]]))
		{
			return true; //si existe, se debe habilitar el menu
		}
	}
	return $permitido;
}

//-----------------------------------------------------------
/**
 * Valida un permiso sobre un objeto. Obtiene el codigo del usuario de la variable de session
 *
 * @param  object     $basedatos          Objeto base de datos
 */
/*function global_enable_menus($basedatos) {

	//---------------------------------------------
	//validando los permisos para usuario administrador
	if (strtoupper($_SESSION["tipo_usuario"]) == "A") {
		//devolviendo el permiso
		$_SESSION["enable_all_menus"] = true;
		return;
	}

	//---------------------------------------------
	//validando los permisos para usuario comun

	$_SESSION["enable_all_menus"] = false;

	//cambio de contraseña
	$_SESSION["menuprincipal_cambiarpassword"] = true;

	//usuario
	$_SESSION["menuprincipal_mantusuarios"] = false;

	//oficinas
	$_SESSION["menuprincipal_mantoficinas"] = false;

	//clasificacion
	$_SESSION["menuprincipal_mantclasificacion"] = false;

	//productos
	$_SESSION["menuprincipal_mantproductos"] = false;

	//paquetes
	$_SESSION["menuprincipal_mantpaquetes"] = false;

	//salir
	$_SESSION["menuprincipal_salir"] = true;
}*/

//-----------------------------------------------------------
/**
 * Obtiene el contenido del dialog cambiarpassword
 *
 * @param  string     $idventana         Indica el Id del dialogo
 */
/*function global_setcontent_cambiarpassword($idventana) {
	
	//creando los controles
	$texto_html = "";
	$tabindex=100;
	
	$titulo = new tsdk_parrafo("Cambio de contraseña",
								"", //idventana
								"", //idcontrol
								null, //propiedades
								array("font-weight"=>"bold") //estilos
								);
	
	$contrasena_nueva = new tsdk_textbox();
	$contrasena_nueva->set_id_ventana_control($idventana, "contrasena_nueva");
	$contrasena_nueva->config_as_password("", $tabindex++);
	//$contrasena_nueva->size = 30;
	
	$contrasena_nueva_confirmacion = new tsdk_textbox();
	$contrasena_nueva_confirmacion->set_id_ventana_control($idventana, "contrasena_nueva_confirmacion");
	$contrasena_nueva_confirmacion->config_as_password("", $tabindex++);

	$mensaje = new tsdk_span();
	$mensaje->set_id_ventana_control($idventana,"mensaje");
	$mensaje->config_as_mensaje();
	
	//creando la tabla
	$tabla = new tsdk_tabla("","",6, 2);
	$tabla->border="0";
	$tabla->set_estilos_celda(1,0, array("width"=>"130px"));
	$tabla->set_estilos_celda(1,1, array("width"=>"270px"));

	//primera linea
	$tabla->set_valores_fila(0,array($titulo->get_html()));
	$tabla->set_propiedades_celda(0,0,array("colspan"=>"2"));
	$tabla->set_estilos_celda(0,0,array("text-align"=>"center"));

	//tercera linea
	$tabla->set_valores_fila(2,array("Contraseña nueva :",$contrasena_nueva->get_html()));
	$tabla->set_estilos_fila(2,array("background"=>"#DBE2F1",
									 "height"=>"40px"));
	
	//cuarta linea
	$tabla->set_valores_fila(3,array("Contraseña nueva (confirmación):",$contrasena_nueva_confirmacion->get_html()));
	$tabla->set_estilos_fila(3,array("background"=>"#DBE2F1",
									 "height"=>"40px"));

	//quinta linea
	$tabla->set_valores_fila(5,array($mensaje->get_html()));
	$tabla->set_propiedades_celda(5,0,array("colspan"=>"2"));
	
	//imprimiendo los controles
	//$texto_html = $titulo->get_html(); OJO: verificar si se  usa
	$texto_html = $tabla->get_html();
	return $texto_html;
} */

?>
