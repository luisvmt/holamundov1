<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');
 
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");
include_once ("../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
include_once(APPPATH."../../objetos/app/trs_activo.php");
include_once ("../sdk_tnkka/sdk_varios/tsdk_window_browse.php");
include_once ("../sdk_tnkka/sdk_varios/tsdk_window_importar_exportar.php");

//-------------------------------------------------------------------------
//Este objeto implementa la creacion de la vista para la edicion y creacion
//-------------------------------------------------------------------------
class twindow_activos_importar_exportar extends tsdk_window_importar_exportar
{
    // **********************************
	// variables miembro
    // **********************************

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	// **********************************
    // metodos - publicos
	// **********************************
	
    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($id)
    {
		parent::__construct($id);

		//estableciendo permiso
		$this->permiso_importarexportar=false;

		//estableciendo ancho de columnas
		$this->m_ancho_1racolumna="300px";
		$this->m_ancho_2dacolumna="350px";

		//definiendo el codigo javascript
		$this->m_codigo_javascript = "<script language='javascript' type='text/javascript' src='javascript/sdk_javascript/tjs_window_importar_exportar.js'></script>\n";

		//titulos
		$this->m_titulo = "<strong>IMPORTAR/MODIFICAR ACTIVOS</strong>";

		//campos
		$this->m_lista_campos = "";
	}

    //-----------------------------------------------------------
	/**
	* Devuelve html de importar/exportar
	*
	* @example "sample_xxx.php"
	*/
	
	public function print_html()
	{
		return parent::print_html();
	}



}

?>
