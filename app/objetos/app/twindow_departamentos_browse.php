<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../objetos/configuracion_app/constantes.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
include_once(APPPATH."../../objetos/app/trs_departamento.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_window_browse.php");

//-------------------------------------------------------------------------
//Este objeto implementa la creacion de la vista para la edicion y creacion
//-------------------------------------------------------------------------
class twindow_departamentos_browse extends tsdk_window_browse
{

    // **********************************
	// variables miembro
    // **********************************
	protected	$m_nrofilas_by_pagina=0;

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//-----------------------------------------------------------
	/**
	* exporta la tabla de la base de datos hacia un archivo excel
	*
	* @example "sample_xxx.php"
	*/

	protected function print_exportar_excel()
	{
		$this->m_filtro = "";//$filtro;		//estableciendo el filtro
		//$this->m_orderby = ""; //$orderby;	//estableciendo el orden
		//$this->m_sentido = "ASC"; //$sentido;	//estableciendo el sentido del orden
		//$this->m_nropagina=1; //$nropagina;	//estableciendo nro de pagina - no se utiliza

		//creando objeto de base de datos
		$objeto_bd = new trs_departamento($this->m_basedatos );

		$tabladatos = $objeto_bd->get_datos_exportar($this->m_filtro);

		if ($tabladatos == -1)
		{
			return; //no se imprime el error en la pagina, solo en el log
		}

		//--------------------------------
		//rellena datos en el reporte
		$this->create_reporte_exportar($tabladatos);

		return parent::print_excel_base();
	}


	// **********************************
    // metodos - publicos
	// **********************************

	//-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($basedatos)
    {
		parent::__construct($basedatos);

		//inicializando variables
		$this->m_nrofilas_by_pagina=NROITEMS_BROWSE;
        $this->m_tituloreporte_excel = 'ADMINISTRACION DE DEPARTAMENTOS';
		$this->m_idventana = "departamento_browse";
		$this->m_subtitulo = "";

		// #ERROR-E00001# : ERROR PERSONALIZADO
		$this->m_titulos_browse=array("ID",
                                "Nombre de departamento",
                                "&nbsp;","&nbsp;");
        $this->m_titulos_reporte=array("ID",
								"Nombre de departamento"
								);
		$this->m_campos_browse=array("iddepartamento",
                                "nombredepartamento"
                                );
        $this->m_campos_reporte=array("iddepartamento",
                                "nombredepartamento"
                                );
		$this->m_alineamiento_columna= array("left","left");
		$this->m_claveprimaria=array("iddepartamento");
		$this->m_lista_acciones=array("ninguna"=>"- Ninguna -","eliminar"=>"Eliminar seleccionados");

		//opciones a mostrar
		$this->m_showinsertar=true;
		$this->m_showreporte=true;
		$this->m_show_importar_exportar=false;

		//----------------------------------------------------------------
		//validando la correcta definicion de variables
		//----------------------------------------------------------------
		$nro_titulos_browse = count($this->m_titulos_browse)-2;
		$nro_titulos_reporte=count($this->m_titulos_reporte);
		$nro_campos_browse=count($this->m_campos_browse);
		$nro_campos_reporte=count($this->m_campos_reporte);
		$nro_alineamiento=count($this->m_alineamiento_columna);

		if ($nro_titulos_browse != $nro_titulos_reporte ||
			$nro_titulos_browse != $nro_campos_browse ||
			$nro_titulos_browse != $nro_campos_reporte ||
			$nro_titulos_browse != $nro_alineamiento)
		{
			error_log("El numero de campos definido para el browse, reporte, campos_browse , campos_reporte, o alineamiento no es igual\n".
					  "Revisa la definicion de las siguientes variables m_titulos_browse, m_titulos_reporte, m_campos_browse, m_campos_reporte, m_alineamiento_columna\n".
					  "Revisar la definicion de campos en el constructor del objeto. Archivo:".__FILE__." Function:".__FUNCTION__);
			return;
		}
	}

	//-----------------------------------------------------------
	/**
	* obtiene el html de la ventana browse
	*
	* @example "sample_xxx.php"
	*/

	public function get_html_browse($nropagina,$filtro,$orderby,$sentido,$parametros)
	{
		$this->m_filtro = $filtro;		//estableciendo el filtro
		$this->m_orderby = $orderby;	//estableciendo el orden
		$this->m_sentido = $sentido;	//estableciendo el sentido del orden
		$this->m_nropagina=$nropagina;	//estableciendo nro de pagina
		$this->m_parametros=$parametros;

		//creando objeto de base de datos
		$objeto_bd = new trs_departamento($this->m_basedatos );

		//-------------------------------
		//recuperando el nro de paginas
		$this->m_totalregistros = $objeto_bd->get_nro_registros($this->m_filtro,$this->m_parametros);

		$this->m_totalpaginas = ($this->m_totalregistros % $this->m_nrofilas_by_pagina == 0)?$this->m_totalregistros/$this->m_nrofilas_by_pagina:
																		floor($this->m_totalregistros/$this->m_nrofilas_by_pagina) + 1;

		//validando el nro de pagina, corrigiendo
		if ($nropagina > $this->m_totalpaginas)
		{
			$nropagina=1;
			$this->m_nropagina=1;
		}

		//-------------------------------
		//recuperando datos de la tabla
		$tabladatos = $objeto_bd->get_datos_browse_excel($this->m_filtro,$nropagina,$orderby,$sentido,$this->m_parametros);

		if ( is_null($tabladatos) )
		{
			return; //no se imprime el error en la pagina, solo en el log
		}

		//---------------------------------------------------------------------
		//creando los controles que forman parte de la ventana de browse
		//despues de crear los controles, aqui se pueden personalizar
		$this->crear_controles();

		//-----------------------------------------------------
		//crea una tabla html con los datos de $tabladatos
		$regilla = $this->create_tabla($tabladatos, //tabla de datos
										false, //habilita onclick por fila
										true); //habilita ondblclick por fila

		//--------------------------------
		//adiciona propiedades
		$regilla->propiedades->add("width")->value="100%";

		//luis agregar estilo de ancho al div
		//quizas style="height:30%;background:red"

		//$regilla->add_propiedades_columna(0,array("width"=>"5%"));

		return parent::get_html_base($regilla);
	}

	//-----------------------------------------------------------
	/**
	* obtiene reporte en formato excel
	*
	* @example "sample_xxx.php"
	*/

	public function print_reporte_excel($nropagina,$filtro,$orderby,$sentido,$parametros)
	{
		$this->m_filtro = $filtro;		//estableciendo el filtro
		$this->m_orderby = $orderby;	//estableciendo el orden
		$this->m_sentido = $sentido;	//estableciendo el sentido del orden
		$this->m_nropagina=$nropagina;	//estableciendo nro de pagina - no se utiliza
		$this->m_parametros=$parametros;	//se recibe parametros en formato json

		//creando objeto de base de datos
		$objeto_bd = new trs_departamento($this->m_basedatos );

		$tabladatos = $objeto_bd->get_datos_browse_excel($this->m_filtro,0,$orderby,$sentido,$this->m_parametros);

		if ( is_null($tabladatos) )
		{
			return; //no se imprime el error en la pagina, solo en el log
		}

		//--------------------------------
		//rellena datos en el reporte
		$this->create_reporte_excel($tabladatos);

		//--------------------------------
		//adiciona estilos
		//$this->m_tabla->add_propiedades( array("width"=>"100%") );

		//luis agregar estilo de ancho al div
		//quizas style="height:30%;background:red"

		//$this->m_tabla->add_propiedades_columna(0,array("width"=>"5%"));

		/*
			//colocando bordes
			$columnainicial='A';
			$columnafinal=chr(ord($columnainicial)+count($this->m_titulos_reporte)-1);
			$this->m_objPHPExcel->getActiveSheet()->getStyle($columnainicial.$fila.':'.$columnafinal.$fila)->applyFromArray($estilo_bordes);
*/

		return parent::print_excel_base();
	}

}
