<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_interfaz.php");	//definiendo clase interfaz
include_once(APPPATH."../../objetos/varios/funciones.php");				//definiendo funciones globales
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");	//definiendo constantes

class tface_usuarios extends tsdk_interfaz
{
    // **********************************
	// variables miembro
    // **********************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	//protected  $m_basedatos=null;

	/**
	* Determina quien es el manejador de base de datos ('mysql' o 'postgresql')
	* @var string
	*/
	//protected  $m_manejadorbd=null;

	/**#@-*/

    // **********************************
    // metodos - privados y protegidos
    // **********************************

    //-----------------------------------------------------------
	/**
	* obtiene un objeto para insertar o modificar
	*
	* @example "sample_xxx.php"
	*/

	protected function obtener_recordset()
	{
		$mensaje = "";
		$control = "";
		$idview  = "usuario";

        //validando la existencia de controles (buscar: bloque1)
		if ( $this->existen_variables_post( array($idview."_idusuario",
												$idview."_nombres",
												$idview."_usuario",
												$idview."_tipo_usuario",
												$idview."_estado",
												$idview."_email",
												$idview."_iddepartamento"
												)
										   ) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			return null; //es un error de programacion
		}

		//validando datos obligatorios y que sean del tipo correcto (buscar: bloque2)
		if ( $this->validar_controles_obligatorios(
				array(
					array($idview."_usuario","Usuario de login",tsdk_tipodato::tstring_no_vacio,""),
					array($idview."_nombres","Nombres de usuario",tsdk_tipodato::tstring_no_vacio,"")
					),
				$control //control donde mostrar el error
				) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			return json_encode($trama_respuesta); //es un error de uso del aplicativo
		}

		//estableciendo campos de base de datos
		$recordset = new trs_usuario( $this->m_basedatos );

		$recordset->campos->add("idusuario")->value   = trim($this->m_controlador_base->input->post($idview."_idusuario"));
		$recordset->campos->add('nombres')->value  = trim($this->m_controlador_base->input->post($idview."_nombres"));
		$recordset->campos->add('usuario')->value  = trim($this->m_controlador_base->input->post($idview."_usuario"));
		$recordset->campos->add('contrasena')->value  = trim($this->m_controlador_base->input->post($idview."_contrasena"));
		$recordset->campos->add('tipo_usuario')->value  = trim($this->m_controlador_base->input->post($idview."_tipo_usuario"));
		$recordset->campos->add('estado')->value  = trim($this->m_controlador_base->input->post($idview."_estado"));
		$recordset->campos->add('iddepartamento')->value  = trim($this->m_controlador_base->input->post($idview."_iddepartamento"));
		$recordset->campos->add('email')->value  = trim($this->m_controlador_base->input->post($idview."_email"));

		$recordset->campos->add('eliminar')->value = APP_FALSE;

		return $recordset;
	}

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($controlador,$basedatos)
	{
		parent::__construct($controlador,$basedatos);
	}

    //-----------------------------------------------------------
	/**
	* Devuelve el HTML de la ventana de modificacion (modificacion, insercion)
	*
	* @example "sample_xxx.php"
	*/

	public function get_html_browse()
	{
		$respuesta = "";
		$idview="usuario";

        //validando si el usuario actual es el temporal y si ya existen usuarios
        if ($_SESSION['idusuario']==0)
        {
            //validando la cantidad de usuarios
            $vusuario = new trs_usuario($this->m_basedatos);
            if ($vusuario->existen_usuarios() == true)
            {
                //finalizando la session
                tsdk_error_log(tsdk_propiedades::$TRACELOG,
                        "#x Finalizó el ingreso temporal de un usuario administrador.".
                        ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n"
                    );

        		//guardando variable login en session
        		$_SESSION['tsdk_is_logged']=false;
        		session_destroy(); //destruyendo la session

                return "Debe volver a ingresar con el usuario creado.";
            }
        }

		//validando el permiso sobre el objeto
		$permisomodificar = global_has_permiso($this->m_basedatos,$idview,'mantenimiento');
		$permisobrowse = global_has_permiso($this->m_basedatos,$idview,'browse');

		//devolviendo tabla browse
		$objetobrowse = new twindow_usuarios_browse($this->m_basedatos);

		if ($permisomodificar==true)
		{
			$objetobrowse->show_insertar=true;
			$objetobrowse->show_eliminar=true;
		}else{
			$objetobrowse->show_insertar=false;
			$objetobrowse->show_eliminar=false;
		}

		//hallando variables desde POST
		$nropagina= $this->m_controlador_base->input->post('nropagina');
		$filtro = $this->m_controlador_base->input->post('filtro');
		$orderby = $this->m_controlador_base->input->post('orderby');
		$sentido = $this->m_controlador_base->input->post('sentido');
		$parametros = $this->m_controlador_base->input->post('parametros');

		//html
		if ($permisobrowse==true)
		{
			$respuesta = "<!-- OK -->\n".$objetobrowse->get_html_browse($nropagina,$filtro,$orderby,$sentido,$parametros);
		}else{
			$respuesta = "<!-- OK -->\nNO TIENE PERMISO PARA VISUALIZAR ITEMS";
		}

		//javascript ... debe ser llamado luego de print_html
		//$browseusuarios->print_javascript($nropagina,$filtro,$orderby,$sentido);

		return $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* Inserta un registro.
	*
	* @example "sample_xxx.php"
	*/
	public function procesar_insertar_desde_form()
	{
		$control="";
		$recordset = null;

		//obteniendo el objeto recordset a modificar
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			$mensaje="Error procesando solicitud. Reportar con administrador.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}
		if ( gettype($recordset)=="string") //existe un error de uso del app
		{
			return $recordset;
		}

		//insertando
		if ( $recordset->insertar() < 0 )
		{
			$mensaje = $recordset->mensaje;

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('idusuario')->value );
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* modificar el registro (cuando se da clic en grabar)
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_modificar_desde_form()
	{
		$control="";

		//obteniendo el objeto recordset a modificar
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}
		if ( gettype($recordset)=="string") //existe un error de uso del app
		{
			return $recordset;
		}

		//modificando
		if ( $recordset->modificar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('idusuario')->value );
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* eliminando el registro
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_eliminar_registro()
	{
		$control="";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("idusuario") ) == false) //error de programacion, no se envia detalle a navegador
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//estableciendo campos de base de datos
		$recordset = new trs_usuario( $this->m_basedatos );
		$recordset->campos->add("idusuario")->value = $this->m_controlador_base->input->post("idusuario");

		//eliminando
		if ( $recordset->eliminar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error eliminando registro. ". $recordset->mensaje.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="Se eliminó el registro correctamente";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje);
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* clonando el registro
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_clonar_registro()
	{
		$control="";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("id")) == false) //se coloca la variable id desde javascript
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//decodificando la variable id
		$lista_id = json_decode($this->m_controlador_base->input->post("id"),true); //id es un texto json enviado desde el cliente
		if (is_null($lista_id))
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en json_decode.".
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//estableciendo campos de base de datos
		$recordset = new trs_usuario( $this->m_basedatos );
		$recordset->campos->add('idusuario')->value = $lista_id["idusuario"];

		//clonando
		if ( $recordset->clonar() < 0 )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en funcion clonar. ". $recordset->mensaje.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$recordset->mensaje);
			return json_encode($trama_respuesta);
		}

		//generando el json de respuesta
		$datos = json_encode( array("idusuario" => strval($recordset->campos->item("idusuario")->value)) );

		//finalizando
		$mensaje="Se clonó el registro correctamente. Debe grabar si acepta el nuevo registro.";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$datos );
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* obtiene el contenido del dialogo cambiar password
	*
	* @example "sample_xxx.php"
	*/

	public function get_html_cambiar_password()
	{
		//$idventana="usuario";
		$idventana= $this->m_controlador_base->input->post('idventana'); //viene como parametro desde el cliente

		//creando los controles
		$texto_html = "";
		$tabindex=1;

		//creando los controles
		$titulo = new tsdk_parrafo("Cambio de contraseña",
									"", //idventana
									"", //idcontrol
									null, //propiedades
									array("font-weight"=>"bold") //estilos
									);

		$contrasena_nueva = new tsdk_textbox();
		$contrasena_nueva->set_id_ventana_control($idventana, "contrasena_nueva");
		$contrasena_nueva->config_as_password("", $tabindex++);

		$contrasena_nueva_confirmacion = new tsdk_textbox();
		$contrasena_nueva_confirmacion->set_id_ventana_control($idventana, "contrasena_nueva_confirmacion");
		$contrasena_nueva_confirmacion->config_as_password("", $tabindex++);

		$mensaje = new tsdk_span();
		$mensaje->set_id_ventana_control($idventana,"mensaje");
		$mensaje->config_as_mensaje();

		//creando la tabla
		$tabla = new tsdk_tabla("","",6, 2);
		$tabla->border="0";
		$tabla->set_estilos_celda(1,0, array("width"=>"130px"));
		$tabla->set_estilos_celda(1,1, array("width"=>"270px"));

		//primera linea
		$tabla->set_valores_fila(0,array($titulo->get_html()));
		$tabla->set_propiedades_celda(0,0,array("colspan"=>"2"));
		$tabla->set_estilos_celda(0,0,array("text-align"=>"center"));

		//segunda linea
		//vacia

		//tercera linea
		$tabla->set_valores_fila(2,array("Contraseña nueva :",$contrasena_nueva->get_html()));
		$tabla->set_estilos_fila(2,array("background"=>"#DBE2F1",
											"height"=>"40px"));

		//cuarta linea
		$tabla->set_valores_fila(3,array("Contraseña nueva (confirmación):",$contrasena_nueva_confirmacion->get_html()));
		$tabla->set_estilos_fila(3,array("background"=>"#DBE2F1",
											"height"=>"40px"));

		//quinta linea
		$tabla->set_valores_fila(5,array($mensaje->get_html()));
		$tabla->set_propiedades_celda(5,0,array("colspan"=>"2"));

		//imprimiendo los controles
		//$texto_html = $titulo->get_html(); OJO: verificar si se  usa
		$texto_html = $tabla->get_html();
		//return $texto_html;

		return $texto_html;

	}

    //-----------------------------------------------------------
	/**
	* cambia la contraseña del usuario
	*
	* @example "sample_xxx.php"
	*/

	public function ajax_procesar_cambiarpassword()
	{
		//hallando variables
		$password_nuevo= $this->m_controlador_base->input->post('password_nuevo');
		$idusuario = $_SESSION['idusuario'];
		$control = ""; //control que obtendra el focus

		//cambiando la contraseña
		$usuario = new trs_usuario($this->m_basedatos);
		$respuesta = $usuario->cambiar_password($idusuario, $password_nuevo);
		if ($respuesta < 0)
		{
			$mensaje = "Error al cambia la contraseña. Consulte con el administrador.";
		}else{
			$mensaje = "Se ingresó correctamente";
			$respuesta = 0;
		}

		//finalizando
		$trama_respuesta = array("response"=>$respuesta,
									"control"=>$control,
									"mensaje"=>$mensaje,
									"id"=>$_SESSION['idusuario'] );
		return json_encode($trama_respuesta);

	}

}



?>
