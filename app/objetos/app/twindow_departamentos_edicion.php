<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once ("../objetos/configuracion_app/constantes.php");
include_once ("../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");
include_once ("../objetos/app/trs_departamento.php");
include_once ("../sdk_tnkka/sdk_varios/tsdk_window_browse.php");
include_once ("../sdk_tnkka/sdk_varios/tsdk_window_edicion.php");

//-------------------------------------------------------------------------
//Este objeto implementa la creacion de la vista para la edicion y creacion
//-------------------------------------------------------------------------
class twindow_departamentos_edicion extends tsdk_window_edicion
{
    // **********************************
	// variables miembro
    // **********************************

	/**
	* Lista de variables POST que se utiliza para recibir las variables
	* @var array
	*/
	protected		$m_variables_post=array();

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	// **********************************
    // metodos - publicos
    // **********************************

	//-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($basedatos)
    {
		//estableciendo ancho de columnas
		$this->m_ancho_1racolumna="250px";
		$this->m_ancho_2dacolumna="400px";

		//definiendo el codigo javascript
		$this->m_codigo_javascript = "<script language='javascript' type='text/javascript' src='javascript/sdk_javascript/tjs_window_edicion.js'></script>\n";

		//titulos
		$this->m_titulo_insertar="<strong>INSERTAR DEPARTAMENTO</strong>";
		$this->m_titulo_modificar="<strong>MODIFICAR DEPARTAMENTO</strong>";

		parent::__construct($basedatos,"departamento");

	}

    //-----------------------------------------------------------
    /**
    * Get magic method
    */
    public function __get($propiedad)
    {
        switch ($propiedad){

        case "variables_post":

            return $this->m_variables_post;
            break;

        default:
            return parent::__get($propiedad);
            break;
        }

    }

    //-----------------------------------------------------------
    /**
    * Set magic method
    */
    public function __set($propiedad, $value)
    {
        switch ($propiedad){
        case "variables_post":
            $this->m_variables_post = $value;
            break;

        default:
            return parent::__set($propiedad,$value);
            break;
        }
    }


    //-----------------------------------------------------------
	/**
	* Esta funcion devuelve la ventana html de la ventana edicion
	*
	* @example "sample_xxx.php"
	*/

	public function get_html_edicion()
	{
		//definiendo el objeto (basedatos,'id')
		$this->m_listacontroles = new twindow_departamentos_edicion_controles($this->m_basedatos,"departamento");

		//leyendo parametros de post
		$this->m_accion_ejecutar = $this->m_variables_post['accion_ejecutar'];

		//---------------------------------------------
		//cargando valores en controles
		if ( $this->m_listacontroles->load_valores( $this->m_variables_post ) < 0)
		{
			error_log($this->m_listacontroles->mensaje);
			echo "Error en carga. Revisar el log.";
			return;
		}

		//---------------------------------------------
		//creando las tablas

		//----------------------------------------------------------------------------------------------
		//creando sub.tabla 1
		$contadorfila=0;
		$nrocolumnas=2;
		$this->m_titulo_tabla[0]="<h3>Datos del departamento</h3>";
		//$this->m_titulo_tabla[0]=""; //No se pone titulo para que no se visualize

		$this->m_tabla[0] = new tsdk_tabla($this->m_listacontroles->idventana,"tabla-edicion",
											50,2,
											array(	"border"=>"0",
													"class"=>"tvedicion_class"
												));
		$this->m_tabla[0]->set_valores_fila($contadorfila++,array("ID:", $this->m_listacontroles->get_html("iddepartamento")));
		$this->m_tabla[0]->set_valores_fila($contadorfila++,array("NOMBRE DE DEPARTAMENTO:", $this->m_listacontroles->get_html("nombredepartamento")));
		$this->m_tabla[0]->set_dimensiones($contadorfila,$nrocolumnas, false);
		//error_log("paso x4");

		//----------------------------------------------------------------------------------------------
		//creando sub.tabla 2
		/*$contadorfila=0;
		$nrocolumnas=2;

		$this->m_titulo_tabla[1]="<h3>Otros datos</h3>";

		$this->m_tabla[1] = new tsdk_tabla($this->m_listacontroles->idventana,"tabla-edicion",
											50,2,
											array(	"border"=>"0",
													"class"=>"tvedicion_class"
												));
		$this->m_tabla[1]->set_valores_fila($contadorfila++,array("DATOS ADICIONALES:",$this->m_listacontroles->get_html("datos_adicionales")));
		$this->m_tabla[1]->set_dimensiones($contadorfila,$nrocolumnas,false);
*/
		//----------------------------------------------------------------------------------------------

		return parent::get_html_edicion();
	}

}

?>
