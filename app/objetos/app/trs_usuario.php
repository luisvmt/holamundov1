<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once ("../sdk_tnkka/sdk_basedatos/tsdk_resultset.php");
include_once ("../sdk_tnkka/sdk_basedatos/tsdk_tipodato.php");

class trs_usuario extends tsdk_resultset
{
    // **********************************
	// variables miembro
    // **********************************

	protected $m_nrofilas_by_pagina = 0;

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//-----------------------------------------------------------
	/**
	* obtiene el texto que representa el filtro de registros
	*
	* @example "sample_xxx.php"
	*/

	private function get_sentence_filtro($filtro_a_buscar)
	{
		$lista_campos = array(
								"idusuario",
								"nombres",
								"estado",
								"tipo_usuario",
								"email",
								"nombredepartamento"
							);

		return $this->get_sql_filtro($this->m_manejadorbd,		//manejador de base de datos
										"",						//prefijo de los campos
										$lista_campos,			//arreglo de campos que intervienen en el filtro
										$filtro_a_buscar		//filtro a buscar
									);

	}

    //-----------------------------------------------------------
	/**
	* validando que algunos campos en la tabla sean unicos, que no se repitan
	*
	* @example "sample_xxx.php"
	*/

	private function validar_camposunicos()
	{
		if ( $this->campos->key_exists("usuario"))
		{
			//validando que el usuario-login no exista
			$consulta  = "select count(idusuario) as contador ".
						 "from ".$this->m_nombretabla." ".
						 "where usuario='#usuario#' and ".
						 "idusuario <> #idusuario# ".
						 "and eliminar=false ";

			$tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
			if ( is_null($tabla) )
			{
				return -1;
			}

			if ( $tabla->get_valor_campo(0, "contador") != "0" ) //leyendo la fila 0
			{
				$this->mensaje="El usuario (login) ya existe.";
				return -1;
			}
		}

		if ( $this->campos->key_exists("email"))
		{
			if ($this->campos->item('email')->value != "")
			{
				//validando que el email no exista
				$consulta  = "select count(idusuario) as contador ".
				"from ".$this->m_nombrevista." ".
				"where email='#email#' and ".
				"idusuario <> #idusuario# ";

				$tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
				if ( is_null($tabla) )
				{
					return -1;
				}

				if ( $tabla->get_valor_campo(0, "contador") != "0" ) //leyendo la fila 0
				{
					$this->mensaje="El correo electrónico ya existe.";
					return -1;
				}
			}
		}
	}

    //-----------------------------------------------------------
	/**
	* obtener from usuario-login
	*
	* @example "sample_xxx.php"
	*/

	protected function obtener_from_correo()
	{
		//hallando el usuario
		$consulta  = "select * from ".$this->m_nombretabla." where upper(email)='".strtoupper($this->campos->item('email')->value)."' ";
		$consulta .= "and eliminar=false";
		//error_log ($consulta);

		if ($this->obtener_campos_from_sql($consulta) == 0)
		{
			$this->mensaje="Correo [".$this->campos->item('email')->value."] no registrado.";

			return -1;
		}

		//actualizando la contraseña para que no se visualize
		//$this->set_item_valor('contrasena','#no-establecido#');
		$this->campos->item("contrasena")->value = '#no-establecido#';

		return 0;
	}

    //-----------------------------------------------------------
	/**
	* asigna un password temporal
	*
	* @example "sample_xxx.php"
	*/

	protected function asignar_password_temporal()
	{
		//ya se tiene todos los datos del registro cargado

		//obteniendo una contraseña temporal
		$nuevo_password = tsdk_rand_string(10);

		//encriptando el password
		//$this->set_item_valor('contrasena', password_hash($nuevo_password, PASSWORD_DEFAULT) );
		//$this->set_item_valor('contrasena', $nuevo_password );
		$this->campos->item("contrasena")->value = $nuevo_password;

		//devolviendo el nuevo password
		return $nuevo_password;
	}

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($basedatos)
	{
		//llamando al padre
		parent::__construct($basedatos,MANEJADORBD);

		//inicializando variables
		$this->m_nombretabla = "usuario";
		$this->m_nombrevista = "usuario_browse";
		$this->m_nrofilas_by_pagina=NROITEMS_BROWSE;
	}

    //-----------------------------------------------------------
	/**
	* destructor
	*
	* @example "sample_xxx.php"
	*/
	public function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* inserta un registro en la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function insertar()
	{
		//validando que algunos campos sean unicos
		if ($this->validar_camposunicos()<0)
		{
			return -1;
		}

		//hallando el hash de la contraseña
		if ( $this->campos->key_exists("contrasena") )
		{
			if ( $this->campos->item("contrasena")->value != '#no-establecido#' )
			{
				$this->campos->item("contrasena")->value = password_hash( $this->campos->item("contrasena")->value,
																			PASSWORD_DEFAULT);
			}
		}

		//actualizando datos por defecto
		if (! $this->campos->key_exists("eliminar"))
		{
			$this->campos->add("eliminar")->value=false;
		}

		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariocreacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechacreacion")->value = "now()";
		//$this->campos->add("fechamodificacion")->value = "now()";

		//llamando al padre, indicando que idcargo es autoincrement
		return parent::insertar("idusuario", //campo autoincrement
                                array() //array("fechacreacion","fechamodificacion") //variables que se respeta tal cual
								);

	}

    //-----------------------------------------------------------
	/**
	* obtiene datos de un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function obtener()
	{
		$respuesta=parent::obtener();

		//actualizando la contraseña para que no se visualize
		if ( ( strcasecmp($this->campos->item('contrasena')->value,"#no-establecido#") !=0 ) and
		     ( strcasecmp($this->campos->item('contrasena')->value,"") !=0 ) )
		{
			$this->campos->add('contrasena')->value = '#no-establecido#';
		}else{
			$this->campos->add('contrasena')->value = '';
		}

		return $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* modifica un registro en la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function modificar()
	{
		//validando que hallan ingresado la clave primaria
		if ( !$this->campos->key_exists("idusuario"))
		{
			$this->mensaje="No ha ingresado el id de usuario.";
			return -1;
		}

		//validando que algunos campos sean unicos
		if ($this->validar_camposunicos()<0)
		{
			return -1;
		}

		//hallando el hash de la contraseña
		if ( $this->campos->key_exists("contrasena") )
		{
			if ( $this->campos->item("contrasena")->value != '#no-establecido#' )
			{
				$this->campos->item("contrasena")->value = password_hash( $this->campos->item("contrasena")->value,
																			PASSWORD_DEFAULT);
			}else{
				//Si es igual a '#no-establecido#' lo retiramos de la lista para que no modifique la contraseña
				$this->campos->delete("contrasena");
			}
		}

		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechamodificacion")->value = "now()";

		//llamando al padre, indicando que idcargo es autoincrement
		return parent::modificar(array("idusuario") , //variables que conforman la clave primaria
                                array() //array("fechamodificacion") //variables que se respeta tal cual
								);

	}

    //-----------------------------------------------------------
	/**
	* clona un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function clonar()
	{
		//hallando el objeto actual
		if ($this->obtener()<0)
		{
			return -1;
		}

		//modificando el campo eliminar
		//$this->set_item_valor('eliminar',true);	//true
		$this->campos->add("eliminar")->value = true; //true

		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariocreacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechacreacion")->value = "now()";
		//$this->campos->add("fechamodificacion")->value = "now()";

		//llamando al padre, indicando que idcargo es autoincrement
		return parent::insertar("idusuario", //campo autoincrement
                                array() //array("fechacreacion","fechamodificacion") //variables que se respeta tal cual
								);
	}

	//-----------------------------------------------------------
	/**
	* obtiene el total de registros de la tabla
	*
	* @example "sample_xxx.php"
	*/

	public function get_nro_registros($filtro_a_buscar,$parametros)
	{
		$consulta = "";
		$filtro = $this->get_sentence_filtro($filtro_a_buscar);

		$consulta = "select count(*) as contador ".
					"from ".$this->m_nombrevista;

		if ($filtro != ""){
			$consulta .= " where ".$filtro;
		}

		//obteniendo los datos
		$tabladatos = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
		$nroregistros=$tabladatos->get_valor_campo(0,"contador");

		return $nroregistros;
	}

    //-----------------------------------------------------------
	/**
	* validar usuario y password de un usuario
	*
	* @example "sample_xxx.php"
	*/

	public function validarusuario	($usuario, $password)
	{
        //validando la cantidad de usuarios que existe en la BD, si no existen usuarios se le permitirá el acceso sin validar contraseña
        $consulta = "select count(*) as contador from ".$this->m_nombretabla;

        $tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
        if ( is_null($tabla) )
        {
            return false;
        }

        if ( $tabla->get_valor_campo(0, "contador") == "0" ) //leyendo la fila 0
        {
            tsdk_error_log(tsdk_propiedades::$TRACELOG,
                    "#x No existe usuarios creados, se permite el ingreso temporalmente. Se debe crear usuarios por medida de seguridad.".
                    ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                    "\n"
                );

            $this->campos->add("idusuario")->value=0;
            $this->campos->add("email")->value="ninguno@ninguno";
            $this->campos->add("nombres")->value="ninguno";
            $this->campos->add("contrasena")->value="ninguno";
            $this->campos->add("tipo_usuario")->value="A";
            $this->campos->add("estado")->value="A";

            return true;
        }

		//hallando el usuario
		$consulta = "select * from ".$this->m_nombretabla." where upper(usuario)='".strtoupper($usuario)."' and eliminar=false";

		if ($this->obtener_campos_from_sql($consulta)<=0)
		{
			$this->mensaje = "Usuario [".$usuario."] no registrado.";
			return false;
		}

		if ($this->campos->item("estado")->value != "A")
		{
			$this->mensaje = "Usuario [".$usuario."] no activo.";
			return false;
		}

		if ( $this->campos->item("contrasena")->value == '#no-establecido#' ||
			 $this->campos->item("contrasena")->value == '' )
		{
			return true;
		}

		if ( password_verify(trim($password),$this->campos->item("contrasena")->value ) )
		{
			return true;
		}else{
			//$this->mensaje=tsdk_reemplazar_html("Contraseña no válida");
			$this->mensaje="Contraseña no válida";

			return false;
		}

		return false;
	}

    //-----------------------------------------------------------
	/**
	* validar si existe mas de un usuario en la base de datos.
    * De existir mas de un usuario y el usuario actual es anonimo, se debe desloguear
	*
	* @example "sample_xxx.php"
	*/

	public function existen_usuarios()
	{
        //validando la cantidad de usuarios que existe en la BD, si no existen usuarios se le permitirá el acceso sin validar contraseña
        $consulta = "select count(*) as contador from ".$this->m_nombretabla;

        $tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
        if ( is_null($tabla) )
        {
            return true; //devuelve como si existieran usuarios
        }

        if ( $tabla->get_valor_campo(0, "contador") == "0" ) //leyendo la fila 0
        {
            return false;
        }

        return true;
	}

    //-----------------------------------------------------------
	/**
	* obtiene los datos desde el usuario login
	*
	* @example "sample_xxx.php"
	*/

	public function obtener_from_usuariologin()
	{
		//hallando el usuario
		$consulta  = "select * from ".$this->m_nombretabla." where upper(usuario)='".strtoupper($this->campos->item('usuario')->value)."' ";
		$consulta .= "and eliminar=false";
		if ($this->obtener_campos_from_sql($consulta)<0)
		{
			$this->mensaje="Usuario [".$this->campos->item('usuario')->value."] no registrado.";

			return -1;
		}

		//actualizando la contraseña para que no se visualize
		//$this->set_item_valor('contrasena','#no-establecido#');
		$this->campos->item("contrasena")->value = '#no-establecido#';
		return 0;
	}

    //-----------------------------------------------------------
	/**
	* obtiene los datos para ventana browse o excel
	*
	* @example "sample_xxx.php"
	*/

	public function get_datos_browse_excel($filtro_a_buscar,$nropagina,$orderby,$sentido,$parametros) //el reporte de excel no esta paginado, por ello $nropagina=0
	{
		$consulta = "";
		$filtro = $this->get_sentence_filtro($filtro_a_buscar);

		// #ERROR-E00001# : ERROR PERSONALIZADO
		$consulta = "select * ".
					"from ".$this->m_nombrevista." ";

		if ($filtro != ""){
			$consulta .= " where ".$filtro;
		}

		$consulta .= " order by ".$orderby." ".$sentido;

		if ($nropagina > 0)
		{
			$consulta .= " LIMIT #LIMIT# OFFSET #OFFSET#";

			$offset = $this->m_nrofilas_by_pagina * ($nropagina - 1);

			//reemplazando el valor del item
			$this->parametros->add("limit")->value = $this->m_nrofilas_by_pagina;
			$this->parametros->add("offset")->value = $offset;
		}

		return $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
	}

    //-----------------------------------------------------------
	/**
	* ambiando unicamente el password del usuario
	*
	* @example "sample_xxx.php"
	*/

	public function cambiar_password($idusuario,$password_nuevo)
	{
		//cifrando el password
		$cifrado = password_hash($password_nuevo, PASSWORD_DEFAULT);

		//creando la consulta con la base de datos
		$consulta =  "update ".$this->m_nombretabla;
		$consulta .= " set contrasena='".$cifrado."' ";
		$consulta .= " where idusuario=".$idusuario;

		//ejecutando la consulta
		$respuesta = $this->execute_sql($consulta);
		return $respuesta;
	}
}
