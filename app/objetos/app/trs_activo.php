<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once ("../sdk_tnkka/sdk_basedatos/tsdk_resultset.php");
include_once ("../sdk_tnkka/sdk_basedatos/tsdk_tipodato.php");

class trs_activo extends tsdk_resultset
{ 

    // **********************************
	// variables miembro
    // **********************************
	protected $m_nrofilas_by_pagina = 0;

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	//-----------------------------------------------------------
	/**
	* obtiene el texto que representa el filtro de registros
	*
	* @example "sample_xxx.php"
	*/
	private function get_sentence_filtro($filtro_a_buscar)
	{
		$lista_campos = array(
							"idactivo",
							"nombre_activo",
							"tipo_activo",
							"estado",
							"usuario_responsable",
							"nombredepartamento",
							"nombre_activo_padre"
							);

		return $this->get_sql_filtro($this->m_manejadorbd,		//manejador de base de datos
										"",						//prefijo de los campos
										$lista_campos,			//arreglo de campos que intervienen en el filtro
										$filtro_a_buscar		//filtro a buscar
									);			
	}

    //-----------------------------------------------------------
	/**
	* validando que algunos campos en la tabla sean unicos, que no se repitan
	*
	* @example "sample_xxx.php"
	*/

	private function validar_camposunicos()
	{
		if ( $this->campos->key_exists("nombre_activo")) //validando que no exista este valor
		{
			//validando que el nombre-activo no exista
			$consulta  = "select count(nombre_activo) as contador ".
						 "from ".$this->m_nombretabla." ".
						 "where nombre_activo='#nombre_activo#' and ".
						 "idactivo <> #idactivo# ".
						 "and eliminar=false ";

			$tabla = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
			if ( is_null($tabla) )
			{
				return -1;
			}

			if ( $tabla->get_valor_campo(0, "contador") != "0" ) //leyendo la fila 0
			{
				$this->mensaje="El activo ya existe.";
				return -1;
			}
		}
	}
	
	// **********************************
    // metodos - publicos
    // **********************************

	
	//-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($basedatos)
	{
		//llamando al padre
		parent::__construct($basedatos,MANEJADORBD);

		//inicializando variables
		$this->m_nombretabla = "activo";
		$this->m_nombrevista = "activo_browse";
		$this->m_nrofilas_by_pagina=NROITEMS_BROWSE;
	}

	//-----------------------------------------------------------
	/**
	* destructor
	*
	* @example "sample_xxx.php"
	*/

	public function __destruct()
	{
	}

	//-----------------------------------------------------------
	/**
	* inserta un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function insertar()
	{
		//validando que algunos campos sean unicos
		if ($this->validar_camposunicos()<0)
		{
			return -1;
		}
		
		//actualizando datos por defecto
		if (! $this->campos->key_exists("eliminar"))
		{
			$this->campos->add("eliminar")->value=false;
		}
		
		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariocreacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechacreacion")->value = "now()";
		//$this->campos->add("fechamodificacion")->value = "now()";
		
		//llamando al padre, indicando que idactivo es autoincrement
		return parent::insertar("idactivo", //campo autoincrement
                                array() //array("fechacreacion","fechamodificacion") //variables que se respeta tal cual
								);
	}

	//-----------------------------------------------------------
	/**
	* obtiene un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/
	public function obtener()
	{
		$respuesta=parent::obtener();

		return $respuesta;
	}
	
    //-----------------------------------------------------------
	/**
	* modifica un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/

	public function modificar()
	{
		//validando que algunos campos sean unicos
		if ($this->validar_camposunicos()<0)
		{
			return -1;
		}
		
		
		//validando que hallan ingresado la clave primaria
		if ( !$this->campos->key_exists("idactivo"))
		{
			$this->mensaje="No ha ingresado el id de activo.";
			return -1;
		}
		
		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechamodificacion")->value = "now()";
		
		//llamando al padre, indicando que idactivo es autoincrement
		return parent::modificar(array("idactivo") , //variables que conforman la clave primaria
                                array() //array("fechamodificacion") //variables que se respeta tal cual
								);
		
	}

    //-----------------------------------------------------------
	/**
	* duplica un registro de la base de datos
	*
	* @example "sample_xxx.php"
	*/
	
	public function clonar()
	{
		//hallando el objeto actual
		if ($this->obtener()<0)
		{
			return -1;
		}

		//modificando el campo eliminar
		//$this->set_item_valor('eliminar',true);	//true
		$this->campos->add("eliminar")->value = true; //true

		//modificando el usuario que crea y modifica
		//$this->campos->add("idusuariocreacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("idusuariomodificacion")->value = $_SESSION["idusuario"];
		//$this->campos->add("fechacreacion")->value = "now()";
		//$this->campos->add("fechamodificacion")->value = "now()";
		
		//llamando al padre, indicando que idactivo es autoincrement
		return parent::insertar("idactivo", //campo autoincrement
                                array() //array("fechacreacion","fechamodificacion") //variables que se respeta tal cual
								);
	}

	//-----------------------------------------------------------
	/**
	* obtiene el total de registros de la tabla
	*
	* @example "sample_xxx.php"
	*/
	 
	public function get_nro_registros($filtro_a_buscar,$parametros)
	{
		$consulta = "";
		$filtro = $this->get_sentence_filtro($filtro_a_buscar);

		$consulta = "select count(*) as contador ".
					"from ".$this->m_nombrevista;		

		if ($filtro != ""){
			$consulta .= " where ".$filtro;
		}
		
		/*consulta: 
		select a.idactivo, a.nombre_activo, 
		a.tipo_activo, 
		a.estado, 
		u.nombres, a.datos_adicionales, d.nombredepartamento, padre.nombre_activo as nombre_activo_padre 
		from activo a left join activo padre on a.idactivo_padre = padre.idactivo, 
		usuario u, departamento d
		where a.idusuario_responsable = u.idusuario and
		u.iddepartamento = d.iddepartamento;
		*/

		//obteniendo los datos
		$tabladatos = $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
		$nroregistros=$tabladatos->get_valor_campo(0,"contador");

		return $nroregistros;
	}

    //-----------------------------------------------------------
	/**
	* obtiene los datos para ventana browse o excel
	*
	* @example "sample_xxx.php"
	*/

	public function get_datos_browse_excel($filtro_a_buscar,$nropagina,$orderby,$sentido,$parametros) //el reporte de excel no esta paginado, por ello $nropagina=0
	{
		$consulta = "";
		$filtro = $this->get_sentence_filtro($filtro_a_buscar);
		
		// #ERROR-E00001# : ERROR PERSONALIZADO
		//$consulta = "select idactivo, ".
		//			"coalesce(nombre_activo,'') as nombre_activo, ".
		//			"coalesce(codigo_activo,'') as codigo_activo ".
		//			"from activo ".
		//			"where ".
		//			"activo.idactivo>1 and ". 
		//			"activo.eliminar=false ";

		$consulta = "select * ".
					"from ".$this->m_nombrevista." ";

		if ($filtro != ""){
			$consulta .= " where ".$filtro;
		}

		$consulta .= " order by ".$orderby." ".$sentido;

		if ($nropagina > 0)
		{
			$consulta .= " LIMIT #LIMIT# OFFSET #OFFSET#";

			$offset = $this->m_nrofilas_by_pagina * ($nropagina - 1);

			//reemplazando el valor del item
			$this->parametros->add("limit")->value = $this->m_nrofilas_by_pagina;
			$this->parametros->add("offset")->value = $offset;
		}

		return $this->obtener_tabla_from_sql($consulta); //retorna un objeto tsdk_datos_sql
	}

}


