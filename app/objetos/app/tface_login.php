<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../objetos/app/trs_usuario.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_interfaz.php");

include_once(APPPATH."../../objetos/varios/funciones.php");				//definiendo funciones globales
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");	//definiendo constantes

class tface_login extends tsdk_interfaz
{
    // **********************************
	// variables miembro
    // **********************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	//protected  $m_basedatos=null;

	/**
	* Determina quien es el manejador de base de datos ('mysql' o 'postgresql')
	* @var string
	*/
	//protected  $m_manejadorbd=null;

	/**#@-*/

    // **********************************
    // metodos - privados y protegidos
    // **********************************

    //-----------------------------------------------------------
	/**
	* obtiene un objeto para insertar o modificar
	*
	* @example "sample_xxx.php"
	*/

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($controlador,$basedatos)
	{
		parent::__construct($controlador,$basedatos);
	}

    //-----------------------------------------------------------
	/**
	* Procesa un login de usuario
	*
	* @example "sample_xxx.php"
	*/

	public function process_login()
    {
        //validando la existencia de variables
		$mensaje="";
		$control="";
        $idventana="login_";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array($idventana."txtusuario", $idventana."txtpassword") ) == false)
		{
            error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

            //redirigiendo a otro controlador
            $parametros = new tsdk_parametros_controlador();
            $parametros->mensaje = $this->m_mensaje;

            $this->m_controlador_base->redirect("/clogin",$parametros);

            return;
		}

		//validando datos obligatorios y que sean del tipo correcto (buscar: bloque2)
		if ( $this->validar_controles_obligatorios(
                				array(
	                					array($idventana."txtusuario","Usuario",tsdk_tipodato::tstring_no_vacio,""),
	                					array($idventana."txtpassword","Contraseña",tsdk_tipodato::tstring_no_vacio,"")
                					),
                				$control //nombre del control donde mostrar el error
				            ) == false)
		{
            error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

            //redirigiendo a otro controlador
            $parametros = new tsdk_parametros_controlador();
            $parametros->mensaje = $this->m_mensaje;

            $this->m_controlador_base->redirect("/clogin",$parametros);

            return;
		}

		//recuperando variables
		$usuario = trim($this->m_controlador_base->input->post($idventana.'txtusuario'));
		$password = trim($this->m_controlador_base->input->post($idventana.'txtpassword'));

		//validando el valor de las variables
		$vusuario = new trs_usuario($this->m_basedatos );
		if ($vusuario->validarusuario($usuario,$password)==false)
		{
			$_SESSION['tsdk_is_logged']=false;

			//estableciendo parametros para controlador
			$parametros = new tsdk_parametros_controlador();
			$parametros->mensaje = $vusuario->mensaje;

            $this->m_controlador_base->redirect("/clogin",$parametros);
			return;
		}

		//guardando variable login en session
		$_SESSION['tsdk_is_logged']=true;

		//obteniendo datos del usuario
		$vusuario->campos->add("usuario")->value = $usuario;
		if ($vusuario->obtener_from_usuariologin()<0)
		{
			$_SESSION['tsdk_is_logged']=false;

	        //redirigiendo a otro controlador
	        $parametros = new tsdk_parametros_controlador();
			$parametros->mensaje = $vusuario->mensaje;

	        $this->m_controlador_base->redirect("/clogin",$parametros);

			return;
		}

		$_SESSION['idusuario']=$vusuario->campos->item("idusuario")->value;
		$_SESSION['nombres']=$vusuario->campos->item("nombres")->value;
		$_SESSION['email']=$vusuario->campos->item("email")->value;
		$_SESSION['tipo_usuario']=$vusuario->campos->item("tipo_usuario")->value;

		//habilitando menus
		//global_enable_menus($this->m_basedatos); //ojo-revisar

        //redirigiendo a otro controlador
        $parametros = new tsdk_parametros_controlador();
		$parametros->idmenu = "menuprincipal"; //permite seleccionar un menu
		$parametros->iditem_parent = "administracion";
		$parametros->iditem = "admoportunidades";
		$parametros->mensaje = "";

        $this->m_controlador_base->redirect("/cdepartamentos",$parametros);

	}

}



?>
