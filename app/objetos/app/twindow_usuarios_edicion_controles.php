<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../objetos/app/trs_usuario.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tipocontrol.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_list_controls.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_combobox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_lista_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_checkbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textarea.php");

//---------------------------------------------------------------------------------------
//Este objeto implementa la creacion de la vista para la edicion y creacion de registros
//---------------------------------------------------------------------------------------
class twindow_usuarios_edicion_controles extends tsdk_list_controls
{
    // **********************************
	// variables miembro
    // **********************************
	public	 	$m_recordset;
	protected	$m_basedatos;

    // **********************************
    // metodos - privados y protegidos
    // **********************************

	// **********************************
    // metodos - publicos
	// **********************************

	//-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($basedatos,$idventana)
    {
		parent::__construct($idventana);

		//inicializando variables
		$this->m_basedatos=$basedatos;
		$this->m_manejadorbd = MANEJADORBD;
		$this->m_recordset = new trs_usuario( $this->m_basedatos );
		$tabindex=1;

		//-----
		$control = new tsdk_textbox();
		$control->config_as_text_disabled("0",$tabindex++);
		$this->add("idusuario",$control);

		//-----
		$control = new tsdk_textbox();
		$control->config_as_text("",$tabindex++);
		$this->add("usuario",$control);

		//-----
		$control = new tsdk_textbox();
		$control->config_as_password("",$tabindex++);
		$this->add("contrasena",$control);

		//-----
		$control = new tsdk_textbox();
		$control->config_as_text("",$tabindex++);
		$this->add("nombres",$control);

		//-----
		$control = new tsdk_combobox();
		$control->config_as_combofijo($tabindex++);
		$control->add_item( new tsdk_lista_item("A","Activo") );
		$control->add_item( new tsdk_lista_item("D","Deshabilitado") );
		$this->add("estado",$control);

		//-----
		$control = new tsdk_combobox();
		$control->config_as_combofijo($tabindex++);
		$control->add_item( new tsdk_lista_item("U","Usuario") );
		$control->add_item( new tsdk_lista_item("A","Administrador") );
		$this->add("tipo_usuario",$control);

		//-----
		$control = new tsdk_textbox();
		$control->config_as_text("",$tabindex++);
		$this->add("email",$control);

		//----- 
		//$control = new tsdk_checkbox();
		//$control->propiedades->set_propiedades(array("tabindex"=>$tabindex++,
		//								   "checked"=>null));
		//$this->add("hab_usuario",$control);

		//----- la salida debe ser id,texto
		$texto_sql  = "select iddepartamento as id, nombredepartamento as texto ";
		$texto_sql .= "from departamento where departamento.eliminar=false ";

		$control = new tsdk_combobox();
		$control->config_as_combosql($tabindex++,$texto_sql);
		$this->add("iddepartamento",$control);

/*
		//-----
		$texto_sql  = "select iddepartamento as id, nombre as texto ";
		$texto_sql .= "from departamento where eliminar=false ";

		$control = new tsdk_combobox();
		$control->propiedades->set_propiedades(array( "tabindex"=>$tabindex++,
										 "required" => NULL ));
		$control->set_estilos(array("width"=>"80%"));
		$control->sql=$texto_sql;
		$this->add("iddepartamento",$control);

		//-----
		$texto_sql  = "select idcargo as id, nombrecargo as texto ";
		$texto_sql .= "from cargo where eliminar=false ";

		$control = new tsdk_combobox();
		$control->propiedades->set_propiedades(array( "tabindex"=>$tabindex++,
										 "required" => NULL ));
		$control->set_estilos(array("width"=>"80%"));
		$control->sql=$texto_sql;
		$this->add("idcargo",$control);*/

        /*
        //EJEMPLO: TEXTBOX COMO CONTRASEÑA
		//----- contraseña
		$control = new tsdk_textbox();
		$control->propiedades->set_propiedades(array("type"=>"password",
										   "value"=>"",
										   "size"=>"40",
										   "tabindex"=>$tabindex++,
										   "maxlength"=>"128"));
		$control->set_estilos(array("width"=>"80%"));
		$this->add("contrasena",$control); */

        /*
        //EJEMPLO: CHECKBOX
		//----- habilitado
		$control = new tsdk_textbox();
		$control->propiedades->set_propiedades(array("type"=>"checkbox",
										   "tabindex"=>$tabindex++,
										   "checked"=>null));
		$this->add("habilitado",$control);*/

        /*
        //EJEMPLO: COMBO BOX CON DATOS FIJOS
		//----- tipo de usuario
		$control = new tsdk_combobox();
		$control->propiedades->set_propiedades(array( "tabindex"=>$tabindex++,
										 "required" => NULL ));
		$control->set_estilos(array("width"=>"80%"));
		$control->add_item( new tsdk_lista_item("U","Usuario") );
		$control->add_item( new tsdk_lista_item("A","Administrador") );

		$this->add("tipo_usuario",$control);

        //EJEMPLO: COMBO BOX CON CARGA DE DATOS DE TABLA. LA CONSULTA TIENE COMO TITULO: ID, TEXTO
		//----- idsalesstage
		$texto_sql  = "select idsalesstage as id, descripcion as texto from sales_stage ";
		$texto_sql .= "where eliminar=false ";

		//----- idsalesstage
		$control = new tsdk_combobox();
		$control->propiedades->set_propiedades(array( "tabindex"=>$tabindex++,
										 "required" => NULL ));
		$control->set_estilos(array("width"=>"80%"));
		$control->sql=$texto_sql;
		$this->add("idsalesstage",$control);
        */

		//------------------------------------------------------------------
		//----- cmdgrabar
		$control = new tsdk_button();
		$control->propiedades->set_propiedades(array( "type"=>"button",
										 "tabindex"=>$tabindex++,
										  "value"=>"Grabar"));
		
		$control->estilos->add("width")->value = "80px";
		$control->eventos->add("onclick",new tsdk_coleccion_item("onclick()"));
		$this->add("cmdgrabar",$control);
		
		//----- cmdclonar
		$control = new tsdk_button();
		$control->propiedades->set_propiedades(array( "type"=>"button",
										 "tabindex"=>$tabindex++,
										 "value"=>"Clonar"));
		$control->estilos->add("width")->value = "80px";
		$control->eventos->add("onclick",new tsdk_coleccion_item("onclick()"));
		$this->add("cmdclonar",$control);

		//----- cmdcerrar
		$control = new tsdk_button();
		$control->propiedades->set_propiedades(array( "type"=>"button",
										 "tabindex"=>$tabindex++,
										 "value"=>"Cerrar"));
		$control->estilos->add("width")->value = "80px";
		$control->eventos->add("onclick",new tsdk_coleccion_item("onclick()"));
		$this->add("cmdcerrar",$control);

		//----- mensaje
		$control = new tsdk_span();
		$control->estilos->add("color",new tsdk_coleccion_item("#FF0000"));
		$control->estilos->add("font-size",new tsdk_coleccion_item("15px"));

		$this->add("mensaje",$control);

	}

    //-----------------------------------------------------------
	/**
	* recupera valores de la base de datos y los pasa a los controles
	*
	* @example "sample_xxx.php"
	*/

	public function load_valores( $listapost )
	{
		//----------------------------------------------------------------------------------
		//recuperando la lista de controles de la ventana como un arreglo de una dimension. 
		//para ejecutar su correspondiente SQL
		$lista_controles = &$this->find_array(true);

		//ejecutando el sql de cada control
		foreach ($lista_controles as $clave => &$control)
		{
			if ($control->sql != "")
			{
				$this->exec_sql_into_control($this->m_basedatos,$control);
			}

		}

		//---------------------------
		if ($listapost['accion_ejecutar']=="insertar")
		{
			//cuando se presenta la ventana insertar, se debe poner por defecto el tipo de usuario "usuario"
			$cmbtipo_usuario = $this->find_item("tipo_usuario");
			$cmbtipo_usuario->select_item("U");
		}
		
		//---------------------------
		if ($listapost['accion_ejecutar']=="modificar")
		{
			if (isset($listapost['idusuario']) == false)
			{
				$this->m_mensaje = "No definido variable idusuario. Archivo:".__FILE__.
									", Linea: ".__LINE__." funcion:".__FUNCTION__;
				return -1;
			}

			$idclave = $listapost['idusuario'];
			//$this->m_recordset->set_item_valor("idusuario",$idclave);
			$this->m_recordset->campos->add("idusuario")->value = $idclave;
			if ($this->m_recordset->obtener() <0)
			{
				//grabando en el log
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".
						  "Error obteniendo datos de registro. ".$this->m_recordset->mensaje );

				$this->m_mensaje = "Error obteniendo datos de registro.";
				return -1;
			}

			if ($this->load_values_from_recordset($lista_controles, $this->m_recordset ) < 0)
			{
				//grabando en el log
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".
						  "Error cargando valores a items desde recordset. ".$this->m_recordset->mensaje );

				return -1;
			}

			//si el password es #no-establecido#, mostrando vacio
			//error_log($this->m_recordset->get_item_valor("contrasena"));
			/*if ( strcasecmp($this->m_recordset->get_item_valor("contrasena"),"#no-establecido#") == 0)
			{
				$this->m_listaitems['contrasena']->value='';
			}*/
		}

		return 0;
	}


}

?>
