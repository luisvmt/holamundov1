<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_interfaz.php");	//definiendo clase interfaz
include_once(APPPATH."../../objetos/varios/funciones.php");				//definiendo funciones globales
include_once(APPPATH."../../objetos/configuracion_app/constantes.php");	//definiendo constantes

class tface_departamentos extends tsdk_interfaz
{
    // **********************************
	// variables miembro
    // **********************************

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	//protected  $m_basedatos=null;

	/**
	* Determina quien es el manejador de base de datos ('mysql' o 'postgresql')
	* @var string
	*/
	//protected  $m_manejadorbd=null;

	/**#@-*/

    // **********************************
    // metodos - privados y protegidos
    // **********************************

    //-----------------------------------------------------------
	/**
	* obtiene un objeto para insertar o modificar
	*
	* @example "sample_xxx.php"
	*/

	protected function obtener_recordset()
	{
		$mensaje = "";
		$control = "";
		$idview  = "departamento";

        //validando la existencia de controles (buscar: bloque1)
		if ( $this->existen_variables_post( array($idview."_iddepartamento",
												$idview."_nombredepartamento"
												)
										   ) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
			return null; //es un error de programacion
		}

		//validando datos obligatorios y que sean del tipo correcto (buscar: bloque2)
		if ( $this->validar_controles_obligatorios(
				array(
					array($idview."_nombredepartamento","Nombre de departamento",tsdk_tipodato::tstring_no_vacio,"")
					),
				$control //control donde mostrar el error
				) == false)
		{
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$this->m_mensaje);
			return json_encode($trama_respuesta); //es un error de uso del aplicativo
		}

		//estableciendo campos de base de datos
		$recordset = new trs_departamento( $this->m_basedatos );

		$recordset->campos->add("iddepartamento")->value   = trim($this->m_controlador_base->input->post($idview."_"."iddepartamento"));
		$recordset->campos->add('nombredepartamento')->value  = trim($this->m_controlador_base->input->post($idview."_"."nombredepartamento"));

		$recordset->campos->add('eliminar')->value = APP_FALSE;

		return $recordset;
	}

	// **********************************
    // metodos - publicos
    // **********************************

    //-----------------------------------------------------------
	/**
	* constructor
	*
	* @example "sample_xxx.php"
	*/

	public function __construct($controlador,$basedatos)
	{
		parent::__construct($controlador,$basedatos);
	}

    //-----------------------------------------------------------
	/**
	* Devuelve el HTML de la ventana de modificacion (modificacion, insercion)
	*
	* @example "sample_xxx.php"
	*/

	public function get_html_browse()
	{
		$respuesta = "";
		$idview="departamento";

		//validando el permiso sobre el objeto
		$permisomodificar = global_has_permiso($this->m_basedatos,$idview,'mantenimiento');
		$permisobrowse = global_has_permiso($this->m_basedatos,$idview,'browse');

		//hallando variables
		$nropagina= $this->m_controlador_base->input->post('nropagina');
		$filtro = $this->m_controlador_base->input->post('filtro');
		$orderby = $this->m_controlador_base->input->post('orderby');
		$sentido = $this->m_controlador_base->input->post('sentido');
		$parametros = $this->m_controlador_base->input->post('parametros');

		//devolviendo tabla browse
		$objetobrowse = new twindow_departamentos_browse($this->m_basedatos);

		if ($permisomodificar==true)
		{
			$objetobrowse->show_insertar=true;
			$objetobrowse->show_eliminar=true;
		}else{
			$objetobrowse->show_insertar=false;
			$objetobrowse->show_eliminar=false;
		}

		//html
		if ($permisobrowse==true)
		{
			$respuesta = "<!-- OK -->\n".$objetobrowse->get_html_browse($nropagina,$filtro,$orderby,$sentido,$parametros);
		}else{
			$respuesta = "<!-- OK -->\nNO TIENE PERMISO PARA VISUALIZAR ITEMS";
		}

		//javascript ... debe ser llamado luego de print_html
		//$browseusuarios->print_javascript($nropagina,$filtro,$orderby,$sentido);

		return $respuesta;
	}

    //-----------------------------------------------------------
	/**
	* Inserta un registro.
	*
	* @example "sample_xxx.php"
	*/
	public function procesar_insertar_desde_form()
	{
		$control="";
		$recordset = null;

		//obteniendo el objeto recordset a modificar
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			$mensaje="Error procesando solicitud. Reportar con administrador.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}
		if ( gettype($recordset)=="string") //existe un error de uso del app
		{
			return $recordset;
		}

		//insertando
		if ( $recordset->insertar() < 0 )
		{
			$mensaje = $recordset->mensaje;

			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('iddepartamento')->value );
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* modificar el registro (cuando se da clic en grabar)
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_modificar_desde_form()
	{
		$control="";

		//obteniendo el objeto recordset a modificar
		$recordset = $this->obtener_recordset();
		if ($recordset==null)
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}
		if ( gettype($recordset)=="string") //existe un error de uso del app
		{
			return $recordset;
		}

		//modificando
		if ( $recordset->modificar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$mensaje);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$recordset->campos->item('iddepartamento')->value );
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* eliminando el registro
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_eliminar_registro()
	{
		$control="";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("iddepartamento") ) == false)
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//estableciendo campos de base de datos
		$recordset = new trs_departamento( $this->m_basedatos );
		$recordset->campos->add("iddepartamento")->value = $this->m_controlador_base->input->post("iddepartamento");

		//eliminando
		if ( $recordset->eliminar() < 0 )
		{
			$mensaje = $recordset->mensaje;
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error eliminando registro. ". $recordset->mensaje.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//finalizando
		$mensaje="Se eliminó el registro correctamente";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje);
		return json_encode($trama_respuesta);
	}

    //-----------------------------------------------------------
	/**
	* clonando el registro
	*
	* @example "sample_xxx.php"
	*/

	public function procesar_clonar_registro()
	{
		$control="";

		//validando la existencia de controles
		if ( $this->existen_variables_post( array("id")) == false) //se coloca la variable id desde javascript
		{
			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//decodificando la variable id
		$lista_id = json_decode($this->m_controlador_base->input->post("id"),true); //id es un texto json enviado desde el cliente
		if (is_null($lista_id))
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en json_decode.".
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$mensaje="Error procesando solicitud. Reportar con administrdor.";
			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$mensaje);
			return json_encode($trama_respuesta);
		}

		//estableciendo campos de base de datos
		$recordset = new trs_departamento( $this->m_basedatos );
		$recordset->campos->add('iddepartamento')->value = $lista_id["iddepartamento"];

		//clonando
		if ( $recordset->clonar() < 0 )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,
					"#x Error en funcion clonar. ". $recordset->mensaje.
					", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			$trama_respuesta = array("response"=>"e","control"=>$control,"mensaje"=>$recordset->mensaje);
			return json_encode($trama_respuesta);
		}

		//generando el json de respuesta
		$datos = json_encode( array("iddepartamento" => strval($recordset->campos->item("iddepartamento")->value)) );

		//finalizando
		$mensaje="Se clonó el registro correctamente. Debe grabar si acepta el nuevo registro.";
		$trama_respuesta = array("response"=>"0","control"=>$control,"mensaje"=>$mensaje,
								 "id"=>$datos );
		return json_encode($trama_respuesta);
	}

}
