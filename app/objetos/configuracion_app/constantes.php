<?php   if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
            exit(__FILE__.': No direct script access allowed');

    define('NROITEMS_BROWSE', 10);

    //definicion para linux-centos 7
    //define('TRACELOG', '/var/apps/holamundov1/log/backtrace.log');

    //definicion para OSX
    define('TRACELOG', '/Users/luisvmt/Developer/proyectos/apps.web/holamundov1/log/backtrace.log');

    //define('FILE_ERROR', '/var/apps/casosnegocio/log/error_runtime.log');
    define('TAMANO_MAXIMO_FILE', 100000); //100kb - VALOR EN BYTES QUE SE PUEDE SUBIR

    //definicion para linux-centos7
    //define('DIRECTORIO_TEMPORAL','/var/apps/holamundov1/log');

    //definicion para OSX
    define('DIRECTORIO_TEMPORAL','/Users/luisvmt/Developer/proyectos/apps.web/holamundov1/log');

    //definicion donde se encuentran los archivos nuevos y definitivos
    //define('DIRECTORIO_ARCHIVOS_NUEVOS','/Users/luisvmt/Developer/proyectos/apps.web/holamundov1/app/archivos/archivos_nuevos/');
    //define('DIRECTORIO_ARCHIVOS_DEFINITIVOS','/Users/luisvmt/Developer/proyectos/apps.web/holamundov1/app/archivos/archivos_definitivos/');

    //----------------------------------------
    //variables para el funcionamiento de la aplicacion
    //----------------------------------------
    define('APP_VERSION', 'V1');
    define('APP_NOMBRE_EMPRESA', 'CCUBO');
    define('APP_TRUE', 1);
    define('APP_FALSE', 0);
    define('APP_NOMBRE', 'Hola mundo v1');
    define('APP_DEBUG_CONTROLADOR', "tsdk_gestor_errores");  //los posibles valores son:
                                            // tsdk_gestor_errores      -> gestor con trace de errores
                                            // tsdk_gestor_errores_lite -> gestor sin trace de errores
                                            // web                      -> los errores se mostraran en la ventana

    //----------------------------------------
    //variables para envio de correo
    //----------------------------------------
    define('SMTP_SERVER', 'smtp.zoho.com');
    define('SMTP_USER', 'soportetkn@tnkka.xyz');
    define('SMTP_PASSWORD', 'halion2000.');
    define('SMTP_SECURE', 'tls');
    define('SMTP_PORT', 587);
    define('SMTP_FROM', "soportetkn@tnkka.xyz");
    define('SMTP_FROMNAME', "Soporte");

    //----------------------------------------
    //conexion a base de datos (mysql)
    //----------------------------------------
    define('USUARIOBD', 'admholamundov1');
    define('CONTRASENABD', 'month111');
    define('BASEDATOS', 'holamundov1');
    define('SERVIDORBD', '192.168.236.2');
    define('PUERTOBD', '3306');
    define('ESQUEMABD', '');
    define("MANEJADORBD","mysql");

    //----------------------------------------
    //conexion a base de datos (postgresql)
    //----------------------------------------
    //define("USUARIOBD","holamundov1");
    //define("CONTRASENABD","luchito");
    //define("BASEDATOS","appsweb");
    ////define("SERVIDORBD","localhost");
    //define("SERVIDORBD","192.168.236.2");
    //define("PUERTOBD","5432");
    //define("ESQUEMABD","holamundov1");
    //define("MANEJADORBD","postgresql");

    //------------------------------------------------
    //constantes para la visualizacion de controles
    //------------------------------------------------

    define('CONTROL_SIZE',"40");
    define('CONTROL_WIDTH_PERCENTAJE',"100%");
    define('CONTROL_MAX_LENGTH',128);
    define('CONTROL_BACKGROUND_DISABLED',"#E6E6E6");

    define('CONTROL_COLOR_MENSAJE',"#FF0000");
    define('CONTROL_TAMANO_LETRA_MENSAJE',"15px");

	define('CONTROL_TEXTAREA_NROFILAS',"5");

    //------------------------------------------------
    //CONFIGURACION DE PERMISOS
    //------------------------------------------------

	//---------------------------------
	//habilitando los menus de opciones
	$global_menu_opciones = array(
							//tipo de usuario: E (empleado)
							"E" => array(	"micuenta",
												"cambiarpassword",
											"administracion",
												"admcuentas",
												"admoportunidades",
											"mantenimiento_tablas",
												"mantusuarios",
												"mantdepartamentos",
												"mantcargos",
											"salir"
										),

							//tipo de usuario: O (otros)
							"O" => array(	"menuprincipal_salir"
										)
							);

	$GLOBALS['global_menu_opciones'] = $global_menu_opciones;

	//---------------------------------
	//habilitando los permisos para los usuarios
	$global_permisos = array(
							//tipo de usuario: E (empleado)
							"E" => array(	"usuario_browse"=>1,
											"usuario_mantenimiento"=>0, //mantenimiento: insertar, modificar, clonar, eliminar

											"departamento_browse"=>1,
											"departamento_mantenimiento"=>0,

											"cargo_browse"=>1,
											"cargo_mantenimiento"=>0,

											"cuenta_browse"=>1,
											"cuenta_mantenimiento"=>1,

											"contacto_browse"=>1,
											"contacto_mantenimiento"=>1,

											"oportunidad_browse"=>1,
											"oportunidad_mantenimiento"=>1,

											"otros"=>0
										),

							//tipo de usuario: O (otros)
							"O" => array()
							);

	$GLOBALS['global_permisos'] = $global_permisos;

?>
