
<?php

define("TSDK_FILE_MAIN", "Hola mundo.");
define("APPPATH", "/Users/luisvmt/proyectos/apps.web/holamundov1/app/codeigniter/application/");
//define('TRACELOG', '/Users/luisvmt/proyectos/apps.web/holamundov1/log/backtrace.log');

//incluyendo los java.script comun para todas la vistas

include_once(APPPATH."../../objetos/configuracion_app/constantes.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tab.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_tab_item.php");


//definiendo variables del TSDK
tsdk_propiedades::$TRACELOG = TRACELOG;
tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
tsdk_propiedades::$SMTP_USER = SMTP_USER;
tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

define("VISTA_IDVENTANA", "ejemplo_tabs");
?>




<!-- ========================================== -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;" />

<title>TITULO TABS</title>



<!-- ========================================== -->
<script language="javascript" type="text/javascript" src="../javascript/sdk_moments/moment.min.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/funciones.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/global.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/sdk_javascript/tjs_funciones.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/sdk_javascript/tjs_textbox.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/sdk_javascript/tjs_span.js" ></script>
<script language="javascript" type="text/javascript" src="../javascript/sdk_javascript/tjs_tabla.js" ></script>

<!-- ====================== jquery ====================== -->
<script language="javascript" type="text/javascript" src="../javascript/jquery/jquery-3.2.0.min.js"></script>
<script language="javascript" type="text/javascript" src="../javascript/jquery/jquery-ui-1.12.1/jquery-ui.js"></script>
<link   rel="stylesheet"      href="../javascript/jquery/jquery-ui-themes-1.12.1/themes/base/jquery-ui.modificado.css" />

<!-- ======== Ejecutando javascrip de JQuery ================================== -->

<script language="javascript" type="text/javascript">

//-----------------------------------
//funcion para crear los tabs

$( function() {
    $( ".tsdk_tab" ).tabs();
  } );


</script>


</head>
<body>

<?php

echo "-------------------------------------</br>\n";
echo "Ejemplo.</br>\n";
echo "Creando un tab sencillo</br>\n";
echo "-------------------------------------</br>\n";

$tabindex=0;

//creando un textbox
$txtcorreo = new tsdk_textbox();
$txtcorreo->config_as_text("",++$tabindex);
$txtcorreo->propiedades->add("autocomplete")->value="off";
$txtcorreo->set_id_ventana_control(VISTA_IDVENTANA,"txtcorreo");
$txtcorreo->eventos->add_evento_standard("onkeypress","event,this");

//creando un boton
$cmdaceptar = new tsdk_button();
$cmdaceptar->config_as_button_standard("Aceptar",++$tabindex);
$cmdaceptar->estilos->add("width",new tsdk_coleccion_item("150px"));
$cmdaceptar->set_id_ventana_control(VISTA_IDVENTANA,"cmdaceptar");
$cmdaceptar->eventos->add_evento_standard("onclick");

//creando un mensaje span
$mensaje = new tsdk_span();
$mensaje->config_as_mensaje();
$mensaje->set_id_ventana_control(VISTA_IDVENTANA,"mensaje");
$mensaje->texto = "Esta es una prueba";

//imprimiendo tab
$control = new tsdk_tab(VISTA_IDVENTANA,"ejemplo");

$item = $control->tabs->add("contactos_01",new tsdk_tab_item("titulo contactos 01","Este es el contenido de contactos 01"));

$item = $control->tabs->add("contactos_02", new tsdk_tab_item("titulo contactos 02"));
$item->contenido = "Este es el contenido del tab contactos 02";

$control->tabs->add("contactos_03",new tsdk_tab_item());
$control->tabs->item("contactos_03")->titulo = "titulo contactos 03";
$control->tabs->item("contactos_03")->contenido = "Este es el contenido del tab contactos 03";

$control->tabs->add("contactos_04",new tsdk_tab_item());
$control->tabs("contactos_04")->titulo = "titulo contactos 04";
$control->tabs("contactos_04")->contenido = "Este es el contenido del tab contactos 04";

$item = $control->tabs->add("contactos_04"); //por verificar
$item->titulo = "titulo contactos 04";
$item->contenido = "Este es el contenido del tab contactos 04";

$item = $control->tabs->add("contactos_05"); //por verificar
$item->titulo = "titulo contactos 05";
$item->contenido = "Este es el contenido del tab contactos 05";

//$control->tabs->add("contactos_03")->value = new tsdk_tab_item(); //no funciona, por defecto crea un tsdk_coleccion_item
//$control->tabs->item("contactos_03")->titulo = "titulo contactos 03";
//$control->tabs->item("contactos_03")->contenido = "Este es el contenido del tab contactos 03";

//añadiendo el contenido

/*$div_right = new tsdk_div(VISTA_IDVENTANA_2,"diseno_contenido_rightxx");
$div_right->propiedades->add("class")->value = "diseno_contenido_rightxx";
$div_right->estilos->add("clear")->value = "both";
$div_right->estilos->add("width")->value = "96%";
$div_right->estilos->add("margin-top")->value = "10px";*/

//añadiendo contenido browse

//$tab->contenidos->add(0)->value = "prueba de fuego"; //$div_right->get_html();
//$tab->contenidos->add(1)->value = "otro"; //$div_right->get_html();

//$tab->print_html();

//echo "Probando el control tab:";

//imprimiendo la tabla
echo "\n".$control->get_html();



//echo "<br>";
tsdk_error_log_trace("tab",$control);

echo "\n<br>Fin de ejemplo";

?>

</body>
