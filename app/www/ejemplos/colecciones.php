<?php

define("TSDK_FILE_MAIN", "Hola mundo.");
define("APPPATH", "/Users/luisvmt/proyectos/apps.web/holamundov1/app/codeigniter/application/");
//define('TRACELOG', '/Users/luisvmt/proyectos/apps.web/holamundov1/log/backtrace.log');

include_once(APPPATH."../../objetos/configuracion_app/constantes.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//definiendo variables del TSDK
tsdk_propiedades::$TRACELOG = TRACELOG;
tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
tsdk_propiedades::$SMTP_USER = SMTP_USER;
tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

//----------------------------------------------
//Ejemplo 01
//----------------------------------------------
//Creando una coleccion de texto, se añade el item al final
//No se crea un indice

echo "----------</br>";
echo "Ejemplo 01</br>";
echo "----------</br>";


$lista = new tsdk_coleccion();

$lista->add(null,"nodo 00");
$lista->add(null,"nodo 01");
$lista->add(null,"nodo 02");
$lista->add(null,"nodo 03");

//recorriendo e imprimiendo los items
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 01",$lista);

//----------------------------------------------
//Ejemplo 02
//----------------------------------------------
//Creando una coleccion de tsdk_coleccion_item, se añade el item al final
//No se crea un indice

echo "----------</br>";
echo "Ejemplo 02</br>";
echo "----------</br>";

$lista = new tsdk_coleccion();

$lista->add(null,new tsdk_coleccion_item("nodo 00")); //ok
$lista->add(null,new tsdk_coleccion_item("nodo 01")); //ok
$lista->add(null,new tsdk_coleccion_item("nodo 02")); //ok
$lista->add(null)->value = "nodo 03"; //ok, por defecto se crea un item tsdk_coleccion_item
$lista->add(null)->value = new tsdk_coleccion_item("nodo 04"); //forma incorrecta, dentro de tsdk_coleccion_item crea otro item del tipo tsdk_coleccion_item

$item = $lista->add(null,new tsdk_coleccion_item()); //ok, utilizando una referencia al item creado
$item->value="nodo 05";

//imprimiendo los elementos
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 02",$lista);


//----------------------------------------------
//Ejemplo 03
//----------------------------------------------
//Modificando un item ... utilizando una referencia

echo "----------</br>";
echo "Ejemplo 03</br>";
echo "----------</br>";

//los items empiezan por 0 cero, modificando por indice
$lista(1)->value="nodo 01 modificado";

//imprimiendo los elementos
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 03",$lista);

//----------------------------------------------
//Ejemplo 04
//----------------------------------------------
//eliminando un item

echo "---------------------------------------------------------------------</br>";
echo "Ejemplo 04 - borrando un indice numerico, los indices no se renumeran</br>";
echo "---------------------------------------------------------------------</br>";

$lista->delete(3); //ok
$lista->delete("tres"); //no funciona, porque el indice no es texto

//imprimiendo los elementos
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 04",$lista);

//----------------------------------------------
//Ejemplo 05
//----------------------------------------------
//Creando una coleccion con indices texto

echo "----------</br>";
echo "Ejemplo 05</br>";
echo "----------</br>";

$lista = new tsdk_coleccion();
$lista->add("cero",new tsdk_coleccion_item("nodo cero"));
$lista->add("uno",new tsdk_coleccion_item("nodo uno"));
$lista->add("dos",new tsdk_coleccion_item("nodo dos"));
$lista->add("tres","nodo tres"); //creando un nuevo elemento
$lista->add("cuatro")->value = "nodo cuatro"; //creando un nuevo elemento
$lista->add("cinco")->value = "nodo cinco"; //creando un nuevo elemento
$lista->add("seis")->value = "nodo seis"; //creando un nuevo elemento
$lista->add("siete")->value = "nodo siete"; //creando un nuevo elemento

//imprimiendo los elementos
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 05",$lista);

//----------------------------------------------
//Ejemplo 06
//----------------------------------------------
//eliminando un item

echo "-------------------------------------</br>";
echo "Ejemplo 06 - borrando un indice texto</br>";
echo "-------------------------------------</br>";

$lista->delete(0); //no funciona porque los indices son texto, no numericos
$lista->delete("tres");

//imprimiendo los elementos
foreach ($lista as $clave=>$item)
{
	echo "clave:".$clave." ==> item:".$item;
	echo "<br>";
}
echo "<br>";
tsdk_error_log_trace("Ejemplo 06",$lista);


//----------------------------------------------
//Ejemplo 07
//----------------------------------------------
//contando elementos

echo "-------------------------------------</br>";
echo "Ejemplo 07 - contando</br>";
echo "-------------------------------------</br>";

echo "La lista tiene: ".$lista->count()." elementos<br>";


//----------------------------------------------
//Ejemplo 08
//----------------------------------------------
//verifica la existencia de un elemento

echo "-------------------------------------</br>";
echo "Ejemplo 08 - verificando</br>";
echo "-------------------------------------</br>";

if ($lista->key_exists("tres"))
{
	echo "La lista si tiene indice 'tres'<br>";
}else{
	echo "La lista no tiene indice 'tres'<br>";
}

if ($lista->key_exists("cinco"))
{
	echo "La lista si tiene indice 'cinco'<br>";
}else{
	echo "La lista no tiene indice 'cinco'<br>";
}




echo "<br>fin de script";
?>
