<?php

define("TSDK_FILE_MAIN", "Hola mundo."); 
define("APPPATH", "/Users/luisvmt/proyectos/apps.web/holamundov1/app/codeigniter/application/");
//define('TRACELOG', '/Users/luisvmt/proyectos/apps.web/holamundov1/log/backtrace.log');

include_once(APPPATH."../../objetos/configuracion_app/constantes.php");

include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_coleccion_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_propiedades_html.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_textbox.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_button.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_span.php");
include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_tabla.php");


//definiendo variables del TSDK
tsdk_propiedades::$TRACELOG = TRACELOG;
tsdk_propiedades::$SMTP_SERVER = SMTP_SERVER;
tsdk_propiedades::$SMTP_USER = SMTP_USER;
tsdk_propiedades::$SMTP_PASSWORD = SMTP_PASSWORD;
tsdk_propiedades::$SMTP_SECURE = SMTP_SECURE;
tsdk_propiedades::$SMTP_PORT = SMTP_PORT;
tsdk_propiedades::$SMTP_FROM = SMTP_FROM;
tsdk_propiedades::$SMTP_FROMNAME = SMTP_FROMNAME;

define("VISTA_IDVENTANA", "ejemplo_tablas");

echo "-------------------------------------</br>";
echo "Ejemplo.</br>";
echo "Creando una tabla sencilla</br>";
echo "-------------------------------------</br>";

$tabindex=0;

//creando un textbox
$txtcorreo = new tsdk_textbox();
$txtcorreo->config_as_text("",++$tabindex);
$txtcorreo->propiedades->add("autocomplete")->value="off";
$txtcorreo->set_id_ventana_control(VISTA_IDVENTANA,"txtcorreo");
$txtcorreo->eventos->add_evento_standard("onkeypress","event,this");

//creando un boton
$cmdaceptar = new tsdk_button();
$cmdaceptar->config_as_button_standard("Aceptar",++$tabindex);
$cmdaceptar->estilos->add("width",new tsdk_coleccion_item("150px"));
$cmdaceptar->set_id_ventana_control(VISTA_IDVENTANA,"cmdaceptar");
$cmdaceptar->eventos->add_evento_standard("onclick");

//creando un mensaje span
$mensaje = new tsdk_span();
$mensaje->config_as_mensaje();
$mensaje->set_id_ventana_control(VISTA_IDVENTANA,"mensaje");
$mensaje->texto = "Esta es una prueba";

//creando una tabla
$tabla = new tsdk_tabla("idventana","tabla",
						5,  //filas
						3,  //columnas
						  array("width"=>"600",
								"border"=>"1")
						); //filas,columnas

$tabla->set_valores_fila(0,array("Ingrese su correo:",
								$txtcorreo->get_html(),
								$cmdaceptar->get_html()
								 ));

$tabla->set_valores_fila(1,array("",$mensaje->get_html()));

//imprimiendo la tabla
echo $tabla->get_html();


echo "-------------------------------------</br>";
echo "Ejemplo.</br>";
echo "Modificando una fila en particulas</br>";
echo "-------------------------------------</br>";

//primera forma de modificar
$tabla->filas->item(2)->celdas->item(0)->value = "Celda 2,0 modificada"; //fila 2, columna 0

//segunda forma de modificar
$tabla->filas(2)->celdas(1)->value = "Celda 2,1 modificada"; //fila 2, columna 0

//tercera forma de modificar
$tabla->set_valor_celda(3,0,"Celda 3,0 modificada");

//imprimiendo la tabla
echo $tabla->get_html();

//echo "<br>";
//tsdk_error_log_trace("Ejemplo 01",$lista);


?>
