
//---------------------------------------------------------------
//Eliminacion multiple de elementos
function usuario_eliminar_items()
{
	//buscando los items seleccionados y ejecutando
	var idventana="usuario";
	//var urlbrowse="cusuarios/ajax_get_html_browse";
	var urleliminar="cusuarios/ajax_procesar_eliminar_registro";

	for (i=1; i<100; i++)
	{
		var control=document.getElementById(idventana+'_browse_chkfila'+i);
		if (control == null) {
			break;
		}

		if (control.checked == true)
		{
			//obteniendo id
			var datos = jQuery.parseJSON(control.value);
			var parametros = {
					'accion_ejecutar':'eliminar',
					'idusuario':datos.idusuario
				};

			if (tjs_funcion_browse_eliminar_ajax(idventana,
													parametros,
													urleliminar, //funcion que se encarga de eliminar
													"usuario_actualizar_items();" //funcion que se ejecutara luego de eliminar los items
													)<0)
			{
				return;
			}

		}
		//control.checked=document.getElementById(idtabla+'_chktitulo').checked;
	}

}

//---------------------------------------------------------------
//Actualiza la lista de items, luego de eliminarlos
function usuario_actualizar_items()
{
//buscando los items seleccionados y ejecutando
var idventana="usuario";
var urlbrowse="cusuarios/ajax_get_html_browse";
//var urleliminar="cusuarios/ajax_procesar_eliminar_registro";

	//mostrando reloj
	tjs_funcion_show_contenido(idventana,"contenido_waiting");

	//durmiendo un tiempo para actualizar la eliminacion
    //var parametros = { "tiempo_segundos" : "2" };
	//tjs_ajax_exec_sincrono("/cfunciones/ajax_delay",parametros,"");

	//actualizando browse
	var nropagina = $.trim( $("#" + idventana + "_browse_nropagina").val() );
	var filtro  = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idventana + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idventana + "_sentido").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
							urlbrowse, //url de browse
							nropagina, //nropagina
							filtro, //filtro
							orderby, //orderby
							sentido, //sentido
							parametros, //parametros
							"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse

}

//---------------------------------------------------------------
//Esta funcion se encarga de ejecutar la accion multiple
function evento_usuario_browse_link_ejecutar_varios_onclick()
{
	//buscando los items seleccionados y ejecutando
	var idventana="usuario";
	var urlbrowse="cusuarios/ajax_get_html_browse";
	var urleliminar="cusuarios/ajax_procesar_eliminar_registro";

	var combo = document.getElementById(idventana+'_browse_combo_acciones');
	var valor = combo.options[combo.selectedIndex].value;

	//validando la accion a realizar
	switch (valor)
	{
		case 'ninguna':  //ninguna accion
			//mostrando messagebox
			(new tjs_messagebox(idventana)).show("Observación",
														"Seleccione una acción a realizar",
														function () {					//funcion a ejecutar al presionar primer boton
															$("#"+idventana+"_browse_combo_acciones").focus();
														}
														);
			return;

		case 'eliminar':  //eliminar items

			(new tjs_messagebox(idventana)).show("Confirmar",
														"Esta seguro eliminar los items seleccionados?",
														function(){ //primer_boton, eliminando los items seleccionados
															nombre_funcion = "usuario_eliminar_items();";
															eval(nombre_funcion);
														},
														"", //funcion segundo boton
														"", //funcion tercer boton
														false, //boton_aceptar
														false, //boton_aceptar_cancelar
														true //botones SI_NO
														);
			
			break;

		default:
			return;
	}
/*
	//mostrando reloj
	tjs_funcion_show_contenido(idventana,"contenido_waiting");

	//durmiendo un tiempo para actualizar la eliminacion
    var parametros = { "tiempo_segundos" : "2" };
	tjs_ajax_exec_sincrono("/cfunciones/ajax_delay",parametros,"");

	//actualizando browse
	var nropagina = $.trim( $("#" + idventana + "_browse_nropagina").val() );
	var filtro  = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idventana + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idventana + "_sentido").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );

	//load_pagina(nropagina,filtro,orderby,sentido);

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
							urlbrowse, //url de browse
							nropagina, //nropagina
							filtro, //filtro
							orderby, //orderby
							sentido, //sentido
							parametros, //parametros
							"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse
*/
}

//---------------------------------------------------------------
//Esta funcion se encarga de seleccionar los checkboxs
function evento_usuario_browse_chktitulo_onclick()
{
	var idventana="usuario";
	tjs_funcion_browse_chktitulo(idventana);
}

//--------------------------------------------------------
//Esta funcion actualiza la tabla con el filtro ingresado
function evento_usuario_browse_filtro_onkeypress(e)
{
	var idventana="usuario";
	tjs_funcion_browse_filtro(idventana, //idventana
							e, //evento
							"cusuarios/ajax_get_html_browse" //url browse
							);

}

//--------------------------------------------------------
//funcion que carga pagina insertar, mediante ajax
function evento_usuario_browse_link_insertar_onclick()
{
	var idventana="usuario";

	var parametros_post = {
			'accion_ejecutar':'insertar'
		};

	tjs_funcion_browse_insertar_ajax(idventana,
							parametros_post,
							"cusuarios/ajax_get_html_edicion", //url
							"usuario_usuario",  //control que tendra el foco
							"usuario_posterior_insertar_modificar()" //funcion que se ejecuta luego de recibir la ventana
							);

}

//------------------------------------------------------------
//Esta funcion se ejecuta posterior a la carga por ajax de la 
//ventana de modificacion. 
function usuario_posterior_insertar_modificar()
{ 
	//seleccionando los textos
	$("input[type=text]").focus(function(){	   
		this.select();
	  });
	
	$("textarea").focus(function(){	   
		this.select();
	  });
	
}

//--------------------------------------------------------
//funcion que carga pagina insertar, mediante ajax
function evento_usuario_browse_link_reporte_onclick()
{
	var idventana="usuario";
	tjs_funcion_browse_reporte_excel(idventana,'cusuarios/ajax_get_reporte_excel');

}

//--------------------------------------------------------
//funcion que carga pagina insertar, mediante ajax
function evento_usuario_browse_importar_exportar_onclick()
{
	var idventana="usuario";
	tjs_funcion_browse_importar_exportar(idventana,
							"cusuarios/ajax_get_html_importar_exportar", //url
							"" //idfocus
							);
}

//------------------------------------------------------------
//ejecutando funcion para mostrar ventana de modificacion a traves de ajax
function evento_usuario_browse_link_modificar_onclick(clave)
{
	var idventana = "usuario";

	//obteniendo id
	var datos = jQuery.parseJSON(clave);
	var idusuario = datos.idusuario;
	var parametros_post = {
			'accion_ejecutar':'modificar',
			'idusuario':idusuario
		};

	if (tjs_funcion_browse_modificar_ajax(idventana,
										parametros_post, //parametros POST a pasar a url
										"cusuarios/ajax_get_html_edicion", //url_get_edicion
										"usuario_usuario", //idfocus
										"usuario_posterior_insertar_modificar()" //funcion que se ejecuta luego de recibir la ventana
										)<0) {
		return;
	}

}

//------------------------------------------------------------
//eliminando un item
function usuario_eliminar_item(clave)
{
	var idventana = "usuario";

	var datos = jQuery.parseJSON(clave);
	var idusuario = datos.idusuario;
	var parametros = {
			'accion_ejecutar':'eliminar',
			'idusuario':idusuario
		};

	if (tjs_funcion_browse_eliminar_ajax(idventana,
							parametros, //parametros POST a pasar a url
							"cusuarios/ajax_procesar_eliminar_registro", //urleliminar
							"usuario_actualizar_items();" //funcion que se ejecutara luego de eliminar los items
							)<0) {
		return;
	}
/*
	//mostrando reloj
	tjs_funcion_show_contenido(idventana,"contenido_waiting");

	//durmiendo un tiempo para actualizar la eliminacion
    var parametros = { "tiempo_segundos" : "2" };
	tjs_ajax_exec_sincrono("/cfunciones/ajax_delay",parametros,"");

	//actualizando browse
	var nropagina = $.trim( $("#" + idventana + "_browse_nropagina").val() );
	var filtro  = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idventana + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idventana + "_browse_sentido").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
		"cusuarios/ajax_get_html_browse", //url de browse
		nropagina, //nropagina
		filtro, //filtro
		orderby, //orderby
		sentido, //sentido
		parametros, //parametros
		"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse
*/
}

//------------------------------------------------------------
//ejecutando funcion para eliminar item
function evento_usuario_browse_link_eliminar_onclick(clave)
{
	var idventana = "usuario";

	/*if (!confirm("Esta seguro eliminar el item seleccionado?")) {
		return;
	}*/

	(new tjs_messagebox(idventana)).show("Confirmar",
										"Esta seguro eliminar el item seleccionado?",
										function(){ //clic en primer boton
											nombre_funcion = idventana+"_eliminar_item('"+clave+"');";
											eval(nombre_funcion);
										},
										"", //funcion segundo boton
										"", //funcion tercer boton
										false, //boton_aceptar
										false, //boton_aceptar_cancelar
										true //botones SI_NO
										);

}

//----------------------------------------------------------
function evento_usuario_browse_link_actualizar_onclick(nropagina) //se utiliza para navegar entre paginas
{
	//moviendose a la pagina correspondiente
	usuario_browse_gopagina(nropagina);
}

//----------------------------------------------------------
function evento_usuario_browse_link_anterior_onclick(nropagina) //se utiliza para navegar entre paginas
{
	//moviendose a la pagina correspondiente
	usuario_browse_gopagina(nropagina);
}

//----------------------------------------------------------
function evento_usuario_browse_link_siguiente_onclick(nropagina) //se utiliza para navegar entre paginas
{
	//moviendose a la pagina correspondiente
	usuario_browse_gopagina(nropagina);
}

//----------------------------------------------------------
function usuario_browse_gopagina(nropagina) //se utiliza para navegar entre paginas
{
	var idventana = "usuario";

	//hallando el valor del filtro
	var filtro  = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idventana + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idventana + "_browse_sentido").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );
							
	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
		"cusuarios/ajax_get_html_browse", //url de browse
		nropagina, //nropagina
		filtro, //filtro
		orderby, //orderby
		sentido, //sentido
		parametros, //parametros
		"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse

}

//---------------------------------------------------------
function evento_usuario_browse_combo_nropagina_onchange()
{
	var idventana = "usuario";

	var nropagina = document.getElementById(idventana + "_browse_combo_nropagina").value;
	var filtro  = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idventana + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idventana + "_browse_sentido").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
		"cusuarios/ajax_get_html_browse", //url de browse
		nropagina, //nropagina
		filtro, //filtro
		orderby, //orderby
		sentido, //sentido
		parametros, //parametros
		"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse

}


//--------------------------------------------------------
//dando click en el titulo
function evento_usuario_browse_link_ordenar_onclick(orderby,sentido)
{
	var idventana = "usuario";

	var nropagina = document.getElementById(idventana + "_browse_combo_nropagina").value;
	var filtro = $.trim( $("#" + idventana + "_browse_filtro").val() );
	var parametros = $.trim( $("#" + idventana + "_browse_parametros").val() );

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idventana, //idventana
		"cusuarios/ajax_get_html_browse", //url de browse
		nropagina, //nropagina
		filtro, //filtro
		orderby, //orderby
		sentido, //sentido
		parametros, //parametros
		"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse

}

//--------------------------------------------------------
//funcion que detecta la seleccion de una fila
function evento_usuario_browse_tabla_fila_ondblclick(nrofila,clave)
{
	evento_usuario_browse_link_modificar_onclick(clave); //llamando al link modificar
	return;
}

//--------------------------------------------------------
//funcion que permite seleccionar textos al dar clic
function funcion_seleccionar_textos()
{
	$("input[type=text]").focus(function(){	   
		this.select();
	  });
	
	$("textarea").focus(function(){	   
		this.select();
	  });
	
}

//--------------------------------------------------------
//funcion que carga la pagina

function evento_usuario_body_onload()
{
	//seleccionando el menu de opciones
	var idventana = "usuario";
	var idmenu  		= $.trim( $("#" + idventana + "_idmenu").val() );
	var iditem_parent   = $.trim( $("#" + idventana + "_iditem_parent").val() );
	var iditem  		= $.trim( $("#" + idventana + "_iditem").val() );

	seleccionar_menu( idmenu,iditem_parent,iditem);

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina("usuario", //idventana
							"cusuarios/ajax_get_html_browse", //url de browse
							1, //nropagina
							"", //filtro
							"nombres", //orderby
							"asc", //sentido
							"", //parametros
							"funcion_seleccionar_textos()" ); //se ejecutara esta funcion posterior a cargar el browse
}

