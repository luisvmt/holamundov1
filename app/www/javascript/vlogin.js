
function evento_login_txtusuario_onkeypress(e,control) {
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
            //submit
            evento_login_cmdaceptar_onclick();
    }
}

function evento_login_txtpassword_onkeypress(e,control) {
    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
            //submit
            evento_login_cmdaceptar_onclick();
    }
}

//esta funcion evalua si se ingreso datos y luego hace submit
function evento_login_cmdaceptar_onclick()
{
    //validando el ingreso de datos
    var usuario = $.trim( $("#login_txtusuario").val() );
    if (usuario == "")
    {
            $('#login_mensaje').text( "Ingrese usuario" );
            $('#login_txtusuario').focus();
            return;
    }

    var password = $.trim( $("#login_txtpassword").val() );
    if (password == "")
    {
            $('#login_mensaje').text( "Ingrese la contraseña" );
            $('#login_txtpassword').focus();
            return;
    }

    //enviando formulario para su atencion
    //var formulario=
    $("#frmlogin").submit();
    //alert("saludos");
}

//esta funcion realiza el cambio de contraseña del ususario

function evento_login_cmdnewpassword_onclick()
{
    $('#frmlogin').attr('action','/cnewpassword');
    //validando el ingreso de datos
    /*var usuario = $.trim( $("#login_txtusuario").val() );
    if (usuario == "")
    {
            $('#login_mensaje').text( "Ingrese usuario" );
            $('#login_txtusuario').focus();
            return;
    }

    var password = $.trim( $("#login_txtpassword").val() );
    if (password == "")
    {
            $('#login_mensaje').text( "Ingrese la contraseña" );
            $('#login_txtpassword').focus();
            return;
    }*/

    //enviando formulario para su atencion
    //var formulario=
    $("#frmlogin").submit();
    //alert("saludos");
}

function evento_login_body_onload(){
    $("#login_txtusuario").focus();

}
