
/* funcion que se emplea para reemplazar caracteres */
function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

/* cambia caracteres html a unicode */
function change_html_to_unicode(origen)
{
    var destino=origen;

    destino=replaceAll(destino,"&Aacute;","\u00C1");
    destino=replaceAll(destino,"&aacute;","\u00E1");
    destino=replaceAll(destino,"&Eacute;","\u00C9");
    destino=replaceAll(destino,"&eacute;","\u00E9");
    destino=replaceAll(destino,"&Iacute;","\u00CD");
    destino=replaceAll(destino,"&iacute;","\u00ED");
    destino=replaceAll(destino,"&Oacute;","\u00D3");
    destino=replaceAll(destino,"&oacute;","\u00F3");
    destino=replaceAll(destino,"&Uacute;","\u00DA");
    destino=replaceAll(destino,"&uacute;","\u00FA");
    destino=replaceAll(destino,"&Uuml;","\u00DC");
    destino=replaceAll(destino,"&uuml;","\u00FC");
    destino=replaceAll(destino,"&Ntilde;","\u00D1");
    destino=replaceAll(destino,"&ntilde;","\u00F1");

    return destino;
}

function es_entero(numero){
    if (isNaN(numero)){
        return false;
    }
    else{
        if (numero % 1 == 0) {
            return true;
        }
        else{
            return false;
        }
    }
}


//------------------------------------------------------------
//Esta funcion suma una cantidad de dias a una fecha dada.
//Si no se coloca una fecha, suma al dia actual
//el formato de fecha es dd-mm-yyyy ó dd/mm/yyyy
//
// Ejemplo de uso
// actualizando la fecha
//	txt_fechafin.set_text( sumar_dias_fecha_ddmmyyyy(dias_totales,txt_fechainicio.get_text()) );
//------------------------------------------------------------
sumar_dias_fecha_ddmmyyyy = function(dias, fecha)
{
    var Fecha = new Date();
    var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fecha = aFecha[2]+'/'+aFecha[1]+'/'+aFecha[0];

    fecha= new Date(fecha);
    fecha.setDate(fecha.getDate()+parseInt(dias));
    var anno=fecha.getFullYear();
    var mes= fecha.getMonth()+1;
    var dia= fecha.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaFinal = dia+sep+mes+sep+anno;
    return (fechaFinal);
}

//------------------------------------------------------------
//Esta funcion suma una cantidad de dias a una fecha dada.
//Si no se coloca una fecha, suma al dia actual
//el formato de fecha es yyyy-mm-dd ó yyyy/mm/dd
//
// Ejemplo de uso
// actualizando la fecha
//	txt_fechafin.set_text( sumar_dias_fecha_yyyymmdd(dias_totales,txt_fechainicio.get_text()) );
//------------------------------------------------------------
sumar_dias_fecha_yyyymmdd = function(dias, fecha)
{
    var Fecha = new Date();
    var sFecha = fecha || (Fecha.getDate() + "/" + (Fecha.getMonth() +1) + "/" + Fecha.getFullYear());
    var sep = sFecha.indexOf('/') != -1 ? '/' : '-';
    var aFecha = sFecha.split(sep);
    var fecha = aFecha[0]+'/'+aFecha[1]+'/'+aFecha[2];
    
    fecha= new Date(fecha);
    fecha.setDate(fecha.getDate()+parseInt(dias));
    var anno=fecha.getFullYear();
    var mes= fecha.getMonth()+1;
    var dia= fecha.getDate();
    mes = (mes < 10) ? ("0" + mes) : mes;
    dia = (dia < 10) ? ("0" + dia) : dia;
    var fechaFinal = anno+sep+mes+sep+dia
    return (fechaFinal);
}
