//<script language="javascript" type="text/javascript">

//---------------------------------------------------------------
//Esta funcion se encarga de insertar o modificar un registro
function evento_activo_cmdgrabar_onclick()
{
	var idventana="activo";

	lista_id = new Array(1);
	lista_id[0] = "idactivo";
	
	tjs_funcion_edicion_cmdgrabar(idventana,"activo_txt_accion",   //accion a ejecutar, 1:modificar, 0:insertar
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"/cactivos/ajax_procesar_insertar_desde_form",
								"/cactivos/ajax_procesar_modificar_desde_form",
								"evento_activo_cmdcerrar_onclick()"); //funcion a llamar si grabar es ok

}

//---------------------------------------------------------------
//Esta funcion se encarga de clonar un registro
function evento_activo_cmdclonar_onclick()
{
	var idventana="activo";

	lista_id = new Array(1);
	lista_id[0] = "idactivo";
	
	tjs_funcion_edicion_cmdclonar(idventana,
								//"activo_idactivo", //id con la clave primaria
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"activo_nombre_activo", //id con el primer control a dar focus
								"/cactivos/ajax_procesar_clonar_registro");

}

//--------------------------------------------------------
//Esta funcion se encarga de cerrar la ventana edicion
function evento_activo_cmdcerrar_onclick()
{
	var idventana="activo";
	tjs_funcion_edicion_cerrar(idventana,
							"cactivos/ajax_get_html_browse" //url browse
							);
}


//</script>
