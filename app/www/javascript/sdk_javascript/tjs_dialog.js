
//-----------------------------------------------------------------
//Definiendo funciones para implementar un Dialogo en Jquery
//La ventana o messagebox es un DIV que ha sido formateado
//
//ver la siguiente url
//https://rstopup.com/personalizado-de-la-colocacion-de-los-botones-en-el-cuadro-de-dialogo-de-interfaz-de-usuario-jquery.html
//-----------------------------------------------------------------

function tjs_dialog(idventana) {

	//definicion de propiedades
	this.m_idventana = idventana;
	//this.m_idcontrol = idcontrol;

	//definiciendo de metodos 
	this.show = tjs_dialog_show;

	//this.aceptar = tjs_messagebox_aceptar;
	//this.cancelar = tjs_messagebox_cancelar;
}

function tjs_dialog_show(titulo,  //titulo del dialogo
						texto_contenido_dialog="", //texto que indica el contenido del dialogo, sino se completa se espera que se ingrese la URL
						url_contenido_dialog="", //url que completa el contenido del dialogo
						parametros_url="", //parametros que se envia a la url, se envian en la variable post
						nombre_primer_boton="", //Indica el nombre del primer boton
						nombre_segundo_boton="", //Indica el nombre del segundo boton
						nombre_tercer_boton="", //Indica el nombre del tercer boton
						funcion_primer_boton="", //funcion a ejecutar cuando se da clic en el boton
						funcion_segundo_boton="", //funcion a ejecutar cuando se da clic en el boton
						funcion_tercer_boton="", //funcion a ejecutar cuando se da clic en el boton
						width=0, //ancho de la ventana
						close_on_escape=false) //cierra el dialogo con la tecla escape
{
	
	//obteniendo el contenido del dialogo
	contenido = "..."
	var div_message = null
	var nombre_dialogo = this.m_idventana;

	//validando el el elemento ya existe
	if($("#"+nombre_dialogo).length > 0) {
		//El elemento existe
		div_message = document.getElementById(nombre_dialogo);
		div_message.remove();
	}

	//creando el elemento
	div_message = document.createElement("div");
	div_message.id = nombre_dialogo;	
	div_message.style.float="left";
	div_message.style.clear="left";
	document.body.appendChild(div_message);		
		
	//añadiendo un div a la ventana
	//div_message.innerHTML = "<p>"+mensaje+ 
	//						" &nbsp; &thinsp; &ensp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p>"; //para añadir una linea en blanco
	div_message.title = titulo;
	div_message.innerHTML = contenido;

    //estableciendo propiedades del messagbox
    $( "#"+nombre_dialogo).dialog({
		cache: false,
        closeOnEscape: close_on_escape,
        autoOpen: false,
        modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },  
		close: function(event, ui) { },    
        show: {
            effect: "Blind",
            duration: 100
        },
        hide: {
            effect: "Blind",
            duration: 100
		}
    });

	//configurando botones
	if (nombre_tercer_boton != "") //los 3 botones estan habilitados
	{
		$( "#"+nombre_dialogo).dialog({
			buttons: [
				{   
					text: nombre_primer_boton, 
					click: function () { 
						if (funcion_primer_boton!=""){
							//eval(funcion_primer_boton);
							funcion_primer_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				},
				{
					text: nombre_segundo_boton, 
					click: function () { 
						if (funcion_segundo_boton!=""){
							//eval(funcion_segundo_boton);
							funcion_segundo_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				},
				{
					text: nombre_tercer_boton, 
					click: function () { 
						if (funcion_tercer_boton!=""){
							//eval(funcion_tercer_boton);
							funcion_tercer_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				}
			]
		});  

	}else if (nombre_segundo_boton != "") //los 2 botones estan habilitados
	{
		$( "#"+nombre_dialogo).dialog({
			buttons: [
				{   
					text: nombre_primer_boton, 
					click: function () { 
						if (funcion_primer_boton!=""){
							//eval(funcion_primer_boton);
							funcion_primer_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				},
				{
					text: nombre_segundo_boton, 
					click: function () { 
						if (funcion_segundo_boton!=""){
							//eval(funcion_segundo_boton);
							funcion_segundo_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				}
			]
		});  

	}else if (nombre_primer_boton != "") //el primer boton esta habilitado
	{
		$( "#"+nombre_dialogo).dialog({
			buttons: [
				{   
					text: nombre_primer_boton, 
					click: function () { 
						if (funcion_primer_boton!=""){
							//eval(funcion_primer_boton);
							funcion_primer_boton();
						}
					}, 
					//class:"sampleClass1", 
					//style:"color:Red"  //estilo del texto del boton
				}
			]
		});  

	}
	
    //estableciendo ancho
    if (width >= 0)
    {
        //$("#"+nombre_dialogo).dialog( "option", "width", 500 );
        $("#"+nombre_dialogo).dialog( "option", "width", width );
    }
	
	//actualizando el div
	if (texto_contenido_dialog != "")
	{
		this.m_div_ventana.innerHTML = texto_contenido_dialog;
	}else{

		//enviando el idventana como parametro
		if (parametros_url == "")
		{
			parametros_url = { 'idventana' : this.m_idventana };
		}else{
			parametros_url.idventana = this.m_idventana;
		}

		//actualizando el contenido		
		$.ajax({
			async   : true, //la llamada es asincrona
			url     : url_contenido_dialog,
			data    : parametros_url,
			type    : "POST",
			datatype: "json",
			success : function (data) 
			{
				div_message.innerHTML = data;
			},
			error: function (xhr,status){
				alert('Un error ejecutando ajax. xhr:'+xhr+", status:"+status);
			},
		});
	}

    //mostrando ventana de dialogo
	$("#"+nombre_dialogo).dialog("open");
}

