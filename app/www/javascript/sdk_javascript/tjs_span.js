
//---------------------------------------------------------
// Definiendo funciones para span
//---------------------------------------------------------

function tjs_span(idventana,idcontrol="") {
	
	//definicion de propiedades
	this.m_id = "";
	this.m_control = "";

	if (idcontrol == "")
	{
		this.m_id = idventana;
		this.m_control = document.getElementById(idventana);
	}else{
		//definicion de propiedades
		this.m_id = idventana + "_" + idcontrol;
		this.m_control = document.getElementById(idventana + "_" + idcontrol);
	}

	//definiciendo de metodos
	this.set_text = tjs_span_set_text;
	this.get_text = tjs_span_get_text;
}

function tjs_span_set_text(texto)
{
	return this.m_control.innerHTML = texto;
}

function tjs_span_get_text()
{
	return this.m_control.innerHTML;
}
