
//---------------------------------------------------------
// Definiendo funciones para listbox
//---------------------------------------------------------

function tjs_textbox(idventana,idcontrol="") {
	
	//definicion de propiedades
	this.m_id = "";
	this.m_control = "";

	if (idcontrol == "")
	{
		this.m_id = idventana;
		this.m_control = document.getElementById(idventana);
	}else{
		//definicion de propiedades
		this.m_id = idventana + "_" + idcontrol;
		this.m_control = document.getElementById(idventana + "_" + idcontrol);
	}
	
	//definiciendo de metodos
	this.set_text = tjs_textbox_set_text;
	this.get_text = tjs_textbox_get_text;
	this.set_focus = tjs_textbox_set_focus;
}

function tjs_textbox_set_text(texto)
{
	//this.m_control.value = texto;
	$("#" + this.m_id).val( texto );
}

function tjs_textbox_get_text()
{
	return this.m_control.value;
}

function tjs_textbox_set_focus()
{
	this.m_control.focus();
	this.m_control.select();
}
