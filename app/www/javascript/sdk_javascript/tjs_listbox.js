
//---------------------------------------------------------
// Definiendo funciones para listbox
//---------------------------------------------------------

function tjs_listbox(idventana,idcontrol="") {
	
	//definicion de propiedades
	this.m_id = "";
	this.m_control = "";

	if (idcontrol == "")
	{
		this.m_id = idventana;
		this.m_control = document.getElementById(idventana);
	}else{
		//definicion de propiedades
		this.m_id = idventana + "_" + idcontrol;
		this.m_control = document.getElementById(idventana + "_" + idcontrol);
	}

	//definiciendo de metodos
	this.set_focus = tjs_listbox_set_focus;
	this.get_selectedindex = tjs_listbox_get_selectedindex;
	this.set_selectedindex = tjs_listbox_set_selectedindex;
	this.seleccionar_item = tjs_listbox_seleccionar_item;
	this.get_value_selected = tjs_listbox_get_value_selected;
	this.get_text_selected = tjs_listbox_get_text_selected;
	this.get_text = tjs_listbox_get_text; //obtiene el texto de un valor dado
	this.add_item = tjs_listbox_add_item; //añade un item al control
	this.delete_item_by_value = tjs_listbox_delete_item_by_value; //elimina un item del control
	this.delete_item_by_index = tjs_listbox_delete_item_by_index;
	this.obtener_json = tjs_listbox_obtener_json;
}

function tjs_listbox_get_selectedindex()
{
	return this.m_control.selectedIndex;
}

function tjs_listbox_set_selectedindex(indice)
{
	this.m_control.selectedIndex = indice;
}

//devuelve valores del item seleccionado
function tjs_listbox_get_value_selected()
{
	return this.m_control.options[this.m_control.selectedIndex].value;
}

//devuelve valores del item seleccionado
function tjs_listbox_get_text_selected()
{
	return this.m_control.options[this.m_control.selectedIndex].text;
}

function tjs_listbox_get_text(valor)
{
	for (var i = 0; i < this.m_control.length; i++) 
	{
	    var opt = this.m_control.options[i];

	    if (opt.value == valor)
	    {
	    	return opt.text;
	    }
	}

	return null;
}

function tjs_listbox_add_item(valor,texto)
{
	//utilizando jquery
	$('#'+this.m_id).append('<option value="'+valor+'">'+texto+'</option>');
}

function tjs_listbox_delete_item_by_value(valor)
{
	for (var i = 0; i < this.m_control.length; i++) 
	{
	    var opt = this.m_control.options[i];

	    if (opt.value == valor)
	    {
	    	this.m_control.remove(i);
	    	return;
	    }
	} 
}

function tjs_listbox_delete_item_by_index(indice)
{
	this.m_control.remove(indice);
}

function tjs_listbox_obtener_json()
{
	var datos = "{";
//clasificaciones_seleccionadas = '{"a":"1","b":"11"}';

	for (var i = 0; i < this.m_control.length; i++) 
	{
	    var opt = this.m_control.options[i];

	    datos = datos+'"'+opt.value+'"';
	    datos = datos+':'
	    datos = datos+'"'+opt.text+'"';
	    datos = datos+',';
	} 

	//eliminando ultima coma
	if (datos.length>1)
		datos = datos.substring(0,datos.length-1);

	datos = datos + "}";
	return datos;
}

function tjs_listbox_set_focus()
{
	this.m_control.focus();
	this.m_control.select();
}


function tjs_listbox_seleccionar_item(valor)
{
	var cantidad = this.m_control.length;
	for (i = 0; i < cantidad; i++) 
	{
		if (this.control[i].value == valor) 
		{
			this.m_control[i].selected = true;
			return;
		}   
	}
}

/*
LIMPIAR NUESTRO SELECT
$('#exampleSelect').html('');

para seleccionar un item

function setValueSelect(SelectId, Value) {
        SelectObject = document.getElementById(SelectId);
        for(index = 0;  index < SelectObject.length;  index++) {
            if(SelectObject[index].value == Value) SelectObject.selectedIndex = index;
        }
    }

    */
