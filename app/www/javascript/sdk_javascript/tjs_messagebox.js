
//-----------------------------------------------------------------
//Definiendo funciones para implementar un MessageBox en Jquery
//La ventana o messagebox es un DIV que ha sido formateado
//
//ver la siguiente url
//https://rstopup.com/personalizado-de-la-colocacion-de-los-botones-en-el-cuadro-de-dialogo-de-interfaz-de-usuario-jquery.html
//-----------------------------------------------------------------

function tjs_messagebox(idventana) {
	//definicion de propiedades
    this.m_idventana = idventana;
    //this.m_idcontrol = idcontrol;

    //definiciendo de metodos 
    this.show = tjs_messagebox_show;
	//this.aceptar = tjs_messagebox_aceptar;
	//this.cancelar = tjs_messagebox_cancelar;
}

function tjs_messagebox_show(titulo,  //titulo del dialogo
								mensaje, //mensaje a presentar en el dialogo
								funcion_primer_boton="", //funcion a ejecutar luego de cerrar el dialogo, si se presiona el primer boton
								funcion_segundo_boton="", //funcion a ejecutar luego de cerrar el dialogo, si se presiona el segundo boton
								funcion_tercer_boton="", //funcion a ejecutar luego de cerrar el dialogo, si se presiona el tercer boton
								button_aceptar=true,
								button_aceptar_cancelar=false,
								button_si_no=false,
								button_si_no_cancelar=false,
								width=0)
{
	var nombre_dialogo = this.m_idventana+"_messagebox_";//+nro_aleatorio;
	var div_message = null;

	//alert("paso0.1:"+funcion_primer_boton);

	//validando si ya existe el div
	if($("#"+nombre_dialogo).length > 0) {
		//El elemento existe
		div_message = document.getElementById(nombre_dialogo);
		div_message.remove();
	}

	//alert("paso0.2:"+funcion_primer_boton);
	//creando objeto
	div_message = document.createElement("div");
	//var nro_aleatorio = Math.floor(Math.random() * (100 - 1)) + 1; //numero entre 1 y 100

	//añadiendo un div a la ventana
	div_message.id = nombre_dialogo;	
	div_message.title = titulo;
	div_message.style.float="left";
	div_message.style.clear="left";
	div_message.innerHTML = "<p>"+mensaje+ 
							" &nbsp; &thinsp; &ensp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</p>"; //para añadir una linea en blanco
	document.body.appendChild(div_message);

	//alert("paso0.3:"+funcion_primer_boton);
    //estableciendo propiedades del messagbox
    $( "#"+nombre_dialogo).dialog({
        closeOnEscape: false,
        autoOpen: false,
        modal: true,
        open: function (event, ui) { $(".ui-dialog-titlebar-close").hide(); },      
        show: {
            effect: "Blind",
            duration: 100
        },
        hide: {
            effect: "Blind",
            duration: 100
        }
    });

	//alert("paso0.4:"+funcion_primer_boton);
    //configurando botones
    if (button_aceptar){

        $( "#"+nombre_dialogo).dialog({
            buttons: {
              Aceptar: function() {
				$("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso1:"+funcion_primer_boton);


				if (funcion_primer_boton != "")
					//eval(funcion_primer_boton);   
					funcion_primer_boton();        
              }
            }
          });

    }else if (button_aceptar_cancelar){

        $( "#"+nombre_dialogo).dialog({
            buttons: {
              Aceptar: function() {
				$("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso2:"+funcion_primer_boton);

				if (funcion_primer_boton != "")
					//eval(funcion_primer_boton);       
					funcion_primer_boton();        

              },
              Cancelar: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso3:"+funcion_primer_boton);
				if (funcion_segundo_boton != "")
					//eval(funcion_segundo_boton);           
					funcion_segundo_boton();
              }
            }
          });

    }else if (button_si_no){

        $( "#"+nombre_dialogo).dialog({
            buttons: {
              Si: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso4:"+funcion_primer_boton);
				if (funcion_primer_boton != "")
					//eval(funcion_primer_boton);      
					funcion_primer_boton();        

              },
              No: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso5:"+funcion_primer_boton);
				if (funcion_segundo_boton != "")
					//eval(funcion_segundo_boton);           
					funcion_segundo_boton();
              }
            }
          });

    }else if (button_si_no_cancelar){

        $( "#"+nombre_dialogo).dialog({
            buttons: {
              Si: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso6:"+funcion_primer_boton);
				if (funcion_primer_boton != "")
					//eval(funcion_primer_boton);           
					funcion_primer_boton();
              },
              No: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso7:"+funcion_primer_boton);
				if (funcion_segundo_boton != "")
					//eval(funcion_segundo_boton);           
					funcion_segundo_boton();
              },
              Cancelar: function() {
                $("#"+nombre_dialogo).dialog("close"); //cerrando el dialogo
				
				//alert("paso8:"+funcion_primer_boton);
				if (funcion_tercer_boton != "")
					//eval(funcion_tercer_boton);           
					funcion_tercer_boton();
              }
            }
          });

    }

	//alert("paso9:"+funcion_primer_boton);
    //estableciendo ancho
    if (width >= 0)
    {
        $("#"+nombre_dialogo).dialog( "option", "width", 500 );
    }

	//alert("paso10:"+funcion_primer_boton);
    //mostrando ventana de dialogo
    $("#"+nombre_dialogo).dialog("open");
}

