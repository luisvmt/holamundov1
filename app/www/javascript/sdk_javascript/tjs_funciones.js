

//--------------------------------------------------------
//mostrando un div
function tjs_funcion_show_contenido(idobjeto,idpanel)
{
    switch (idpanel){
        case "contenido_waiting":
                $('#'+idobjeto+'_contenido_browse').hide();
                $('#'+idobjeto+'_contenido_modificar').hide();
                $('#'+idobjeto+'_contenido_importar_exportar').hide();
                $('#'+idobjeto+'_contenido_waiting').show();
                break;
        case "contenido_browse":
                $('#'+idobjeto+'_contenido_waiting').hide();
                $('#'+idobjeto+'_contenido_modificar').hide();
                $('#'+idobjeto+'_contenido_importar_exportar').hide();
                $('#'+idobjeto+'_contenido_browse').show();
                break;
        case "contenido_modificar":
                $('#'+idobjeto+'_contenido_waiting').hide();
                $('#'+idobjeto+'_contenido_browse').hide();
                $('#'+idobjeto+'_contenido_importar_exportar').hide();
                $('#'+idobjeto+'_contenido_modificar').show();
                break;
        case "contenido_importar_exportar":
                $('#'+idobjeto+'_contenido_waiting').hide();
                $('#'+idobjeto+'_contenido_browse').hide();
                $('#'+idobjeto+'_contenido_modificar').hide();
                $('#'+idobjeto+'_contenido_importar_exportar').show();
                break;
    }
}

//--------------------------------------------------------
//funcion que carga browse, mediante ajax
//--------------------------------------------------------
function tjs_funcion_browse_load_pagina(idobjeto,
                                        urlbrowse,
                                        nropagina,
                                        filtro,
                                        orderby,
                                        sentido,
                                        parametros,
                                        funcion_posterior="" //funcion a ejecutar posterior al ajax
                                        )
{
    tjs_funcion_show_contenido(idobjeto,"contenido_waiting");
    
    //cargando la pagina con ajax
    $.ajax({
        url : urlbrowse,
        data: { "nropagina": nropagina, "filtro": filtro, "orderby": orderby, "sentido": sentido, "parametros": parametros },
        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //validando si es error y se presenta con PRE
            if (data.substr(0,11) != "<!-- OK -->")
            {
                    $('#'+idobjeto+'_contenido_browse').html("<pre>"+data+"</pre>");
            }else{
                    $('#'+idobjeto+'_contenido_browse').html(data);
            }

            tjs_funcion_show_contenido(idobjeto,"contenido_browse");

            //seleccionando el filtro
            $( "#" + idobjeto + "_browse_filtro" ).focus();
            $( "#" + idobjeto + "_browse_filtro" ).select();

            //ejecutando la funcion posterior
            if (funcion_posterior != "")
                eval( funcion_posterior );
        },

        error: function (xhr,status){
            alert('un problema cargando ajax');
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });
} 

//-------------------------------------------
//Esta funcion elimina un item
//-------------------------------------------
function tjs_funcion_browse_eliminar_ajax(idobjeto,
                                            parametros,
                                            urlejecutar,
                                            funcion_posterior="") //si no hay error, se ejecuta esta funcion
{
    var exist_error=false;

    //ocultando la ventana
    tjs_funcion_show_contenido(idobjeto,"contenido_waiting");

    //haciendo submit por ajax
    $.ajax({
        url : urlejecutar,
        data: parametros,

        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //parseando la data
            //contiene los siguientes campos:

            //	response=0    ok
            //	response=e	error
            //	response=i	informativo

            //	control=""	no indica ningun control html
            //	control		indica el id del control donde colocar el focus

            //	mensaje		indica el mensaje a presentar

            //mostrando la ventana
            tjs_funcion_show_contenido(idobjeto,"contenido_browse");

            var datos;

            data = data.trim();

            try {
                datos = jQuery.parseJSON(data);

                if (datos.response != "0")
                {
                    alert(change_html_to_unicode(datos.mensaje));
                    exist_error=true;
                }else{
                    //ejecutando la funcion posterior
                    if (funcion_posterior != "")
                        eval( funcion_posterior );
                }

            } catch (e) {
                alert(data);
                alert(e);
                exist_error=true;
            }

        },

        error: function (xhr,status){
                alert('un problema cargando ajax');
                exist_error=true;
        },

        complete: function (xhr,status){
                //alert("complete");
                //return 0;
        }
    });

    if (exist_error) {
            return -1;
    }else{
            return 0;
    }
    return 0;

}

//---------------------------------------------------------------------------
//Esta funcion presenta la ventana de modificacion
//---------------------------------------------------------------------------
function tjs_funcion_browse_modificar_ajax(idobjeto,
                                            parametros_post,   //parametros a pasar al controlador
                                            urlejecutar,  //controlador a ejecutar
                                            idfocus="",      //id del control que tendra el foco en la edicion
                                            funcion_posterior="") //funcion a ejecutar posterior al ajax
{ 
    var exist_error=false;

    //ocultando la ventana
    tjs_funcion_show_contenido(idobjeto,"contenido_waiting");

    //cargando la pagina insertar/modificar con ajax
    $.ajax({
        url : urlejecutar,
        data: parametros_post,
        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //alert(data);
            //validando si es error y se presenta con PRE
            if (data.substr(0,11) != "<!-- OK -->")
            {
                $('#'+idobjeto+'_contenido_modificar').html("<pre>"+data+"</pre>");
            }else{
                $('#'+idobjeto+'_contenido_modificar').html(data);
            }
            tjs_funcion_show_contenido(idobjeto,"contenido_modificar");

            if (idfocus!="")
            {
                document.getElementById(idfocus).focus();

                if ( document.getElementById(idfocus).type == "text" )
                {
                    document.getElementById(idfocus).select();
                }
            }

            //ejecutando la funcion posterior
            if (funcion_posterior != "")
                eval( funcion_posterior );
        },

        error: function (xhr,status){
            alert('un problema cargando ajax');
            exist_error=true;
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

    if (exist_error) {
    	return -1;
    }

    return 0;
}

 
//-----------------------------------------------
//Esta funcion se ejecuta al clic en ejecutar
//-----------------------------------------------
function tjs_funcion_browse_chktitulo(idobjeto)
{
    for (i=1; i<100; i++)
    {
        var control=document.getElementById(idobjeto+'_browse_chkfila'+i);
        if (control == null) {
                break;
        }

        control.checked=document.getElementById(idobjeto+'_browse_chktitulo').checked;
    }
}

//----------------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da clic en grabar en la ventana edicion
//----------------------------------------------------------------------------
function tjs_funcion_edicion_cmdgrabar(idobjeto,  //prefijo de objetos
                                    txt_accion,     //si contiene 0 se utiliza insertar, si contiene distinto a 0 se utiliza modificar
                                                    //0: insertar, 1: modificar
                                                    //se puede utilizar un texto oculto
                                    lista_id,         //contiene lista de controles donde se mostrara los nuevos valores
                                    urlinsertar,  //url que se encargara de insertar (controlador)
                                    urlmodificar, //url que se encargara de modificar (controlador)
                                    funcion_posterior="" //funcion a ejecutar posterior al ajax
                                    )
{
    //ocultando la ventana
    tjs_funcion_show_contenido(idobjeto,"contenido_waiting");

    //averiguando la accion a realizar
    var valorid = $.trim( $("#" + txt_accion).val() );

    //validando que exista el control
    if (valorid == "")
    {
        alert("ERROR: No existe el control: #"+txt_accion+". Accion detenida.");
        return;
    }

    //validando el valor del control
    if (valorid == "0") {
        var_url=urlinsertar;
    }else{
        var_url=urlmodificar;
    }

    //haciendo submit por ajax
    $.ajax({
        url : var_url,
        data:  $("form#"+idobjeto+"_form-edicion").serialize(),
        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //parseando la data
            //contiene los siguientes campos:

            //	response=0    ok
            //	response=e	error
            //	response=i	informativo

            //	control=""	no indica ningun control html
            //	control		indica el id del control donde colocar el focus

            //	mensaje		indica el mensaje a presentar

            //mostrando la ventana
            tjs_funcion_show_contenido(idobjeto,"contenido_modificar");

            var datos;
            data = data.trim();

            try {
                datos = jQuery.parseJSON(data);

                if (datos.control != "")  //enfocando el control
                {
                        $('#'+datos.control).focus();
                        $('#'+datos.control).select();
                }

                if (datos.response == "0") {
                    if (datos.mensaje != "")
                        alert(change_html_to_unicode(datos.mensaje));

                    //mostrando nuevo id
                    //$('#' + txt_id).val( datos.id );
                    
                    lista_id.forEach(
                        function (elemento, indice) 
                        {
                            $('#'+idobjeto+"_"+elemento).val(datos.elemento);
                        });

                    //ejecutando la funcion posterior
                    if (funcion_posterior != "")
                        eval( funcion_posterior );
                        
                }else{
                    if (datos.control != "")
                    {
                            $('#'+datos.control).focus();
                            $('#'+datos.control).select();
                    }

                    $('#'+idobjeto+'_mensaje').html(datos.mensaje);
                    
                    //alert(change_html_to_unicode(datos.mensaje));
                    
                    //mostrando mensaje
                    (new tjs_messagebox(idobjeto)).show("Observación",
                                                    change_html_to_unicode(datos.mensaje),
                                                    "" //"$('#"+idventana+"_browse_combo_acciones').focus();" //funcion a ejecutar al presionar primer boton
                                                    );
                                                            
                    return; //saliendo de la funcion
                }

            } catch (e) {
                alert(data);
                alert(e);
            }

        },

        error: function (xhr,status){
                alert('un problema cargando ajax');
        },

        complete: function (xhr,status){
                //alert("complete");
        }
    });

}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da clic en clonar
//--------------------------------------------------------------------
function tjs_funcion_edicion_cmdclonar(idobjeto, //prefijo de objetos
                                        lista_id,  //contiene lista de controles que contiene el ID de la clave primaria
                                        idfocus,
                                        urlclonar,
                                        funcion_posterior="") //funcion a ejecutar posterior al ajax
{
    //ocultando la ventana
    tjs_funcion_show_contenido(idobjeto,"contenido_waiting");

    //hallando el id
    //var valorid = $.trim( $("#"+ txt_id).val() );

    var datos = "{"; //creando un texto json
    
    lista_id.forEach(
        function (elemento, indice) 
        {
            datos += "\"" + elemento + "\":\"";
            datos += $('#'+idobjeto+"_"+elemento).val();
            datos += "\",";
        });

    //quitando la ultima coma
    datos = datos.substring(0,datos.length-1);
    datos += "}";
    
    //haciendo submit por ajax
    $.ajax({
        url : urlclonar,
        data: { "accion_ejecutar": "clonar", "id":datos },
        //data: datos,
        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //parseando la data
            //contiene los siguientes campos:

            //	response=0    ok
            //	response=e	error
            //	response=i	informativo

            //	control=""	no indica ningun control html
            //	control		indica el id del control donde colocar el focus

            //	mensaje		indica el mensaje a presentar

            //mostrando la ventana
            tjs_funcion_show_contenido(idobjeto,"contenido_modificar");

            var datos;

            data = data.trim();

            try {
                datos = jQuery.parseJSON(data);

                if (datos.response != "0") {
                    $('#' + idobjeto + '_mensaje').html(datos.mensaje);
                    return;
                }

                //mostrando mensaje
                $('#' + idobjeto + '_mensaje').html(datos.mensaje);

                //mostrando nuevo id
                //$('#' + txt_id).val( datos.id );
                
                //parseando el campo id
                //alert(datos.id);
                
                var items = JSON.parse(datos.id);
                $.each(items, function (indice, value) {
                   
                    //alert("index:"+indice+",value:"+value);
                   $('#'+idobjeto+"_"+indice).val(value);
                });

                //dando focus
                if (idfocus!="")
                {
                    document.getElementById(idfocus).focus();

                    if ( document.getElementById(idfocus).type == "text" )
                    {
                            document.getElementById(idfocus).select();
                    }
                }

                //mostrando mensaje
                (new tjs_messagebox(idobjeto)).show("Observación",
                                                    change_html_to_unicode(datos.mensaje),
                                                    funcion_posterior //"$('#"+idventana+"_browse_combo_acciones').focus();" //funcion a ejecutar al presionar primer boton
                                                    );
                /*
                alert(change_html_to_unicode(datos.mensaje));
                alert("Modficiado");
                
                //ejecutando la funcion posterior
                if (funcion_posterior != "")
                    eval( funcion_posterior );
*/
                return; //finalizando funcion

            } catch (e) {
                alert(data);
                alert(e);
            }

        },

        error: function (xhr,status){
            alert('un problema cargando ajax');
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da clic en cerrar
//--------------------------------------------------------------------
function tjs_funcion_edicion_cerrar(idobjeto,urlbrowse){

	//actualizando lista de items
	var nropagina = $.trim( $("#" + idobjeto + "_browse_nropagina").val() );
	var filtro = $.trim( $("#" + idobjeto + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idobjeto + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idobjeto + "_browse_sentido").val() );
	var parametros = $.trim( $("#" + idobjeto + "_browse_parametros").val() );

	//load_pagina(nropagina,filtro,orderby,sentido);

	//refrescando la lista de items, usando ajax
	tjs_funcion_browse_load_pagina(idobjeto, //idobjeto
							urlbrowse, //url de browse
							nropagina, //nropagina
							filtro, //filtro
							orderby, //orderby
							sentido, //sentido
							parametros); //parametros

	//mostrando lista de items
	tjs_funcion_show_contenido(idobjeto,"contenido_browse");
	$("#" + idobjeto + "_browse_filtro").focus();
	$("#" + idobjeto + "_browse_filtro").select();
}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da enter en filtro
//--------------------------------------------------------------------
function tjs_funcion_browse_filtro(idobjeto,
                                    e,
                                    urlbrowse,
                                    funcion_posterior="") //funcion a ejecutar posterior al ajax
{

    if (typeof e == 'undefined' && window.event) { e = window.event; }
    if (e.keyCode == 13)
    {
        //hallando el nro de pagina
        //var nropagina = $.trim( $("#usuario_browse_nropagina").val() );
        var nropagina=1;
        var filtro  = $.trim( $("#" + idobjeto + "_browse_filtro").val() );
        var orderby = $.trim( $("#" + idobjeto + "_browse_orderby").val() );
        var sentido = $.trim( $("#" + idobjeto + "_browse_sentido").val() );
        var parametros = $.trim( $("#" + idobjeto + "_browse_parametros").val() );

        //alert(nropagina);
        //load_pagina(nropagina,filtro,orderby,sentido);

        //refrescando la lista de items, usando ajax
        tjs_funcion_browse_load_pagina(idobjeto, //idobjeto
                                        urlbrowse, //url de browse
                                        nropagina, //nropagina
                                        filtro, //filtro
                                        orderby, //orderby
                                        sentido, //sentido
                                        parametros, //parametros
                                        funcion_posterior); //funcion a ejecutar posterior al ajax
    }
}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da enter en filtro
//--------------------------------------------------------------------
function tjs_funcion_browse_insertar_ajax(idobjeto,
                                            parametros_post, //parametros a pasar al controlador
                                            urlinsertar,
                                            idfocus,
                                            funcion_posterior="") //funcion a ejecutar posterior al ajax
{

	//cargando la pagina insertar con ajax
	$.ajax({
		url : urlinsertar,
		//data: { "accion_ejecutar": "insertar" },
		data: parametros_post,
		type: "POST",
		datatype: "html",
		success: function (data)
		{
			//alert(data);
			//validando si es error y se presenta con PRE
			if (data.substr(0,11) != "<!-- OK -->")
			{
				$('#'+idobjeto+'_contenido_modificar').html("<pre>"+data+"</pre>");
			}else{
				$('#'+idobjeto+'_contenido_modificar').html(data);
			}
			tjs_funcion_show_contenido(idobjeto,"contenido_modificar");

			if (idfocus!="")
			{
			    //alert("buscando:"+idfocus);
				document.getElementById(idfocus).focus();

				if ( document.getElementById(idfocus).type == "text" )
				{
					document.getElementById(idfocus).select();
				}
			}
            
            //ejecutando la funcion posterior
            if (funcion_posterior != "")
                eval( funcion_posterior );
		},

		error: function (xhr,status){
			alert('un problema cargando ajax');
		},

		complete: function (xhr,status){
			//alert("complete");
		}
	});
}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da enter en filtro
//--------------------------------------------------------------------
function tjs_funcion_browse_importar_exportar(idobjeto,
                                                urlinsertar,
                                                idfocus,
                                                funcion_posterior="") //funcion a ejecutar posterior al ajax
{

	//cargando la pagina insertar con ajax
	$.ajax({
		url : urlinsertar,
		data: { "accion_ejecutar": "importar_exportar" },
		type: "POST",
		datatype: "html",
		success: function (data)
		{
			//validando si es error y se presenta con PRE
			if (data.substr(0,11) != "<!-- OK -->")
			{
				$('#'+idobjeto+'_contenido_importar_exportar').html("<pre>"+data+"</pre>");
			}else{
				$('#'+idobjeto+'_contenido_importar_exportar').html(data);
			}
			tjs_funcion_show_contenido(idobjeto,"contenido_importar_exportar");

			if (idfocus!="")
			{
				document.getElementById(idfocus).focus();

				if ( document.getElementById(idfocus).type == "text" )
				{
					document.getElementById(idfocus).select();
				}
            }
            
            //ejecutando la funcion posterior
            if (funcion_posterior != "")
                eval( funcion_posterior );
		},

		error: function (xhr,status){
			alert('un problema cargando ajax');
		},

		complete: function (xhr,status){
			//alert("complete");
		}
	});
}

//--------------------------------------------------------------------
//Esta funcion se ejecuta cuando se genera un reporte en excel
//--------------------------------------------------------------------
function tjs_funcion_browse_reporte_excel(idobjeto,urlreporte_excel){

	var url = urlreporte_excel;
	var body = document.body;
	var parametros_form = {  'mensaje':''};
	var nropagina=1;
	var filtro = $.trim( $("#" + idobjeto + "_browse_filtro").val() );
	var orderby = $.trim( $("#" + idobjeto + "_browse_orderby").val() );
	var sentido = $.trim( $("#" + idobjeto + "_browse_sentido").val() );
	var parametros = $.trim( $("#" + idobjeto + "_browse_parametros").val() );

    parametros_form['nropagina']=nropagina;
	parametros_form['filtro']=filtro;
	parametros_form['orderby']=orderby;
	parametros_form['sentido']=sentido;
	parametros_form['parametros']=parametros;

	form=document.createElement('form');
	form.method = 'POST';
	form.action = url;
	form.name = 'jsform';
	for (index in parametros_form) {
		var input = document.createElement('input');
		input.type='hidden';
		input.name=index;
		input.id=index;
		input.value=parametros_form[index];
		form.appendChild(input);
	}
	body.appendChild(form);
	form.submit();
}


//----------------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da clic en exportar archivo
//----------------------------------------------------------------------------
function tjs_funcion_ie_exportar_file(idobjeto,urlexportar,funcion_cerrar_ie)
{
	var url = urlexportar;
	var body = document.body;
	var parametros_form = {  'mensaje':''};

	form=document.createElement('form');
	form.method = 'POST';
	form.action = url;
	form.name = 'jsform';
	for (index in parametros_form) {
		var input = document.createElement('input');
		input.type='hidden';
		input.name=index;
		input.id=index;
		input.value=parametros_form[index];
		form.appendChild(input);
	}
	body.appendChild(form);
	form.submit();

	//cerrando ventana actual
	eval( funcion_cerrar_ie );
}

//----------------------------------------------------------------------------
//Esta funcion se ejecuta cuando se da clic en importar archivo
//----------------------------------------------------------------------------
function tjs_funcion_ie_importar_file(idobjeto,
                                        urlimportar,
                                        funcion_posterior //es el nombre de una funcion
                                        )
{

    //ocultando la ventana
	tjs_funcion_show_contenido(idobjeto,"contenido_waiting");

	//hallando el formulario
	var formData = new FormData(document.getElementById(idobjeto+"_form_importar"));

	//haciendo submit por ajax
	$.ajax({
		url : urlimportar,
		data: formData,
		type: "POST",
		datatype: "html",
		cache: false,
		contentType:false,
		processData: false,
		success: function (data)
		{
			//parseando la data
			//contiene los siguientes campos:

			//	response=0    ok
			//	response=e	error
			//	response=i	informativo

			//	control=""	no indica ningun control html
			//	control		indica el id del control donde colocar el focus

			//	mensaje		indica el mensaje a presentar

			//mostrando la ventana
			tjs_funcion_show_contenido(idobjeto,"contenido_importar_exportar");

			var datos;

			data = data.trim();

			try {

				datos = jQuery.parseJSON(data);

				if (datos.control != "")  //enfocando el control
				{
					$('#'+datos.control).focus();
					$('#'+datos.control).select();
				}

				if (datos.response == "0")
				{
					alert(change_html_to_unicode(datos.mensaje));

					//cerrando la ventana.
					//usuario_cmdcerrar_onclick();
                    if (funcion_posterior != "")
                        eval( funcion_posterior );
                    
				}else{
					$('#'+idobjeto+'_mensaje').html(datos.mensaje);
					alert(change_html_to_unicode(datos.mensaje));

					if (datos.control != "")
					{
						$('#'+datos.control).focus();
						$('#'+datos.control).select();
					}
				}

			} catch (e) {
				alert(data);
				alert(e);
			}

		},

		error: function (xhr,status){
			alert('un problema cargando ajax');
		},

		complete: function (xhr,status){
			//alert("complete");
		}
	});

}



//----------------------------------------------------------------------------
//llamada sincrona a una funcion (implementacion propia de REST)
//----------------------------------------------------------------------------
/*function tjs_ajax_exec_sincrono(urlejecutar,parametros)
{
    var salida = "";

    //solicitando datos x ajax sincrono
    $.ajax({
	async:false, //lo convierte en una llamada sincrona
        url : urlejecutar,
        data: parametros,
        type: "POST",
        datatype: "json",
        success: function (data)
        {
            salida = data;
        },

        error: function (xhr,status){
            alert('un problema cargando ajax');
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

    return salida;
}*/
                
//---------------------------------------------------------------------------------------------
// Simulando una llamada sincrona a una funcion (implementacion propia de REST)
// 
// return   : Devuelve la respuesta del ajax. Si devuelve nulo, hubo un error
//
// Ejemplo:
//    var parametros = { "password_nuevo" : password_nuevo.get_text() };
//    tjs_ajax_exec_sincrono("/cusuarios/ajax_procesar_cambiarpassword",parametros,"global_cambiarpassword_aceptar_fin");
//
// Comentarios:
// - En ciertas ocasiones la funcion sincrona no funciona como esperamos, para ello es util utilizar el 
//   parametro 'funcion_posterior'
//---------------------------------------------------------------------------------------------
function tjs_ajax_exec_sincrono(urlejecutar,            //la url a ejecutar
                                parametros,             //parametros a enviar a la url,
                                funcion_posterior=null)   //es la funcion que se ejecuta luego de finalizar el ajax, es un callback
{
    //var salida = "";
    //var nombre_funcion=""
    var respuesta = null;

    alert("OJO: Esta funcion esta desaconsejada utilizar");

    //solicitando datos x ajax 
    $.ajax({
	    async   : false, //lo convierte en una llamada sincrona
        url     : urlejecutar,
        data    : parametros,
        type    : "POST",
        datatype: "json",
        success : function (data)
        {
            //validando si se coloco funcion_posterior
            if (funcion_posterior != null)
            {
                funcion_posterior(data); //llamando a callback
            }
            respuesta = data;
        },
        
        error: function (xhr,status){
            alert('Un error ejecutando ajax. xhr:'+xhr+", status:"+status);
            respuesta = null;
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

    //devolviendo respuesta
    return respuesta;
}
          
//---------------------------------------------------------------------------------------------
// Simulando una llamada asincrona a una funcion (implementacion propia de REST)
// Las variables son enviadas desde el argumento
// 
// return   : No devuelve datos, porque la llamada es asincrona
//
// Ejemplo:
//    var parametros = { "password_nuevo" : password_nuevo.get_text() };
//    tjs_ajax_exec_asincrono("/cusuarios/ajax_procesar_cambiarpassword",parametros,"global_cambiarpassword_aceptar_fin");
//
// Comentarios:
// - Ninguno
//---------------------------------------------------------------------------------------------
function tjs_ajax_exec_asincrono(urlejecutar,            //la url a ejecutar
                                argumentos,             //argumentos a enviar a la url, se envian como POST
                                funcion_posterior=null)   //es la funcion que se ejecuta luego de finalizar el ajax, es un callback
{
    //var salida = "";
    //var nombre_funcion=""
    //var respuesta = null;

    //solicitando datos x ajax 
    $.ajax({
        async       : true, //la llamada es asincrona
        url         : urlejecutar,
        data        : argumentos,
        type        : "POST",
        datatype    : "json",
        success     : function (data)
        {
            //validando si se coloco funcion_posterior
            if (funcion_posterior != null)
            {
                funcion_posterior(data); //llamando a callback
            }
        },

        error: function (xhr,status){
            alert('Un error ejecutando ajax. xhr:'+xhr+", status:"+status);
            //respuesta = null;
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

    //devolviendo respuesta
    //return respuesta;
}

   
//---------------------------------------------------------------------------------------------
// Simulando una llamada asincrona a una funcion (implementacion propia de REST)
// Los datos son enviados desde un Formulario
// 
// return   : No devuelve datos, porque la llamada es asincrona
//
// Ejemplo:
//    var parametros = { "password_nuevo" : password_nuevo.get_text() };
//    tjs_ajax_exec_asincrono("/cusuarios/ajax_procesar_cambiarpassword",parametros,"global_cambiarpassword_aceptar_fin");
//
// Comentarios:
// - Ninguno
//---------------------------------------------------------------------------------------------
function tjs_ajax_exec_form_asincrono(urlejecutar,            //la url a ejecutar
                                        idformulario,             //id del formulario a enviar a la url, se envian como POST
                                        funcion_posterior=null)   //es la funcion que se ejecuta luego de finalizar el ajax, es un callback
{
    //var salida = "";
    //var nombre_funcion=""
    //var respuesta = null;

    //solicitando datos x ajax 
    $.ajax({
        async       : true, //la llamada es asincrona
        url         : urlejecutar,
        data        : $("form#"+idformulario).serialize(),
        type        : "POST",
        datatype    : "html ",
        success     : function (data)
        {
            //validando si se coloco funcion_posterior
            if (funcion_posterior != null)
            {
                funcion_posterior(data); //llamando a callback
            }
        },

        error: function (xhr,status){
            alert('Un error ejecutando ajax. xhr:'+xhr+", status:"+status);
            //respuesta = null;
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });

}

//---------------------------------------------------------------------------------------------
// Esta funcion ejecuta un ajax y devuelve el contenido retornado dentro de un div
// 
// return   : no devuelve valor, porque la llamada es asincrona
//
// Ejemplo:
//    var parametros = { "password_nuevo" : password_nuevo.get_text() };
//    xxxxxxxxx("/cusuarios/ajax_procesar_cambiarpassword",parametros,"global_cambiarpassword_aceptar_fin");
//
// Comentarios:
// - En ciertas ocasiones la funcion sincrona no funciona como esperamos, para ello es util utilizar el 
//   parametro 'funcion_posterior'
//---------------------------------------------------------------------------------------------
/*function tjs_ajax_exec_to_div(urlejecutar,      //url a ejecutar
                                parametros,     //parametros a enviar
                                idsalida,       //div donde grabar la salida
                                idfocus="")     //control que tendra el foco luego de grabar
{
    var datosiniciales="";
    var imagen = "<img src='images/loading3.gif' border='0' >";

    //colocando gif de espera
    if (idsalida != "")
    {
    	datosiniciales = $(idsalida).html();
    	$(idsalida).html( imagen );
    }

    //solicitando datos x ajax
    $.ajax({
	    //async:false, //lo convierte en una llamada sincrona
        url : urlejecutar,
        data: parametros,
        type: "POST",
        datatype: "html",
        success: function (data)
        {
            //procesando la salida
            if (idsalida != "")
            {                
                $(idsalida).html(data);
            }

            //estableciendo el focus
            if (idfocus!="")
            {
                tjs_funcion_focus(idfocus);
            }
        },

        error: function (xhr,status){
            alert('Un error ejecutando ajax. xhr:'+xhr+", status:"+status);
            respuesta = null;
        },

        complete: function (xhr,status){
            //alert("complete");
        }
    });
}
*/

//---------------------------------------------------------------------------------------------
// Esta funcion establece el focus de un control
//---------------------------------------------------------------------------------------------
function tjs_funcion_focus(idfocus)
{
    if (idfocus!="")
    {
        document.getElementById(idfocus).focus();

        if ( document.getElementById(idfocus).type == "text" )
        {
                document.getElementById(idfocus).select();
        }
    }
}

