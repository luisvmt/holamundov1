
//---------------------------------------------------------
// Definiendo funciones para tabla
//---------------------------------------------------------
function tjs_tabla(idventana,idcontrol="") {
	
	//definicion de propiedades
	this.m_id = "";
	this.m_control = "";

	if (idcontrol == "")
	{
		this.m_id = idventana;
		this.m_control = document.getElementById(idventana);
	}else{
		//definicion de propiedades
		this.m_id = idventana + "_" + idcontrol;
		this.m_control = document.getElementById(idventana + "_" + idcontrol);
	}

	this.m_permit_select = false;
	this.m_permit_select_multiple = false;

	//definiciendo de metodos
	this.permit_select = tjs_tabla_permit_select;
	this.select_fila = tjs_tabla_select_fila;
	//this.select_multiples_filas = tjs_tabla_select_multiples_filas;
	this.get_cell_div = tjs_tabla_get_cell_div;
	this.get_cell_text = tjs_tabla_get_cell_text;
	this.set_cell_text = tjs_tabla_set_cell_text;
	this.get_dataset_fila = tjs_tabla_get_dataset_fila; 

	//devuelve numero de filas de la tabla
	this.get_nro_filas = tjs_tabla_get_nro_filas;

	//devuelve numero de columnas de la tabla
	this.get_nro_columnas = tjs_tabla_get_nro_columnas;

	//devuelve si la fila esta seleccionada
	this.is_fila_selected = tjs_tabla_is_fila_selected;

	//devuelve la cantidad de filas seleccionadas
	this.get_nro_filas_selected = tjs_tabla_get_nro_filas_selected;
}

function tjs_tabla_get_nro_filas_selected()
{
	var nrofilas_selected = 0;
	var nrofilas = this.m_control.rows.length;
	
	//recorriendo la tabla
	for (i=1; i<nrofilas; i++)
	{
		//averiguando si la fila esta seleccionada
		if (this.is_fila_selected(i) == true)
		{
			nrofilas_selected++;
		}
	}

	return nrofilas_selected;
}

function tjs_tabla_is_fila_selected(nrofila)
{
	var rowCount = this.m_control.rows.length; //hallando numero de filas

	if (nrofila <0 || nrofila > rowCount)
		return null;

	var mRow = this.m_control.rows[nrofila]; //obteniendo la fila
	if (mRow.dataset.is_selected == "true")
	{
		return true;
	}else{
		return false;
	}
}

function tjs_tabla_get_cell_div(nrofila,nrocolumna)
{
	return this.m_control.rows[nrofila].cells[nrocolumna].getElementsByTagName("div")[0];
}

function tjs_tabla_get_cell_text(nrofila,nrocolumna)
{
	var div = this.tjs_tabla_get_cell_div(nrofila,nrocolumna);
	return div.innerHTML;
}

function tjs_tabla_set_cell_text(nrofila,nrocolumna,texto)
{
	var div = this.tjs_tabla_get_cell_div(nrofila,nrocolumna);
	div.innerHTML = texto;
}

function tjs_tabla_permit_select(multiple=false)
{
	if (multiple==true)
	{
		this.m_permit_select = true;
		this.m_permit_select_multiple = true;
	}else{
		this.m_permit_select = true;
		this.m_permit_select_multiple = false;
	}
}

//----------------------------
//permite seleccionar una fila de una tabla
//la opcion force=true, significa que no se elimina la seleccion, aun cuando se vuelve a dar clic sobre la misma fila.
//                      es decir fuerza a que siempre haya una fila seleccionada
//la opcion force=false, significa que si se puede eliminar la seleccion volviendo a dar clic
//function tjs_tabla_select_fila(nrofila,class_selected="tjs_tabla_selected_class", class_noselected="tjs_tabla_noselected_class", force=false)
function tjs_tabla_select_fila(nrofila,class_selected="tsdk_tabla_selected_class", class_noselected="tsdk_tabla_noselected_class", force=false)
{
	var filainicial=0;
	var filafinal=0;

    if (this.m_permit_select==false)
            return; //no permite seleccionar

    var rowCount = this.m_control.rows.length; //hallando numero de filas

    for (var i=0; i<rowCount; i++)
    {
            var mRow = this.m_control.rows[i]; //obteniendo la fila
            if (i == nrofila)   //selectedRowID is a tr id
            {
                    if (mRow.className == class_selected && force==false)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }else{
                            mRow.className=class_selected;
                            mRow.dataset.is_selected = "true";
                    }

                    /*if (this.m_permit_select_multiple==true && force==false)
                    {
						break;
                    }*/
            }else{
                    if (force==true)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }	
                    if (!this.m_permit_select_multiple)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }			
			}
			
			//hallando fila inicial y final
			if (mRow.dataset.is_selected == "true")
			{
				if (filainicial == 0){ //todavia no se guardo la fila inicial
					filainicial = i; //guardando la fila inicial
					filafinal=i
				}else{ //ya se guardo la fila inicial
					filafinal=i;
				}
			}
	}
	
	//seleccionando las filas intermedias
	if (filafinal > filainicial){
		for (var i=filainicial; i<=filafinal; i++)
		{
			var mRow = this.m_control.rows[i]; //obteniendo la fila
			mRow.className=class_selected;
			mRow.dataset.is_selected = "true";
		}
	}
}

/*
//----------------------------
//permite seleccionar varias filas de una tabla. Para ello se supone que el usuario ya seleccion una fila anteriormente
function tjs_tabla_select_multiples_filas(nrofila,class_selected="tsdk_tabla_selected_class", class_noselected="tsdk_tabla_noselected_class")
{ 
	var seleccionar=false;

    if (this.m_permit_select==false)
            return; //no permite seleccionar

    var rowCount = this.m_control.rows.length; //hallando numero de filas

    for (var i=0; i<rowCount; i++)
    {
			var mRow = this.m_control.rows[i]; //obteniendo la fila
			mRow.dataset.is_selected = "false";
			
			if (i == nrofila || mRow.dataset.is_selected)
            {
				if (seleccionar == false){
					seleccionar=true; //inicia la seleccion
					luis
				}else{

				}

                    if (mRow.className == class_selected && force==false)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }else{
                            mRow.className=class_selected;
                            mRow.dataset.is_selected = "true";
                    }

                    if (this.m_permit_select_multiple==true && force==false)
                    {
                            return;
                    }
            }else{
                    if (force==true)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }	
                    if (!this.m_permit_select_multiple)
                    {
                            mRow.className=class_noselected;
                            mRow.dataset.is_selected = "false";
                    }			
            }
    }
}*/

//----------------------------------------------------
//Esta funcion recupera el dataset de una fila "data-"
function tjs_tabla_get_dataset_fila(nrofila,txtdataset)
{ 
    var rowCount = this.m_control.rows.length; //hallando numero de filas

    for (var i=0; i<rowCount; i++)
    {
		var mRow = this.m_control.rows[i]; //obteniendo la fila
		if (i == nrofila)   //selectedRowID is a tr id
		{
			//return mRow.dataset.clave;
			return mRow.dataset[txtdataset];
		}
	}
	return "";
}

function tjs_tabla_get_nro_filas()
{
	return this.m_control.rows.length;
}

function tjs_tabla_get_nro_columnas(nrofila=0)
{
	return this.m_control.rows[nrofila].cells.length;
}

/*

//--------------------------------------------------------
//funcion que detecta la seleccion de una fila
function evento_tipousuario_browse_tabla_fila_onclick(nrofila,clave)
{
	var idventana = "tipousuario";

	var msgTable = document.getElementById(idventana+"_browse_tabla");
	var rowCount = msgTable.rows.length;
	for (var i=0; i<rowCount; i++)
	{
		var mRow = msgTable.rows[i];
		if (i == nrofila)   //selectedRowID is a tr id
		{
			mRow.className="tvbrowse_tabla_selected_class";
		}else{
			mRow.className="tvbrowse_tabla_noselected_class";
		}
	}

	//cargando la lista de permisos asociados al tipo de usuario
	onload_permisos(clave);
}*/


