
//---------------------------------------------------------
// Definiendo funciones para combobox
//---------------------------------------------------------

function tjs_combobox(idventana,idcontrol="") {
	
	//definicion de propiedades
	this.m_id = "";
	this.m_control = "";

	if (idcontrol == "")
	{
		this.m_id = idventana;
		this.m_control = document.getElementById(idventana);
	}else{
		//definicion de propiedades
		this.m_id = idventana + "_" + idcontrol;
		this.m_control = document.getElementById(idventana + "_" + idcontrol);
	}

	//definiciendo de metodos
	this.set_focus = tjs_combobox_set_focus;
	this.get_selectedindex = tjs_combobox_get_selectedindex;
	this.set_selectedindex = tjs_combobox_set_selectedindex;
	this.seleccionar_item = tjs_combobox_seleccionar_item;
	this.get_value_selected = tjs_combobox_get_value_selected;
	this.get_text_selected = tjs_combobox_get_text_selected;
	this.get_text = tjs_combobox_get_text; //obtiene el texto de un valor dado
	this.add_item = tjs_combobox_add_item; //añade un item al control
	this.remove_all = tjs_combobox_remove_all; //elimina todos los items del combobox
}

function tjs_combobox_get_selectedindex()
{
	return this.m_control.selectedIndex;
}
 
function tjs_combobox_set_selectedindex(indice)
{
	this.m_control.selectedIndex = indice;
}

//devuelve valores del item seleccionado
function tjs_combobox_get_value_selected()
{
	return this.m_control.options[this.m_control.selectedIndex].value;
}

//devuelve valores del item seleccionado
function tjs_combobox_get_text_selected()
{
	return this.m_control.options[this.m_control.selectedIndex].text;
}

function tjs_combobox_get_text(valor)
{
	for (var i = 0; i < this.m_control.length; i++) 
	{
	    var opt = this.m_control.options[i];

	    if (opt.value == valor)
	    {
	    	return opt.text;
	    }
	}

	return null;
}

function tjs_combobox_add_item(valor,texto,selected=false)
{
	//utilizando jquery
	//$('#'+this.m_id).append('<option value="'+valor+'">'+texto+'</option>');

	var opt = document.createElement('option');
    opt.value = valor;
	opt.innerHTML = texto;
	if (selected)
		opt.selected = true;
		
    this.m_control.appendChild(opt);
}

function tjs_combobox_remove_all()
{
	while (this.m_control.length > 0) {
		this.m_control.remove(this.m_control.length-1);
	}
}

function tjs_combobox_set_focus()
{
	this.m_control.focus();
	//this.m_control.select();
}

function tjs_combobox_seleccionar_item(valor)
{
	var cantidad = this.m_control.length;
	for (i = 0; i < cantidad; i++) 
	{
		if (this.m_control[i].value == valor) 
		{
			this.m_control[i].selected = true;
			return;
		}   
	}
}

