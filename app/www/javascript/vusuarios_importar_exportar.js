
//-----------------------------------------------------------------
//Esta funcion se encarga de cerrar la ventana importar/exportar
function evento_usuario_cmdcerrar_ie_onclick()
{
	var idventana="usuario";
	tjs_funcion_edicion_cerrar(idventana,
							"cusuarios/ajax_get_html_browse" //url browse
							);
}

//--------------------------------------------------------
//Esta funcion se encarga de procesar la importacion
function evento_usuario_cmdimportfile_onclick()
{
	var idventana="usuario";

	tjs_funcion_ie_importar_file(idventana,"/cusuarios/ajax_procesar_importar_desde_form",
								"usuario_cmdcerrar_ie_onclick()"); //funcion a llamar si grabar es ok

}

//--------------------------------------------------------
//Esta funcion se encarga de procesar la exportacion
function evento_usuario_cmdexportfile_onclick()
{
	var idventana="usuario";

	tjs_funcion_ie_exportar_file(idventana,"/cusuarios/ajax_procesar_exportar_desde_form",
								"usuario_cmdcerrar_ie_onclick()"); //funcion a llamar si grabar es ok

}
