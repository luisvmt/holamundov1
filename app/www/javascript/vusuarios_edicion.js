
//---------------------------------------------------------------
//Esta funcion se encarga de insertar o modificar un registro
function evento_usuario_cmdgrabar_onclick()
{
	var idventana="usuario";

	lista_id = new Array(1);
	lista_id[0] = "idusuario";
	
	tjs_funcion_edicion_cmdgrabar(idventana,"usuario_txt_accion",   //accion a ejecutar, 1:modificar, 0:insertar
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"/cusuarios/ajax_procesar_insertar_desde_form",
								"/cusuarios/ajax_procesar_modificar_desde_form",
								"evento_usuario_cmdcerrar_onclick()"); //funcion a llamar si grabar es ok

}

//---------------------------------------------------------------
//Esta funcion se encarga de clonar un registro
function evento_usuario_cmdclonar_onclick()
{
	var idventana="usuario";

	lista_id = new Array(1);
	lista_id[0] = "idusuario";
	
	tjs_funcion_edicion_cmdclonar(idventana,
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"usuario_usuario", //id con el primer control a dar focus
								"/cusuarios/ajax_procesar_clonar_registro");

}

//--------------------------------------------------------
//Esta funcion se encarga de cerrar la ventana edicion
function evento_usuario_cmdcerrar_onclick()
{
	var idventana="usuario";
	tjs_funcion_edicion_cerrar(idventana,
							"cusuarios/ajax_get_html_browse" //url browse
							);
}


