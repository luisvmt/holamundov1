
//---------------------------------------------------------------
//Este scripts son utilizados por todas las vistas
//---------------------------------------------------------------

//---------------------------------------------------------------
//Esta funcion se encarga de cambiar la contraseña del usuario
/*function global_show_cambiarpassword()
{
    var idventana = "global";
 
    //limpiando los textbox
    password_nuevo = new tjs_textbox(idventana+"_contrasena_nueva");
    password_nuevo_confirmacion = new tjs_textbox(idventana+"_contrasena_nueva_confirmacion");
    
    password_nuevo.set_text("");
    password_nuevo_confirmacion.set_text("");
    
    //mostrando ventana de dialogo
    $( "#global_cambiarpassword" ).dialog( "option", "width", 500 );
    $("#global_cambiarpassword").dialog("open");
}*/

//---------------------------------------------------------------
//Esta funcion se encarga de cambiar la contraseña del usuario
/*function global_cambiarpassword_aceptar()
{
    var idventana = "global";
    var respuesta = "";
    
    //validando que se haya ingresado el nuevo password
    password_nuevo = new tjs_textbox(idventana+"_contrasena_nueva");
    password_nuevo_confirmacion = new tjs_textbox(idventana+"_contrasena_nueva_confirmacion");
    control_mensaje = new tjs_span(idventana+"_mensaje");

    if (password_nuevo.get_text() != password_nuevo_confirmacion.get_text())
    {
        control_mensaje.set_text("Las contraseñas no coinciden ... volver a ingresar los datos");
        password_nuevo.set_focus();
        return;
    }
     
    //ejecutando por ajax el cambio de contraseña
    var parametros = { "password_nuevo" : password_nuevo.get_text() };
    tjs_ajax_exec_sincrono("/cusuarios/ajax_procesar_cambiarpassword",parametros,"global_cambiarpassword_aceptar_fin");
    //global_cambiarpassword_aceptar_fin(respuesta);
    
}*/
/*
//funcion que se ejecuta cuando finaliza el ajax
function global_cambiarpassword_aceptar_fin(salida)
{
    var respuesta = salida;
    var exist_error = false;
        
    //recuperando la respuesta (json)
    try {
        respuesta = respuesta.trim();
        datos = jQuery.parseJSON(respuesta);

        if (datos.response != "0")
        {
            alert(change_html_to_unicode(datos.mensaje));
            exist_error=true;
        }

    } catch (e) {
            alert(respuesta);
            alert(e);
            exist_error=true;
    }

    if (exist_error == true)
    {
        return;
    }
    
    //cerrando el dialogo
    $("#global_cambiarpassword").dialog("close");
}*/

//---------------------------------------------------------------
//Esta funcion se encarga de cambiar la contraseña del usuario
/*function global_cambiarpassword_cancelar()
{
	$("#global_cambiarpassword").dialog("close");
}
*/

