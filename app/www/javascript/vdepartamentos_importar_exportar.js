//<script language="javascript" type="text/javascript">

//-----------------------------------------------------------------
//Esta funcion se encarga de cerrar la ventana importar/exportar
function evento_departamento_cmdcerrar_ie_onclick()
{
	var idventana="departamento";
	tjs_funcion_edicion_cerrar(idventana,
							"cdepartamentos/ajax_get_html_browse" //url browse
							);
}

//--------------------------------------------------------
//Esta funcion se encarga de procesar la importacion
function evento_departamento_cmdimportfile_onclick()
{
	var idventana="departamento";

	tjs_funcion_ie_importar_file(idventana,"/cdepartamentos/ajax_procesar_importar_desde_form",
								"evento_departamento_cmdcerrar_ie_onclick()"); //funcion a llamar si grabar es ok

}

//--------------------------------------------------------
//Esta funcion se encarga de procesar la exportacion
function evento_departamento_cmdexportfile_onclick()
{
	var idventana="departamento";

	tjs_funcion_ie_exportar_file(idventana,"/cdepartamentos/ajax_procesar_exportar_desde_form",
								"evento_departamento_cmdcerrar_ie_onclick()"); //funcion a llamar si grabar es ok

}

//</script>
