//<script language="javascript" type="text/javascript">

//---------------------------------------------------------------
//Esta funcion se encarga de insertar o modificar un registro
function evento_departamento_cmdgrabar_onclick()
{
	var idventana="departamento";

	lista_id = new Array(1);
	lista_id[0] = "iddepartamento";
	
	tjs_funcion_edicion_cmdgrabar(idventana,"departamento_txt_accion",   //accion a ejecutar, 1:modificar, 0:insertar
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"/cdepartamentos/ajax_procesar_insertar_desde_form",
								"/cdepartamentos/ajax_procesar_modificar_desde_form",
								"evento_departamento_cmdcerrar_onclick()"); //funcion a llamar si grabar es ok

}

//---------------------------------------------------------------
//Esta funcion se encarga de clonar un registro
function evento_departamento_cmdclonar_onclick()
{
	var idventana="departamento";

	lista_id = new Array(1);
	lista_id[0] = "iddepartamento";
	
	tjs_funcion_edicion_cmdclonar(idventana,
								//"departamento_iddepartamento", //id con la clave primaria
								lista_id, //lista de id con los valores a mostrar luego de insertar/modificar
								"departamento_nombredepartamento", //id con el primer control a dar focus
								"/cdepartamentos/ajax_procesar_clonar_registro");

}

//--------------------------------------------------------
//Esta funcion se encarga de cerrar la ventana edicion
function evento_departamento_cmdcerrar_onclick()
{
	var idventana="departamento";
	tjs_funcion_edicion_cerrar(idventana,
							"cdepartamentos/ajax_get_html_browse" //url browse
							);
}


//</script>
