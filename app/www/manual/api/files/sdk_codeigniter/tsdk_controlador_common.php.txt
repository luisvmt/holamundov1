<?php   
/**
 * Este archivo implementa la clase tsdk_basedatos_mysql
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controladores
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

//Incluyendo la base de datos
include_once(APPPATH."../../sdk_tnkka/sdk_mysql/tsdk_basedatos_mysql.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_parametros_controlador.php");

//include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_funciones.php");

//-----------------------------------------------------------
/**
* Controlador común
*
* Implementa la funcionalidad principal de los controladores del API
*
* Esta clase implementa la funcionalidad principal de los controladores
*
* @author luisvmt@hotmail.com
* @package controladores
*/
class tsdk_controlador_common extends CI_Controller
{
	// ******************************
	// variables miembro
	// ******************************


	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* nombre del controlador
	* @var string
	*/
	protected  $m_nombre_controlador="";
	
	/**
	* id de session. Se obtiene con la funcion session_id();
	* @var string
	*/
	protected  $m_session_id="";
	
	/**
	* Objeto de base de datos
	* @var tsdk_basedatos_mysql
	*/
	protected  $m_basedatos=null;

	/**
	* mensaje de error o exito de la clase
	* @var string
	*/
	protected  $m_mensaje="";

	/**
	* Objeto que contiene las variables pasadas al controlador
	* @var object
	*/
	protected  $m_parametros_controlador;

	/**#@-*/

	// ******************************
	// metodos
	// ******************************

 
	//-----------------------------------------------------------
	/**
	* Implementa el constructor de la clase
	*
	* @param  string    $usuariobd  		Usuario para conexion a base de datos
	* @param  string    $contrasenabd       Contraseña para conexion a base de datos
	* @param  string    $servidorbd         Nombre o IP del servidor de base de datos
	* @param  string    $basedatos          Nombre de la base de datos
	* @param  integer   $puertobd           Numero de puerto TCP donde escucha la base de datos
	* @param  string    $controlador_raiz   Es el nombre del controlador donde se dirigirá sino se hizo login
	* @param  boolean   $validar_login      Indica si se debe validar si se hizo login
	* @param  boolean   $bd_is_persistent   Indica si la conexion a la base de datos será persistente
	*
	* @example "sample_tsdk_controlador_common_construct.txt"
	*/
	public function __construct($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,
								$controlador_raiz="", $validar_login = true,$bd_is_persistent=true) //parametro: $validar_login = true
	{
		parent::__construct();

		//obteniendo los parametros
			//$parametros = func_get_args();
			//$validar_login = isset($parametros['0'])?$parametros['0']:true;

		//inicializando sessiones
		session_start();

		//inicializando variables
		$this->load->helper('url');
		$this->load->library('tsdk_login_library'); //libreria personal

		//obteniendo el id de session
		$this->m_session_id = session_id();

		//validando si el usuario esta loggeado
		if ($validar_login==true)
		{
			//$this->tsdk_login_library->base_url = "http://adminfabystore_cancer01.tnkka.xyz/";
			$this->tsdk_login_library->controlador_raiz = $controlador_raiz;

			$this->tsdk_login_library->check_is_logged();

		}

		//inicializando los parametros del controlador
		$this->m_parametros_controlador = new tsdk_parametros_controlador($this);

		//conectando a la base de datos
		$this->m_basedatos = new tsdk_basedatos_mysql();
		$this->m_basedatos->conectar($usuariobd,$contrasenabd,$servidorbd,$basedatos,$puertobd,$bd_is_persistent);
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    	//error_log("Dentro de destructor del controlador tsdk");
        //$this->m_conexion = null;
    }

	//-----------------------------------------------------------
	/**
	* Implementa la funcion principal del controlador
	*
	*/
	function index()
	{
		//leyendo las variables post ... desde las variables de session

		/*** comentado temporalmente
		if (isset($_SESSION['session_post']))
		{
			$m_session_post = $_SESSION['session_post'];
		} else {
			$m_session_post = array();
		}

		//inicializando las variables que no existen
		if (!isset($m_session_post['mensaje']) )
		{
			$m_session_post['mensaje']="";
		}*/
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion inicializa las variables de session que se utilizaran posteriormente
	* 
	* @return none
	*/
	function inicializar_variables()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();
		}

		//verificando si existe el parametro mensaje
		if (! $this->m_parametros_controlador->exists_parametro("mensaje"))
		{
			$this->m_parametros_controlador->mensaje="";
		}

		//verificando si existe el parametro mensaje
		if (! $_SESSION['parametros_controlador']->exists_parametro("mensaje"))
		{
			$_SESSION['parametros_controlador']->mensaje="";
		}

	}

	//-----------------------------------------------------------
	/**
	* Esta funcion establecer los parametros del controlador.
	* Se debe llamar cada vez que se inicia el controlador (llamado por index o ajax)
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	function load_parametros_controlador()
	{
		//La variable $_SESSION['parametros-controlador'] contiene los parametros enviadosd desde otro controlador

		if (!isset($_SESSION['parametros_controlador']))
		{
			$_SESSION['parametros_controlador']=new tsdk_parametros_controlador();

			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
				 "#x variable SESSION['parametros_controlador'] no existe".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
		}

		$lista = $_SESSION['parametros_controlador']->get_parametros();
		$this->m_parametros_controlador->set_parametros($lista);
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion redirecciona a otra pagina de la aplicacion. 
	* Guarda en una variable de session llamada parametros_controlador un objeto
	* 
	* @param  string 	$url 							URL a donde direccionar la aplicacion. Se prefiere que sea en este mismo servidor
	* @param  array 	$parametros_controlador 		Objeto que administra las variables a pasar a controlador
	* @return none
	*/
	function redirect($url,$parametros_controlador)
	{
		//----------------------------
		//$this->session->set_userdata('session_post', $datos);
		/*if (!is_null($datos))
		  $_SESSION['session_post']=$datos;*/

		$_SESSION["parametros_controlador"] = $parametros_controlador;

		header('Location: '.$url);
	}

	//-----------------------------------------------------------
	/**
	* Esta funcion valida si los controles indicados en la lista existen o no
	* @param  array 	$lista 		Arreglo conteniendo los nombre de los controles a validar. 
	* 								En la lista se ingresa el nombre del ID del control.
	*  								Estos controles los valida contra la lista $_POST
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
	*/
	function existe_controles_post($lista)
	{
		foreach ($lista as $nombrecontrol)
		{
			if (!isset($_POST[$nombrecontrol]))
			{
				$this->m_mensaje=tsdk_reemplazar_html("No existe item: ".$nombrecontrol);
				//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
								 "#x Error en existe_controles_post. ". $this->m_mensaje.
								 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
									"\n"
								);

				return false;
			}
		}
		return true;
	}

	//-----------------------------------------------------------------------
	//validando que el control no este vacio y que el valor sea del
	//tipo configurado
	//-----------------------------------------------------------------------

	//-----------------------------------------------------------
	/**
	* Esta funcion valida que los controles obligatorios no se encuentren vacios, asi como contengan el tipo de dato correcto.
	* @param  array 	$lista 		Arreglo de dos dimensiones. Es una lista de datos a validar.
	* 								Cada nodo tiene un arreglo, donde el primer campo es el ID del control a validar,
	* 								el segundo campo es el nombre, y el tercer campo es el tipo de dato. 
	* @param  object    $nombre     Es el nombre del control que tiene error. Este parametro es pasado por referencia.
	* @return boolean  				Devuelve true si todos los controles existe en $_POST, false si algun control falta.
	*/
	function validar_controles_obligatorios($lista, &$nombre)
	{
		foreach ($lista as $control )
		{
		  $nombrecontrol = $control[0];
		  $desc_control = $control[1];
		  $tipo_dato = $control[2];
		  $mensaje_personalizado = $control[3];

		  //validando que el control tenga un dato
		  //$valor = trim($this->input->post($nombrecontrol));
		  $valor = trim($_POST[$nombrecontrol]);

		  //validando el tipo de dato
		  switch ($tipo_dato){
			case tsdk_tipodato::tstring:
			  {
				break;
			  }
			case tsdk_tipodato::tstring_no_vacio:
			  {
				$cadena=trim($valor);
				if ($cadena=="")
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó datos en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);
					
					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);

					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789+-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<=0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tinteger_mayor_igual_cero:
			  {
				//validando que es entero
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = intval( (int)($valor));
				if ($numero<0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó valor ENTERO mayor o igual a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				break;
			  }
			case tsdk_tipodato::tdouble_mayor_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = (double)($valor);
				if ($numero<=0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			case tsdk_tipodato::tdouble_mayor_igual_cero:
			  {
				//validando que es double
				$valor = (string)($valor);
				$encuentros=preg_match("/[^0123456789\+\-\.]/",$valor); //validando entero positivo/negativo
				if ($encuentros>0 || strlen($valor)==0)
				{
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número válido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}

				$numero = (double)($valor);
				if ($numero<0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un Número mayor o igual a CERO en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
				
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }

			case tsdk_tipodato::temail_o_vacio:
			  {
				//validando si es vacio
				if ( $valor == "")
				{
				  break; //OK
				}

				//validando que sea tipo email
				$valor = (string)($valor);
				$encuentros=preg_match("/^[0-9a-zA-Z]+([\._]?[[:alnum:]])*[[:alnum:]]@[0-9a-zA-Z]+([-]?[[:alnum:]])*[.][a-zA-Z]{2,4}$/",$valor); //validando
				if ($encuentros==0)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un correo valido en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }

			case tsdk_tipodato::tboolean:
			  {
				if ($valor != true && $valor != false)
				{
					
					if ($mensaje_personalizado == "")
					{
						$this->m_mensaje=tsdk_reemplazar_html("No ingresó un BOOLEAN en [ ".$desc_control." ]");
					}else{
						$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
					}
					//error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

					tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
									 "#x Error en validar_controles_obligatorios. ". $this->m_mensaje.
									 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
										"\n"
									);
					
					$nombre=$nombrecontrol;
					return false;
				}
				break;
			  }
			default:
			  {
				
				if ($mensaje_personalizado == "")
				{
					$this->m_mensaje=tsdk_reemplazar_html("No ingresó un TIPO DE DATO correcto, revisar programa !!!!");
				}else{
					$this->m_mensaje=tsdk_reemplazar_html($mensaje_personalizado);
				}
				error_log(basename(__FILE__).", ".__CLASS__."::".__FUNCTION__." ==> ".$this->m_mensaje);

				echo "#x Error en validar_controles_obligatorios. Control:".$nombrecontrol.
				  ", descripcion:".$desc_control.
				  ", tipo de dato:".$tipo_dato.
				  ", valor:".$valor.
				  ". Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
				  "\n";
				debug_print_backtrace();

				$nombre="";
				return false;

				break;
			  }
		  }
		}

		return true;
	}


/*** comentado temporalmente


  //-----------------------------------------------------------------------
  //Inserta desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function insertar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//estableciendo valor de eliminar
	//$objeto->set_item_valor('eliminar',false);
	$objeto->eliminar=false;

	//ejecutando
	if ( $objeto->insertar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }

  //-----------------------------------------------------------------------
  //Modifica desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function modificar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos (primero se obtendra los valores)
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//obteniendo valores ... quizas algunos campos no se modifiquen
	if ( $objeto->obtener() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	//volviendo a establecer para modificar campos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	$respuesta=$objeto->modificar();
	if ( $respuesta < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return $respuesta;
  }

  //-----------------------------------------------------------------------
  //elimina desde una fila de archivo excel
  //-----------------------------------------------------------------------
  protected function eliminar_from_array($objeto,$arreglo)
  {
	//estableciendo campos de base de datos
	foreach ($arreglo as $nombre_campo => $valor)
	{
	  if ($nombre_campo!="" && $nombre_campo!= 'accion')
	  {
		$objeto->set_item_valor($nombre_campo,$valor);
	  }
	}

	//ejecutando
	if ( $objeto->eliminar() < 0 )
	{
	  $this->m_mensaje = $objeto->mensaje;
	  return -1;
	}

	return 0;
  }*/

} //fin de clase


