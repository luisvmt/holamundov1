<?php 
/**
 * Este archivo implementa la clase base de todos los objetos html
 * 
 * @author luisvmt@hotmail.com
 * @version 1.0
 * @package controles
 */

if ( ! ( defined('TSDK_FILE_MAIN')||defined('BASEPATH')) )
	exit(__FILE__.': No direct script access allowed');

include_once(APPPATH."../../sdk_tnkka/sdk_controles_html/tsdk_control.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_filas_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_fila_tabla_item.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_columnas_tabla.php");
include_once(APPPATH."../../sdk_tnkka/sdk_varios/tsdk_columna_tabla_item.php");

//-----------------------------------------------------------
/**
* Este objeto representa una tabla de datos que contiene los valores que son derivados
* de una consulta SQL. Este objeto cuenta con filas y columnas
*
* Implementa una clase que implementa una tabla con datos obtenidos mediante SQL.
*
*/ 

class tsdk_datos_sql
{

	// ******************************
	// variables miembro
	// ******************************

	//-----------------------------------------------------------
	/**#@+
	* @access private
	*/

	//-----------------------------------------------------------
	/**#@+
	* @access protected
	*/

	/**
	* Representa el numero de filas de la cabecera de la tabla. 
	* @var integer
	*/
	//protected   $m_nrofilas_header=0;

	/**
	* Representa el numero de filas del resumen (footer) de la tabla. 
	* @var integer
	*/
	//protected   $m_nrofilas_footer=0;
	
	/**
	* Arreglo donde guarda el contenido de cada celda. 
	* @var array
	*/
	//protected	$m_valor_celda=array();

	/**
	* Arreglo donde guarda las propiedades de cada celda
	* @var array
	*/
	//protected	$m_propiedades_celda=array();

	/**
	* Arreglo donde se guarda los estilos de las celdas
	* @var array
	*/
	//protected	$m_estilos_celda=array();

	/**#@-*/

    //-----------------------------------------------------------
    /**#@+
    * @access public
    */
	
    /**
    * Representa la coleccion de filas de la tabla. 
	 * Cada una de las filas es una coleccion donde el indice es el nombre del campo, y el valor es el valor del campo de la tabla.
	 * @var tsdk_coleccion
     */
	public	$filas=null;
	
    /**#@-*/
	
    // ******************************
    // metodos
    // ******************************

    //-----------------------------------------------------------
    /**
    * Implementa el constructor de la clase
    *
    * @example "sample_tsdk_datos_sql.construct.txt"
    */
	function __construct()
	{
	   //iniciando variables
	   $this->filas = new tsdk_coleccion();
	}

    //-----------------------------------------------------------
    /** 
    * Implementa el destructor de la clase
    *
    */
    function __destruct()
    {
    }

    //-----------------------------------------------------------
    /**
    * Get magic method
    *
    * Implementa la recuperacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_header</b>        :   Devuelve el numero de filas del header</br>
    *    <b>nro_footer</b>        :   Devuelve el numero de filas del resumen</br>
    *    <b>nro_filas</b>         :   Devuelve el numero de filas de la tabla</br>
    *    <b>nro_columnas</b>      :   Devuelve el numero de columnas de la tabla</br>
    * 
    * @example "sample_tsdk_tabla.php"
    */
	/*public function __get($propiedad)
	{
		switch ($propiedad){
		case "nro_header":
			return $this->m_nrofilas_header;
			break;
		
		case "nro_footer":
			return $this->m_nrofilas_footer;
			break;
		
		case "nro_filas":
			//return $this->m_nrofilas;
			return $this->filas->count();
			//break;
		
		case "nro_columnas":
			//return $this->m_nrocolumnas;
			return $this->columnas->count();
			//break;
		
		default:
			return parent::__get($propiedad);
			break;
		}

	}*/

    //-----------------------------------------------------------
    /**
    * Set magic method
    *
    * Implementa la modificacion de propiedades de la clase.
	* @access public
    * @param string $propiedad      La propiedad a devolver
    *
    *    <h5><strong>Posibles valores:</strong></h5> 
    *    <b>nro_header</b>        :   Establece el numero de filas del header</br>
    *    <b>nro_footer</b>        :   Establece el numero de filas del resumen</br>
    *    <b>border</b>            :   Establece el borde de la tabla</br>
    * 
    * @param string $value        El valor a establecer en la propiedad
    * @example "sample_tsdk_tabla.php"
    */
	/*public function __set($propiedad, $value)
	{
		switch ($propiedad){
		case "nro_header":
			$this->m_nrofilas_header=$value;
			break;
		
		case "nro_footer":
			$this->m_nrofilas_footer=$value;
			break;
		
		case "border":
			$this->propiedades->add("border")->value=$value;
			break;
		
		default:
			return parent::__set($propiedad,$value);
			//break;
		}
	}*/

    //-----------------------------------------------------------
    /**
    * Recupera un valor
    * 
	* @access public
    * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
    * @param  integer 	$nombre_columna	Es el nombre de la columna a recuperar
    * @return object
    * 
    * @example "sample_tsdk_datos_sql.get_valor_campo.txt"
    */
	public function get_valor_campo($nrofila,$nombre_columna)
	{
		if ( !$this->filas->key_exists($nrofila) )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
				 "#x El numero de fila '".$nrofila."' no existe.".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			return null;
		}

		if ( !$this->filas->item($nrofila)->key_exists($nombre_columna) )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
				 "#x El nombre de columna '".$nombre_columna."'no existe.".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
			return null;
		}

		return $this->filas->item($nrofila)->item($nombre_columna)->value;
	}

    //-----------------------------------------------------------
    /**
     * Modifica un valor. El campo y fila debe existir, no agrega valores ... solo modifica.
     * 
	 * @access public
     * @param  integer 	$nrofila 		Numero de fila de la celda a modificar (basado en 0)
     * @param  integer 	$nombre_columna	Es el nombre de la columna
	 * @param  object	$valor			Es el valor a modificar   
     * @return none
     * 
     * @example "sample_tsdk_datos_sql.set_valor_campo.txt"
     */
	public function set_valor_campo($nrofila,$nombre_columna,$valor)
	{
		if ( !$this->filas->key_exists($nrofila) )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
				 "#x El numero de fila '".$nrofila."' no existe.".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);

			return null;
		}

		if ( !$this->filas->item($nrofila)->key_exists($nombre_columna) )
		{
			tsdk_error_log(tsdk_propiedades::$TRACELOG,"\n".
				 "#x El nombre de columna '".$nombre_columna."'no existe.".
				 ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
					"\n"
				);
			
			return null;
		}

		return $this->filas->item($nrofila)->item($nombre_columna)->value = $valor;
	}

	
}


/*

<table width="430" border="0">
  <tr>
	<td width="137">usuario:</td>

	<td width="144"><?php echo $txtusuario->get_html(); ?></td>
	<td width="135"><input type="submit" name="cmdaceptar" id="cmdaceptar" value="Aceptar" /></td>
	</tr>
  <tr>
	<td>contrase�a:</td>
	<td><?php echo $txtpassword->get_html(); ?></td>
	<td>&nbsp;</td>
	</tr>
  <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>
  <tr>
	<td colspan="3">Mensaje</td>
	</tr>
  <tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>
</table>*/



