<?php

//----------------------------------------------------------
//Esta libreria se encarga de validar si se realizo login
//----------------------------------------------------------
class tsdk_login_library
{
    //protected   $m_base_url="";
    protected   $m_controlador_base="";
    protected   $m_mensaje="";

    // Constructor
    public function __construct()
    {
        if (!isset($this->CI))
        {
            $this->CI =& get_instance();
        }
    }
/*
    //-----------------------------------------------
    //--- definiendo las funciones magicas get/set
    //-----------------------------------------------
    public function __get($propiedad)
    {
        switch ($propiedad){
            //case "base_url":
            //    return $this->m_base_url;
            //    break;

            case "controlador_base":
                return $this->m_controlador_base;
                break;

            case "mensaje":
                return $this->m_mensaje;
                break;

            default:
                //obteniendo valor
                echo "#x Error en get. Propiedad no encontrada: ".$propiedad.
                     ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n";
                debug_print_backtrace();
                break;
        }
    }

    public function __set($propiedad, $value)
    {
        switch ($propiedad){
            //case "base_url":
            //    $this->m_base_url = $value;
            //    break;

            case "controlador_base":
                $this->m_controlador_base = $value;
                break;

            case "mensaje":
                $this->m_mensaje = $value;
                break;

            default:
                echo "#x Error en set. Propiedad no encontrada: ".$propiedad.
                     ", Archivo: ".basename(__FILE__)."(".__LINE__."), Funcion: ".__CLASS__."::".__FUNCTION__.
                        "\n";
                debug_print_backtrace();
                break;
        }
    }

*/
    
    public function check_is_logged()
    {
        //if ($this->CI->session->userdata('is_logged') != TRUE)
        if ( !isset($_SESSION['tsdk_is_logged']) )
        {
            redirect(base_url().$this->m_controlador_base, 'refresh'); //direccionando al controlador inicial
            exit();
        }

        if ($_SESSION['tsdk_is_logged'] != TRUE)
        {
            redirect(base_url().$this->m_controlador_base, 'refresh'); //direccionando al controlador inicial
            exit();
        }
    }
}


//---------------------------------
//OJO: No se cierra el texto php
//---------------------------------

