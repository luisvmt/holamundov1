#!/bin/bash

if [ $# == 0 ]; then
	echo "No ha ingresado parametros."
	echo "./crear_copia_archivos.sh <nombre-buscar> <nuevo-nombre>"
	exit;
fi
	
#leyendo parametros
nombre_buscar=$1
nuevo_nombre=$2

#buscando y mostrando los archivos a duplicar
echo ""
echo "nombre_buscar : "$nombre_buscar
echo "nuevo_nombre  : "$nuevo_nombre

echo "Los siguientes archivos van a copiarse:"
echo ""	

salir=0
find ../app | grep -i $nombre_buscar | while read linea; do { echo "existe : "$linea; nuevoarchivo=`echo $linea | sed "s/$nombre_buscar/$nuevo_nombre/g"`; echo "nuevo  : "$nuevoarchivo; [ -f $nuevoarchivo ] && echo "" && echo "======> OJO: Archivo nuevo ya existe, se sobreescribira." && echo ""; } done

#echo "texto:"$salir

#if [ "${salir}" == "s" ]; then
#	echo "saliendo"
#	exit;
#fi
	
#solicitando confirmacion
while true; do
	echo
	read -p "Esta seguro de continuar (s/n) ? " yn
	
	case $yn in
		s ) break;;
		n ) echo "Cancelado"; exit;;
		* ) echo "Por favor responda s ó n";;
	esac
 done
 
#creando la copia de los archivos
find ../app | grep -i $nombre_buscar | while read linea; do { nuevoarchivo=`echo $linea | sed "s/$nombre_buscar/$nuevo_nombre/g"`; cp $linea $nuevoarchivo; echo "archivo creado: "$nuevoarchivo; } done

echo "Copia realizada"
