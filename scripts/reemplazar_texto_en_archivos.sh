#!/bin/bash

if [ $# == 0 ]; then
	echo "No ha ingresado parametros."
	echo "./buscar_texto_en_archivos.sh <texto-buscado> <texto-nuevo> <filtro-archivo>"
	exit;
fi
	
#leyendo parametros
texto_buscado=$1
texto_nuevo=$2
filtro_archivo=$3

#mostrando los archivos donde buscar
echo ""
echo "Mostrando los archivos donde se modificara:"
find ../app -name ${filtro_archivo} | while read linea; do { echo $linea;  } done

echo ""
#solicitando confirmacion
while true; do
	echo
	read -p "Esta seguro de continuar (s/n) ? " yn
	
	case $yn in
		s ) break;;
		n ) echo "Cancelado"; exit;;
		* ) echo "Por favor responda s ó n";;
	esac
 done
 
#reemplazando
find ../app -name ${filtro_archivo} | while read linea; do { echo $linea; sed -i "s/${texto_buscado}/${texto_nuevo}/g" $linea; } done

echo "Fin de script"
