#!/bin/bash

if [ $# == 0 ]; then
	echo "No ha ingresado parametros."
	echo "./buscar_texto_en_archivos.sh <texto-buscado> <filtro-archivo>"
	exit;
fi
	
#leyendo parametros
texto_buscado=$1
filtro_archivo=$2

#mostrando los archivos donde buscar
find ../app -name ${filtro_archivo} | while read linea; do { echo "======================"; echo $linea; echo "----------------------"; grep -i $texto_buscado $linea; echo ""; } done

